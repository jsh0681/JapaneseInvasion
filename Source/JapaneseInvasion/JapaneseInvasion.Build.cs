// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class JapaneseInvasion : ModuleRules
{
	public JapaneseInvasion(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "UMG", "GameplayTasks", "HeadMountedDisplay", "NavigationSystem", "AIModule","OceanPlugin","Sockets","Networking" });
		PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore", "JapaneseInvasionSetting" });
	}
}
