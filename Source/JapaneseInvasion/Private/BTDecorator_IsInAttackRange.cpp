#include "BTDecorator_IsInAttackRange.h"
#include "JIAIController.h"
#include "JIAICharacter.h"
#include "JICharacter.h"
#include "BehaviorTree//BlackboardComponent.h"

UBTDecorator_IsInAttackRange::UBTDecorator_IsInAttackRange()
{
	NodeName = TEXT("CanAttack_Korean");
}

bool UBTDecorator_IsInAttackRange::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	bool bResult = Super::CalculateRawConditionValue(OwnerComp, NodeMemory);

	auto ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (ControllingPawn == nullptr)
		return false;

	auto Target = Cast<AJIAICharacter>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AJIAIController::TargetKey));
	if (Target == nullptr)
		return false;

	bResult = (Target->GetDistanceTo(ControllingPawn) <= 150.0f);
	return bResult;
}
