#include "BTDecorator_JP_IsInAttackRange.h"
#include "JIJapanAIController.h"
#include "JICharacter.h"
#include "BehaviorTree//BlackboardComponent.h"


UBTDecorator_JP_IsInAttackRange::UBTDecorator_JP_IsInAttackRange()
{
	NodeName = TEXT("CanAttackJP");
}

bool UBTDecorator_JP_IsInAttackRange::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	bool bResult = Super::CalculateRawConditionValue(OwnerComp, NodeMemory);

	auto ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (ControllingPawn == nullptr)
		return false;

	auto Target = Cast<AJICharacter>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AJIJapanAIController::TargetKey));
	if (Target == nullptr)
		return false;

	bResult = (Target->GetDistanceTo(ControllingPawn) <= 180.f);
	return bResult;
}
