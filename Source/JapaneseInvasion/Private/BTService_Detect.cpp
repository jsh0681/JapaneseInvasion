#include "BTService_Detect.h"
#include "JIAIController.h"
#include "JIGeneral.h"
#include "JICharacter.h"
#include "JIAICharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "DrawDebugHelpers.h"
#include "PacketMgr.h"

UBTService_Detect::UBTService_Detect()
{
	NodeName = TEXT("Detect");
	Interval = 1.0f;
}

void UBTService_Detect::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	APawn* ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (ControllingPawn == nullptr)
		return;

	UWorld* World = ControllingPawn->GetWorld();
	FVector Center = ControllingPawn->GetActorLocation();
	DetectedRadius = 600.f;

	if (nullptr == World)
		return;

	TArray<FOverlapResult> OverlapResults;
	FCollisionQueryParams CollisionQueryParam(NAME_None, false, ControllingPawn);
	bool bResult = World->OverlapMultiByChannel(
		OverlapResults,
		Center,
		FQuat::Identity,
		ECollisionChannel::ECC_EngineTraceChannel2,
		FCollisionShape::MakeSphere(DetectedRadius),
		CollisionQueryParam
		);

	if (bResult)
	{
		for (auto OverlapResult : OverlapResults)
		{
			AJIAICharacter* Character = Cast<AJIAICharacter>(OverlapResult.GetActor());
			if (Character)
			{
				OwnerComp.GetBlackboardComponent()->SetValueAsObject(AJIAIController::TargetKey, Character);
				return;
			}

		}
	}
	else
	{
		OwnerComp.GetBlackboardComponent()->SetValueAsObject(AJIAIController::TargetKey, nullptr);
	}
}