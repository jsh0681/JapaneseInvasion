#include "BTService_JP_Detect.h"
#include "JIJapanAIController.h"
#include "JIGeneral.h"
#include "JICharacter.h"
#include "JIAICharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "DrawDebugHelpers.h"
#include "PacketMgr.h"


UBTService_JP_Detect::UBTService_JP_Detect()
{
	NodeName = TEXT("DetectJP");
	Interval = 1.0f;
}

void UBTService_JP_Detect::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	APawn* ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (ControllingPawn == nullptr)
		return;

	UWorld* World = ControllingPawn->GetWorld();
	FVector Center = ControllingPawn->GetActorLocation();
	float DetectedRadius = 600.f;

	if (nullptr == World)
		return;

	TArray<FOverlapResult> OverlapResults;
	FCollisionQueryParams CollisionQueryParam(NAME_None, false, ControllingPawn);
	bool bResult = World->OverlapMultiByChannel(
		OverlapResults,
		Center,
		FQuat::Identity,
		ECollisionChannel::ECC_EngineTraceChannel2,
		FCollisionShape::MakeSphere(DetectedRadius),
		CollisionQueryParam
		);


	if (bResult)
	{
		for (auto OverlapResult : OverlapResults)
		{
			AJICharacter* Character = Cast<AJICharacter>(OverlapResult.GetActor());
			if (Character)
			{
				OwnerComp.GetBlackboardComponent()->SetValueAsObject(AJIJapanAIController::TargetKey, Character);
				//DrawDebugSphere(World, Center, DetectedRadius, 16, FColor::Green, false, 0.2f);
				//DrawDebugLine(World,ControllingPawn->GetActorLocation(), Character->GetActorLocation(), FColor::Blue, false, 0.2f);
				return;
			}
		
		}
	}
	else
	{
		OwnerComp.GetBlackboardComponent()->SetValueAsObject(AJIJapanAIController::TargetKey, nullptr);
	}
	//DrawDebugSphere(World, Center, DetectedRadius, 16, FColor::Red, false, 0.2f);
}
