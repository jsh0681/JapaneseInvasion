#include "BTTask_Attack.h"
#include "JIAIController.h"
#include "JIAICharacter.h"
#include "JICharacter.h"
#include "PacketMgr.h"

UBTTask_Attack::UBTTask_Attack()
{
	bNotifyTick = true;
	IsAttacking = false;
}

EBTNodeResult::Type UBTTask_Attack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	auto ControllingPawn = Cast<AJICharacter>(OwnerComp.GetAIOwner()->GetPawn());

	if (nullptr == ControllingPawn)
		return EBTNodeResult::Failed;

	//AICharacter->Attack();
	cs_packet_attack packet;
	packet.size = sizeof(cs_packet_attack);
	packet.type = C2S_ATTACK;
	packet.attack = A_KNIFE;
	packet.objtype = (char)OBJ_TYPE::SOLDIER;
	packet.id = ControllingPawn->m_id;
	//JILOG(Warning, TEXT("Character->m_id : %d"), ControllingPawn->m_id);
	PacketMgr::GetInst()->send_packet(&packet);

	IsAttacking = true;
	ControllingPawn->OnAttackEnd.AddLambda([this]()->void {IsAttacking = false; });
	return EBTNodeResult::InProgress;
	//EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	//APawn* ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();

	////빙의된 캐릭터가 없다면 Failed
	//if (nullptr == ControllingPawn)
	//	return EBTNodeResult::Failed;

	//if (ControllingPawn->ActorHasTag("Japan"))
	//{
	//	AJIAICharacter* AICharacter = Cast<AJIAICharacter>(ControllingPawn);

	//	//AICharacter->Attack();
	//	cs_packet_attack packet;
	//	packet.size = sizeof(cs_packet_attack);
	//	packet.type = C2S_ATTACK;
	//	packet.attack = A_KNIFE;
	//	packet.objtype = (char)OBJ_TYPE::SOLDIER;
	//	packet.id = AICharacter->m_id;
	//	JILOG(Warning, TEXT("AICharacter->m_id : %d"), AICharacter->m_id);
	//	PacketMgr::GetInst()->send_packet(&packet);

	//	IsAttacking = true;
	//	AICharacter->OnAttackEnd.AddLambda([this]()->void {IsAttacking = false; });
	//	return EBTNodeResult::InProgress;
	//}
	//else if (ControllingPawn->ActorHasTag("Korea"))
	//{
	//	AJICharacter* AICharacter = Cast<AJICharacter>(ControllingPawn);

	//	//AICharacter->Attack();
	//	cs_packet_attack packet;
	//	packet.size = sizeof(cs_packet_attack);
	//	packet.type = C2S_ATTACK;
	//	packet.attack = A_KNIFE;
	//	packet.objtype = (char)OBJ_TYPE::SOLDIER;
	//	packet.id = AICharacter->m_id;
	//	JILOG(Warning, TEXT("Character->m_id : %d"), AICharacter->m_id);
	//	PacketMgr::GetInst()->send_packet(&packet);

	//	IsAttacking = true;
	//	AICharacter->OnAttackEnd.AddLambda([this]()->void {IsAttacking = false; });
	//	return EBTNodeResult::InProgress;
	//}
	//return EBTNodeResult::Failed;
}


void UBTTask_Attack::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);
	if (!IsAttacking)
	{
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
}
