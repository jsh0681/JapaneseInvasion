#include "BTTask_JP_Attack.h"
#include "JIJapanAIController.h"
#include "JIAICharacter.h"
#include "PacketMgr.h"

UBTTask_JP_Attack::UBTTask_JP_Attack()
{
	bNotifyTick = true;
	IsAttacking = false;
}

EBTNodeResult::Type UBTTask_JP_Attack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	auto ControllingPawn = Cast<AJIAICharacter>(OwnerComp.GetAIOwner()->GetPawn());

	if (nullptr == ControllingPawn)
		return EBTNodeResult::Failed;

	//AICharacter->Attack();
	cs_packet_attack packet;
	packet.size = sizeof(cs_packet_attack);
	packet.type = C2S_ATTACK;
	packet.attack = A_KNIFE;
	packet.objtype = (char)OBJ_TYPE::SOLDIER;
	packet.id = ControllingPawn->m_id;
	//JILOG(Warning, TEXT("AICharacter->m_id : %d"), ControllingPawn->m_id);
	PacketMgr::GetInst()->send_packet(&packet);

	IsAttacking = true;
	ControllingPawn->OnAttackEnd.AddLambda([this]()->void {IsAttacking = false; });
	return EBTNodeResult::InProgress;
}


void UBTTask_JP_Attack::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);
	if (!IsAttacking)
	{
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
}