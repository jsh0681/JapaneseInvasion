#include "BTTask_JP_FindPatrolPos.h"
#include "NavigationSystem.h"
#include "JIJapanAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "JIAICharacter.h"
#include "JICharacter.h"
#include "JIGeneral.h"
#include "PacketMgr.h"

UBTTask_JP_FindPatrolPos::UBTTask_JP_FindPatrolPos()
{
	NodeName = TEXT("FindPatrolPosJP");
}

EBTNodeResult::Type UBTTask_JP_FindPatrolPos::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	auto ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (ControllingPawn == nullptr)
		return EBTNodeResult::Failed;

	UNavigationSystemV1* NavSystem = UNavigationSystemV1::GetNavigationSystem(ControllingPawn->GetWorld());
	if (NavSystem == nullptr)
		return EBTNodeResult::Failed;

	FVector Origin = OwnerComp.GetBlackboardComponent()->GetValueAsVector(AJIJapanAIController::HomePosKey);
	FNavLocation NextPatrol;

	if (NavSystem->GetRandomPointInNavigableRadius(Origin, 500.0f, NextPatrol))
	{
		cs_packet_move packet;

		AJIAICharacter* Character = Cast<AJIAICharacter>(ControllingPawn);
		if (Character)
			packet.m_id = Character->m_id;
		packet.dir = (char)TEAM_COLOR::JAPAN_1;

		/*for (auto& Generals : g_mapPlayer)
		{
			if (Generals.Value->m_eTeamColor != Character->m_teamcolor)
				return EBTNodeResult::Failed;
		}*/
		OwnerComp.GetBlackboardComponent()->SetValueAsVector(AJIJapanAIController::PatrolPosKey, NextPatrol.Location);

		packet.type = C2S_MOVE;
		packet.size = sizeof(cs_packet_move);
		packet.x = ControllingPawn->GetActorLocation().X;
		packet.y = ControllingPawn->GetActorLocation().Y;
		packet.z = ControllingPawn->GetActorLocation().Z;
		packet.velx = ControllingPawn->GetVelocity().X;
		packet.vely = ControllingPawn->GetVelocity().Y;
		packet.velz = ControllingPawn->GetVelocity().Z;
		packet.status = (char)GENERAL_STATUS::RUN;
		packet.objtype = OBJ_TYPE::SOLDIER;
		packet.TurnValue = ControllingPawn->GetActorRotation().Yaw;
		PacketMgr::GetInst()->send_packet(&packet);

		return EBTNodeResult::Succeeded;
	}
	return EBTNodeResult::Failed;
}