#include "BTTask_JP_TurnToTarget.h"
#include "JIAICharacter.h"
#include "JIJapanAIController.h"
#include "JICharacter.h"
#include "BehaviorTree/BlackboardComponent.h"


UBTTask_JP_TurnToTarget::UBTTask_JP_TurnToTarget()
{
	NodeName = TEXT("TurnJP");
}

EBTNodeResult::Type UBTTask_JP_TurnToTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);

	AJIAICharacter* ControllingPawn = Cast<AJIAICharacter>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == ControllingPawn)
		return EBTNodeResult::Failed;

	auto Target = Cast<AJICharacter>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AJIJapanAIController::TargetKey));
	if (nullptr == Target)
		return EBTNodeResult::Failed;

	//만약 둘다 성공했다면 타겟의 위치에서 ai캐릭터의 위치를 빼서 바라보는 방향 벡터를 구한다.
	FVector LookVector = Target->GetActorLocation() - ControllingPawn->GetActorLocation();
	LookVector.Z = 0.0f;

	//MakeFromX :: x축으로만 회전 행렬을 작성하는 함수
	FRotator TargetRot = FRotationMatrix::MakeFromX(LookVector).Rotator();
	//RInterpTo라는 회전보간함수
	ControllingPawn->SetActorRotation(FMath::RInterpTo(ControllingPawn->GetActorRotation(), TargetRot, GetWorld()->GetDeltaSeconds(), 2.0f));

	return EBTNodeResult::Succeeded;
}
