#include "BTTask_TurnToTarget.h"
#include "JIAICharacter.h"
#include "JIAIController.h"
#include "JICharacter.h"
#include "BehaviorTree/BlackboardComponent.h"


UBTTask_TurnToTarget::UBTTask_TurnToTarget()
{
	NodeName = TEXT("Turn");
}

EBTNodeResult::Type UBTTask_TurnToTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AJICharacter* ControllingPawn = Cast<AJICharacter>(OwnerComp.GetAIOwner()->GetPawn());
	if (nullptr == ControllingPawn)
		return EBTNodeResult::Failed;

	auto Target = Cast<AJIAICharacter>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AJIAIController::TargetKey));
	if (nullptr == Target)
		return EBTNodeResult::Failed;

	//만약 둘다 성공했다면 타겟의 위치에서 ai캐릭터의 위치를 빼서 바라보는 방향 벡터를 구한다.
	FVector LookVector = Target->GetActorLocation() - ControllingPawn->GetActorLocation();
	LookVector.Z = 0.0f;

	//MakeFromX :: x축으로만 회전 행렬을 작성하는 함수
	FRotator TargetRot = FRotationMatrix::MakeFromX(LookVector).Rotator();
	//RInterpTo라는 회전보간함수
	ControllingPawn->SetActorRotation(FMath::RInterpTo(ControllingPawn->GetActorRotation(), TargetRot, GetWorld()->GetDeltaSeconds(), 2.0f));

	return EBTNodeResult::Succeeded;

	//EBTNodeResult::Type Result = Super::ExecuteTask(OwnerComp, NodeMemory);


	//APawn* ControllingPawn = OwnerComp.GetAIOwner()->GetPawn();
	//if (nullptr == ControllingPawn)
	//	return EBTNodeResult::Failed;

	//if (ControllingPawn->ActorHasTag("Japan"))
	//{//빙의된 캐릭터가 없다면 Failed
	//	auto AICharacter = Cast<AJIAICharacter>(ControllingPawn);
	//
	//	//컨트롤러에서 타겟 키를 받아와서 타겟이 선정되지 않으면 Failed
	//	auto Target = Cast<AJICharacter>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AJIAIController::TargetKey));
	//	if (nullptr == Target)
	//		return EBTNodeResult::Failed;

	//	//만약 둘다 성공했다면 타겟의 위치에서 ai캐릭터의 위치를 빼서 바라보는 방향 벡터를 구한다.
	//	FVector LookVector = Target->GetActorLocation() - AICharacter->GetActorLocation();
	//	LookVector.Z = 0.0f;

	//	//MakeFromX :: x축으로만 회전 행렬을 작성하는 함수
	//	FRotator TargetRot = FRotationMatrix::MakeFromX(LookVector).Rotator();
	//	//RInterpTo라는 회전보간함수
	//	AICharacter->SetActorRotation(FMath::RInterpTo(AICharacter->GetActorRotation(), TargetRot, GetWorld()->GetDeltaSeconds(), 1.0f));

	//	return EBTNodeResult::Succeeded;
	//}
	//else if(ControllingPawn->ActorHasTag("Korea"))
	//{//빙의된 캐릭터가 없다면 Failed
	//	auto Character = Cast<AJICharacter>(ControllingPawn);


	//	//컨트롤러에서 타겟 키를 받아와서 타겟이 선정되지 않으면 Failed
	//	auto Target = Cast<AJIAICharacter>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AJIAIController::TargetKey));
	//	if (nullptr == Target)
	//		return EBTNodeResult::Failed;

	//	//만약 둘다 성공했다면 타겟의 위치에서 ai캐릭터의 위치를 빼서 바라보는 방향 벡터를 구한다.
	//	FVector LookVector = Target->GetActorLocation() - Character->GetActorLocation();
	//	LookVector.Z = 0.0f;

	//	//MakeFromX :: x축으로만 회전 행렬을 작성하는 함수
	//	FRotator TargetRot = FRotationMatrix::MakeFromX(LookVector).Rotator();
	//	//RInterpTo라는 회전보간함수
	//	Character->SetActorRotation(FMath::RInterpTo(Character->GetActorRotation(), TargetRot, GetWorld()->GetDeltaSeconds(), 1.0f));

	//	return EBTNodeResult::Succeeded;
	//}
	//return EBTNodeResult::Succeeded;
}
