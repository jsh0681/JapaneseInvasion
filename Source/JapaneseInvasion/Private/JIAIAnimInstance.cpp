// Fill out your copyright notice in the Description page of Project Settings.
#include "JIAIAnimInstance.h"

UJIAIAnimInstance::UJIAIAnimInstance()
{
	CurrentPawnSpeed = 0.0f;
	IsInAir = false;
	IsCanDash = false;
	IsDead = false;

	static ConstructorHelpers::FObjectFinder<UAnimMontage> ATTACK_MONTAGE(TEXT("/Game/Custom/Soldier/Montage/Montage_Soldier_Attack"));
	if (ATTACK_MONTAGE.Succeeded())
	{
		AttackMontage = ATTACK_MONTAGE.Object;
	}
	static ConstructorHelpers::FObjectFinder<UAnimMontage> RUN_MONTAGE(TEXT("/Game/Custom/Soldier/Montage/Montage_Soldier_Run"));
	if (RUN_MONTAGE.Succeeded())
	{
		RunMontage = RUN_MONTAGE.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimMontage> IDLE_MONTAGE(TEXT("/Game/Custom/Soldier/Montage/Montage_Soldier_Idle"));
	if (IDLE_MONTAGE.Succeeded())
	{
		IdleMontage = IDLE_MONTAGE.Object;
	}

}

void UJIAIAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	//애니메이션 시스템의 틱에서 폰에 접근해 폰의 값을 얻어오는 함수. TryGetPawnOwner;
	//폰에 접근할때 먼저 폰객체가 유효한지를 점검해야함 
	auto Pawn = TryGetPawnOwner();
	if (!::IsValid(Pawn))
		return;

	if (!IsDead)
	{
		CurrentPawnSpeed = Pawn->GetVelocity().Size();
		auto Character = Cast<ACharacter>(Pawn);
		if (Character)
		{
			IsInAir = Character->GetMovementComponent()->IsFalling();
		}
	}
}

void UJIAIAnimInstance::PlayAttackMontage()
{
	//JICHECK(!IsDead);
	if (!Montage_IsPlaying(AttackMontage))
		Montage_Play(AttackMontage, 1.0f);
}

void UJIAIAnimInstance::PlayRunMontage()
{
	//JICHECK(!IsDead);
	//if (!Montage_IsPlaying(RunMontage))
	//{
	Montage_Play(RunMontage, 1.0f);
	//}
}

void UJIAIAnimInstance::PlayIdleMontage()
{
	//JICHECK(!IsDead);
	//if (!Montage_IsPlaying(IdleMontage))
	//{
	Montage_Play(IdleMontage, 1.0f);
	//}
}


void UJIAIAnimInstance::AnimNotify_AttackHitCheck()
{

	OnAttackHitCheck.Broadcast();
}

void UJIAIAnimInstance::AnimNotify_NextAttackCheck()
{
	OnNextAttackCheck.Broadcast();
}

FName UJIAIAnimInstance::GetAttackMontageSectionName(int32 Section)
{
	JICHECK(FMath::IsWithinInclusive<int32>(Section, 1, 3), NAME_None);
	return FName(*FString::Printf(TEXT("Attack%d"), Section));
}

void UJIAIAnimInstance::JumpToAttackMontageSection(int32 NewSection)
{
	//JICHECK(!IsDead);
	//JICHECK(Montage_IsPlaying(AttackMontage));
	Montage_JumpToSection(GetAttackMontageSectionName(NewSection), AttackMontage);
}
