#include "JIAICharacter.h"
#include "JIJapanAIAnimInstance.h"
#include "JISword.h"
#include "JILance.h"
#include "JICharacterStatComponent.h"
#include "JICharacterWidget.h"
#include "JIJapanAIController.h"
#include "Components/WidgetComponent.h"
#include "PacketMgr.h"


AJIAICharacter::AJIAICharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	GetCapsuleComponent()->SetCapsuleHalfHeight(50.f);
	GetCapsuleComponent()->SetCapsuleRadius(18.f);
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_MESH(TEXT("/Game/Custom/Japan/Soldier/SK_JapanSoldier_Skeleton"));
	if (SK_MESH.Succeeded())
		GetMesh()->SetSkeletalMesh(SK_MESH.Object);
	GetMesh()->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, -50.f), FRotator(0.0f, -90.0f, 0.0f));
	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	static ConstructorHelpers::FClassFinder<UAnimInstance> AI_ANIM(TEXT("/Game/Custom/Japan/Soldier/JapanSoldier_Skeleton_AnimBlueprint"));
	if (AI_ANIM.Succeeded())
	{
		GetMesh()->SetAnimInstanceClass(AI_ANIM.Class);
	}
	ActorTag.Add("Japan");
	Tags = ActorTag;

	IsAttacking = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("Japan"));

	WeaponSocketName = TEXT("RHand_Sword");
	CurWeaponIndex = EWeaponType::LANCE;

	CharacterStat = CreateDefaultSubobject<UJICharacterStatComponent>(TEXT("CHARACTERSTAT"));

	HPBarWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("HPBARWIDGET"));
	HPBarWidget->SetupAttachment(GetMesh());

	HPBarWidget->SetRelativeLocation(FVector(0.0f, 0.0f, 110.0f));
	HPBarWidget->SetWidgetSpace(EWidgetSpace::Screen);

	static ConstructorHelpers::FClassFinder<UUserWidget> UI_HUD(TEXT("/Game/Custom/UI/UI_JPHPBar"));
	if (UI_HUD.Succeeded()) {
		HPBarWidget->SetWidgetClass(UI_HUD.Class);
		HPBarWidget->SetDrawSize(FVector2D(110.f, 30.f));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 600.0f, 0.0f);


	AIControllerClass = AJIJapanAIController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	TargetingCount = 0;
	GetCharacterMovement()->SetJumpAllowed(false);
}

// Called when the game starts or when spawned
void AJIAICharacter::BeginPlay()
{
	Super::BeginPlay();
	Weapon = GetWorld()->SpawnActor<AJILance>(AJILance::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
	if (Weapon != nullptr) Weapon->AttachMeshToPawn(GetMesh(), WeaponSocketName);

	auto CharacterWidget = Cast<UJICharacterWidget>(HPBarWidget->GetUserWidgetObject());
	if (CharacterWidget != nullptr)
	{
		CharacterWidget->BindCharacterStat(CharacterStat);
	}
	//GetMesh()->BodyInstance.SetMassScale(1.f);
	//GetMesh()->SetMassOverrideInKg(NAME_None, 50.f);
	//GetCharacterMovement()->Mass = 10.f;
}

// Called every frame
void AJIAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AJIAICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AJIAICharacter::SetTargetingCount()
{
	TargetingCount++;
}

void AJIAICharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	JIAnim = Cast<UJIJapanAIAnimInstance>(GetMesh()->GetAnimInstance());
	JICHECK(JIAnim != nullptr);
	JIAnim->OnMontageEnded.AddDynamic(this, &AJIAICharacter::OnAttackMontageEnded);
	JIAnim->OnAttackHitCheck.AddUObject(this, &AJIAICharacter::AttackCheck);
	CharacterStat->OnHPIsZero.AddLambda([this]()->void
		{
			//JILOG(Warning, TEXT("OnHPIsZero"));
			JIAnim->SetDeadAnim();
			SetActorEnableCollision(false);
			SetLifeSpan(0.667f);
		});
}

void AJIAICharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	if (IsPlayerControlled())
	{
		GetCharacterMovement()->MaxWalkSpeed = 600.0f;
	}
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = 300.0f;
	}
}

float AJIAICharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float FinalDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	cs_packet_set_hp packet;
	packet.size = sizeof(cs_packet_set_hp);
	packet.type = C2S_SET_HP;
	packet.id = m_id;
	packet.objtype = OBJ_TYPE::SOLDIER;

	if (DamageCauser->ActorHasTag("JoseonSoldier"))
		packet.damagecauser = HIT_OBJ::JOSEON_SOLDIER;
	else if (DamageCauser->ActorHasTag("JoseonGeneral"))
		packet.damagecauser = HIT_OBJ::JOSEON_GENERAL;
	else if (DamageCauser->ActorHasTag("ARROW"))
		packet.damagecauser = HIT_OBJ::ARROW;
	else
		return 0.f;

	PacketMgr::GetInst()->send_packet(&packet);

	return 0.f;
}

void AJIAICharacter::Attack()
{
	if (IsAttacking)
		return;
	JIAnim->PlayAttackMontage();
	IsAttacking = true;
}

void AJIAICharacter::OnAttackMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	JICHECK(IsAttacking);
	IsAttacking = false;
	OnAttackEnd.Broadcast();
}
void AJIAICharacter::AttackCheck()
{
	float AttackRange = 130.0f;
	float AttackRadius = 30.0f;

	FHitResult HitResult;
	FCollisionQueryParams Params(NAME_None, false, this);
	bool bResult = GetWorld()->SweepSingleByChannel(HitResult,
		GetActorLocation(),
		GetActorLocation() + GetActorForwardVector() * AttackRange,
		FQuat::Identity,
		ECollisionChannel::ECC_GameTraceChannel9,
		FCollisionShape::MakeSphere(AttackRadius),
		Params);

#if ENABLE_DRAW_DEBUG
	FVector TraceVec = GetActorForwardVector() * AttackRange;
	FVector Center = GetActorLocation() + TraceVec * 0.5f;
	float HalfHeight = AttackRange * 0.5f + AttackRadius;
	FQuat CapsuleRot = FRotationMatrix::MakeFromZ(TraceVec).ToQuat();
	FColor DrawColor = bResult ? FColor::Green : FColor::Red;
	float DebugLifeTime = 5.0f;

	//DrawDebugCapsule(GetWorld(),Center,HalfHeight,AttackRadius,CapsuleRot,DrawColor,false,DebugLifeTime);

#endif
	if (bResult)
	{
		if (HitResult.Actor.IsValid())
		{
			//JILOG(Warning, TEXT("Hit Actor Name : %s"), *HitResult.Actor->GetName());
			FDamageEvent DamageEvent;
			if (HitResult.Actor->ActorHasTag("Korea"))
			{
				HitResult.Actor->TakeDamage(CharacterStat->GetSwordAttack(), DamageEvent, GetController(), this);
			}
		}
	}
}