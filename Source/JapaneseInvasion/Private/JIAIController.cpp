#include "JIAIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"

const FName AJIAIController::HomePosKey(TEXT("HomePos"));
const FName AJIAIController::PatrolPosKey(TEXT("PatrolPos"));
const FName AJIAIController::TargetKey(TEXT("Target"));

AJIAIController::AJIAIController()
{
	static ConstructorHelpers::FObjectFinder<UBlackboardData>  BBObject(TEXT("/Game/Custom/AI/BB_JICharacter"));
	if (BBObject.Succeeded())
	{
		BBAsset = BBObject.Object;
	}
	static ConstructorHelpers::FObjectFinder<UBehaviorTree>  BTObject(TEXT("/Game/Custom/AI/BT_JICharacter"));
	if (BTObject.Succeeded())
	{
		BTAsset = BTObject.Object;
	}
}

void AJIAIController::OnPossess(APawn* pawn)
{
	Super::OnPossess(pawn);
	if (UseBlackboard(BBAsset, Blackboard))
	{
		Blackboard->SetValueAsVector(HomePosKey, pawn->GetActorLocation());
		if (!RunBehaviorTree(BTAsset))
		{
			//JILOG(Error, TEXT("AIController Couldn't run behavior tree!"));
		}
	}
}
