#include "JIAnimInstance.h"

UJIAnimInstance::UJIAnimInstance()
{
	CurrentPawnSpeed = 0.0f;
	IsInAir = false;
	IsCanDash = false;
	IsDead = false;
	static ConstructorHelpers::FObjectFinder<UAnimMontage> ATTACK_MONTAGE(TEXT("/Game/Custom/General/Montage/Montage_General_Attack"));
	if (ATTACK_MONTAGE.Succeeded())
	{
		AttackMontage = ATTACK_MONTAGE.Object;
	}


	static ConstructorHelpers::FObjectFinder<UAnimMontage> SHOOT_MONTAGE(TEXT("/Game/Custom/General/Montage/Montage_General_Shotting"));
	if (SHOOT_MONTAGE.Succeeded())
	{
		ShootMontage = SHOOT_MONTAGE.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimMontage> RUN_MONTAGE(TEXT("/Game/Custom/General/Montage/Montage_General_Run"));
	if (RUN_MONTAGE.Succeeded())
	{
		RunMontage = RUN_MONTAGE.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimMontage> IDLE_MONTAGE(TEXT("/Game/Custom/General/Montage/Montage_General_Idle"));
	if (IDLE_MONTAGE.Succeeded())
	{
		IdleMontage = IDLE_MONTAGE.Object;
	}   
	
	//사운드
	SwordAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("SwordAudioComponent"));
	static ConstructorHelpers::FObjectFinder<USoundCue> SC_SWORDATTACK(TEXT("/Game/Custom/Sound/General/SwordSwing2_Cue"));
	if (SC_SWORDATTACK.Succeeded())
		SwordSoundCue = SC_SWORDATTACK.Object;

}

void UJIAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	//애니메이션 시스템의 틱에서 폰에 접근해 폰의 값을 얻어오는 함수. TryGetPawnOwner;
	//폰에 접근할때 먼저 폰객체가 유효한지를 점검해야함 
	auto Pawn = TryGetPawnOwner();
	if (!::IsValid(Pawn))
		return;

	if (!IsDead)
	{
		CurrentPawnSpeed = Pawn->GetVelocity().Size();
		auto Character = Cast<ACharacter>(Pawn);
		if (Character)
		{
			IsInAir = Character->GetMovementComponent()->IsFalling();
		}
	}
}

void UJIAnimInstance::PlayAttackMontage()
{
	//JICHECK(!IsDead);
	Montage_Play(AttackMontage, 1.0f);
}


void UJIAnimInstance::PlayShootMontage()
{
	//JICHECK(!IsDead);
	if (!Montage_IsPlaying(ShootMontage))
	{
		Montage_Play(ShootMontage, 1.0f);
	}
}
void UJIAnimInstance::PlayRunMontage()
{
	//JICHECK(!IsDead);
	if (!Montage_IsPlaying(RunMontage))
	{
		Montage_Play(RunMontage, 1.0f);
	}
}

void UJIAnimInstance::PlayIdleMontage()
{	
	//JICHECK(!IsDead);
	if (!Montage_IsPlaying(IdleMontage))
	{
		Montage_Play(IdleMontage, 1.0f);
	}
}


void UJIAnimInstance::AnimNotify_AttackHitCheck()
{
	//JILOG(Warning, TEXT("AttackHitCheck"));
	OnAttackHitCheck.Broadcast();
}

void UJIAnimInstance::AnimNotify_NextAttackCheck()
{
	//JILOG(Warning, TEXT("NextAttackCheck"));
	OnNextAttackCheck.Broadcast();
}

void UJIAnimInstance::AnimNotify_ShootStartCheck()
{
	//JILOG(Warning, TEXT("Shoot Start"));
	OnShootStartCheck.Broadcast();
}

void UJIAnimInstance::AnimNotify_ShootEndCheck()
{
	//JILOG(Warning, TEXT("Shoot End"));
	OnShootEndCheck.Broadcast();
}


FName UJIAnimInstance::GetAttackMontageSectionName(int32 Section)
{
	JICHECK(FMath::IsWithinInclusive<int32>(Section, 1, 3), NAME_None);
	return FName(*FString::Printf(TEXT("Attack%d"), Section));
}

void UJIAnimInstance::JumpToAttackMontageSection(int32 NewSection)
{
	//JICHECK(!IsDead);
	//JICHECK(Montage_IsPlaying(AttackMontage));
	Montage_JumpToSection(GetAttackMontageSectionName(NewSection), AttackMontage);
}
