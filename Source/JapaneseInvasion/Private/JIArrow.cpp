#include "JIArrow.h"
#include "JIAICharacter.h"
#include "PacketMgr.h"

AJIArrow::AJIArrow()
{
	PrimaryActorTick.bCanEverTick = false;

	Collision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("AROOWCOLLISION"));
	RootComponent = Collision;
	Collision->SetCollisionProfileName(TEXT("Projectile"));
	Collision->SetCapsuleSize(5.f, 5.f, true);
	Collision->OnComponentHit.AddDynamic(this, &AJIArrow::OnHit);


	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_ARROW(TEXT("/Game/Custom/StaticMesh/Weapon/Arrow/SK_Arrow"));
	if (SK_ARROW.Succeeded())
	{
		Weapon->SetSkeletalMesh(SK_ARROW.Object);
	}
	//Weapon->SetSimulatePhysics(true);
	Weapon->SetNotifyRigidBodyCollision(true);
	Weapon->SetCollisionProfileName(TEXT("Projectile"));
	Weapon->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 0.0f), FRotator(0.0f, -90.f, 0.0f));
	Weapon->SetupAttachment(RootComponent);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("PROJECTILEMOVEMENT"));
	ProjectileMovement->SetUpdatedComponent(Collision);
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->InitialSpeed = 0.f;
	ProjectileMovement->MaxSpeed = 50000.f;
	ProjectileMovement->Velocity = FVector::ZeroVector;

	///////////////////////////////////////////////////////////////////////////////////////////
	TrailMesh = CreateAbstractDefaultSubobject<USkeletalMeshComponent>(TEXT("TRAIL"));
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_TRAIL(TEXT("/Game/Custom/Trail/SK_BombTrail"));
	if (SK_TRAIL.Succeeded())
	{
		TrailMesh->SetSkeletalMesh(SK_TRAIL.Object);
		TrailMesh->SetCollisionProfileName(TEXT("NoCollision"));
		TrailMesh->SetWorldScale3D(FVector(0.05f, 0.05f, 0.05f));
		TrailMesh->SetupAttachment(Weapon);
		TrailMesh->SetHiddenInGame(true);
	}

	InitialLifeSpan = 3.0f;
}


void AJIArrow::Timer()
{
	bCanSend = true;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AJIArrow::TimeAdd, 0.7f, false);
}

void AJIArrow::TimeAdd()
{
	bCanSend = false;
}

void AJIArrow::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AJIArrow::BeginPlay()
{
	Super::BeginPlay();
}


void AJIArrow::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	TrailMesh->SetAnimationMode(EAnimationMode::AnimationSingleNode);
	UAnimationAsset* AnimAsset = LoadObject<UAnimationAsset>(nullptr, TEXT("/Game/Custom/Trail/BombTrailAnim"));
	if (AnimAsset != nullptr)
		TrailMesh->PlayAnimation(AnimAsset, true);
}


void AJIArrow::FireInDirection(const FVector& ShootDirection)
{
	ProjectileMovement->Velocity = ShootDirection * ProjectileMovement->InitialSpeed;
}

void AJIArrow::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	//if (OtherActor != this && OtherComponent->IsSimulatingPhysics())
	if (OtherActor != this && OtherActor != nullptr)
	{
		FDamageEvent DamageEvent;
		//여기 해야함
		if ((OtherActor->ActorHasTag(TEXT("Japan")) || (OtherActor->ActorHasTag(TEXT("Korea")))))
		{
			Cast<AJIAICharacter>(OtherActor)->TakeDamage(100, DamageEvent, NULL, this);
			SetActorEnableCollision(false);
		}
	}
	g_mapArrow.Remove(m_id);
	Destroy();
}