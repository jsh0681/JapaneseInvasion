#include "JIBomb.h"
#include "JIShip.h"
#include "PacketMgr.h"

AJIBomb::AJIBomb()
{
    PrimaryActorTick.bCanEverTick = false;
    Collision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("SHELLCOLLISION"));
    RootComponent = Collision;
    Collision->SetCollisionProfileName(TEXT("Bomb"));
    Collision->SetCapsuleHalfHeight(13.f);
    Collision->SetCapsuleRadius(12.f);
    Collision->OnComponentHit.AddDynamic(this, &AJIBomb::OnHit);

    Shell = CreateAbstractDefaultSubobject<UStaticMeshComponent>(TEXT("SHELL"));
    static ConstructorHelpers::FObjectFinder<UStaticMesh> SK_SHELL(TEXT("/Game/Custom/StaticMesh/CannonBall"));
    if (SK_SHELL.Succeeded())
    {
        Shell->SetStaticMesh(SK_SHELL.Object);
        //Shell->SetSimulatePhysics(true);
        Shell->SetNotifyRigidBodyCollision(true);
        Shell->SetCollisionProfileName(TEXT("NoCollision"));
        Shell->SetWorldScale3D(FVector(15.0f, 15.0f, 15.0f));
        Shell->SetupAttachment(RootComponent);
    }

    TrailMesh = CreateAbstractDefaultSubobject<USkeletalMeshComponent>(TEXT("TRAIL"));
    static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_TRAIL(TEXT("/Game/Custom/Trail/SK_BombTrail"));
    if (SK_TRAIL.Succeeded())
    {
        TrailMesh->SetSkeletalMesh(SK_TRAIL.Object);
        TrailMesh->SetCollisionProfileName(TEXT("NoCollision"));
        TrailMesh->SetWorldScale3D(FVector(0.1f, 0.1f, 0.1f));
        TrailMesh->SetupAttachment(Shell);
        TrailMesh->SetHiddenInGame(true);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////

    ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("PROJECTILEMOVEMENT"));
    ProjectileMovement->SetUpdatedComponent(RootComponent);
    ProjectileMovement->bRotationFollowsVelocity = true;
    ProjectileMovement->InitialSpeed = 0.f;
    ProjectileMovement->MaxSpeed = 50000.f;
    ProjectileMovement->Velocity = FVector::ZeroVector;

    ///////////////////////////////////////////////////////////////////////////////////////////
    ExplosionEffect = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Effect"));
    ExplosionEffect->SetupAttachment(RootComponent);

    static ConstructorHelpers::FObjectFinder<UParticleSystem> PT_EXPLOSION(TEXT("/Game/Custom/Particle/P_Explosion"));
    if (PT_EXPLOSION.Succeeded())
    {
        ExplosionEffect->SetTemplate(PT_EXPLOSION.Object);
        ExplosionEffect->bAutoActivate = false;
        ExplosionEffect->SetCollisionProfileName("NoCollision");
        ExplosionEffect->SetRelativeScale3D(FVector(10.f, 10.f, 10.f));
    }
 
    ///////////////////////////////////////////////////////////////////////////////////////////
    ExplosionAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("ExplosionAudioComponent"));
    ExplosionAudioComponent->SetupAttachment(RootComponent);
    static ConstructorHelpers::FObjectFinder<USoundCue> SC_Explosion(TEXT("/Game/Custom/Sound/Explosion_Cue"));
    if (SC_Explosion.Succeeded())
        ExplosionSoundCue = SC_Explosion.Object;
}

void AJIBomb::BeginPlay()
{
    Super::BeginPlay();
    if (ExplosionAudioComponent && ExplosionSoundCue)
        ExplosionAudioComponent->SetSound(ExplosionSoundCue);
    ExplosionAudioComponent->FadeOut(10.f, 1000000.f);


    TrailMesh->SetAnimationMode(EAnimationMode::AnimationSingleNode);
    UAnimationAsset* AnimAsset = LoadObject<UAnimationAsset>(nullptr, TEXT("/Game/Custom/Trail/BombTrailAnim"));
    if (AnimAsset != nullptr)
        TrailMesh->PlayAnimation(AnimAsset, true);
}

void AJIBomb::Timer()
{
    bCanSend = true;
    GetWorldTimerManager().SetTimer(TimerHandle, this, &AJIBomb::TimeAdd, 0.7f, false);
}

void AJIBomb::TimeAdd()
{
    bCanSend = false;
}

void AJIBomb::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    FVector vPos = GetActorLocation();
    if (!bCanSend)
    {
        Timer();
       /* cs_packet_shell_move packet;
        packet.type = C2S_SHELL_MOVE;
        packet.size = sizeof(cs_packet_shell_move);
        packet.x = vPos.X;
        packet.y = vPos.Y;
        packet.z = vPos.Z;
        packet.id = m_iId;
        PacketMgr::GetInst()->send_packet(&packet);*/
    }
}

void AJIBomb::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
    FDamageEvent DamageEvent;
    if (ExplosionAudioComponent && ExplosionSoundCue)
        ExplosionAudioComponent->Play(0.f);
   
    if (OtherActor != this && OtherActor != nullptr)
    {
        ExplosionEffect->Activate(true);
        ExplosionEffect->OnSystemFinished.AddDynamic(this, &AJIBomb::OnEffectFinished);
        OtherActor->TakeDamage(100, DamageEvent, NULL, this);
        //OtherComponent->AddImpulseAtLocation(this->GetActorForwardVector() * -10.f, Hit.ImpactPoint);
        Collision->SetCollisionProfileName("NoCollision");
    }
}

void AJIBomb::OnEffectFinished(class UParticleSystemComponent* PSystem)
{
    g_mapShell.Remove(m_iId); // ����
    Destroy();
}