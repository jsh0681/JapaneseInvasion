#include "JIBow.h"

AJIBow::AJIBow()
{
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_BOW(TEXT("/Game/Custom/StaticMesh/Weapon/Arrow/SK_Bow"));
	if (SK_BOW.Succeeded())
	{
		Weapon->SetSkeletalMesh(SK_BOW.Object);
	}
	Weapon->SetCollisionProfileName(TEXT("NoCollision"));
}

void AJIBow::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AJIBow::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void AJIBow::BeginPlay()
{
	Super::BeginPlay();
}