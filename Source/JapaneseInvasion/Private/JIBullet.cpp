#include "JIBullet.h"
#include "JIShip.h"
#include "PacketMgr.h"

AJIBullet::AJIBullet()
{
	PrimaryActorTick.bCanEverTick = false;
	Collision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("BULLETCOLLISION"));
	RootComponent = Collision;
	Collision->SetCollisionProfileName(TEXT("Bullet"));
	Collision->SetCapsuleHalfHeight(13.f);
	Collision->SetCapsuleRadius(12.f);
	Collision->OnComponentHit.AddDynamic(this, &AJIBullet::OnHit);


	Bullet = CreateAbstractDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SK_Bullet(TEXT("/Game/Custom/StaticMesh/Weapon/Bullet/ST_Bullet"));
	if (SK_Bullet.Succeeded())
	{
		Bullet->SetStaticMesh(SK_Bullet.Object);
		Bullet->SetNotifyRigidBodyCollision(true);
		Bullet->SetCollisionProfileName(TEXT("NoCollision"));
		Bullet->SetWorldScale3D(FVector(5.0f, 5.0f, 5.0f));
		Bullet->SetupAttachment(RootComponent);
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	TrailMesh = CreateAbstractDefaultSubobject<USkeletalMeshComponent>(TEXT("TRAIL"));
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_TRAIL(TEXT("/Game/Custom/Trail/SK_BombTrail"));
	if (SK_TRAIL.Succeeded())
	{
		TrailMesh->SetSkeletalMesh(SK_TRAIL.Object);
		TrailMesh->SetCollisionProfileName(TEXT("NoCollision"));
		TrailMesh->SetWorldScale3D(FVector(0.2f, 0.2f, 0.2f));
		TrailMesh->SetupAttachment(Bullet);
		TrailMesh->SetHiddenInGame(true);
	}
	///////////////////////////////////////////////////////////////////////////////////////////
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("PROJECTILEMOVEMENT"));
	ProjectileMovement->SetUpdatedComponent(RootComponent);
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->InitialSpeed = 0.f;
	ProjectileMovement->MaxSpeed = 50000.f;
	ProjectileMovement->Velocity = FVector::ZeroVector;

	///////////////////////////////////////////////////////////////////////////////////////////
	ExplosionEffect = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Effect"));
	ExplosionEffect->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UParticleSystem> PT_BULLETCOLLISION(TEXT("/Game/InfinityBladeEffects/Effects/FX_Mobile/Impacts/P_Impact_Shie_2"));
	if (PT_BULLETCOLLISION.Succeeded())
	{
		ExplosionEffect->SetTemplate(PT_BULLETCOLLISION.Object);
		ExplosionEffect->bAutoActivate = false;
		ExplosionEffect->SetCollisionProfileName("NoCollision");
		ExplosionEffect->SetRelativeScale3D(FVector(20.f, 20.f, 20.f));
	}

	ExplosionAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("ExplosionAudioComponent"));
	ExplosionAudioComponent->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<USoundCue> SC_Explosion(TEXT("/Game/Custom/Sound/Explosion_Cue"));
	if (SC_Explosion.Succeeded())
		ExplosionSoundCue = SC_Explosion.Object;
}

void AJIBullet::BeginPlay()
{
	Super::BeginPlay();
	if (ExplosionAudioComponent && ExplosionSoundCue)
	{
		ExplosionAudioComponent->SetSound(ExplosionSoundCue);
	}
	ExplosionAudioComponent->FadeOut(10.f, 1000000.f);


	TrailMesh->SetAnimationMode(EAnimationMode::AnimationSingleNode);
	UAnimationAsset* AnimAsset = LoadObject<UAnimationAsset>(nullptr, TEXT("/Game/Custom/Trail/BombTrailAnim"));
	if (AnimAsset != nullptr)
		TrailMesh->PlayAnimation(AnimAsset, true);
	
}

void AJIBullet::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	FDamageEvent DamageEvent;
	if (ExplosionAudioComponent && ExplosionSoundCue)
		ExplosionAudioComponent->Play(0.f);

	if (OtherActor != this && OtherActor != nullptr)
	{
		ExplosionEffect->Activate(true);
		ExplosionEffect->OnSystemFinished.AddDynamic(this, &AJIBullet::OnEffectFinished);
		OtherActor->TakeDamage(100, DamageEvent, NULL, this);
		//OtherComponent->AddImpulseAtLocation(this->GetActorForwardVector() * -10.f, Hit.ImpactPoint);
		Collision->SetCollisionProfileName("NoCollision");
	}
}

void AJIBullet::OnEffectFinished(class UParticleSystemComponent* PSystem)
{
	g_mapShell.Remove(m_iId); // ����
	Destroy();
}