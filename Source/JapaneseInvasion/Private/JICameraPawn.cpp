#include "JICameraPawn.h"
#include "JIPlayerController.h"
#include "Blueprint/UserWidget.h"

#define DEFAULT_SPRINGARM 

bool bGeneralSpawn = false;

AJICameraPawn::AJICameraPawn()
{
    PrimaryActorTick.bCanEverTick = true;
    Mesh = CreateDefaultSubobject<UStaticMeshComponent>("MESH");
    bUseControllerRotationRoll = true;
    bUseControllerRotationPitch = true;
    bUseControllerRotationYaw = true;

    static ConstructorHelpers::FObjectFinder<UStaticMesh> MESH(TEXT("/Game/StarterContent/Shapes/Shape_Sphere"));
    if (MESH.Succeeded())
        Mesh->SetStaticMesh(MESH.Object);
    RootComponent = Mesh;


    CurrentLocationIndicatorInMinimap = CreateDefaultSubobject<UStaticMeshComponent>("CURRENTLOCATIONINDICATORMESH");
    static ConstructorHelpers::FObjectFinder<UStaticMesh> MinimapMesh(TEXT("/Game/Custom/StaticMesh/MinimapIndicator"));
    if (MinimapMesh.Succeeded())
        CurrentLocationIndicatorInMinimap->SetStaticMesh(MinimapMesh.Object);

    SpringArm = CreateDefaultSubobject<USpringArmComponent>("SPRINGARM");
    Camera = CreateDefaultSubobject<UCameraComponent>("CAMERA");
    Movement = CreateDefaultSubobject<UFloatingPawnMovement>("MOVEMENT");

    SpringArm->SetupAttachment(RootComponent);
    Camera->SetupAttachment(SpringArm);
    CurrentLocationIndicatorInMinimap->SetupAttachment(RootComponent);

    CurrentLocationIndicatorInMinimap->AddLocalRotation(FRotator(45.f, 0.0f, 0.0f));
    CurrentLocationIndicatorInMinimap->SetRelativeLocation(FVector(0.0f, 0.0f, 30000.f));

    SpringArm->bUsePawnControlRotation = true;
    SpringArm->bInheritPitch = true;
    SpringArm->bInheritRoll = true;
    SpringArm->bInheritYaw = true;
    SpringArm->TargetArmLength = 20000.f;
    SpringArm->bDoCollisionTest = false;

    CurrentVelocity = 1000000.f;
    ActorTag.Add("CameraPawn");
    Tags = ActorTag;
    bLogin = false;
    Mesh->SetCollisionProfileName(TEXT("NoCollision"));
    CurrentLocationIndicatorInMinimap->SetCollisionProfileName(TEXT("NoCollision"));
    CurrentLocationIndicatorInMinimap->SetVisibility(false);
}

void AJICameraPawn::BeginPlay()
{
    Super::BeginPlay();
    SpringArm->bUsePawnControlRotation = true;
}

void AJICameraPawn::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (!bLogin&& bGeneralSpawn)
    {
        bLogin = true;
        for (auto& General : g_mapPlayer)
        {
            if (General.Value->m_bIsOwner)
            {
                FVector pos = General.Value->GetActorLocation();
                SetActorLocation(FVector(pos.X, pos.Y, 0.0f));
            }
        }

        AJIPlayerController* controller = Cast<AJIPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));

        if (controller->GetViewMode() == EViewMode::TOPVIEW)
        {
            CurrentLocationIndicatorInMinimap->SetVisibility(true);
        }
        else if (controller->GetViewMode() == EViewMode::TOPVIEW)
        {
            CurrentLocationIndicatorInMinimap->SetVisibility(false);
        }
        TArray<AActor*> FoundActors;
        UGameplayStatics::GetAllActorsOfClass(GetWorld(), AJICameraPawn::StaticClass(), FoundActors);
        for (auto Actor : FoundActors) {
            AJICameraPawn* FoundActor = Cast<AJICameraPawn>(Actor);
            if (FoundActor)
            {
                controller->Possess(FoundActor);
            }
        }
    }
}

// Called to bind functionality to input
void AJICameraPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAxis("MoveForward", this, &AJICameraPawn::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &AJICameraPawn::MoveRight);
}

void AJICameraPawn::MoveRight(float NewAxisValue)
{
    MoveDirection += GetActorRightVector() * FMath::Clamp(NewAxisValue, -1.0f, 1.0f);
}

void AJICameraPawn::MoveForward(float NewAxisValue)
{
    FVector Direction = FVector::CrossProduct(AActor::GetActorForwardVector(), AActor::GetActorRightVector());
    MoveDirection += Direction * FMath::Clamp(NewAxisValue, -1.0f, 1.0f);
}

void AJICameraPawn::Roll(float NewAxisValue)
{
    //90도일때
    //AddControllerRollInput(NewAxisValue);
    //45도일때
    AddControllerYawInput(NewAxisValue);

}