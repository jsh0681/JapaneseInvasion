#include "JICharacter.h"
#include "JIAIAnimInstance.h"
#include "JICharacterWidget.h"
#include "JISword.h"
#include "JIAIController.h"
#include "JICharacterStatComponent.h"
#include "Components/WidgetComponent.h"
#include "PacketMgr.h"

// Sets default values
AJICharacter::AJICharacter()
{
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = false;

	CharacterStat = CreateDefaultSubobject<UJICharacterStatComponent>(TEXT("CHARACTERSTAT"));
	HPBarWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("HPBARWIDGET"));

	GetCapsuleComponent()->SetCapsuleHalfHeight(50.f);
	GetCapsuleComponent()->SetCapsuleRadius(18.f);
	GetMesh()->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, -50.f), FRotator(0.0f, -90.0f, 0.0f));
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_MESH(TEXT("/Game/Custom/Soldier/SK_Soldier"));
	if (SK_MESH.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SK_MESH.Object);
	}

	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	static ConstructorHelpers::FClassFinder<UAnimInstance> CHARACTER_ANIM(TEXT("/Game/Custom/Soldier/Animation/AnimBlueprint_Soldier"));
	if (CHARACTER_ANIM.Succeeded())
	{
		GetMesh()->SetAnimInstanceClass(CHARACTER_ANIM.Class);
	}

	static ConstructorHelpers::FClassFinder<UUserWidget> UI_HUD(TEXT("/Game/Custom/UI/UI_HPBar"));
	if (UI_HUD.Succeeded())
	{
		HPBarWidget->SetWidgetClass(UI_HUD.Class);
		HPBarWidget->SetDrawSize(FVector2D(110.f, 30.f));
	}

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("Korea"));

	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f);


	////////////콤보 공격///////////////
	HPBarWidget->SetupAttachment(GetMesh());
	HPBarWidget->SetRelativeLocation(FVector(0.0f, 0.0f, 110.0f));
	HPBarWidget->SetWidgetSpace(EWidgetSpace::Screen);
	CurWeaponIndex = EWeaponType::SWORD;

	ActorTag.Add("Korea");
	Tags = ActorTag;

	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	AIControllerClass = AJIAIController::StaticClass();
	TargetingCount = 0;
	GetCharacterMovement()->SetJumpAllowed(false);
}

void AJICharacter::BeginPlay()//레벨이 시작되면 호출 됨.
{
	Super::BeginPlay();

	auto CharacterWidget = Cast<UJICharacterWidget>(HPBarWidget->GetUserWidgetObject());
	if (CharacterWidget) {
		CharacterWidget->BindCharacterStat(CharacterStat);
	}
	Weapon = GetWorld()->SpawnActor<AJISword>(AJISword::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
	if (Weapon != nullptr)
		Weapon->AttachMeshToPawn(GetMesh(), TEXT("RHand_Sword"));
}

void AJICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AJICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AJICharacter::SetTargetingCount()
{
	TargetingCount++;
}

void AJICharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	JIAnim = Cast<UJIAIAnimInstance>(GetMesh()->GetAnimInstance());
	JICHECK(JIAnim != nullptr);
	JIAnim->OnMontageEnded.AddDynamic(this, &AJICharacter::OnAttackMontageEnded);

	JIAnim->OnAttackHitCheck.AddUObject(this, &AJICharacter::AttackCheck);
	CharacterStat->OnHPIsZero.AddLambda([this]()->void
		{
			JILOG(Warning, TEXT("OnHPIsZero"));
			JIAnim->SetDeadAnim();
			SetActorEnableCollision(false);
			SetLifeSpan(0.667f);
		});
}

void AJICharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	if (IsPlayerControlled())
	{
		GetCharacterMovement()->MaxWalkSpeed = 600.0f;
	}
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = 300.0f;
	}
}

float AJICharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{

	float FinalDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	//JILOG(Warning, TEXT("Actor : %s took Damage : %f"), *GetName(), FinalDamage);
	//CharacterStat->SetDamage(FinalDamage);

	cs_packet_set_hp packet;
	packet.size = sizeof(cs_packet_set_hp);
	packet.type = C2S_SET_HP;
	packet.id = m_id;
	packet.objtype = OBJ_TYPE::SOLDIER;

	if (DamageCauser->ActorHasTag("JapanSoldier"))
		packet.damagecauser = HIT_OBJ::JAPAN_SOLDIER;
	else if (DamageCauser->ActorHasTag("JapanGeneral"))
		packet.damagecauser = HIT_OBJ::JAPAN_GENERAL;
	else if (DamageCauser->ActorHasTag("ARROW"))
		packet.damagecauser = HIT_OBJ::ARROW;
	else
		return 0.f;

	PacketMgr::GetInst()->send_packet(&packet);

	return 0.f;
}

void AJICharacter::Attack()
{
	if (IsAttacking)
		return;
	JIAnim->PlayAttackMontage();
	IsAttacking = true;
}

void AJICharacter::OnAttackMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	JICHECK(IsAttacking);
	IsAttacking = false;
	OnAttackEnd.Broadcast();
}

void AJICharacter::AttackCheck()
{
	float AttackRange = 150.0f;
	float AttackRadius = 30.0f;
	FHitResult HitResult;
	FCollisionQueryParams Params(NAME_None, false, this);
	bool bResult = GetWorld()->SweepSingleByChannel(HitResult,
		GetActorLocation(),
		GetActorLocation() + GetActorForwardVector() * AttackRange,
		FQuat::Identity,
		ECollisionChannel::ECC_GameTraceChannel6,
		FCollisionShape::MakeSphere(AttackRadius),
		Params);

#if ENABLE_DRAW_DEBUG
	FVector TraceVec = GetActorForwardVector() * AttackRange;
	FVector Center = GetActorLocation() + TraceVec * 0.5f;
	float HalfHeight = AttackRange * 0.5f + AttackRadius;
	FQuat CapsuleRot = FRotationMatrix::MakeFromZ(TraceVec).ToQuat();
	FColor DrawColor = bResult ? FColor::Green : FColor::Red;
	float DebugLifeTime = 5.0f;
	//DrawDebugCapsule(GetWorld(),Center,HalfHeight,AttackRadius,CapsuleRot,DrawColor,false,DebugLifeTime);

#endif
	if (bResult)
	{
		if (HitResult.Actor.IsValid())
		{
			//JILOG(Warning, TEXT("Hit Actor Name : %s"), *HitResult.Actor->GetName());
			FDamageEvent DamageEvent;
			if (HitResult.Actor->ActorHasTag("Japan"))
			{
				HitResult.Actor->TakeDamage(CharacterStat->GetSwordAttack(), DamageEvent, GetController(), this);
			}
		}
	}
}