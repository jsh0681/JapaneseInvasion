#include "JICharacterStatComponent.h"
#include "JIGameInstance.h"
#include "JIPlayerController.h"

UJICharacterStatComponent::UJICharacterStatComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
	CharacterType = EObjectType::JOSEON_GENERAL;
}

void UJICharacterStatComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UJICharacterStatComponent::InitializeComponent()
{
	Super::InitializeComponent();
	SetObejctType(CharacterType);
}

void UJICharacterStatComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UJICharacterStatComponent::SetObejctType(EObjectType type)
{
	auto JIGameInstance = Cast<UJIGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	JICHECK(nullptr != JIGameInstance);
	CurrentStatData = JIGameInstance->GetJICharacterData(type);
	if (nullptr != CurrentStatData)
	{
		CharacterType = type;
		SetHP(CurrentStatData->MaxHP);
	}
	//else
		//JILOG(Error, TEXT("Type is null"));
}

void UJICharacterStatComponent::SetDamage(float NewDamage)
{
	JICHECK(CurrentStatData != nullptr);
	SetHP(FMath::Clamp<float>(CurrentHP - NewDamage, 0.0f, CurrentStatData->MaxHP));

}

void UJICharacterStatComponent::SetHP(float NewHP)
{
	CurrentHP = NewHP;
	JILOG(Warning, TEXT("%f"), CurrentHP);
	if (CurrentHP < KINDA_SMALL_NUMBER)
	{
		CurrentHP = 0.0f;
		OnHPIsZero.Broadcast();
	}
	else
	{
		OnHPChanged.Broadcast();
	}
}

float UJICharacterStatComponent::GetSwordAttack()
{
	JICHECK(CurrentStatData != nullptr, 0.0f);
	return CurrentStatData->Sword;
}

float UJICharacterStatComponent::GetArrowAttack()
{
	JICHECK(CurrentStatData != nullptr, 0.0f);
	return CurrentStatData->Arrow;
}

float UJICharacterStatComponent::GetHPRatio()
{
	JICHECK(CurrentStatData != nullptr, 0.0f);
	return (CurrentStatData->MaxHP < KINDA_SMALL_NUMBER) ? 0.0f : (CurrentHP / CurrentStatData->MaxHP);
}