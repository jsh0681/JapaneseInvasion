#include "JICharacterWidget.h"
#include "JICharacterStatComponent.h"
#include "Components/ProgressBar.h"

void UJICharacterWidget::BindCharacterStat(class UJICharacterStatComponent* NewCharacterStat)
{
	JICHECK(NewCharacterStat != nullptr);
	CurrentCharacterStat = NewCharacterStat;
	NewCharacterStat->OnHPChanged.AddUObject(this, &UJICharacterWidget::UpdateHPWidget);
}

void UJICharacterWidget::NativeConstruct()
{
	Super::NativeConstruct();

	HPProgressBar = Cast<UProgressBar>(GetWidgetFromName(TEXT("PB_HPBar")));
	JICHECK(HPProgressBar != nullptr);
	UpdateHPWidget();
}

void UJICharacterWidget::UpdateHPWidget()
{
	if (CurrentCharacterStat.IsValid())
	{
		if (HPProgressBar != nullptr)
			HPProgressBar->SetPercent(CurrentCharacterStat->GetHPRatio());
	}
}
void UJICharacterWidget::SetHPBarJoseonColor()
{
	if (HPProgressBar != nullptr)
	{
		FLinearColor JoseonColor = FLinearColor(0.0f, 1.0f, 0.0f);//초록색;
		HPProgressBar->SetFillColorAndOpacity(JoseonColor);
	}
}

void UJICharacterWidget::SetHPBarJapanColor()
{

	if (HPProgressBar != nullptr)
	{
		FLinearColor JapanColor = FLinearColor(1.0f, 1.0f, 0.0f);//초록색;
		HPProgressBar->SetFillColorAndOpacity(JapanColor);
	}
}
