#include "JIChatBox.h"
#include "Components/TextBlock.h"

UJIChatBox::UJIChatBox(const FObjectInitializer& ObjectInitializer) : UUserWidget(ObjectInitializer)
{
}

void UJIChatBox::NativeConstruct()
{
	Super::NativeConstruct();

	MessageBlock = Cast<UTextBlock>(GetWidgetFromName(TEXT("MessageWindow")));
	JICHECK(MessageBlock != nullptr); 

}