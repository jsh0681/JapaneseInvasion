#include "JIChattingWidget.h"
#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "Components/ScrollBox.h"
#include "Components/TextBlock.h"
#include "JIChatBox.h"



#include "Sockets.h"
#include "SocketSubsystem.h"
#include "Networking/Public/Interfaces/IPv4/IPv4Address.h"
#include "protocol.h"
#include "PacketMgr.h"

FString Message;
TCHAR* ChatMessage;

extern FString CharacterName;

UJIChattingWidget::UJIChattingWidget(const FObjectInitializer& ObjectInitializer) : UUserWidget(ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<UJIChatBox> UI_CHATBOX(TEXT("/Game/Custom/UI/UI_ChatText"));
	if (UI_CHATBOX.Succeeded())
		ChatBoxClass = UI_CHATBOX.Class;

	PacketMgr::GetInst()->SetChattingWidget(this);
}

void UJIChattingWidget::NativeConstruct()
{
	Super::NativeConstruct();

	ScrollBoxChattingLog = Cast<UScrollBox>(GetWidgetFromName(TEXT("ScrollBox_ChatLog")));
	JICHECK(ScrollBoxChattingLog != nullptr);

	TextChattingBox = Cast<UEditableTextBox>(GetWidgetFromName(TEXT("Edit_Chatting")));
	JICHECK(TextChattingBox != nullptr);
	TextChattingBox->ClearKeyboardFocusOnCommit = true;
	TextChattingBox->OnTextCommitted.AddDynamic(this, &UJIChattingWidget::OnChattingCommitted);


	btnSendMessage = Cast<UButton>(GetWidgetFromName(TEXT("btn_Send")));
	JICHECK(btnSendMessage != nullptr);
	btnSendMessage->OnClicked.AddDynamic(this, &UJIChattingWidget::OnBtnSendMessageClicked);

	MessageTextBlock = Cast<UTextBlock>(GetWidgetFromName(TEXT("TxtBlock_Temp")));
	JICHECK(MessageTextBlock != nullptr);
	
}

UJIChatBox* UJIChattingWidget::MakeChatBox()
{
	UJIChatBox* ChatBoxWidget = CreateWidget<UJIChatBox>(this, ChatBoxClass);
	ChatBoxWidget->NativeConstruct();
	return ChatBoxWidget;
}

void UJIChattingWidget::OnBtnSendMessageClicked()
{
	//JILOG(Warning, TEXT("OnBtnSendMessageClicked"));
	Message = TextChattingBox->GetText().ToString();
	if (Message.Len() <= 0|| Message.Len() >= 45)
		return;

	ChatMessage = Message.GetCharArray().GetData();

	cs_packet_chat chatpacket;
	chatpacket.size = 102;// sizeof(chatpacket);
	chatpacket.type = C2S_CHAT;
	wcscpy_s(chatpacket.message, ChatMessage);
	PacketMgr::GetInst()->send_packet(&chatpacket);

	FString ID = CharacterName;
	UJIChatBox* ChatBoxWidget = CreateWidget<UJIChatBox>(this, ChatBoxClass);
	ChatBoxWidget->NativeConstruct();
	FString Dot = TEXT(" : ");
	FString IDCombineDot = ID.Append(Dot);
	FString IDCombineMessage = IDCombineDot.Append(Message);
	ChatBoxWidget->MessageBlock->SetText(FText::AsCultureInvariant(IDCombineMessage));

	FString clear = TEXT("");

	TextChattingBox->SetText(FText::AsCultureInvariant(clear));

	ScrollBoxChattingLog->AddChild(ChatBoxWidget);
	ScrollBoxChattingLog->ScrollToEnd();
	TextChattingBox->SetKeyboardFocus();
}

void UJIChattingWidget::OnChattingCommitted(const FText& InText, ETextCommit::Type InCommitType)
{
	//JILOG(Warning, TEXT("OnChattingCommitted"));
	OnBtnSendMessageClicked();
}