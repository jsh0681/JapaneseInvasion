#include "JIDefeatWidget.h"
#include "Components/Button.h"

void UJIDefeatWidget::NativeConstruct()
{
	Super::NativeConstruct();

	btnReturnToTitle = Cast<UButton>(GetWidgetFromName(TEXT("BTN_ReturnToTitle")));
	if (btnReturnToTitle != nullptr)
		btnReturnToTitle->OnClicked.AddDynamic(this, &UJIDefeatWidget::OnBtnReturnToTitle);

	btnQuit = Cast<UButton>(GetWidgetFromName(TEXT("BTN_Quit")));
	if (btnQuit != nullptr)
		btnQuit->OnClicked.AddDynamic(this, &UJIDefeatWidget::OnBtnQuit);
}

void UJIDefeatWidget::OnBtnReturnToTitle()
{
	//JILOG(Warning, TEXT("OnBtnReturnToTitle"));
	//UGameplayStatics::OpenLevel(GetWorld(), "Title" ,true);
}

void UJIDefeatWidget::OnBtnQuit()
{
	//JILOG(Warning, TEXT("OnBtnQuit"));
	//UKismetSystemLibrary::QuitGame(GetWorld(), GetOwningPlayer(), EQuitPreference::Quit, false);
}