#include "JIFireDecal.h"

AJIFireDecal::AJIFireDecal()
{
	PrimaryActorTick.bCanEverTick = false;

	Target = CreateAbstractDefaultSubobject<UStaticMeshComponent>(TEXT("TARGET"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_TARGET(TEXT("/Game/Custom/StaticMesh/Plane"));
	if (ST_TARGET.Succeeded())
	{
		Target->SetStaticMesh(ST_TARGET.Object);
		Target->SetCollisionProfileName(TEXT("NoCollision"));
		Target->SetWorldScale3D(FVector(15.f, 15.f, 15.f));
		RootComponent = Target;
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> MT_TARGET(TEXT("/Game/Custom/Decal/M_TargetDecal"));
	if (MT_TARGET.Succeeded())
		Target->SetMaterial(0, MT_TARGET.Object);
}

void AJIFireDecal::BeginPlay()
{
	Super::BeginPlay();
	SetLifeSpan(1.f);
	
}