#include "JIGameInstance.h"

UJIGameInstance::UJIGameInstance()
{
	FString CharacterDataPath = TEXT("/Game/Custom/GameData/JICharacterData");
	static ConstructorHelpers::FObjectFinder<UDataTable> DT_JICHARACTER(*CharacterDataPath);
	JICHECK(DT_JICHARACTER.Succeeded());
	JICharacterTable = DT_JICHARACTER.Object;
	JICHECK(JICharacterTable->GetRowMap().Num() > 0);

	FString KoreaShipDataPath = TEXT("/Game/Custom/GameData/JIKoreaShipData");
	static ConstructorHelpers::FObjectFinder<UDataTable> DT_JIKOREASHIP(*KoreaShipDataPath);
	JICHECK(DT_JIKOREASHIP.Succeeded());
	JIKoreaShipTable = DT_JIKOREASHIP.Object;
	JICHECK(JIKoreaShipTable->GetRowMap().Num() > 0);

	FString OceanStatePath = TEXT("/Game/Custom/GameData/OceanValue");
	static ConstructorHelpers::FObjectFinder<UDataTable> DT_OCEAN_STATE(*OceanStatePath);
	JICHECK(DT_OCEAN_STATE.Succeeded());
	JIOceanTable = DT_OCEAN_STATE.Object;
	JICHECK(JIOceanTable->GetRowMap().Num() > 0);

}

void UJIGameInstance::Init()
{
	Super::Init();
}

FJICharacterData* UJIGameInstance::GetJICharacterData(EObjectType ObjectType)
{
	return JICharacterTable->FindRow<FJICharacterData>(*FString::FromInt((int32)ObjectType), TEXT(""));
}


FJIKoreaShipData* UJIGameInstance::GetJIKoreaShipData(int32 Level)
{
	return JIKoreaShipTable->FindRow<FJIKoreaShipData>(*FString::FromInt(Level), TEXT(""));
}

FOceanValue* UJIGameInstance::GetOceanValue(int32 Time)
{
	return JIOceanTable->FindRow<FOceanValue>(*FString::FromInt(Time), TEXT(""));
}



