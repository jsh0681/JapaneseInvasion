// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "JIGameMode.h"
#include "JIGeneral.h"
#include "JIHUD.h"
#include "JIPlayerController.h"
#include "JIUIPlayerController.h"

#include "Sockets.h"
#include "SocketSubsystem.h"
#include "Networking/Public/Interfaces/IPv4/IPv4Address.h"
#include "protocol.h"
#include "PacketMgr.h"

FSocket* g_Socket = nullptr;

AJIGameMode::AJIGameMode()
{
    HUDClass = AJIHUD::StaticClass();
    PlayerControllerClass = AJIPlayerController::StaticClass();
}
cs_packet_login l_packet;
void AJIGameMode::PostLogin(APlayerController* NewPlayer)
{
    cs_packet_enter_ingame packet;
    packet.size = sizeof(cs_packet_enter_ingame);
    packet.type = C2S_ENTER_INGAME;
    PacketMgr::GetInst()->send_packet(&packet);

    Super::PostLogin(NewPlayer);
    JILOG_S(Warning);
}