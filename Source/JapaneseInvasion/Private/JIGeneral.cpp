#include "JIGeneral.h"
#include "JIBow.h"
#include "JIArrow.h"
#include "JISword.h"
#include "JICharacterWidget.h"
#include "JIAICharacter.h"
#include "JICharacter.h"
#include "JICharacterStatComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"
#include "Components/WidgetComponent.h"
#include "JIBomb.h"
#include "JIGeneralSetting.h"
#include "JIGameInstance.h"
#include "JIPlayerController.h"

#include "PacketMgr.h"
#include "PacketMove.h"
#include "PacketLogin.h"

AJIGeneral::AJIGeneral()
{
    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.bStartWithTickEnabled = true;

    //컴포넌트들을 생성

    GetCapsuleComponent()->SetCapsuleHalfHeight(50.f);
    GetCapsuleComponent()->SetCapsuleRadius(18.f);

    SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("BackViewSpringArm"));
    Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("BackViewCamera"));
    HPBarWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("HPBARWIDGET"));
    CharacterStat = CreateDefaultSubobject<UJICharacterStatComponent>(TEXT("CHARACTERSTAT"));

    //부모 컴포넌트에 자식을 붙힘.
    SpringArm->SetupAttachment(GetCapsuleComponent());
    Camera->SetupAttachment(SpringArm);
    HPBarWidget->SetupAttachment(GetMesh());

    //체력바의 위치를 정하고 UI블루프린트를 할당함
    HPBarWidget->SetRelativeLocation(FVector(0.0f, 0.0f, 110.0f));
    HPBarWidget->SetWidgetSpace(EWidgetSpace::Screen);

    static ConstructorHelpers::FClassFinder<UUserWidget> UI_HUD(TEXT("/Game/Custom/UI/UI_GeneralHP"));
    if (UI_HUD.Succeeded())
    {
        HPBarWidget->SetWidgetClass(UI_HUD.Class);
        HPBarWidget->SetDrawSize(FVector2D(150.f, 50.f));
    }

    //메쉬의 위치와 회전을 조정하고 메쉬를 할당한다.
    GetMesh()->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, -50.f), FRotator(0.0f, -90.0f, 0.0f));
    static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_MESH(TEXT("/Game/Custom/General/SK_General"));
    if (SK_MESH.Succeeded())
        GetMesh()->SetSkeletalMesh(SK_MESH.Object);

    GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
    static ConstructorHelpers::FClassFinder<UAnimInstance> CHARACTER_ANIM(TEXT("/Game/Custom/General/Animation/AnimBlueprint_General"));
    if (CHARACTER_ANIM.Succeeded())
        GetMesh()->SetAnimInstanceClass(CHARACTER_ANIM.Class);

    SpringArm->TargetArmLength = 300.f;
    SpringArm->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));//카메라를 위로 50만큼 올림
    SpringArm->SetRelativeRotation(FRotator::ZeroRotator);
    SpringArm->bUsePawnControlRotation = true;
    SpringArm->bInheritPitch = true;
    SpringArm->bInheritRoll = true;
    SpringArm->bInheritYaw = true;
    SpringArm->bDoCollisionTest = true;
    bUseControllerRotationYaw = false;

    IsAttacking = false;
    IsShooting = true;

    ////////////콤보 공격///////////////
    MaxCombo = 3;
    AttackEndComboState();
    GetCapsuleComponent()->SetCollisionProfileName(TEXT("General"));


    AttackRange = 130.0f;
    AttackRadius = 30.0f;
    /////////////////무기///////////////////////

    CurWeaponIndex = EWeaponType::SWORD;
    WeaponSocketName = TEXT("RHand_Sword");
    WeaponSocketName = TEXT("LHand_Bow");

    
    //사운드
    SwordAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("SwordAudioComponent"));
    SwordAudioComponent->SetupAttachment(RootComponent);
    static ConstructorHelpers::FObjectFinder<USoundCue> SC_SWORDATTACK(TEXT("/Game/Custom/Sound/General/SwordSwing2_Cue"));
    if (SC_SWORDATTACK.Succeeded())
        SwordSoundCue = SC_SWORDATTACK.Object;

    SwordFinalAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("SwordFinalAudioComponent"));
    static ConstructorHelpers::FObjectFinder<USoundCue> SC_SWORDFINALATTACK(TEXT("/Game/Custom/Sound/General/SwordSwing3_Cue"));
    if (SC_SWORDFINALATTACK.Succeeded())
        SwordFinalSoundCue = SC_SWORDFINALATTACK.Object;


    ArrowAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("ArrowAudioComponent"));
    ArrowAudioComponent->SetupAttachment(RootComponent);
    static ConstructorHelpers::FObjectFinder<USoundCue> SC_ARROWSHOOT(TEXT("/Game/Custom/Sound/General/ShootingSound_Cue"));
    if (SC_ARROWSHOOT.Succeeded())
        ArrowSoundCue = SC_ARROWSHOOT.Object;

    GetCharacterMovement()->SetJumpAllowed(false);
}

// Called when the game starts or when spawned
void AJIGeneral::BeginPlay()//레벨이 시작되면 호출 됨.
{
    Super::BeginPlay();

    //4.21버전부터는 UI의 객체 생성이 BeginPlay에서 진행된다. UUserWiget의 widget을 반환받아야하는데 Widget이 Null값을 나타냄
    auto CharacterWidget = Cast<UJICharacterWidget>(HPBarWidget->GetUserWidgetObject());
    if (nullptr != CharacterWidget)
    {
        CharacterWidget->BindCharacterStat(CharacterStat);
    }

    Weapon = GetWorld()->SpawnActor<AJISword>(AJISword::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);

    if (Weapon != nullptr)
        Weapon->AttachMeshToPawn(GetMesh(), TEXT("RHand_Sword"));

    HPBarWidget->SetVisibility(false);

    SwordAudioComponent->SetSound(SwordSoundCue);
    ArrowAudioComponent->SetSound(ArrowSoundCue);
    SwordFinalAudioComponent->SetSound(SwordFinalSoundCue);
}

void AJIGeneral::PossessedBy(AController* NewController)
{
    Super::PossessedBy(NewController);
    //JILOG(Warning, TEXT("빙의했써유"));
}

sc_packet_move packetss;
void AJIGeneral::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if (Controller != nullptr)
    {
        AJIPlayerController* controller = Cast<AJIPlayerController>(Controller);
        if (controller->GetViewMode()==EViewMode::BACKVIEW)
        {
            HPBarWidget->SetVisibility(true);
        }
        else if (controller->GetViewMode() == EViewMode::TOPVIEW)
        {
            HPBarWidget->SetVisibility(false);
        }
    }

    if (CurWeaponIndex == EWeaponType::BOW)
    {
        Camera->SetRelativeLocation(FVector(100.0f, 100.0f, 50.0f));
    }
    else
    {
        Camera->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
    }

    //서버부분
    if (CharacterStat->CurrentHP <= 0) // 수정
    {
        g_mapPlayer.Remove(m_iId);
        return;
    }

    if (GetMovementComponent()->IsFalling() && m_bIsOwner) // 수정
    {
        cs_packet_move packet;
        packet.type = C2S_MOVE;
        packet.size = sizeof(cs_packet_move);

        FVector pos = GetActorLocation();
        FVector vel = GetVelocity();
        packet.x = pos.X;
        packet.y = pos.Y;
        packet.z = pos.Z;
        packet.velx = vel.X;
        packet.vely = vel.Y;
        packet.velz = vel.Z;
        packet.m_id = m_iId;
        packet.objtype = m_eObjType;
        //packet.dir = D_UP;

        packet.TurnValue = GetActorRotation().Yaw;
        //packet.RotValue = NewAxisValue;
        PacketMgr::GetInst()->send_packet(&packet);

        //cs_packet_move packet;
        //
        //packet.type = C2S_MOVE;
        //packet.size = sizeof(cs_packet_move);
        //packet.m_id = m_iId;
        ////packet.x =110000.f;
        ////packet.y = 139400.f;
        //packet.z = GetActorLocation().Z;
        //packet.objtype = OBJ_TYPE::GENERAL;
        PacketMgr::GetInst()->send_packet(&packet);
    }
}

void AJIGeneral::PostInitializeComponents()//액터의 컴포넌트 초기화 완료 후 호출됨.
{
    Super::PostInitializeComponents();
    JIAnim = Cast<UJIAnimInstance>(GetMesh()->GetAnimInstance());
    JICHECK(JIAnim != nullptr);
    JIAnim->OnMontageEnded.AddDynamic(this, &AJIGeneral::OnAttackMontageEnded);//Animinstance에서 몽타주가 끝나면 Character에 OnAttackMontageEnded를 호출하게 만든다.
    JIAnim->OnNextAttackCheck.AddLambda([this]()->void
        {
            //JILOG(Warning, TEXT("OnNextAttackCheck"));
            CanNextCombo = false;
            if (IsComboInputOn)
            {
                AttackStartComboState();
                JIAnim->JumpToAttackMontageSection(CurrentCombo);
                if (CurrentCombo == 2)
                    SwordAudioComponent->Play(0.f);
                else if (CurrentCombo == 3)
                    SwordFinalAudioComponent->Play(0.005f);
            }
        });

    JIAnim->OnAttackHitCheck.AddUObject(this, &AJIGeneral::AttackCheck);
    JIAnim->OnShootStartCheck.AddUObject(this, &AJIGeneral::Shoot);

    CharacterStat->OnHPIsZero.AddLambda([this]()->void
        {
            JILOG(Warning, TEXT("OnHPIsZero"));
            JIAnim->SetDeadAnim();
            SetActorEnableCollision(false);
            SetLifeSpan(3.0f);
        });
}

void AJIGeneral::OnAttackMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
    //JICHECK는 조건이 참이 아니면 붉게 표시함
    JICHECK(IsAttacking);
    JICHECK(CurrentCombo > 0);
    IsAttacking = false;   //공격중이 아니다는 것을 알려줌
    AttackEndComboState();
}

void AJIGeneral::AttackStartComboState()
{
    CanNextCombo = true;
    IsComboInputOn = false;
    JICHECK(FMath::IsWithinInclusive<int32>(CurrentCombo, 0, MaxCombo - 1));
    CurrentCombo = FMath::Clamp<int32>(CurrentCombo + 1, 1, MaxCombo);
}

void AJIGeneral::AttackEndComboState()
{
    IsComboInputOn = false;
    CanNextCombo = false;
    CurrentCombo = 0;
}

// Called to bind functionality to input
void AJIGeneral::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AJIGeneral::Jump);
    PlayerInputComponent->BindAction("WeaponChange", IE_Pressed, this, &AJIGeneral::WeaponChange);
}

void AJIGeneral::WeaponChange()
{
    //for (auto& Generals : g_mapPlayer)
    //{
        //JILOG(Warning, TEXT("Generals.value->m_iId : %d, m_iId : %d"), Generals.Value->m_iId, m_iId);

    //   if (Generals.Value->m_eTeamColor == m_eTeamColor)
    //   {

    switch (CurWeaponIndex)
    {
    case EWeaponType::SWORD:
    {
        /*       if (Weapon != nullptr)
               {
                   Weapon->DetachMeshFromPawn();
                   Weapon->Destroy(true);
               }
               WeaponSocketName = (TEXT("RHand_Bow"));
               Weapon = GetWorld()->SpawnActor<AJIBow>(AJIBow::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
               Weapon->AttachMeshToPawn(GetMesh(), WeaponSocketName);
               Weapon->SetOwner(this);
               CurWeaponIndex = EWeaponType::BOW;*/


        cs_packet_change_weapon packet;
        packet.size = sizeof(cs_packet_change_weapon);
        packet.type = C2S_CHANGE_WEAPON;
        packet.weapon = A_BOW;

        //for (auto& Generals : g_mapPlayer)
        //{
        //   if (Generals.Value->m_eTeamColor == m_eTeamColor)
        packet.general_id = m_iId;
        //}

        PacketMgr::GetInst()->send_packet(&packet);

        break;
    }
    case EWeaponType::BOW:
    {
        //if (Weapon != nullptr)
        //{
        //    Weapon->DetachMeshFromPawn();
        //    Weapon->Destroy(true);
        //}
        //WeaponSocketName = (TEXT("RHand_Sword"));
        //Weapon = GetWorld()->SpawnActor<AJISword>(AJISword::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
        //Weapon->AttachMeshToPawn(GetMesh(), WeaponSocketName);
        //Weapon->SetOwner(this);
        //CurWeaponIndex = EWeaponType::SWORD;

        cs_packet_change_weapon packet;
        packet.size = sizeof(cs_packet_change_weapon);
        packet.type = C2S_CHANGE_WEAPON;
        packet.weapon = A_KNIFE;

        //for (auto& Generals : g_mapPlayer)
        //{
        //   if (Generals.Value->m_eTeamColor == m_eTeamColor)
        packet.general_id = m_iId;
        //}

        PacketMgr::GetInst()->send_packet(&packet);

        break;
    }
    }
    //   }
    //}
}

void AJIGeneral::Attack()
{
    switch (CurWeaponIndex)
    {
    case EWeaponType::SWORD:
        if (IsAttacking)
        {
            //IsWithinInclusive(해당 값, 최소 값, 최대 값) -> 해당 값이 최소값과 최대값을 포함하여 값이 범위 내에 있는지 확인
            JICHECK(FMath::IsWithinInclusive<int32>(CurrentCombo, 1, MaxCombo));//범위 안에 있는 지 체크
            if (CanNextCombo)
            {
                IsComboInputOn = true;
            }
        }
        else
        {
            JICHECK(CurrentCombo == 0);
            AttackStartComboState();
            JIAnim->PlayAttackMontage();
            JIAnim->JumpToAttackMontageSection(CurrentCombo);
            SwordAudioComponent->Play(0.03f);
            IsAttacking = true;
        }
        break;

    case EWeaponType::BOW:
        JIAnim->PlayShootMontage();
        ArrowAudioComponent->Play(0.f);
        break;
    }
}

void AJIGeneral::MoveForward(float NewAxisValue)
{
    //두 가지의 의미가 같다.
    //AddMovementInput(FRotationMatrix(FRotator(0.0f, GetControlRotation().Yaw, 0.0f)).GetUnitAxis(EAxis::X), NewAxisValue);
    AddMovementInput(FRotationMatrix(GetControlRotation()).GetUnitAxis(EAxis::X), NewAxisValue);
}

void AJIGeneral::MoveRight(float NewAxisValue)
{
    //if (CurWeaponIndex == EWeaponType::SWORD)
    AddMovementInput(FRotationMatrix(GetControlRotation()).GetUnitAxis(EAxis::Y), NewAxisValue);
}

float AJIGeneral::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
    float FinalDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
    //클라부분
   /* JILOG(Warning, TEXT("Actor : %s took Damage : %f"), *GetName(), FinalDamage);
    CharacterStat->SetDamage(FinalDamage);
    return FinalDamage;*/


    cs_packet_set_hp packet;
    packet.size = sizeof(cs_packet_set_hp);
    packet.type = C2S_SET_HP;
    packet.id = m_iId;
    packet.objtype = m_eObjType;

    if (ActorHasTag(TEXT("JOSEON")))
    {
        if (DamageCauser->ActorHasTag("JapanSoldier"))
            packet.damagecauser = HIT_OBJ::JAPAN_SOLDIER;
        else if (DamageCauser->ActorHasTag("JapanGeneral"))
            packet.damagecauser = HIT_OBJ::JAPAN_GENERAL;
        else if (DamageCauser->ActorHasTag("ARROW"))
            packet.damagecauser = HIT_OBJ::ARROW;
        else
            return 0.f;

        PacketMgr::GetInst()->send_packet(&packet);
    }
    else if (ActorHasTag(TEXT("JAPAN")))
    {
        if (DamageCauser->ActorHasTag("JoseonSoldier"))
            packet.damagecauser = HIT_OBJ::JOSEON_SOLDIER;
        else if (DamageCauser->ActorHasTag("JoseonGeneral"))
            packet.damagecauser = HIT_OBJ::JOSEON_GENERAL;
        else if (DamageCauser->ActorHasTag("ARROW"))
            packet.damagecauser = HIT_OBJ::ARROW;
        else
            return 0.f;

        PacketMgr::GetInst()->send_packet(&packet);
    }
    return 0.f;
}

void AJIGeneral::AttackCheck()
{
    FHitResult HitResult;
    FCollisionQueryParams Params(NAME_None, false, this);
    bool bResult = GetWorld()->SweepSingleByChannel(HitResult,
        GetActorLocation(),
        GetActorLocation() + GetActorForwardVector() * AttackRange,
        FQuat::Identity,
        ECollisionChannel::ECC_EngineTraceChannel2,
        FCollisionShape::MakeSphere(AttackRadius),
        Params);

#if ENABLE_DRAW_DEBUG
    FVector TraceVec = GetActorForwardVector() * AttackRange;
    FVector Center = GetActorLocation() + TraceVec * 0.5f;
    float HalfHeight = AttackRange * 0.5f + AttackRadius;
    FQuat CapsuleRot = FRotationMatrix::MakeFromZ(TraceVec).ToQuat();
    FColor DrawColor = bResult ? FColor::Green : FColor::Red;
    float DebugLifeTime = 5.0f;

    //DrawDebugCapsule(GetWorld(),
    //    Center,
    //    HalfHeight,
    //    AttackRadius,
    //    CapsuleRot,
    //    DrawColor,
    //    false,
    //    DebugLifeTime);

#endif
    if (bResult)
    {
        if (HitResult.Actor.IsValid())
        {
            //JILOG(Warning, TEXT("Hit Actor Name : %s"), *HitResult.Actor->GetName());
            FDamageEvent DamageEvent;
            if (HitResult.Actor->ActorHasTag("JAPAN") || HitResult.Actor->ActorHasTag("JOSEON")|| Controller != nullptr)
            {
                HitResult.Actor->TakeDamage(CharacterStat->GetSwordAttack(), DamageEvent, GetController(), this);
            }
        }
    }
}

void AJIGeneral::Shoot() // 컨트롤러 없어서 쏘는 장군 있으면 문제생김
{
    if (!m_bIsOwner|| Controller == nullptr)
        return;

    if (CurWeaponIndex == EWeaponType::BOW)
    {
        float arcValue = 0.97f;                       // ArcParam (0.0-1.0)
        ArrowVelocity = FVector::ZeroVector;   // 결과 Velocity
        FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);//현재 바라보고 있는 방향 

        if (UGameplayStatics::SuggestProjectileVelocity_CustomArc(this, ArrowVelocity, GetActorLocation(), GetActorLocation() + Direction * 300.f, GetWorld()->GetGravityZ(), arcValue))
        {
            //FPredictProjectilePathParams predictParams(5.0f, GetActorLocation(), ArrowVelocity, 5.0f);   // 20: tracing 보여질 프로젝타일 크기, 15: 시물레이션되는 Max 시간(초)
            //predictParams.DrawDebugTime = 0.5;     //디버그 라인 보여지는 시간 (초)
            //predictParams.DrawDebugType = EDrawDebugTrace::Type::ForDuration;  // DrawDebugTime 을 지정하면 EDrawDebugTrace::Type::ForDuration 필요.
            //predictParams.OverrideGravityZ = GetWorld()->GetGravityZ();
            //FPredictProjectilePathResult result;
            //UGameplayStatics::PredictProjectilePath(this, predictParams, result);

            FRotator Rotater = FRotationMatrix(Controller->GetControlRotation()).Rotator();
            Rotater.Pitch = 0.0f;
            Rotater.Roll = 0.0f;

            FVector vPos = GetActorLocation();

            cs_packet_arrow_spawn packet;
            packet.size = sizeof(cs_packet_arrow_spawn);
            packet.type = C2S_ARROW_SPAWN;
            packet.posx = vPos.X + 40.f;
            packet.posy = vPos.Y;
            packet.posz = vPos.Z + 30.f;
            packet.controllerRotx = Controller->GetControlRotation().Pitch;
            packet.controllerRoty = Controller->GetControlRotation().Yaw;
            packet.controllerRotz = Controller->GetControlRotation().Roll;
            packet.rotx = Rotater.Pitch;
            packet.roty = Rotater.Yaw;
            packet.rotz = Rotater.Roll;
            packet.velx = ArrowVelocity.X;
            packet.vely = ArrowVelocity.Y;
            packet.velz = ArrowVelocity.Z;
            packet.id = m_iId;
            PacketMgr::GetInst()->send_packet(&packet);
        }

        FRotator Rotater = FRotationMatrix(Controller->GetControlRotation()).Rotator();
        Rotater.Pitch = 0.0f;
        Rotater.Roll = 0.0f;

    }
}

void AJIGeneral::Turn(float NewAxisValue)
{
    AddControllerYawInput(NewAxisValue);
}

void AJIGeneral::LookUp(float NewAxisValue)
{
    AddControllerPitchInput(NewAxisValue);
}

void AJIGeneral::OnAssetLoadCompleted()
{
    AssetStreamingHandle->ReleaseHandle();
    TSoftObjectPtr<USkeletalMesh> LoadedAssetPath(GeneralAssetToLoad);
    if (LoadedAssetPath.IsValid())
        GetMesh()->SetSkeletalMesh(LoadedAssetPath.Get());
}

void AJIGeneral::SetAsset()
{
    auto DefaultSetting = GetDefault<UJIGeneralSetting>();
    GeneralAssetToLoad = DefaultSetting->GeneralAssets[AssetIndex];
    auto JIGameInstance = Cast<UJIGameInstance>(GetGameInstance());
    if (JIGameInstance != nullptr)
        AssetStreamingHandle = JIGameInstance->StreamableManager.RequestAsyncLoad(GeneralAssetToLoad, FStreamableDelegate::CreateUObject(this, &AJIGeneral::OnAssetLoadCompleted));
}