#include "JIGeneralHUDWidget.h"
#include "Components/Image.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

UJIGeneralHUDWidget::UJIGeneralHUDWidget(const FObjectInitializer& ObjectInitializer)
	: UUserWidget(ObjectInitializer)
{
	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_GENERAL_STATE(TEXT("/Game/Custom/Image/General_Bar"));
	if (TEX_GENERAL_STATE.Succeeded())
		GeneralStateBarTex = TEX_GENERAL_STATE.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_BOW(TEXT("/Game/Custom/Image/UI/CurrentWeapon_Bow"));
	if (TEX_BOW.Succeeded())
		BowTex = TEX_BOW.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_SWORD(TEXT("/Game/Custom/Image/UI/CurrentWeapon_Sword"));
	if (TEX_SWORD.Succeeded())
		SwordTex = TEX_SWORD.Object;
			
}

void UJIGeneralHUDWidget::NativeConstruct()
{
	Super::NativeConstruct();

	GeneralStateImg = Cast<UImage>(GetWidgetFromName(TEXT("Img_GeneralState")));
	JICHECK(nullptr != GeneralStateImg);

	WeaponStateImg = Cast<UImage>(GetWidgetFromName(TEXT("Img_Weapon")));
	JICHECK(nullptr != WeaponStateImg);

	GeneralStateImg->SetBrush(MakeBrush(GeneralStateBarTex, 1280, 99));
}

void UJIGeneralHUDWidget::SetBowImg()
{
	WeaponStateImg->SetBrush(MakeBrush(BowTex, 980, 980));
}

void UJIGeneralHUDWidget::SetSwordImg()
{
	WeaponStateImg->SetBrush(MakeBrush(SwordTex, 980, 980));
	
}

FSlateBrush UJIGeneralHUDWidget::MakeBrush(UTexture2D * inputTexture, int x, int y)
{
	FSlateBrush itemBrush = UWidgetBlueprintLibrary::MakeBrushFromTexture(inputTexture);
	itemBrush.ImageSize.X = x;
	itemBrush.ImageSize.Y = y;
	itemBrush.DrawAs = ESlateBrushDrawType::Image;
	return itemBrush;
}
