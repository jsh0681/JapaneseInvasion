#include "JIHUD.h"

void AJIHUD::DrawHUD()
{
	if (m_bStartSelecting)
	{
		FoundActors.Empty();//초기화해주고 범위안에있으면 다시 추가하고를 매틱마다 반복
		m_vCurrentPoint = GetMousePos2D();
		DrawRect(FLinearColor(0.0f, 0.0f, 1.0f, 0.15f), m_vInitialPoint.X, m_vInitialPoint.Y, m_vCurrentPoint.X - m_vInitialPoint.X, m_vCurrentPoint.Y - m_vInitialPoint.Y);
		GetActorsInSelectionRectangle<AJIShip>(m_vInitialPoint, m_vCurrentPoint, FoundActors, false, false);
	}
}

FVector2D AJIHUD::GetMousePos2D()
{
	float fPosX, fPosY;
	GetOwningPlayerController()->GetMousePosition(fPosX, fPosY);
	return FVector2D(fPosX, fPosY);
}
