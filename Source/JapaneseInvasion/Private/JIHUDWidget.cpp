﻿#include "JIHUDWidget.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "JIKoreaShipStatComponent.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "JIShip.h"

#define SHIP_NUM 12
#define PANOK 0
#define TURTLE 1
#define ANTAKE 2

UJIHUDWidget::UJIHUDWidget(const FObjectInitializer& ObjectInitializer) 
	: UUserWidget(ObjectInitializer) 
{
	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_Null(TEXT("/Game/Custom/Image/UI/image_null"));
	if (TEX_Null.Succeeded())
		NullTex = TEX_Null.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_Panok(TEXT("/Game/Custom/Image/Image_Panok"));
	if (TEX_Panok.Succeeded())
		PanokTex = TEX_Panok.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_Turtle(TEXT("/Game/Custom/Image/Image_Turtle"));
	if (TEX_Turtle.Succeeded())
		TurtleTex = TEX_Turtle.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_Panok_Red(TEXT("/Game/Custom/Image/Image_Panok_Green"));
	if (TEX_Panok_Red.Succeeded())
		PanokTexGreen = TEX_Panok_Red.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_Panok_Green(TEXT("/Game/Custom/Image/Image_Panok_Red"));
	if (TEX_Panok_Green.Succeeded())
		PanokTexRed = TEX_Panok_Green.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_Panok_Yellow(TEXT("/Game/Custom/Image/Image_Panok_Yellow"));
	if (TEX_Panok_Yellow.Succeeded())
		PanokTexYellow = TEX_Panok_Yellow.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_Turtle_Red(TEXT("/Game/Custom/Image/Image_Turtle_Red"));
	if (TEX_Turtle_Red.Succeeded()) 
		TurtleTexRed = TEX_Turtle_Red.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_Turtle_Green(TEXT("/Game/Custom/Image/Image_Turtle_Green"));
	if (TEX_Turtle_Green.Succeeded())
		TurtleTexGreen = TEX_Turtle_Green.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_Turtle_Yellow(TEXT("/Game/Custom/Image/Image_Turtle_Yellow"));
	if (TEX_Turtle_Yellow.Succeeded())
		TurtleTexYellow = TEX_Turtle_Yellow.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_Antake_Red(TEXT("/Game/Custom/Image/Image_Antake_Red"));
	if (TEX_Antake_Red.Succeeded())
		AntakeTexRed = TEX_Antake_Red.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_Antake_Green(TEXT("/Game/Custom/Image/Image_Antake_Green"));
	if (TEX_Antake_Green.Succeeded())
		AntakeTexGreen = TEX_Antake_Green.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_Antake_Yellow(TEXT("/Game/Custom/Image/Image_Antake_Yellow"));
	if (TEX_Antake_Yellow.Succeeded())
		AntakeTexYellow = TEX_Antake_Yellow.Object;



	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_MoveNormal(TEXT("/Game/Custom/Image/UI/btn_move"));
	if (TEX_MoveNormal.Succeeded())
		MoveNoramlTex = TEX_MoveNormal.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_MovePressed(TEXT("/Game/Custom/Image/UI/btn_move_click"));
	if (TEX_MovePressed.Succeeded())
		MovePressedTex = TEX_MovePressed.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_StopNormal(TEXT("/Game/Custom/Image/UI/btn_stop"));
	if (TEX_StopNormal.Succeeded())
		StopNoramlTex = TEX_StopNormal.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_StopPressed(TEXT("/Game/Custom/Image/UI/btn_stop_click"));
	if (TEX_StopPressed.Succeeded())
		StopPressedTex = TEX_StopPressed.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_AttackNoraml(TEXT("/Game/Custom/Image/UI/btn_attack"));
	if (TEX_AttackNoraml.Succeeded())
		AttackNoramlTex = TEX_AttackNoraml.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_AttackPressed(TEXT("/Game/Custom/Image/UI/btn_attack_click"));
	if (TEX_AttackPressed.Succeeded())
		AttackPressedTex = TEX_AttackPressed.Object;


	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_ZoomNormal(TEXT("/Game/Custom/Image/UI/btn_Zoom"));
	if (TEX_ZoomNormal.Succeeded())
		ZoomNoramlTex = TEX_ZoomNormal.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_ZoomPressed(TEXT("/Game/Custom/Image/UI/btn_zoom_click"));
	if (TEX_ZoomPressed.Succeeded())
		ZoomPressedTex = TEX_ZoomPressed.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_OceanNormal(TEXT("/Game/Custom/Image/UI/btn_ocean"));
	if (TEX_OceanNormal.Succeeded())
		OceanNoramlTex = TEX_OceanNormal.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_OceanPressed(TEXT("/Game/Custom/Image/UI/btn_ocean_click"));
	if (TEX_OceanPressed.Succeeded())
		OceanPressedTex = TEX_OceanPressed.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_OptionNoraml(TEXT("/Game/Custom/Image/UI/btn_option"));
	if (TEX_OptionNoraml.Succeeded())
		OptionNoramlTex = TEX_OptionNoraml.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_OptionPressed(TEXT("/Game/Custom/Image/UI/btn_option_click"));
	if (TEX_OptionPressed.Succeeded())
		OptionPressedTex = TEX_OptionPressed.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_ReloadNormal(TEXT("/Game/Custom/Image/UI/btn_reload"));
	if (TEX_ReloadNormal.Succeeded())
		ReloadNoramlTex = TEX_ReloadNormal.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_ReloadPressed(TEXT("/Game/Custom/Image/UI/btn_reload_click"));
	if (TEX_ReloadPressed.Succeeded())
		ReloadPressedTex = TEX_ReloadPressed.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_MapNormal(TEXT("/Game/Custom/Image/UI/btn_map"));
	if (TEX_MapNormal.Succeeded())
		MapNoramlTex = TEX_MapNormal.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_MapPressed(TEXT("/Game/Custom/Image/UI/btn_map_click"));
	if (TEX_MapPressed.Succeeded())
		MapPressedTex = TEX_MapPressed.Object;
}
void UJIHUDWidget::SetAudioComponent(UAudioComponent* ClickAudio)
{
	ClickAudioComponent = ClickAudio;
}

void UJIHUDWidget::NativeConstruct()
{
	Super::NativeConstruct();

	WS_HUD = Cast<UWidgetSwitcher>(GetWidgetFromName(TEXT("WS_State")));
	JICHECK(nullptr != WS_HUD);

	MoveButton = Cast<UButton>(GetWidgetFromName(TEXT("icon_move")));
	JICHECK(nullptr != MoveButton);
	MoveButton->OnClicked.AddDynamic(this, &UJIHUDWidget::OnMoveButtonClicked);
	MoveButton->OnReleased.AddDynamic(this, &UJIHUDWidget::OnMoveButtonReleased);

	StopButton = Cast<UButton>(GetWidgetFromName(TEXT("icon_stop")));
	JICHECK(nullptr != StopButton);
	StopButton->OnClicked.AddDynamic(this, &UJIHUDWidget::OnStopButtonClicked);
	StopButton->OnReleased.AddDynamic(this, &UJIHUDWidget::OnStopButtonReleased);

	AttackButton = Cast<UButton>(GetWidgetFromName(TEXT("icon_attack")));
	JICHECK(nullptr != AttackButton);
	AttackButton->OnClicked.AddDynamic(this, &UJIHUDWidget::OnAttackButtonClicked);
	AttackButton->OnReleased.AddDynamic(this, &UJIHUDWidget::OnAttackButtonReleased);

	MapButton = Cast<UButton>(GetWidgetFromName(TEXT("icon_minimap")));
	JICHECK(nullptr != MapButton);
	MapButton->OnClicked.AddDynamic(this, &UJIHUDWidget::OnMapButtonClicked);
	MapButton->OnReleased.AddDynamic(this, &UJIHUDWidget::OnMapButtonReleased);

	ZoomButton = Cast<UButton>(GetWidgetFromName(TEXT("icon_Wheel")));
	JICHECK(nullptr != ZoomButton);
	ZoomButton->OnClicked.AddDynamic(this, &UJIHUDWidget::OnZoomButtonClicked);
	ZoomButton->OnReleased.AddDynamic(this, &UJIHUDWidget::OnZoomButtonReleased);

	ReloadButton = Cast<UButton>(GetWidgetFromName(TEXT("icon_reload")));
	JICHECK(nullptr != ReloadButton);
	ReloadButton->OnClicked.AddDynamic(this, &UJIHUDWidget::OnReloadButtonClicked);
	ReloadButton->OnReleased.AddDynamic(this, &UJIHUDWidget::OnReloadButtonReleased);

	OptionButton = Cast<UButton>(GetWidgetFromName(TEXT("icon_option")));
	JICHECK(nullptr != OptionButton);
	OptionButton->OnClicked.AddDynamic(this, &UJIHUDWidget::OnOptionButtonClicked);
	OptionButton->OnReleased.AddDynamic(this, &UJIHUDWidget::OnOptionButtonReleased);

	OceanButton = Cast<UButton>(GetWidgetFromName(TEXT("icon_ocean")));
	JICHECK(nullptr != OceanButton);
	OceanButton->OnClicked.AddDynamic(this, &UJIHUDWidget::OnOceanButtonClicked);
	OceanButton->OnReleased.AddDynamic(this, &UJIHUDWidget::OnOceanButtonReleased);

	Img_Button = Cast<UButton>(GetWidgetFromName(TEXT("Img_But")));
	JICHECK(nullptr != Img_Button);

	CurrentHP = Cast<UTextBlock>(GetWidgetFromName(TEXT("Txt_CurrentHP")));
	JICHECK(nullptr != CurrentHP);

	MaxHP = Cast<UTextBlock>(GetWidgetFromName(TEXT("Txt_MaxHP")));
	JICHECK(nullptr != MaxHP);

	ResidualShell = Cast<UTextBlock>(GetWidgetFromName(TEXT("Txt_ResidualShell")));
	JICHECK(nullptr != ResidualShell);

	MaxShell = Cast<UTextBlock>(GetWidgetFromName(TEXT("Txt_MaxShell")));
	JICHECK(nullptr != MaxShell);

	Damage = Cast<UTextBlock>(GetWidgetFromName(TEXT("Txt_Damage")));
	JICHECK(nullptr != Damage);

	Speed = Cast<UTextBlock>(GetWidgetFromName(TEXT("Txt_Speed")));
	JICHECK(nullptr != Speed);

	Kinds = Cast<UTextBlock>(GetWidgetFromName(TEXT("Txt_Kinds")));
	JICHECK(nullptr != Kinds);

	for (int i = 0; i < SHIP_NUM; ++i)
	{
		FString str = "Btn";
		FString strCombine = str + (FString::FromInt(i));
		Btn.Add(Cast<UButton>(GetWidgetFromName(FName(*strCombine))));
	}
	for (int i = 0; i < SHIP_NUM; i++)
	{
		Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(NullTex, 199, 201));
	}
	Btn[0]->OnReleased.AddDynamic(this, &UJIHUDWidget::OnShipButtonReleased0);
	Btn[1]->OnReleased.AddDynamic(this, &UJIHUDWidget::OnShipButtonReleased1);
	Btn[2]->OnReleased.AddDynamic(this, &UJIHUDWidget::OnShipButtonReleased2);
	Btn[3]->OnReleased.AddDynamic(this, &UJIHUDWidget::OnShipButtonReleased3);
	Btn[4]->OnReleased.AddDynamic(this, &UJIHUDWidget::OnShipButtonReleased4);
	Btn[5]->OnReleased.AddDynamic(this, &UJIHUDWidget::OnShipButtonReleased5);
	Btn[6]->OnReleased.AddDynamic(this, &UJIHUDWidget::OnShipButtonReleased6);
	Btn[7]->OnReleased.AddDynamic(this, &UJIHUDWidget::OnShipButtonReleased7);
	Btn[8]->OnReleased.AddDynamic(this, &UJIHUDWidget::OnShipButtonReleased8);
	Btn[9]->OnReleased.AddDynamic(this, &UJIHUDWidget::OnShipButtonReleased9);
	Btn[10]->OnReleased.AddDynamic(this, &UJIHUDWidget::OnShipButtonReleased10);
	Btn[11]->OnReleased.AddDynamic(this, &UJIHUDWidget::OnShipButtonReleased11);
}


void UJIHUDWidget::OnMoveButtonClicked()
{
	MoveButton->WidgetStyle = MakeButtonStyle(MakeBrush(MovePressedTex, 415, 415));
}

void UJIHUDWidget::OnMoveButtonReleased()
{
	MoveButton->WidgetStyle = MakeButtonStyle(MakeBrush(MoveNoramlTex, 415, 415));
}

void UJIHUDWidget::OnStopButtonClicked()
{
	StopButton->WidgetStyle = MakeButtonStyle(MakeBrush(StopPressedTex, 415, 415));
}

void UJIHUDWidget::OnStopButtonReleased()
{
	StopButton->WidgetStyle = MakeButtonStyle(MakeBrush(StopNoramlTex, 415, 415));
}

void UJIHUDWidget::OnAttackButtonClicked()
{
	AttackButton->WidgetStyle = MakeButtonStyle(MakeBrush(AttackPressedTex, 415, 415));
}

void UJIHUDWidget::OnAttackButtonReleased()
{
	AttackButton->WidgetStyle = MakeButtonStyle(MakeBrush(AttackNoramlTex, 415, 415));
}

void UJIHUDWidget::OnZoomButtonClicked()
{
	ZoomButton->WidgetStyle = MakeButtonStyle(MakeBrush(ZoomPressedTex, 415, 415));
}
void UJIHUDWidget::OnZoomButtonReleased()
{
	ZoomButton->WidgetStyle = MakeButtonStyle(MakeBrush(ZoomNoramlTex, 415, 415));
}

void UJIHUDWidget::OnOptionButtonClicked()
{
	OptionButton->WidgetStyle = MakeButtonStyle(MakeBrush(OptionPressedTex, 415, 415));
}
void UJIHUDWidget::OnOptionButtonReleased()
{
	OptionButton->WidgetStyle = MakeButtonStyle(MakeBrush(OptionNoramlTex, 415, 415));
}

void UJIHUDWidget::OnReloadButtonClicked()
{
	ReloadButton->WidgetStyle = MakeButtonStyle(MakeBrush(ReloadPressedTex, 415, 415));
}
void UJIHUDWidget::OnReloadButtonReleased()
{
	ReloadButton->WidgetStyle = MakeButtonStyle(MakeBrush(ReloadNoramlTex, 415, 415));
}

void UJIHUDWidget::OnOceanButtonClicked()
{
	OceanButton->WidgetStyle = MakeButtonStyle(MakeBrush(OceanPressedTex, 415, 415));
}
void UJIHUDWidget::OnOceanButtonReleased()
{
	OceanButton->WidgetStyle = MakeButtonStyle(MakeBrush(OceanNoramlTex, 415, 415));
}

void UJIHUDWidget::OnMapButtonClicked()
{
	MapButton->WidgetStyle = MakeButtonStyle(MakeBrush(MapPressedTex, 415, 415));
}
void UJIHUDWidget::OnMapButtonReleased()
{
	MapButton->WidgetStyle = MakeButtonStyle(MakeBrush(MapNoramlTex, 415, 415));
}


void UJIHUDWidget::SetSelectedShipImage(TArray<AJIShip*> &arr)
{
	WS_HUD->SetActiveWidgetIndex(1);
	SelectedShip.Empty();
	for (int i = 0; i < SHIP_NUM; ++i)
		Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(NullTex, 199, 201));
	int j = 0;
	for (auto& Actor : arr) {
		if (Actor) 
			SelectedShip.AddUnique(Actor);
	}
	if (SelectedShip.Num() == 1)
		ClickOnShip();
	else
	{
		if (SelectedShip.Num() > SHIP_NUM)
		{
			for (int i = 0; i < SHIP_NUM; ++i)
			{
				if (SelectedShip[i]->m_eTeamColor==TEAM_COLOR::JOSEON_1|| SelectedShip[i]->m_eTeamColor == TEAM_COLOR::JOSEON_2)
				{

					if (SelectedShip[i]->AssetIndex == PANOK)
					{
						if (SelectedShip[i]->ShipStat->CurrentHP > SelectedShip[i]->ShipStat->GetMaxHP() * 0.7f) {
							Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(PanokTexGreen, 500, 333));
						}
						else if (SelectedShip[i]->ShipStat->CurrentHP <= SelectedShip[i]->ShipStat->GetMaxHP() * 0.7f && SelectedShip[i]->ShipStat->CurrentHP > SelectedShip[i]->ShipStat->GetMaxHP() * 0.3f) {
							Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(PanokTexYellow, 500, 333));
						}
						else if (SelectedShip[i]->ShipStat->CurrentHP <= SelectedShip[i]->ShipStat->GetMaxHP() * 0.3f) {
							Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(PanokTexRed, 500, 333));
						}
					}
					else if (SelectedShip[i]->AssetIndex == TURTLE)
					{
						if (SelectedShip[i]->ShipStat->CurrentHP > SelectedShip[i]->ShipStat->GetMaxHP() * 0.7f) {
							Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(TurtleTexGreen, 500, 333));
						}
						else if (SelectedShip[i]->ShipStat->CurrentHP <= SelectedShip[i]->ShipStat->GetMaxHP() * 0.7f && SelectedShip[i]->ShipStat->CurrentHP > SelectedShip[i]->ShipStat->GetMaxHP() * 0.3f) {
							Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(TurtleTexYellow, 500, 333));
						}
						else if (SelectedShip[i]->ShipStat->CurrentHP <= SelectedShip[i]->ShipStat->GetMaxHP() * 0.3f) {
							Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(TurtleTexRed, 500, 333));
						}
					}
				}
				else if (SelectedShip[i]->m_eTeamColor == TEAM_COLOR::JAPAN_1 || SelectedShip[i]->m_eTeamColor == TEAM_COLOR::JAPAN_2)
				{
					if (SelectedShip[i]->ShipStat->CurrentHP > SelectedShip[i]->ShipStat->GetMaxHP() * 0.7f) {
						Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(AntakeTexGreen, 500, 333));
					}
					else if (SelectedShip[i]->ShipStat->CurrentHP <= SelectedShip[i]->ShipStat->GetMaxHP() * 0.7f && SelectedShip[i]->ShipStat->CurrentHP > SelectedShip[i]->ShipStat->GetMaxHP() * 0.3f) {
						Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(AntakeTexYellow, 500, 333));
					}
					else if (SelectedShip[i]->ShipStat->CurrentHP <= SelectedShip[i]->ShipStat->GetMaxHP() * 0.3f) {
						Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(AntakeTexRed, 500, 333));
					}
				}
			}
		}
		else
		{
			for (int i = 0; i < SelectedShip.Num(); ++i)
			{
				if (SelectedShip[i]->m_eTeamColor == TEAM_COLOR::JOSEON_1 || SelectedShip[i]->m_eTeamColor == TEAM_COLOR::JOSEON_2)
				{
					if (SelectedShip[i]->AssetIndex == PANOK)
					{
						if (SelectedShip[i]->ShipStat->CurrentHP > SelectedShip[i]->ShipStat->GetMaxHP() * 0.7f) {
							Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(PanokTexGreen, 500, 333));
						}
						else if (SelectedShip[i]->ShipStat->CurrentHP <= SelectedShip[i]->ShipStat->GetMaxHP() * 0.7f && SelectedShip[i]->ShipStat->CurrentHP > SelectedShip[i]->ShipStat->GetMaxHP() * 0.3f) {
							Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(PanokTexYellow, 500, 333));
						}
						else if (SelectedShip[i]->ShipStat->CurrentHP <= SelectedShip[i]->ShipStat->GetMaxHP() * 0.3f) {
							Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(PanokTexRed, 500, 333));
						}
					}
					else if (SelectedShip[i]->AssetIndex == TURTLE)
					{
						if (SelectedShip[i]->ShipStat->CurrentHP > SelectedShip[i]->ShipStat->GetMaxHP() * 0.7f) {
							Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(TurtleTexGreen, 500, 333));
						}
						else if (SelectedShip[i]->ShipStat->CurrentHP <= SelectedShip[i]->ShipStat->GetMaxHP() * 0.7f && SelectedShip[i]->ShipStat->CurrentHP > SelectedShip[i]->ShipStat->GetMaxHP() * 0.3f) {
							Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(TurtleTexYellow, 500, 333));
						}
						else if (SelectedShip[i]->ShipStat->CurrentHP <= SelectedShip[i]->ShipStat->GetMaxHP() * 0.3f) {
							Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(TurtleTexRed, 500, 333));
						}
					}
				}
				else if (SelectedShip[i]->m_eTeamColor == TEAM_COLOR::JAPAN_1 || SelectedShip[i]->m_eTeamColor == TEAM_COLOR::JAPAN_2)
				{
					if (SelectedShip[i]->ShipStat->CurrentHP > SelectedShip[i]->ShipStat->GetMaxHP() * 0.7f) {
						Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(AntakeTexGreen, 500, 333));
					}
					else if (SelectedShip[i]->ShipStat->CurrentHP <= SelectedShip[i]->ShipStat->GetMaxHP() * 0.7f && SelectedShip[i]->ShipStat->CurrentHP > SelectedShip[i]->ShipStat->GetMaxHP() * 0.3f) {
						Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(AntakeTexYellow, 500, 333));
					}
					else if (SelectedShip[i]->ShipStat->CurrentHP <= SelectedShip[i]->ShipStat->GetMaxHP() * 0.3f) {
						Btn[i]->WidgetStyle = MakeButtonStyle(MakeBrush(AntakeTexRed, 500, 333));
					}
				}
			}
		}
	}
}

FButtonStyle UJIHUDWidget::MakeButtonStyle(const FSlateBrush brush)
{
	FButtonStyle style;
	style.SetNormal(brush);
	style.SetPressed(brush);
	style.SetHovered(brush);
	return style;
}

FSlateBrush UJIHUDWidget::MakeBrush(UTexture2D* inputTexture, int x, int y)
{
	FSlateBrush itemBrush = UWidgetBlueprintLibrary::MakeBrushFromTexture(inputTexture);
	itemBrush.ImageSize.X = 199;
	itemBrush.ImageSize.Y = 201;
	itemBrush.DrawAs = ESlateBrushDrawType::Image;
	return itemBrush;
}

UWidgetAnimation* UJIHUDWidget::GetAnimationByName(FName AnimationName) const
{
	UWidgetAnimation* const* WidgetAnim = AnimationsMap.Find(AnimationName);
	if (WidgetAnim)
		return *WidgetAnim;
	return nullptr;
}
bool UJIHUDWidget::PlayAnimationByName(FName AnimationName, float StartAtTime, int32 NumLoopsToPlay, EUMGSequencePlayMode::Type PlayMode, float PlaybackSpeed)
{// 위젯 애니메이션 실행
	UWidgetAnimation* WidgetAnim = GetAnimationByName(AnimationName);
	if (WidgetAnim)
		PlayAnimation(WidgetAnim, StartAtTime, NumLoopsToPlay, PlayMode, PlaybackSpeed);
		return true;
	return false;
}
void UJIHUDWidget::FillAnimationMap()
{// 애니메이션 맵 비우기
	AnimationsMap.Empty();

	// 해당 클래스의 모든 속성 중 위젯 애니메이션 찾기
	UProperty* Prop = GetClass()->PropertyLink;
	while (Prop != nullptr)
	{
		// 객체 특성이라면
		if (Prop->GetClass() == UObjectProperty::StaticClass())
		{
			// 위젯 애니메이션 속성이라면
			UObjectProperty* ObjProp = Cast<UObjectProperty>(Prop);
			if (ObjProp->PropertyClass == UWidgetAnimation::StaticClass())
			{
				// 블루프린트에서 만든 변수 가져오기
				UObject* Obj = ObjProp->GetObjectPropertyValue_InContainer(this);

				// 오브젝트가 위젯 애니메이션이고 무비 씬이 존재한다면
				UWidgetAnimation* WidgetAnim = Cast<UWidgetAnimation>(Obj);
				if (WidgetAnim != nullptr && WidgetAnim->MovieScene != nullptr)
				{
					// 애니메이션 맵에 추가
					FName AnimName = WidgetAnim->MovieScene->GetFName();
					AnimationsMap.Add(AnimName, WidgetAnim);
				}
			}
		}
		Prop = Prop->PropertyLinkNext;
	}
}

void UJIHUDWidget::ClickOnShip()
{
	if (SelectedShip.Num() <= 0)
		return;
	WS_HUD->SetActiveWidgetIndex(0);
	if (SelectedShip[0]->m_eTeamColor == TEAM_COLOR::JOSEON_1 || SelectedShip[0]->m_eTeamColor == TEAM_COLOR::JOSEON_2)
	{
		if (SelectedShip[0]->AssetIndex == PANOK)
		{
			if (SelectedShip[0]->ShipStat->CurrentHP > SelectedShip[0]->ShipStat->GetMaxHP() * 0.7f)
				Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(PanokTexGreen, 500, 333));
			else if (SelectedShip[0]->ShipStat->CurrentHP <= SelectedShip[0]->ShipStat->GetMaxHP() * 0.7f && SelectedShip[0]->ShipStat->CurrentHP > SelectedShip[0]->ShipStat->GetMaxHP() * 0.3f)
				Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(PanokTexYellow, 500, 333));
			else if (SelectedShip[0]->ShipStat->CurrentHP <= SelectedShip[0]->ShipStat->GetMaxHP() * 0.3f)
				Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(PanokTexRed, 500, 333));
		}
		else if (SelectedShip[0]->AssetIndex == TURTLE)
		{
			if (SelectedShip[0]->ShipStat->CurrentHP > SelectedShip[0]->ShipStat->GetMaxHP() * 0.7f)
				Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(TurtleTexGreen, 500, 333));
			else if (SelectedShip[0]->ShipStat->CurrentHP <= SelectedShip[0]->ShipStat->GetMaxHP() * 0.7f && SelectedShip[0]->ShipStat->CurrentHP > SelectedShip[0]->ShipStat->GetMaxHP() * 0.3f)
				Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(TurtleTexYellow, 500, 333));
			else if (SelectedShip[0]->ShipStat->CurrentHP <= SelectedShip[0]->ShipStat->GetMaxHP() * 0.3f)
				Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(TurtleTexRed, 500, 333));
		}
	}
	else if (SelectedShip[0]->m_eTeamColor == TEAM_COLOR::JAPAN_1 || SelectedShip[0]->m_eTeamColor == TEAM_COLOR::JAPAN_2)
	{
		if (SelectedShip[0]->ShipStat->CurrentHP > SelectedShip[0]->ShipStat->GetMaxHP() * 0.7f)
			Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(AntakeTexGreen, 500, 333));
		else if (SelectedShip[0]->ShipStat->CurrentHP <= SelectedShip[0]->ShipStat->GetMaxHP() * 0.7f && SelectedShip[0]->ShipStat->CurrentHP > SelectedShip[0]->ShipStat->GetMaxHP() * 0.3f)
			Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(AntakeTexYellow, 500, 333));
		else if (SelectedShip[0]->ShipStat->CurrentHP <= SelectedShip[0]->ShipStat->GetMaxHP() * 0.3f)
			Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(AntakeTexRed, 500, 333));
	}
	SetShipStat(SelectedShip[0]);
}

void UJIHUDWidget::OnShipButtonReleased0()
{
	SetMakeButtonStyle(0);
}

void UJIHUDWidget::OnShipButtonReleased1()
{
	SetMakeButtonStyle(1);
}

void UJIHUDWidget::OnShipButtonReleased2()
{
	SetMakeButtonStyle(2);
}

void UJIHUDWidget::OnShipButtonReleased3()
{
	SetMakeButtonStyle(3);
}

void UJIHUDWidget::OnShipButtonReleased4()
{
	SetMakeButtonStyle(4);
}

void UJIHUDWidget::OnShipButtonReleased5()
{
	SetMakeButtonStyle(5);
}

void UJIHUDWidget::OnShipButtonReleased6()
{
	SetMakeButtonStyle(6);
}

void UJIHUDWidget::OnShipButtonReleased7()
{
	SetMakeButtonStyle(7);
}

void UJIHUDWidget::OnShipButtonReleased8()
{
	SetMakeButtonStyle(8);
}

void UJIHUDWidget::OnShipButtonReleased9()
{
	SetMakeButtonStyle(9);
}

void UJIHUDWidget::OnShipButtonReleased10()
{
	SetMakeButtonStyle(10);
}

void UJIHUDWidget::OnShipButtonReleased11()
{
	SetMakeButtonStyle(11);
}


void UJIHUDWidget::SetMakeButtonStyle(int index)
{
	if (SelectedShip.Num() <= index)
		return;
	ClickAudioComponent->Play(0.f);
	WS_HUD->SetActiveWidgetIndex(0);
	if (SelectedShip[index]->m_eTeamColor == TEAM_COLOR::JOSEON_1 || SelectedShip[index]->m_eTeamColor == TEAM_COLOR::JOSEON_2)
	{
		if (SelectedShip[index]->AssetIndex == PANOK)
		{
			if (SelectedShip[index]->ShipStat->CurrentHP > SelectedShip[index]->ShipStat->GetMaxHP() * 0.7f)
				Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(PanokTexGreen, 500, 333));
			else if (SelectedShip[index]->ShipStat->CurrentHP <= SelectedShip[index]->ShipStat->GetMaxHP() * 0.7f && SelectedShip[index]->ShipStat->CurrentHP > SelectedShip[index]->ShipStat->GetMaxHP() * 0.3f)
				Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(PanokTexYellow, 500, 333));
			else if (SelectedShip[index]->ShipStat->CurrentHP <= SelectedShip[index]->ShipStat->GetMaxHP() * 0.3f)
				Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(PanokTexRed, 500, 333));
		}
		else if (SelectedShip[index]->AssetIndex == TURTLE)
		{
			if (SelectedShip[index]->ShipStat->CurrentHP > 70)
				Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(TurtleTexGreen, 500, 333));
			else if (SelectedShip[index]->ShipStat->CurrentHP <= 70 && SelectedShip[index]->ShipStat->CurrentHP > SelectedShip[index]->ShipStat->GetMaxHP() * 0.3f)
				Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(TurtleTexYellow, 500, 333));
			else if (SelectedShip[index]->ShipStat->CurrentHP <= SelectedShip[index]->ShipStat->GetMaxHP() * 0.3f)
				Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(TurtleTexRed, 500, 333));
		}
	}
	else if (SelectedShip[index]->m_eTeamColor == TEAM_COLOR::JAPAN_1 || SelectedShip[index]->m_eTeamColor == TEAM_COLOR::JAPAN_2)
	{
		if (SelectedShip[index]->ShipStat->CurrentHP > SelectedShip[index]->ShipStat->GetMaxHP() * 0.7f)
			Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(AntakeTexGreen, 500, 333));
		else if (SelectedShip[index]->ShipStat->CurrentHP <= SelectedShip[index]->ShipStat->GetMaxHP() * 0.7f && SelectedShip[index]->ShipStat->CurrentHP > SelectedShip[index]->ShipStat->GetMaxHP() * 0.3f)
			Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(AntakeTexYellow, 500, 333));
		else if (SelectedShip[index]->ShipStat->CurrentHP <= SelectedShip[index]->ShipStat->GetMaxHP() * 0.3f)
			Img_Button->WidgetStyle = MakeButtonStyle(MakeBrush(AntakeTexRed, 500, 333));
	}
	SetShipStat(SelectedShip[index]);
}


void UJIHUDWidget::SetShipStat(AJIShip* ship)
{
	CurrentHP->SetText(FText::FromString(FString::FromInt(ship->ShipStat->GetCurrentHP())));
	MaxHP->SetText(FText::FromString(FString::FromInt(ship->ShipStat->GetMaxHP())));

	ResidualShell->SetText(FText::FromString(FString::FromInt(ship->ShipStat->GetResidualShell())));
	MaxShell->SetText(FText::FromString(FString::FromInt(ship->ShipStat->GetMaxShell())));

	Damage->SetText(FText::FromString(FString::FromInt(ship->ShipStat->GetDamage())));
	Speed->SetText(FText::FromString(FString::FromInt(ship->m_fShipSpeed)));



	if (ship->AssetIndex==PANOK)
	{
		FString name = TEXT("판옥선");
		Kinds->SetText(FText::FromString(name));
	}
	else if (ship->AssetIndex == TURTLE)
	{
		FString name = TEXT("거북선");
		Kinds->SetText(FText::FromString(name));
	}
	else if (ship->AssetIndex == ANTAKE)
	{
		FString name = TEXT("안택선");
		Kinds->SetText(FText::FromString(name));
	}
}