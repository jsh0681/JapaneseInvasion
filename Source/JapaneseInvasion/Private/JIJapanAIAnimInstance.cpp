#include "JIJapanAIAnimInstance.h"

UJIJapanAIAnimInstance::UJIJapanAIAnimInstance()
{
	CurrentPawnSpeed = 0.0f;
	IsInAir = false;
	IsDead = false;

	static ConstructorHelpers::FObjectFinder<UAnimMontage> ATTACK_MONTAGE(TEXT("/Game/Custom/Japan/Soldier/Montage/Montage_JapanSoldier_Attack"));
	if (ATTACK_MONTAGE.Succeeded())
	{
		AttackMontage = ATTACK_MONTAGE.Object;
	}
	static ConstructorHelpers::FObjectFinder<UAnimMontage> RUN_MONTAGE(TEXT("/Game/Custom/Japan/Soldier/Montage/Montage_JapanSoldier_Run"));
	if (RUN_MONTAGE.Succeeded())
	{
		RunMontage = RUN_MONTAGE.Object;
	}

	static ConstructorHelpers::FObjectFinder<UAnimMontage> IDLE_MONTAGE(TEXT("/Game/Custom/Japan/Soldier/Montage/Montage_JapanSoldier_Idle"));
	if (IDLE_MONTAGE.Succeeded())
	{
		IdleMontage = IDLE_MONTAGE.Object;
	}
}

void UJIJapanAIAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	auto Pawn = TryGetPawnOwner();
	if (!::IsValid(Pawn))
		return;

	if (!IsDead)
	{
		CurrentPawnSpeed = Pawn->GetVelocity().Size();
		auto Character = Cast<ACharacter>(Pawn);
		if (Character)
		{
			IsInAir = Character->GetMovementComponent()->IsFalling();
		}
	}
}



void UJIJapanAIAnimInstance::PlayAttackMontage()
{
	JICHECK(!IsDead);
	if (!Montage_IsPlaying(AttackMontage))
		Montage_Play(AttackMontage, 1.0f);
}

void UJIJapanAIAnimInstance::PlayRunMontage()
{
	JICHECK(!IsDead);
	//if (!Montage_IsPlaying(RunMontage))
	//{
	Montage_Play(RunMontage, 1.0f);
	//}
}

void UJIJapanAIAnimInstance::PlayIdleMontage()
{
	JICHECK(!IsDead);
	//if (!Montage_IsPlaying(IdleMontage))
	//{
	Montage_Play(IdleMontage, 1.0f);
	//}
}


void UJIJapanAIAnimInstance::AnimNotify_AttackHitCheck()
{

	OnAttackHitCheck.Broadcast();
}

void UJIJapanAIAnimInstance::AnimNotify_NextAttackCheck()
{
	OnNextAttackCheck.Broadcast();
}

FName UJIJapanAIAnimInstance::GetAttackMontageSectionName(int32 Section)
{
	JICHECK(FMath::IsWithinInclusive<int32>(Section, 1, 3), NAME_None);
	return FName(*FString::Printf(TEXT("Attack%d"), Section));
}

void UJIJapanAIAnimInstance::JumpToAttackMontageSection(int32 NewSection)
{
	JICHECK(!IsDead);
	//JICHECK(Montage_IsPlaying(AttackMontage));
	Montage_JumpToSection(GetAttackMontageSectionName(NewSection), AttackMontage);
}

