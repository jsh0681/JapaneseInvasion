#include "JIJapanAIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"

const FName AJIJapanAIController::HomePosKey(TEXT("HomePos"));
const FName AJIJapanAIController::PatrolPosKey(TEXT("PatrolPos"));
const FName AJIJapanAIController::TargetKey(TEXT("Target"));

AJIJapanAIController::AJIJapanAIController()
{
	static ConstructorHelpers::FObjectFinder<UBlackboardData>  BBObject(TEXT("/Game/Custom/AI/BB_JapanSoldier"));
	if (BBObject.Succeeded())
	{
		BBAsset = BBObject.Object;
	}
	static ConstructorHelpers::FObjectFinder<UBehaviorTree>  BTObject(TEXT("/Game/Custom/AI/BT_JapanSoldier"));
	if (BTObject.Succeeded())
	{
		BTAsset = BTObject.Object;
	}
}

void AJIJapanAIController::OnPossess(APawn* pawn)
{
	Super::OnPossess(pawn);
	if (UseBlackboard(BBAsset, Blackboard))
	{
		Blackboard->SetValueAsVector(HomePosKey, pawn->GetActorLocation());
		if (!RunBehaviorTree(BTAsset))
		{
			JILOG(Error, TEXT("AIController Couldn't run behavior tree!"));
		}
	}
}
