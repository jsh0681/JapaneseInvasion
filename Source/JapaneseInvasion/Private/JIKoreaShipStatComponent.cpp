#include "JIKoreaShipStatComponent.h"
#include "JIGameInstance.h"

UJIKoreaShipStatComponent::UJIKoreaShipStatComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
	Level = 1;
}

void UJIKoreaShipStatComponent::BeginPlay()
{
	Super::BeginPlay();
	CurrentReload = 30;
}

void UJIKoreaShipStatComponent::SetReloadBullet()
{
	CurrentReload = CurrentStatData->Reload;
}

void UJIKoreaShipStatComponent::InitializeComponent()
{
	Super::InitializeComponent();
	SetNewLevel(Level);
}

void UJIKoreaShipStatComponent::SetNewLevel(int32 NewLevel)
{
	auto JIGameInstance = Cast<UJIGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	JICHECK(JIGameInstance != nullptr);
	CurrentStatData = JIGameInstance->GetJIKoreaShipData(NewLevel);
	if (nullptr != CurrentStatData)
	{
		Level = NewLevel;
		SetHP(CurrentStatData->MaxHP);
	}
	else
		JILOG(Error, TEXT("Level (%d) data doesn't exist"), NewLevel);
}

float UJIKoreaShipStatComponent::GetAttack()
{
	JICHECK(CurrentStatData != nullptr, 0.0f);
	return CurrentStatData->Attack;
}

float UJIKoreaShipStatComponent::GetHPRatio()
{
	JICHECK(nullptr != CurrentStatData, 0.0f);
	return (CurrentStatData->MaxHP < KINDA_SMALL_NUMBER ? 0.0f : (CurrentHP / CurrentStatData->MaxHP));
}

float UJIKoreaShipStatComponent::GetCurrentHP()
{
	return CurrentHP;
}
float UJIKoreaShipStatComponent::GetMaxHP()
{
	return CurrentStatData->MaxHP;
}
float UJIKoreaShipStatComponent::GetDamage()
{
	return CurrentStatData->Attack;
}
float UJIKoreaShipStatComponent::GetResidualShell()
{
	return CurrentReload;
}
float UJIKoreaShipStatComponent::GetMaxShell()
{
	return CurrentStatData->Reload;
}

float UJIKoreaShipStatComponent::GetReload()
{
	JICHECK(nullptr != CurrentStatData, 0.0f);
	return (CurrentStatData->Reload < KINDA_SMALL_NUMBER ? 0.0f : (CurrentReload / CurrentStatData->Reload));

}

void UJIKoreaShipStatComponent::SetDamage(float NewDamage)
{
	JICHECK(CurrentStatData != nullptr);
	SetHP(FMath::Clamp<float>(CurrentHP - NewDamage, 0.0f, CurrentStatData->MaxHP));
}

void UJIKoreaShipStatComponent::SetHP(float NewHP)
{
	CurrentHP = NewHP;
	OnHPChanged.Broadcast();
	if (CurrentHP < KINDA_SMALL_NUMBER)
	{
		CurrentHP = 0.0f;
		OnHPIsZero.Broadcast();
	}
}

void UJIKoreaShipStatComponent::SetReload(float RemainTime)
{
	CurrentReload = RemainTime;
	OnReloadOn.Broadcast();
	//if (CurrentReload < KINDA_SMALL_NUMBER)
	//{
	//	CurrentReload  = 0;
		//OnHPIsZero.Broadcast();
	//}
}

void UJIKoreaShipStatComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}