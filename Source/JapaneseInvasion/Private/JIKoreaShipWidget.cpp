#include "JIKoreaShipWidget.h"
#include "JIKoreaShipStatComponent.h"
#include "Components//ProgressBar.h"

void UJIKoreaShipWidget::BindKoreaShipStat(UJIKoreaShipStatComponent* NewKoreaShipStat)
{
	JICHECK(nullptr != NewKoreaShipStat);
	CurrentKoreaShipStat = NewKoreaShipStat;
	NewKoreaShipStat->OnHPChanged.AddUObject(this, &UJIKoreaShipWidget::UpdateHPWidget);
	NewKoreaShipStat->OnReloadOn.AddUObject(this, &UJIKoreaShipWidget::UpdateReloadWidget);
}

void UJIKoreaShipWidget::NativeConstruct()
{
	Super::NativeConstruct();
	HPProgressBar = Cast<UProgressBar>(GetWidgetFromName(TEXT("PB_HPBar")));
	JICHECK(HPProgressBar != nullptr);

	ReloadProgressBar = Cast<UProgressBar>(GetWidgetFromName(TEXT("PB_RELoadBar")));
	JICHECK(ReloadProgressBar != nullptr);

	UpdateHPWidget();
	UpdateReloadWidget();
}

void UJIKoreaShipWidget::UpdateHPWidget()
{
	if (CurrentKoreaShipStat.IsValid())
	{
		if (HPProgressBar != nullptr)
			HPProgressBar->SetPercent(CurrentKoreaShipStat->GetHPRatio());
	}
}

void UJIKoreaShipWidget::UpdateReloadWidget()
{
	if (CurrentKoreaShipStat.IsValid())
	{
		if (ReloadProgressBar != nullptr)
			ReloadProgressBar->SetPercent(CurrentKoreaShipStat->GetReload());
	}
}