#include "JILance.h"

AJILance::AJILance()
{
	PrimaryActorTick.bCanEverTick = false;

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_LANCE(TEXT("/Game/Custom/StaticMesh/Weapon/Lance/SK_Lance"));
	if (SK_LANCE.Succeeded())
	{
		Weapon->SetSkeletalMesh(SK_LANCE.Object);
	}
	Weapon->SetCollisionProfileName(TEXT("NoCollision"));
	Weapon->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
}

void AJILance::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void AJILance::BeginPlay()
{
	Super::BeginPlay();

	//Weapon->SetAnimationMode(EAnimationMode::AnimationSingleNode);
	/*AnimAsset = LoadObject<UAnimationAsset>(nullptr, TEXT("/Game/Custom/StaticMesh/Weapon/Sword/SK_Sword_Anim"));
	if (AnimAsset != nullptr)
		Weapon->PlayAnimation(AnimAsset, true);*/

}

void AJILance::PlayAnimForTrail()
{
	//Weapon->PlayAnimation(AnimAsset, true);
}