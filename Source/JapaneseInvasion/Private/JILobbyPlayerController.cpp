#include "JILobbyPlayerController.h"
#include "JILobbyWidget.h"
#include "JILobbySelectWidget.h"
#include "JILobbySelectWidget.h"
#include "Blueprint/UserWidget.h"
#include "PacketMgr.h"

AJILobbyPlayerController::AJILobbyPlayerController()
{
	static ConstructorHelpers::FClassFinder<UJILobbySelectWidget> UI_LOBBYSELECT(TEXT("/Game/Custom/UI/UI_SelectLobby"));
	if (UI_LOBBYSELECT.Succeeded())
		LobbySelectWidgetClass = UI_LOBBYSELECT.Class;

	static ConstructorHelpers::FClassFinder<UJILobbyWidget> UI_LOBBY(TEXT("/Game/Custom/UI/UI_Lobby"));
	if (UI_LOBBY.Succeeded())
		LobbyWidgetClass = UI_LOBBY.Class;

	LobbyBGMAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("LobbyBGMComponent"));
	static ConstructorHelpers::FObjectFinder<USoundCue> SC_LobbyBGM(TEXT("/Game/Custom/Sound/MyeongRyangBGM_Cue"));
	if (SC_LobbyBGM.Succeeded())
		LobbyBGMSoundCue = SC_LobbyBGM.Object;

	HoveredAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("HoveredAudioComponent"));
	static ConstructorHelpers::FObjectFinder<USoundCue> SC_Hovered(TEXT("/Game/Custom/Sound/sound222/Hovered_Cue"));
	if (SC_Hovered.Succeeded())
		HoveredSoundCue = SC_Hovered.Object;
}


void AJILobbyPlayerController::BeginPlay()
{
	Super::BeginPlay();

	PacketMgr::GetInst()->SetWorld(GetWorld());

	LobbySelectWidget  = CreateWidget<UJILobbySelectWidget>(this, LobbySelectWidgetClass);
	JICHECK(LobbySelectWidget != nullptr);

	LobbyWidget = CreateWidget<UJILobbyWidget>(this, LobbyWidgetClass);
	JICHECK(LobbyWidget != nullptr);

	if (HoveredAudioComponent && HoveredSoundCue)
		HoveredAudioComponent->SetSound(HoveredSoundCue);

	LobbySelectWidget->SetAudioComponent(HoveredAudioComponent);
	LobbySelectWidget->SetLobbyWidget(LobbyWidget);
	LobbySelectWidget->AddToViewport();

	FInputModeUIOnly Mode;
	Mode.SetWidgetToFocus(LobbyWidget->GetCachedWidget());
	SetInputMode(Mode);
	bShowMouseCursor = true;

	if (LobbyBGMAudioComponent && LobbyBGMSoundCue)
	{
		LobbyBGMAudioComponent->SetSound(LobbyBGMSoundCue);
		LobbyBGMAudioComponent->Play(10.f);
	}
}

const int buf_size = 255;
void AJILobbyPlayerController::Tick(float DeltaTime)
{
	char buf[buf_size];
	int32 received = 0;
	uint32 datasize;
	g_Socket->HasPendingData(datasize);

	auto result = g_Socket->Recv(reinterpret_cast<uint8*>(buf), buf_size, received);

	if (received > 0)
	{
		PacketMgr::GetInst()->process_data(buf, received);
	}
}