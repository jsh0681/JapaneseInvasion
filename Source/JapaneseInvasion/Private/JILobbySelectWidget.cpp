#include "JILobbySelectWidget.h"
#include "Components/Button.h"
#include "JILobbyWidget.h"
#include "PacketMgr.h"

bool g_bLogin;

void UJILobbySelectWidget::NativeConstruct()
{
	Super::NativeConstruct();

	btnLobbyFirst = Cast<UButton>(GetWidgetFromName(TEXT("btn_LobbyFirst")));
	if (btnLobbyFirst != nullptr)
		btnLobbyFirst->OnClicked.AddDynamic(this, &UJILobbySelectWidget::OnBtnLobbyFirstClicked);

	btnLobbySecond = Cast<UButton>(GetWidgetFromName(TEXT("btn_LobbySecond")));
	if (btnLobbySecond != nullptr)
		btnLobbySecond->OnClicked.AddDynamic(this, &UJILobbySelectWidget::OnBtnLobbySecondClicked);

	btnLobbyThird = Cast<UButton>(GetWidgetFromName(TEXT("btn_LobbyThird")));
	if (btnLobbyThird != nullptr)
		btnLobbyThird->OnClicked.AddDynamic(this, &UJILobbySelectWidget::OnBtnLobbyThirdClicked);

	btnLobbyFourth = Cast<UButton>(GetWidgetFromName(TEXT("btn_LobbyFourth")));
	if (btnLobbyFourth != nullptr)
		btnLobbyFourth->OnClicked.AddDynamic(this, &UJILobbySelectWidget::OnBtnLobbyFourthClicked);
}

void UJILobbySelectWidget::OnBtnLobbyFirstClicked()
{
	//타이틀 위젯에서 반복을 막기 위해 사용한 변수를 다시 true로 해준다.
	g_bLogin = true;

	//JILOG(Warning, TEXT("OnBtnLobbyClicked"));
	LobbyWidget->SetAudioComponent(HoveredAudioComponent);
	LobbyWidget->AddToViewport();

	cs_packet_select_lobby packet;
	packet.size = sizeof(cs_packet_select_lobby);
	packet.type = C2S_SELECT_LOBBY;
	packet.lobbynum = 0; // 0~6 선택할 수 있게
	PacketMgr::GetInst()->send_packet(&packet);
}

void UJILobbySelectWidget::OnBtnLobbySecondClicked()
{
	//타이틀 위젯에서 반복을 막기 위해 사용한 변수를 다시 true로 해준다.
	g_bLogin = true;

	//JILOG(Warning, TEXT("OnBtnLobbySecondClicked"));
	LobbyWidget->SetAudioComponent(HoveredAudioComponent);
	LobbyWidget->AddToViewport();

	cs_packet_select_lobby packet;
	packet.size = sizeof(cs_packet_select_lobby);
	packet.type = C2S_SELECT_LOBBY;
	packet.lobbynum = 1; // 0~6 선택할 수 있게
	PacketMgr::GetInst()->send_packet(&packet);
}

void UJILobbySelectWidget::OnBtnLobbyThirdClicked()
{
	//타이틀 위젯에서 반복을 막기 위해 사용한 변수를 다시 true로 해준다.
	g_bLogin = true;

	//JILOG(Warning, TEXT("OnBtnLobbyThirdClicked"));
	LobbyWidget->SetAudioComponent(HoveredAudioComponent);
	LobbyWidget->AddToViewport();

	cs_packet_select_lobby packet;
	packet.size = sizeof(cs_packet_select_lobby);
	packet.type = C2S_SELECT_LOBBY;
	packet.lobbynum = 2; // 0~6 선택할 수 있게
	PacketMgr::GetInst()->send_packet(&packet);
}

void UJILobbySelectWidget::OnBtnLobbyFourthClicked()
{
	//타이틀 위젯에서 반복을 막기 위해 사용한 변수를 다시 true로 해준다.
	g_bLogin = true;

	//JILOG(Warning, TEXT("OnBtnLobbyFourthClicked"));
	LobbyWidget->SetAudioComponent(HoveredAudioComponent);
	LobbyWidget->AddToViewport();

	cs_packet_select_lobby packet;
	packet.size = sizeof(cs_packet_select_lobby);
	packet.type = C2S_SELECT_LOBBY;
	packet.lobbynum = 3; // 0~6 선택할 수 있게
	PacketMgr::GetInst()->send_packet(&packet);
}

void UJILobbySelectWidget::SetAudioComponent(UAudioComponent* Audio)
{
	HoveredAudioComponent = Audio;
}

void UJILobbySelectWidget::SetLobbyWidget(UJILobbyWidget* widget)
{
	LobbyWidget = widget;
}