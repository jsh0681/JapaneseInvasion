#include "JILobbyWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h" 
#include "Blueprint/WidgetBlueprintLibrary.h"

#include "Sockets.h"
#include "SocketSubsystem.h"
#include "Networking/Public/Interfaces/IPv4/IPv4Address.h"
#include "protocol.h"
#include "PacketMgr.h"

extern FString CharacterName;
extern TCHAR* UserID;
#define USER_NUM 4
#define TEAM_JOSEON_NUM 2
#define TEAM_JAPAN_NUM 2


UJILobbyWidget::UJILobbyWidget(const FObjectInitializer& ObjectInitializer)
    : UUserWidget(ObjectInitializer)
{
    static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_JOSEON_FLAG(TEXT("/Game/Custom/Image/JoseonFlag"));
    if (TEX_JOSEON_FLAG.Succeeded())
        JoseonFlagTex = TEX_JOSEON_FLAG.Object;

    static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_JAPAN_FLAG(TEXT("Texture2D'/Game/Custom/Image/japanflag.JapanFlag'"));
    if (TEX_JAPAN_FLAG.Succeeded())
        JapanFlagTex = TEX_JAPAN_FLAG.Object;

    static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_EMPTY(TEXT("/Game/Custom/Image/UI/image_null"));
    if (TEX_EMPTY.Succeeded())
        NullTexture = TEX_EMPTY.Object;

    static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_CROWN(TEXT("/Game/Custom/Image/Crown"));
    if (TEX_CROWN.Succeeded())
        CrownTex = TEX_CROWN.Object;

    static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_NULLBACK(TEXT("/Game/Custom/Image/BackNull"));
    if (TEX_NULLBACK.Succeeded())
        nullBack = TEX_NULLBACK.Object;


    PacketMgr::GetInst()->SetLobbyWidget(this);
}

void UJILobbyWidget::NativeConstruct()
{
    Super::NativeConstruct();

    btnJoseon = Cast<UButton>(GetWidgetFromName(TEXT("btnJoseon2")));
    if (btnJoseon != nullptr)
    {
        btnJoseon->OnHovered.AddDynamic(this, &UJILobbyWidget::OnBtnJoseonHovered);
        btnJoseon->OnClicked.AddDynamic(this, &UJILobbyWidget::OnBtnJoseonClicked);
    }

    btnJapan = Cast<UButton>(GetWidgetFromName(TEXT("btnJapan2")));
    if (btnJapan != nullptr)
    {
        btnJapan->OnHovered.AddDynamic(this, &UJILobbyWidget::OnBtnJapanHovered);
        btnJapan->OnClicked.AddDynamic(this, &UJILobbyWidget::OnBtnJapanClicked);
    }

    btnGameStart = Cast<UButton>(GetWidgetFromName(TEXT("btnGameStart2")));
    if (btnGameStart != nullptr)
    {
        btnGameStart->OnHovered.AddDynamic(this, &UJILobbyWidget::OnBtnGameStartHovered);
        btnGameStart->OnClicked.AddDynamic(this, &UJILobbyWidget::OnBtnGameStartClicked);
    }

    btnBack = Cast<UButton>(GetWidgetFromName(TEXT("btnBack2")));
    if (btnBack != nullptr)
    {
        btnBack->OnHovered.AddDynamic(this, &UJILobbyWidget::OnBtnBackHovered);
        btnBack->OnClicked.AddDynamic(this, &UJILobbyWidget::OnBtnBackClicked);
    }
    for (int i = 1; i <= USER_NUM; ++i)
    {
        FString str = "Txt_ID";
        FString strCombine = str + (FString::FromInt(i));
        CurrentUserEnterTxtBlock.Add(Cast<UTextBlock>(GetWidgetFromName(FName(*strCombine))));
    }

    TeamJoseonImg.Add(Cast<UImage>(GetWidgetFromName(FName("ImgTeamJoseon1"))));
    TeamJoseonImg.Add(Cast<UImage>(GetWidgetFromName(FName("ImgTeamJoseon2"))));
    TeamJapanImg.Add(Cast<UImage>(GetWidgetFromName(FName("ImgTeamJapan1"))));
    TeamJapanImg.Add(Cast<UImage>(GetWidgetFromName(FName("ImgTeamJapan2"))));

    TeamJoseonTxtBlock.Add(Cast<UTextBlock>(GetWidgetFromName(FName("TxtTeamJoseon1"))));
    TeamJoseonTxtBlock.Add(Cast<UTextBlock>(GetWidgetFromName(FName("TxtTeamJoseon2"))));
    TeamJapanTxtBlock.Add(Cast<UTextBlock>(GetWidgetFromName(FName("TxtTeamJapan1"))));
    TeamJapanTxtBlock.Add(Cast<UTextBlock>(GetWidgetFromName(FName("TxtTeamJapan2"))));

    CrownImg.Add(Cast<UImage>(GetWidgetFromName(FName("Img_Crown1"))));
    CrownImg.Add(Cast<UImage>(GetWidgetFromName(FName("Img_Crown2"))));
    CrownImg.Add(Cast<UImage>(GetWidgetFromName(FName("Img_Crown3"))));
    CrownImg.Add(Cast<UImage>(GetWidgetFromName(FName("Img_Crown4"))));
}

void UJILobbyWidget::SetAudioComponent(UAudioComponent* Audio)
{
    HoveredAudioComponent = Audio;
}

void UJILobbyWidget::OnBtnJoseonClicked()
{
    //JILOG(Warning, TEXT("OnBtnJoseonClicked"));
    cs_packet_select_team packet;
    packet.size = sizeof(cs_packet_select_team);
    packet.type = C2S_SELECT_TEAM;

    if (joseon_team_num == 0)
        packet.team = (char)TEAM_COLOR::JOSEON_1;
    else if (joseon_team_num >= 1)
        packet.team = (char)TEAM_COLOR::JOSEON_2;

    PacketMgr::GetInst()->send_packet(&packet);
}

void UJILobbyWidget::OnBtnJapanClicked()
{
   // JILOG(Warning, TEXT("OnBtnJapanClicked"));
    cs_packet_select_team packet;
    packet.size = sizeof(cs_packet_select_team);
    packet.type = C2S_SELECT_TEAM;

    if (japan_team_num == 0)
        packet.team = (char)TEAM_COLOR::JAPAN_1;
    else if (japan_team_num >= 1)
        packet.team = (char)TEAM_COLOR::JAPAN_2;

    PacketMgr::GetInst()->send_packet(&packet);
}

void UJILobbyWidget::OnBtnGameStartClicked()
{
    cs_packet_start_game packet;
    packet.size = sizeof(cs_packet_start_game);
    packet.type = C2S_START_GAME;

    PacketMgr::GetInst()->send_packet(&packet);
}

void UJILobbyWidget::OnBtnBackClicked()
{
    //JILOG(Warning, TEXT("OnBtnBackClicked"));
    UGameplayStatics::SetGamePaused(GetWorld(), true);
    UGameplayStatics::OpenLevel(GetWorld(), "Lobby", true);

    cs_packet_quit_lobby packet;
    packet.type = C2S_QUIT_LOBBY;
    packet.size = sizeof(cs_packet_quit_lobby);

    PacketMgr::GetInst()->send_packet(&packet);

}

void UJILobbyWidget::OnBtnJoseonHovered()
{
    HoveredAudioComponent->Play(0.f);
}

void UJILobbyWidget::OnBtnJapanHovered()
{
    HoveredAudioComponent->Play(0.f);
}

void UJILobbyWidget::OnBtnGameStartHovered()
{
    HoveredAudioComponent->Play(0.f);
}

void UJILobbyWidget::OnBtnBackHovered()
{
    HoveredAudioComponent->Play(0.f);
}

FSlateBrush UJILobbyWidget::MakeBrush(UTexture2D* inputTexture, int x, int y)
{
    FSlateBrush itemBrush = UWidgetBlueprintLibrary::MakeBrushFromTexture(inputTexture);
    itemBrush.ImageSize.X = x;
    itemBrush.ImageSize.Y = y;
    itemBrush.DrawAs = ESlateBrushDrawType::Image;
    return itemBrush;
}