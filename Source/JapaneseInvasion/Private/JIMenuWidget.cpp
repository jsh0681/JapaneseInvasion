#include "JIMenuWidget.h"
#include "Components/Button.h"

#include "PacketMgr.h"
#include "protocol.h"


void UJIMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();

	OnBtnResume = Cast<UButton>(GetWidgetFromName(TEXT("btnResume")));
	if (OnBtnResume != nullptr)
		OnBtnResume->OnClicked.AddDynamic(this, &UJIMenuWidget::OnBtnResumeClicked);

	OnBtnReturnToTitle = Cast<UButton>(GetWidgetFromName(TEXT("btnReturnToTitle")));
	if (OnBtnReturnToTitle != nullptr)
		OnBtnReturnToTitle->OnClicked.AddDynamic(this, &UJIMenuWidget::OnBtnReturnToTitleClicked);
}

void UJIMenuWidget::OnBtnReturnToTitleClicked()
{
	cs_packet_exit packet;
	packet.size = sizeof(cs_packet_exit);
	packet.type = C2S_EXIT;
	PacketMgr::GetInst()->send_packet(&packet);
}

void UJIMenuWidget::OnBtnResumeClicked()
{
	cs_packet_resume packet;
	packet.size = sizeof(cs_packet_resume);
	packet.type = C2S_RESUME;
	PacketMgr::GetInst()->send_packet(&packet);
}


void UJIMenuWidget::Resume()
{
	auto JIPlayerController = Cast<AJIPlayerController>(GetOwningPlayer());
	JICHECK(nullptr != JIPlayerController);

	RemoveFromParent();
	JIPlayerController->ChangeInputMode(true);
	JIPlayerController->SetPause(false);
}