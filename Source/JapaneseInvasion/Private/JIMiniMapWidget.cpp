#include "JIMiniMapWidget.h"
#include "Components/Image.h"
#include "Components/Button.h"
#include <Engine/Texture2D.h>	
#include "Blueprint/SlateBlueprintLibrary.h"
#include"JIPlayerController.h"

UJIMiniMapWidget::UJIMiniMapWidget(const FObjectInitializer& ObjectInitializer)
	: UUserWidget(ObjectInitializer)
{
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	static ConstructorHelpers::FObjectFinder<USoundCue> SC_Animation(TEXT("/Game/Custom/Sound/sound222/Animation_Cue"));
	if (SC_Animation.Succeeded())
		SoundCue = SC_Animation.Object;
}

void UJIMiniMapWidget::NativePreConstruct()
{
	Super::NativePreConstruct(); 
	if (AudioComponent && SoundCue)
		AudioComponent->SetSound(SoundCue);
}

void UJIMiniMapWidget::NativeConstruct()
{
	Super::NativeConstruct();
	FillAnimationMap();
	bIsOn = false;

	MinimapButton = Cast<UButton>(GetWidgetFromName(TEXT("btnMinimap")));
	JICHECK(nullptr != MinimapButton);
	MinimapButton->OnClicked.AddDynamic(this, &UJIMiniMapWidget::OnMapClicked);
	MinimapImage = Cast<UImage>(GetWidgetFromName(TEXT("UI_MinimapImage")));
}

void UJIMiniMapWidget::SetAudioComponent(UAudioComponent* Audio)
{
	AudioComponent = Audio;
}

void UJIMiniMapWidget::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseEnter(InGeometry, InMouseEvent);
	if (!bIsOn)
	{
		//JILOG(Warning, TEXT("NativeOnMouseEnter"));
		//화면 위치를 픽셀 단위로 지정된 형상이있는 위젯의 로컬 공간으로 변환합니다.
		FVector2D ScreenPos;
		USlateBlueprintLibrary::ScreenToWidgetLocal(this, InGeometry, InMouseEvent.GetScreenSpacePosition(), ScreenPos);
		//AbsoluteCoordinate를이 Geometry의 로컬 공간으로 변환합니다.
		WidgetClickPos = USlateBlueprintLibrary::AbsoluteToLocal(InGeometry, InMouseEvent.GetScreenSpacePosition());
		FVector2D WorldClickPos;
		if (WidgetClickPos.X > 1635 && WidgetClickPos.Y > 800 && WidgetClickPos.X < 1905 && WidgetClickPos.Y < 1070)
		{
			//JILOG(Warning, TEXT("%f , %f"), WidgetClickPos.X, WidgetClickPos.Y);
			WorldClickPos.X = WidgetClickPos.X - 1635;
			WorldClickPos.Y = WidgetClickPos.Y - 800;
			WorldClickPos.X = WorldClickPos.X * 2356.5f;
			WorldClickPos.Y = WorldClickPos.Y * 2356.5f;
			WorldClickPos.X -= 160000.f;
			WorldClickPos.Y -= 300000.f;
			AJIPlayerController* controller = Cast<AJIPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
			controller->MinimapClick(WorldClickPos);
		}
	}
}
UWidgetAnimation* UJIMiniMapWidget::GetAnimationByName(FName AnimationName) const
{
	UWidgetAnimation* const* WidgetAnim = AnimationsMap.Find(AnimationName);
	if (WidgetAnim)
		return *WidgetAnim;
	return nullptr;
}

void UJIMiniMapWidget::OnMapClicked()
{
	AudioComponent->Play(0.f);
	if (!bIsOn)
	{
		bIsOn = true;
		PlayAnimationByName(FName("CallBack"));
	}
	else
	{
		bIsOn = false;
		PlayAnimationByName(FName("CallUp"));
	}
}

bool UJIMiniMapWidget::PlayAnimationByName(FName AnimationName, float StartAtTime, int32 NumLoopsToPlay, EUMGSequencePlayMode::Type PlayMode, float PlaybackSpeed)
{
	UWidgetAnimation* WidgetAnim = GetAnimationByName(AnimationName);
	if (WidgetAnim)
	{
		PlayAnimation(WidgetAnim, StartAtTime, NumLoopsToPlay, PlayMode, PlaybackSpeed);
		return true;
	}
	return false;
}

void UJIMiniMapWidget::FillAnimationMap()
{
	AnimationsMap.Empty();
	UProperty* Prop = GetClass()->PropertyLink;
	while (Prop != nullptr)
	{
		if (Prop->GetClass() == UObjectProperty::StaticClass())
		{
			UObjectProperty* ObjProp = Cast<UObjectProperty>(Prop);
			if (ObjProp->PropertyClass == UWidgetAnimation::StaticClass())
			{
				UObject* Obj = ObjProp->GetObjectPropertyValue_InContainer(this);
				UWidgetAnimation* WidgetAnim = Cast<UWidgetAnimation>(Obj);
				if (WidgetAnim != nullptr && WidgetAnim->MovieScene != nullptr)
				{
					FName AnimName = WidgetAnim->MovieScene->GetFName();
					AnimationsMap.Add(AnimName, WidgetAnim);
				}
			}
		}
		Prop = Prop->PropertyLinkNext;
	}
}