#include "JIMinimapPawn.h"
#include "GameFramework/Pawn.h"

AJIMinimapPawn::AJIMinimapPawn()
{
	PrimaryActorTick.bCanEverTick = false;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("MESH");
	MiniMapSpringArm = CreateDefaultSubobject<USpringArmComponent>("MINIMAPSPRINGARM");
	Minimap = CreateDefaultSubobject<USceneCaptureComponent2D>("MINIMAP");

	static ConstructorHelpers::FObjectFinder<UStaticMesh> MESH(TEXT("/Game/StarterContent/Shapes/Shape_Sphere"));
	if (MESH.Succeeded())
		Mesh->SetStaticMesh(MESH.Object);
	RootComponent = Mesh;
	Mesh->SetCollisionProfileName(TEXT("NoCollision"));

	MiniMapSpringArm->SetupAttachment(RootComponent);
	MiniMapSpringArm->TargetArmLength = 60000.f;
	Minimap->SetupAttachment(MiniMapSpringArm);

	static ConstructorHelpers::FObjectFinder<UTextureRenderTarget2D> RT_MINIMAP(TEXT("/Game/Custom/Minimap/Minimap_RenderTarget"));
	if (RT_MINIMAP.Succeeded())
		Minimap->TextureTarget = RT_MINIMAP.Object;
}

void AJIMinimapPawn::BeginPlay()
{
	Super::BeginPlay();
}

void AJIMinimapPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}