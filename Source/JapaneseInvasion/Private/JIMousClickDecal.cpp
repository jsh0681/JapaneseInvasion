#include "JIMousClickDecal.h"

AJIMousClickDecal::AJIMousClickDecal()
{
	PrimaryActorTick.bCanEverTick = false;

	Click = CreateAbstractDefaultSubobject<UStaticMeshComponent>(TEXT("PLANE"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_CLICK(TEXT("/Game/Custom/StaticMesh/Plane"));
	if (ST_CLICK.Succeeded())
	{
		Click->SetStaticMesh(ST_CLICK.Object);
		Click->SetCollisionProfileName(TEXT("NoCollision"));
		Click->SetWorldScale3D(FVector(10.f, 10.f, 10.f));
		RootComponent = Click;
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> MT_CLICK(TEXT("/Game/Custom/Decal/M_ClickDecal"));
	if (MT_CLICK.Succeeded())
		Click->SetMaterial(0,MT_CLICK.Object);
}

void AJIMousClickDecal::BeginPlay()
{
	Super::BeginPlay();
	SetLifeSpan(1.f);
}
