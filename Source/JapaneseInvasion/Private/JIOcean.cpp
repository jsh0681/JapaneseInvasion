// Fill out your copyright notice in the Description page of Project Settings.


#include "JIOcean.h"
#include "Kismet/KismetArrayLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetMaterialLibrary.h"
#include "JIOceanWidget.h"

#define FPS 60
#define TIMER_INTERVAL 0.5f
AJIOcean::AJIOcean()
{
	PrimaryActorTick.bStartWithTickEnabled = true;
	//프레임당 호출을 하기 싫다면 여길 수정
	//SetActorTickInterval(0.1f);

	Icon = CreateDefaultSubobject<UBillboardComponent>(TEXT("ICON"));
	RootComponent = Icon;
	static ConstructorHelpers::FObjectFinder<UTexture2D> TEX_ICON(TEXT("/Game/OceanProject/Ocean/Textures/WaveIcon"));
	if (TEX_ICON.Succeeded())
	{
		Icon->SetSprite(TEX_ICON.Object);
		Icon->bIsScreenSizeScaled = true;
	}

	OceanPlane = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("OCEANPLANE"));
	OceanPlane->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_OCEANPLANE(TEXT("/Game/OceanProject/Ocean/Meshes/SM_OceanTop"));
	if (ST_OCEANPLANE.Succeeded())
	{
		OceanPlane->SetStaticMesh(ST_OCEANPLANE.Object);
	}


	DepthPlaneUnderside = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DEPTHPLANEUNDERSIDE"));
	DepthPlaneUnderside->SetupAttachment(OceanPlane);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_DEPTHPLANEUNDERSIDE(TEXT("/Game/OceanProject/Ocean/Meshes/SM_UnderWater2"));
	if (ST_DEPTHPLANEUNDERSIDE.Succeeded())
	{
		DepthPlaneUnderside->SetStaticMesh(ST_DEPTHPLANEUNDERSIDE.Object);
	}

	DepthPlaneTopside = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DEPTHPLANETOPSIDE"));
	DepthPlaneTopside->SetupAttachment(OceanPlane);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_DEPTHPLANETOPSIDE(TEXT("/Game/OceanProject/Ocean/Meshes/SM_OceanTop"));
	if (ST_DEPTHPLANETOPSIDE.Succeeded())
	{
		DepthPlaneTopside->SetStaticMesh(ST_DEPTHPLANETOPSIDE.Object);
	}


	InfiniteSystem = CreateDefaultSubobject<UInfiniteSystemComponent>(TEXT("INFINITESYSTEM"));
	InfiniteSystem->SetupAttachment(OceanPlane);

	PlanarReflection = CreateDefaultSubobject<UPlanarReflectionComponent>(TEXT("PLANARREFLECTION"));

	static ConstructorHelpers::FObjectFinder<UMaterialInstance> MIC_OCEANSHADER(TEXT("/Game/OceanProject/Ocean/Materials/M_Ocean_Versions/M_Inst_Ocean_Ultra"));
	if (MIC_OCEANSHADER.Succeeded())
	{
		OceanShader = MIC_OCEANSHADER.Object;
	}

	static ConstructorHelpers::FObjectFinder<UMaterialInstance> MIC_OCEANDEPTHSHADER(TEXT("/Game/OceanProject/Ocean/Materials/M_Ocean_Versions/M_Inst_Ocean_Depth_Ultra"));
	if (MIC_OCEANDEPTHSHADER.Succeeded())
	{
		OceanDepthShader = MIC_OCEANDEPTHSHADER.Object;
	}


	MPC_Ocean = CreateDefaultSubobject<UMaterialParameterCollection>(TEXT("MPC_OCEAN"));
	static ConstructorHelpers::FObjectFinder<UMaterialParameterCollection> MPC_OCEAN(TEXT("/Game/Custom/MPC_Ocean"));
	if (MPC_OCEAN.Succeeded())
	{
		MPC_Ocean = MPC_OCEAN.Object;
	}

	


	//Apperance
	BaseColorDark = FLinearColor(0.0f, 0.003f, 0.01f, 1.0f);
	BaseColorLight = FLinearColor(0.08f, 0.15f, 0.88f, 1.0f);
	ShallowWaterColor = FLinearColor(0.145f, 0.22f, 0.26f, 1.0f);
	BaseColorLerp = 0.85f;
	FresnelPower = 2.0f;
	BaseFresnelReflect = 0.4f; 
	Metallic = 0.05f;
	Roughness = 0.015f;
	Specular = 1.0f;
	TesselationMultiplier = 0.85f;
	Opacity = 1.0f;
	BaseDepthFade = 50.0f;
	DistortionStrength = 0.03f;
	SceneColorCustomDepth = 3000.0f;
	FoamScale = 900.0f;
	FoamDepth1 = 10.0f;
	FoamDepth2 = 110.0f;
	FoamTimeScale = 0.45f;
	FoamSoftness1 = 0.1f;
	SceneDepthSoftness = 0.15f;
	BaseDepthFadeSoftness = 0.5f;


	SSS_Color = FLinearColor(0.3f, 0.7f, 0.6f, 0.0f);
	SSS_Scale = 0.01f;
	SSS_Intensity = 0.4f;
	SSS_LightDepth = 300.0f;
	SSS_MacroNormalStrength = 0.6f;

	DetailNormalScale =2000.f;
	DetailNormalSpeed = 0.1f;
	DetailNormalStrength= 0.5f;
	MediumNormalScale= 10000.0f;
	MediumNormalSpeed=0.2f;
	MediumNormalStrength=0.4f;
	FarNormalScale= 100000.0f;
	FarNormalSpeed= 0.03f;
	FarNormalStrength= 0.4f;
	FarNormalBlendDistance= 20000.0f;
	FarNormalBlendFalloff= 20000.0f;



	PanWaveLerp= 0.875f;
	PanWaveIntensity = 0.225f;
	PanWaveTimeScale = 0.85f;
	PanWaveSize = 6800.0f;
	Panner01Speed = FVector(-0.015, -0.05,0.0);
	Panner02Speed= FVector(0.02,-0.045,0.0);
	Panner03Speed = FVector(-0.015, -0.085, 0.0f);
	MacroWaveScale = 1500.0f;
	MacroWaveSpeed = 1.0f;
	MacroWaveAmplify = 0.25f;


	FoamCapsOpacity = 0.5f;
	FoamCapsHeight = 120.0f;
	FoamCapsPower = 6.0f;


	SeafoamScale = 3000.0f;
	SeafoamSpeed = 0.2;
	SeafoamDistortion = 0.01f;
	SeafoamHeightPower = 7.5f;
	SeafoamHeightMultiply = 2500.0f;

	HeightmapDisplacement = 10000.0f;
	HeightmapScale = 0.3f;
	HeightmapSpeed = 30.0f;


	CubemapReflectionStrength = 0.3f;

	UpdateInEditor = true;
	FollowMethod = EFollowMethod::FollowCamera;
	GridSnapSize = 0.0f;
	MaxLookAtDistance = 20000.0f;
	ScaleByDistance = true;

	ScaleDistanceFactor = 1000.0f;
	ScaleStartDistance = 1.0f;
	ScaleMin = 1.0f;
	ScaleMax = 15.0f;

	InterpSpeed = 0.0001f;
	Time = 1;
	bCanPlus = false;
}


void AJIOcean::CreateWaveSet()
{
	if (WaveSetOffsetsOverride.Num() > 0)
	{
		SetParams = WaveSetOffsetsOverride[0];
	}
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("Set01Wave01"), UKismetMathLibrary::MakeColor(SetParams.Wave01.Rotation, SetParams.Wave01.Length, SetParams.Wave01.Amplitude, SetParams.Wave01.Steepness));
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("Set01Wave02"), UKismetMathLibrary::MakeColor(SetParams.Wave02.Rotation, SetParams.Wave02.Length, SetParams.Wave02.Amplitude, SetParams.Wave02.Steepness));
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("Set01Wave03"), UKismetMathLibrary::MakeColor(SetParams.Wave03.Rotation, SetParams.Wave03.Length, SetParams.Wave03.Amplitude, SetParams.Wave03.Steepness));
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("Set01Wave04"), UKismetMathLibrary::MakeColor(SetParams.Wave04.Rotation, SetParams.Wave04.Length, SetParams.Wave04.Amplitude, SetParams.Wave04.Steepness));
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("Set01Wave05"), UKismetMathLibrary::MakeColor(SetParams.Wave05.Rotation, SetParams.Wave05.Length, SetParams.Wave05.Amplitude, SetParams.Wave05.Steepness));
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("Set01Wave06"), UKismetMathLibrary::MakeColor(SetParams.Wave06.Rotation, SetParams.Wave06.Length, SetParams.Wave06.Amplitude, SetParams.Wave06.Steepness));
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("Set01Wave07"), UKismetMathLibrary::MakeColor(SetParams.Wave07.Rotation, SetParams.Wave07.Length, SetParams.Wave07.Amplitude, SetParams.Wave07.Steepness));
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("Set01Wave08"), UKismetMathLibrary::MakeColor(SetParams.Wave08.Rotation, SetParams.Wave08.Length, SetParams.Wave08.Amplitude, SetParams.Wave08.Steepness));
}

void AJIOcean::SetGlobalParameters()
{
	if (WaveClusters.Num() > 0)
	{
		FLinearColor LinearColor;
		LinearColor.R = GlobalWaveDirection.X;
		LinearColor.G = GlobalWaveDirection.Y;
		LinearColor.B = 0.0f;
		LinearColor.A = 0.0f;
		UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("WavesDirectionV2"), LinearColor);
		UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("WaveSetRotation"), WaveClusters[0].Rotation);
		UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("WaveSetLength"), WaveClusters[0].Length);
		UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("WaveSetAmplitude"), WaveClusters[0].Amplitude*GlobalWaveAmplitude);
		UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("WaveSetSteepness"), WaveClusters[0].Steepness);
		UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("WaveSetTimeMultiplier"), WaveClusters[0].TimeScale*GlobalWaveSpeed);;
	}
	if (UKismetSystemLibrary::IsValid(Landscape))
	{
		if (UKismetSystemLibrary::IsValid(HeightmapTexture))
		{
			FLinearColor LinearColor;
			LinearColor.R = HeightmapTexture->GetSizeX();
			LinearColor.G = HeightmapTexture->GetSizeY();
			LinearColor.B = 0.0f;
			LinearColor.A = 0.0f;

			UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("HeightMapSizeXY"), LinearColor);
			UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("ModulationStartHeight"), ModulationStartHeight);
			UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("ModulationMaxHeight"), ModulationMaxHeight);
			UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("ModulationPower"), ModulationPower);
			MIDOcean->SetTextureParameterValue(TEXT("LandHeightmap"), HeightmapTexture);
			MIDOceanDepth->SetTextureParameterValue(TEXT("LandHeightmap"), HeightmapTexture);
			
			
			LinearColor = UKismetMathLibrary::Conv_VectorToLinearColor(Landscape->GetActorLocation());
			UKismetMaterialLibrary::SetVectorParameterValue(this, MPC_Ocean, TEXT("LandScapeLoc"), LinearColor);
			
			LinearColor = UKismetMathLibrary::Conv_VectorToLinearColor(Landscape->GetActorScale3D());
			UKismetMaterialLibrary::SetVectorParameterValue(this, MPC_Ocean, TEXT("LandScapeScale"), LinearColor);
					
		}
	}
}
void AJIOcean::SetOceanState(float DeltaTime)
{

	if (Time < 144)
	{
		float WaveSpeedDifference = (JIGameInstance->GetOceanValue(Time)->WaveSpeed - JIGameInstance->GetOceanValue(Time + 1)->WaveSpeed) / (FPS*TIMER_INTERVAL);
		float WaveDirXDifference = (JIGameInstance->GetOceanValue(Time)->WaveDirectionX - JIGameInstance->GetOceanValue(Time + 1)->WaveDirectionX) / (FPS * TIMER_INTERVAL);
		float WaveDirYDifference = (JIGameInstance->GetOceanValue(Time)->WaveDirectionY - JIGameInstance->GetOceanValue(Time + 1)->WaveDirectionY) / (FPS * TIMER_INTERVAL);
		float WaveAmplitubeDifference = (JIGameInstance->GetOceanValue(Time)->WaveAmplitube - JIGameInstance->GetOceanValue(Time + 1)->WaveAmplitube) / (FPS * TIMER_INTERVAL);

		//JILOG(Warning, TEXT("Time: %d, Speed: %f, DirX: %f, DirY: %f"), Time / 6,GlobalWaveSpeed, GlobalWaveDirection.X, GlobalWaveDirection.Y);
		GlobalWaveSpeed -= WaveSpeedDifference;
		GlobalWaveDirection.X -= WaveDirXDifference;
		GlobalWaveDirection.Y -= WaveDirYDifference;
		GlobalWaveAmplitude -= WaveAmplitubeDifference;
	}
	else if (Time == 144)
	{
		float WaveSpeedDifference = (JIGameInstance->GetOceanValue(Time)->WaveSpeed - JIGameInstance->GetOceanValue(1)->WaveSpeed) / (FPS * TIMER_INTERVAL);
		float WaveDirXDifference = (JIGameInstance->GetOceanValue(Time)->WaveDirectionX - JIGameInstance->GetOceanValue(1)->WaveDirectionX) / (FPS * TIMER_INTERVAL);
		float WaveDirYDifference = (JIGameInstance->GetOceanValue(Time)->WaveDirectionY - JIGameInstance->GetOceanValue(1)->WaveDirectionY) / (FPS * TIMER_INTERVAL);
		float WaveAmplitubeDifference = (JIGameInstance->GetOceanValue(Time)->WaveAmplitube - JIGameInstance->GetOceanValue(1)->WaveAmplitube) / (FPS * TIMER_INTERVAL);
		//JILOG(Warning, TEXT("Time: %d, Speed: %f, DirX: %f, DirY: %f"), Time / 6, GlobalWaveSpeed, GlobalWaveDirection.X, GlobalWaveDirection.Y);
		GlobalWaveSpeed -= WaveSpeedDifference;
		GlobalWaveDirection.X -= WaveDirXDifference;
		GlobalWaveDirection.Y -= WaveDirYDifference;
		GlobalWaveAmplitude -= WaveAmplitubeDifference;

	}
	else
	{
		float WaveSpeedDifference = (JIGameInstance->GetOceanValue(Time)->WaveSpeed - JIGameInstance->GetOceanValue(Time + 1)->WaveSpeed) / (FPS * TIMER_INTERVAL);
		float WaveDirXDifference = (JIGameInstance->GetOceanValue(Time)->WaveDirectionX - JIGameInstance->GetOceanValue(Time + 1)->WaveDirectionX) / (FPS * TIMER_INTERVAL);
		float WaveDirYDifference = (JIGameInstance->GetOceanValue(Time)->WaveDirectionY - JIGameInstance->GetOceanValue(Time + 1)->WaveDirectionY) / (FPS * TIMER_INTERVAL);
		float WaveAmplitubeDifference = (JIGameInstance->GetOceanValue(Time)->WaveAmplitube - JIGameInstance->GetOceanValue(Time + 1)->WaveAmplitube) / (FPS * TIMER_INTERVAL);
		//JILOG(Warning, TEXT("Time: %d, Speed: %f, DirX: %f, DirY: %f"), Time/6, GlobalWaveSpeed, GlobalWaveDirection.X, GlobalWaveDirection.Y);
		GlobalWaveSpeed -= WaveSpeedDifference;
		GlobalWaveDirection.X -= WaveDirXDifference;
		GlobalWaveDirection.Y -= WaveDirYDifference;
		GlobalWaveAmplitude -= WaveAmplitubeDifference;
	}
}
void AJIOcean::PrintLog()
{
	//JILOG(Warning, TEXT("Time : %d"), Time);
	//JILOG(Warning, TEXT("WaveX:% f, WaveY : % f"), GlobalWaveDirection.X, GlobalWaveDirection.Y);

		/*JILOG(Warning, TEXT("Time : %d, WaveSpeed : %f, WaveX : %f,WaveY : %f, WaveAmplitube : %f "),
		Time,
		GlobalWaveSpeed,
		GlobalWaveDirection.X,
		GlobalWaveDirection.Y,
		GlobalWaveAmplitude);*/
}

void AJIOcean::SetDisplayParameters()
{
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("BaseColorDark"), BaseColorDark);
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("BaseColorLight"), BaseColorLight);
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("ShallowColor"), ShallowWaterColor);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("BaseColorLerp"), BaseColorLerp);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("BaseFresnelPower"), FresnelPower);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("BaseFresnelReflect"), BaseFresnelReflect);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("Metallic"), Metallic);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("Roughness"), Roughness);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("Specular"), Specular);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("TesselationMultiplier"), TesselationMultiplier);
	
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("Opacity"), Opacity);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("DepthFade"), BaseDepthFade);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("DistortionStrength"), DistortionStrength);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("SceneColorDepthFade"), SceneColorCustomDepth);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("FoamScale"), FoamScale);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("DepthTest1"), FoamDepth1);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("DepthTest2"), FoamDepth2);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("FoamTimeScale"), FoamTimeScale);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("FoamSoftness"), FoamSoftness1);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("SceneDepthSoftness"), SceneDepthSoftness);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("BaseDepthFadeSoftness"), BaseDepthFadeSoftness);
	
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("SSS_Color"), SSS_Color);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("SSS_Scale"), SSS_Scale);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("SSS_Intensity"), SSS_Intensity);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("SSS_LightDepth"), SSS_LightDepth);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("SSS_MacroNormalStrength"), SSS_MacroNormalStrength);
	
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("PanWaveLerp"), PanWaveLerp);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("PanWaveIntensity"), PanWaveIntensity);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("PanWaveTimeScale"), PanWaveTimeScale);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("PanWaveSize"), PanWaveSize);
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("PanWave01SpeedV2"), Panner01Speed);
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("PanWave01SpeedV2"), Panner02Speed);
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), MPC_Ocean, TEXT("PanWave01SpeedV2"), Panner03Speed);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("MacroScale"), MacroWaveScale);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("MacroSpeed"), MacroWaveSpeed);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("MacroAmplify"), MacroWaveAmplify);
	
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("DetailNormalScale"), DetailNormalScale);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("DetailNormalSpeed"), DetailNormalSpeed);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("DetailNormalStrength"), DetailNormalStrength);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("MediumNormalScale"), MediumNormalScale);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("MediumNormalSpeed"), MediumNormalSpeed);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("MediumNormalStrength"), MediumNormalStrength);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("FarNormalScale"), FarNormalScale);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("FarNormalSpeed"), FarNormalSpeed);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("FarNormalStrength"), FarNormalStrength);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("FarNormalBlendDistance"), FarNormalBlendDistance);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("FarNormalBlendFalloff"), FarNormalBlendFalloff);
	
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("HeightmapDisplacement"), HeightmapDisplacement);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("HeightmapScale"), HeightmapScale);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("HeightmapSpeed"), HeightmapSpeed);
	
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("SeafoamScale"), SeafoamScale);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("SeafoamSpeed"), SeafoamSpeed);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("SeafoamDistortion"), SeafoamDistortion);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("SeafoamHeightPower"), SeafoamHeightPower);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("SeafoamHeightMultiply"), SeafoamHeightMultiply);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("FoamCapsOpacity"), FoamCapsOpacity);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("FoamCapsHeight"), FoamCapsHeight);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("FoamCapsPower"), FoamCapsPower);
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), MPC_Ocean, TEXT("CubemapReflectionStrength"), CubemapReflectionStrength);
	
	
	MIDOcean->SetTextureParameterValue(TEXT("WaveNormal"), MediumWaveNormal);
	MIDOcean->SetTextureParameterValue(TEXT("SmallWaveNormal"), SmallWaveNormal);
	MIDOcean->SetTextureParameterValue(TEXT("FarWaveNormal"), FarWaveNormal);
	MIDOcean->SetTextureParameterValue(TEXT("LargeWaveHeight"), HeightmapWaves);
	MIDOcean->SetTextureParameterValue(TEXT("ShoreFoam"), ShoreFoam);
	MIDOcean->SetTextureParameterValue(TEXT("ShoreFoam2"), ShoreFoam2);
	MIDOcean->SetTextureParameterValue(TEXT("ShoreFoamRoughness"), ShoreFoamRoughness);
	MIDOcean->SetTextureParameterValue(TEXT("SeafoamTexture"), Seafoam);
	MIDOcean->SetTextureParameterValue(TEXT("ReflectionCubemap"), ReflectionCubemap);
	MIDOcean->SetTextureParameterValue(TEXT("ReflectionCubemap"), ReflectionCubemap);
	
	InfiniteSystem->UpdateInEditor = UpdateInEditor;
	InfiniteSystem->FollowMethod = FollowMethod;
	InfiniteSystem->GridSnapSize = GridSnapSize;
	InfiniteSystem->MaxLookAtDistance = MaxLookAtDistance;
	InfiniteSystem->ScaleByDistance = ScaleByDistance;
	InfiniteSystem->ScaleDistanceFactor = ScaleDistanceFactor;
	InfiniteSystem->ScaleStartDistance = ScaleStartDistance;
	InfiniteSystem->ScaleMin = ScaleMin;
	InfiniteSystem->ScaleMax = ScaleMax;
}
void AJIOcean::Timer()
{
	bCanPlus = true;
	GetWorldTimerManager().SetTimer(UnusedHandle, this, &AJIOcean::TimeAdd, TIMER_INTERVAL, false);
}

void AJIOcean::TimeAdd()
{
	if (Time < 144)
	{
		Time += 1;
		GlobalWaveSpeed = JIGameInstance->GetOceanValue(Time)->WaveSpeed;
		GlobalWaveDirection.X = JIGameInstance->GetOceanValue(Time)->WaveDirectionX;
		GlobalWaveDirection.Y = JIGameInstance->GetOceanValue(Time)->WaveDirectionY;
		GlobalWaveAmplitude = JIGameInstance->GetOceanValue(Time)->WaveAmplitube;
		//JILOG(Warning, TEXT("X : %f, Y : %f"), GlobalWaveDirection.X, GlobalWaveDirection.Y);

	}
	else
	{
		Time = 1;
		GlobalWaveSpeed = JIGameInstance->GetOceanValue(Time)->WaveSpeed;
		GlobalWaveDirection.X = JIGameInstance->GetOceanValue(Time)->WaveDirectionX;
		GlobalWaveDirection.Y = JIGameInstance->GetOceanValue(Time)->WaveDirectionY;
		GlobalWaveAmplitude = JIGameInstance->GetOceanValue(Time)->WaveAmplitube;
		//JILOG(Warning, TEXT("X : %f, Y : %f"), GlobalWaveDirection.X, GlobalWaveDirection.Y);
	}
	bCanPlus = false;
}

void AJIOcean::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SetGlobalParameters();

	if (!bCanPlus)
		Timer();

	SetOceanState(DeltaTime);
	//PrintLog(); 

	SetDisplayParameters();
}

AJIOcean* AJIOcean::GetOcean()
{
	return this;
}

void AJIOcean::BeginPlay()
{
	Super::BeginPlay();

	JIGameInstance = Cast<UJIGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	CreateWaveSet();
	SetGlobalParameters();
	SetDisplayParameters();

	GlobalWaveSpeed = JIGameInstance->GetOceanValue(Time)->WaveSpeed;
	GlobalWaveDirection.X = JIGameInstance->GetOceanValue(Time)->WaveDirectionX;
	GlobalWaveDirection.Y = JIGameInstance->GetOceanValue(Time)->WaveDirectionY;
	GlobalWaveAmplitude = JIGameInstance->GetOceanValue(Time)->WaveAmplitube;
}

void AJIOcean::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	MIDOcean = OceanPlane->CreateDynamicMaterialInstance(0, OceanShader);
	MIDOceanDepth = DepthPlaneTopside->CreateDynamicMaterialInstance(0, OceanDepthShader);
	
	DepthPlaneUnderside->SetMaterial(0, MIDOceanDepth);

	
	SetGlobalParameters();
	SetDisplayParameters();
	CreateWaveSet();
}