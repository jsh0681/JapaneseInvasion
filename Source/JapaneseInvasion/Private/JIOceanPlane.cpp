// Fill out your copyright notice in the Description page of Project Settings.


#include "JIOceanPlane.h"

// Sets default values
AJIOceanPlane::AJIOceanPlane()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	
	Plane = CreateAbstractDefaultSubobject<UStaticMeshComponent>(TEXT("PLANE"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_PLANE(TEXT("/Game/Custom/StaticMesh/OceanPlane"));
	if (ST_PLANE.Succeeded())
	{
		Plane->SetStaticMesh(ST_PLANE.Object);
		Plane->SetCollisionProfileName(TEXT("Ocean"));
		Plane->SetWorldScale3D(FVector(6500.0f, 6500.0f, 15.0f));
		Plane->SetVisibility(false);
		Plane->SetHiddenInGame(true);
		RootComponent = Plane;
	}

}

// Called when the game starts or when spawned
void AJIOceanPlane::BeginPlay()
{
	Super::BeginPlay();
}
