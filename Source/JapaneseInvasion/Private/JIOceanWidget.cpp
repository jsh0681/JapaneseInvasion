#include "JIOceanWidget.h"
#include "JIOcean.h"
#include "Components/Image.h"
#include "Components/Button.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

//��: 1, 0
//�� : -1.0
//�� : 0, 1
//�� : 0, -1
//�ϼ� : -1, -1
//�ϵ� : 1, -1
//���� : -1, 1
//���� : 1, 1

#define OCEAN_STATE 16
#define EAST 1
#define WEST -1
#define SOUTH 1
#define NORTH -1
#define NULL 0
#define IMAGE_SIZE 60

UJIOceanWidget::UJIOceanWidget(const FObjectInitializer& ObjectInitializer)
	: UUserWidget(ObjectInitializer)
{
	static ConstructorHelpers::FObjectFinder<UTexture2D> RED_EAST_TEX(TEXT("/Game/Custom/Image/Direction/Red_East"));
	if (RED_EAST_TEX.Succeeded())
		RedEastTex = RED_EAST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> RED_WEST_TEX(TEXT("/Game/Custom/Image/Direction/Red_West"));
	if (RED_WEST_TEX.Succeeded())
		RedWestTex = RED_WEST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> RED_SOUTH_TEX(TEXT("/Game/Custom/Image/Direction/Red_South"));
	if (RED_SOUTH_TEX.Succeeded())
		RedSouthTex = RED_SOUTH_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> RED_NORTH_TEX(TEXT("/Game/Custom/Image/Direction/Red_North"));
	if (RED_NORTH_TEX.Succeeded())
		RedNorthTex = RED_NORTH_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> ORANGE_EAST_TEX(TEXT("/Game/Custom/Image/Direction/Orange_East"));
	if (ORANGE_EAST_TEX.Succeeded())
		OrangeEastTex = ORANGE_EAST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> ORANGE_WEST_TEX(TEXT("/Game/Custom/Image/Direction/Orange_West"));
	if (ORANGE_WEST_TEX.Succeeded())
		OrangeWestTex = ORANGE_WEST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> ORANGE_SOUTH_TEX(TEXT("/Game/Custom/Image/Direction/Orange_South"));
	if (ORANGE_SOUTH_TEX.Succeeded())
		OrangeSouthTex = ORANGE_SOUTH_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> ORANGE_NORTH_TEX(TEXT("/Game/Custom/Image/Direction/Orange_North"));
	if (ORANGE_NORTH_TEX.Succeeded())
		OrangeNorthTex = ORANGE_NORTH_TEX.Object;


	static ConstructorHelpers::FObjectFinder<UTexture2D> YELLOW_EAST_TEX(TEXT("/Game/Custom/Image/Direction/Yellow_East"));
	if (YELLOW_EAST_TEX.Succeeded())
		YellowEastTex = YELLOW_EAST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> YELLOW_WEST_TEX(TEXT("/Game/Custom/Image/Direction/Yellow_West"));
	if (YELLOW_WEST_TEX.Succeeded())
		YellowWestTex = YELLOW_WEST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> YELLOW_SOUTH_TEX(TEXT("/Game/Custom/Image/Direction/Yellow_South"));
	if (YELLOW_SOUTH_TEX.Succeeded())
		YellowSouthTex = YELLOW_SOUTH_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> YELLOW_NORTH_TEX(TEXT("/Game/Custom/Image/Direction/Yellow_North"));
	if (YELLOW_NORTH_TEX.Succeeded())
		YellowNorthTex = YELLOW_NORTH_TEX.Object;




	static ConstructorHelpers::FObjectFinder<UTexture2D> RED_SEAST_TEX(TEXT("/Game/Custom/Image/Direction/Red_SEast"));
	if (RED_SEAST_TEX.Succeeded())
		RedSEastTex = RED_SEAST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> RED_SWEST_TEX(TEXT("/Game/Custom/Image/Direction/Red_SWest"));
	if (RED_SWEST_TEX.Succeeded())
		RedSWestTex = RED_SWEST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> RED_NEAST_TEX(TEXT("/Game/Custom/Image/Direction/Red_NEast"));
	if (RED_NEAST_TEX.Succeeded())
		RedNEastTex = RED_NEAST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> RED_NWEST_TEX(TEXT("/Game/Custom/Image/Direction/Red_NWest"));
	if (RED_NWEST_TEX.Succeeded())
		RedNWestTex = RED_NWEST_TEX.Object;


	static ConstructorHelpers::FObjectFinder<UTexture2D> ORANGE_SEAST_TEX(TEXT("/Game/Custom/Image/Direction/Orange_SEast"));
	if (ORANGE_SEAST_TEX.Succeeded())
		OrangeSEastTex = ORANGE_SEAST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> ORANGE_SWEST_TEX(TEXT("/Game/Custom/Image/Direction/Orange_SWest"));
	if (ORANGE_SWEST_TEX.Succeeded())
		OrangeSWestTex = ORANGE_SWEST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> ORANGE_NEAST_TEX(TEXT("/Game/Custom/Image/Direction/Orange_NEast"));
	if (ORANGE_NEAST_TEX.Succeeded())
		OrangeNEastTex = ORANGE_NEAST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> ORANGE_NWEST_TEX(TEXT("/Game/Custom/Image/Direction/Orange_NWest"));
	if (ORANGE_NWEST_TEX.Succeeded())
		OrangeNWestTex = ORANGE_NWEST_TEX.Object;


	static ConstructorHelpers::FObjectFinder<UTexture2D> YELLOW_SEAST_TEX(TEXT("/Game/Custom/Image/Direction/Yellow_SEast"));
	if (YELLOW_SEAST_TEX.Succeeded())
		YellowSEastTex = YELLOW_SEAST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> YELLOW_SWEST_TEX(TEXT("/Game/Custom/Image/Direction/Yellow_SWest"));
	if (YELLOW_SWEST_TEX.Succeeded())
		YellowSWestTex = YELLOW_SWEST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> YELLOW_NEAST_TEX(TEXT("/Game/Custom/Image/Direction/Yellow_NEast"));
	if (YELLOW_NEAST_TEX.Succeeded())
		YellowNEastTex = YELLOW_NEAST_TEX.Object;

	static ConstructorHelpers::FObjectFinder<UTexture2D> YELLOW_NWEST_TEX(TEXT("/Game/Custom/Image/Direction/Yellow_NWest"));
	if (YELLOW_NWEST_TEX.Succeeded())
		YellowNWestTex = YELLOW_NWEST_TEX.Object;

}
void UJIOceanWidget::NativeConstruct()
{
	Super::NativeConstruct();
	FillAnimationMap();
	bIsOn = false;

	OceanButton = Cast<UButton>(GetWidgetFromName(TEXT("btnOceanAnim")));
	JICHECK(nullptr != OceanButton);
	OceanButton->OnClicked.AddDynamic(this, &UJIOceanWidget::OnOceanClicked);

	for (int i = 1; i <= OCEAN_STATE; ++i)
	{
		FString str = "Img_Direction";
		FString strCombine = str + (FString::FromInt(i));
		OceanStateImg.Add(Cast<UImage>(GetWidgetFromName(FName(*strCombine))));
	}
}

UWidgetAnimation* UJIOceanWidget::GetAnimationByName(FName AnimationName) const
{
	UWidgetAnimation* const* WidgetAnim = AnimationsMap.Find(AnimationName);
	if (WidgetAnim)
		return *WidgetAnim;
	return nullptr;
}

void UJIOceanWidget::OnOceanClicked()
{
	//AudioComponent->Play(0.f);
	//JILOG(Warning, TEXT("OnOceanClicked"));
	if (bIsOn)
	{
		bIsOn = false;
		PlayAnimationByName(FName("OceanCallBack"));
	}
	else
	{
		bIsOn = true;
		PlayAnimationByName(FName("OceanCallUp"));
	}
}

bool UJIOceanWidget::PlayAnimationByName(FName AnimationName, float StartAtTime, int32 NumLoopsToPlay, EUMGSequencePlayMode::Type PlayMode, float PlaybackSpeed)
{
	UWidgetAnimation* WidgetAnim = GetAnimationByName(AnimationName);
	if (WidgetAnim)
	{
		PlayAnimation(WidgetAnim, StartAtTime, NumLoopsToPlay, PlayMode, PlaybackSpeed);
		return true;
	}
	return false;
}

void UJIOceanWidget::FillAnimationMap()
{
	AnimationsMap.Empty();
	UProperty* Prop = GetClass()->PropertyLink;
	while (Prop != nullptr)
	{
		if (Prop->GetClass() == UObjectProperty::StaticClass())
		{
			UObjectProperty* ObjProp = Cast<UObjectProperty>(Prop);
			if (ObjProp->PropertyClass == UWidgetAnimation::StaticClass())
			{
				UObject* Obj = ObjProp->GetObjectPropertyValue_InContainer(this);
				UWidgetAnimation* WidgetAnim = Cast<UWidgetAnimation>(Obj);
				if (WidgetAnim != nullptr && WidgetAnim->MovieScene != nullptr)
				{
					FName AnimName = WidgetAnim->MovieScene->GetFName();
					AnimationsMap.Add(AnimName, WidgetAnim);
				}
			}
		}
		Prop = Prop->PropertyLinkNext;
	}
}

void UJIOceanWidget::SetOceanData()
{
	JICHECK(OceanPtr != nullptr);
	if (OceanPtr->GlobalWaveSpeed < 5.0f)
	{
		if (OceanPtr->GlobalWaveDirection.X == EAST && OceanPtr->GlobalWaveDirection.Y == NULL)//��
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(YellowEastTex,IMAGE_SIZE,IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == WEST && OceanPtr->GlobalWaveDirection.Y == NULL)//��
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(YellowWestTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == NULL && OceanPtr->GlobalWaveDirection.Y == SOUTH)//��
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(YellowSouthTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == NULL && OceanPtr->GlobalWaveDirection.Y == NORTH)//��
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(YellowNorthTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == EAST && OceanPtr->GlobalWaveDirection.Y == NORTH)//�ϵ�
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(YellowNEastTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == WEST && OceanPtr->GlobalWaveDirection.Y == NORTH)//�ϼ�
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(YellowNWestTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == EAST && OceanPtr->GlobalWaveDirection.Y == SOUTH)//����
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(YellowSEastTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == WEST && OceanPtr->GlobalWaveDirection.Y == SOUTH)//����
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(YellowSWestTex, IMAGE_SIZE, IMAGE_SIZE));
		}
	}
	else if (OceanPtr->GlobalWaveSpeed < 10.0f)
	{
		if (OceanPtr->GlobalWaveDirection.X == EAST && OceanPtr->GlobalWaveDirection.Y == NULL)//��
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(OrangeEastTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == WEST && OceanPtr->GlobalWaveDirection.Y == NULL)//��
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(OrangeWestTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == NULL && OceanPtr->GlobalWaveDirection.Y == SOUTH)//��
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(OrangeSouthTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == NULL && OceanPtr->GlobalWaveDirection.Y == NORTH)//��
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(OrangeNorthTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == EAST && OceanPtr->GlobalWaveDirection.Y == NORTH)//�ϵ�
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(OrangeNEastTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == WEST && OceanPtr->GlobalWaveDirection.Y == NORTH)//�ϼ�
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(OrangeNWestTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == EAST && OceanPtr->GlobalWaveDirection.Y == SOUTH)//����
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(OrangeSEastTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == WEST && OceanPtr->GlobalWaveDirection.Y == SOUTH)//����
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(OrangeSWestTex, IMAGE_SIZE, IMAGE_SIZE));
		}
	}
	else
	{
		if (OceanPtr->GlobalWaveDirection.X == EAST && OceanPtr->GlobalWaveDirection.Y == NULL)//��
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(RedEastTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == WEST && OceanPtr->GlobalWaveDirection.Y == NULL)//��
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(RedWestTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == NULL && OceanPtr->GlobalWaveDirection.Y == SOUTH)//��
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(RedSouthTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == NULL && OceanPtr->GlobalWaveDirection.Y == NORTH)//��
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(RedNorthTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == EAST && OceanPtr->GlobalWaveDirection.Y == NORTH)//�ϵ�
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(RedNEastTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == WEST && OceanPtr->GlobalWaveDirection.Y == NORTH)//�ϼ�
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(RedNWestTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == EAST && OceanPtr->GlobalWaveDirection.Y == SOUTH)//����
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(RedSEastTex, IMAGE_SIZE, IMAGE_SIZE));
		}
		else if (OceanPtr->GlobalWaveDirection.X == WEST && OceanPtr->GlobalWaveDirection.Y == SOUTH)//����
		{
			for (int i = 0; i < OCEAN_STATE; i++)
				OceanStateImg[i]->SetBrush(MakeBrush(RedSWestTex, IMAGE_SIZE, IMAGE_SIZE));
		}
	}
}

void UJIOceanWidget::SetOceanPtr(AJIOcean* ocean)
{
	OceanPtr = ocean;
}

FSlateBrush UJIOceanWidget::MakeBrush(UTexture2D* inputTexture, int x, int y)
{
	FSlateBrush itemBrush = UWidgetBlueprintLibrary::MakeBrushFromTexture(inputTexture);
	itemBrush.ImageSize.X = x;
	itemBrush.ImageSize.Y = y;
	itemBrush.DrawAs = ESlateBrushDrawType::Image;
	return itemBrush;
}