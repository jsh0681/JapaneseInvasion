#include "JIPlayerController.h"
#include "JICharacter.h"
#include "JIAICharacter.h"
#include "JIFireDecal.h"
#include "Input/Events.h"
#include "JIMousClickDecal.h"
#include "DrawDebugHelpers.h"
#include "JIBomb.h"
#include "JIGeneral.h"
#include "JIKoreaShipStatComponent.h"
#include "JIOcean.h"
#include "JIHUDWidget.h"
#include "JIMenuWidget.h"
#include "JITitleWidget.h"
#include "JIMinimapWidget.h"
#include "JIVictoryWidget.h"
#include "JIDefeatWidget.h"
#include "Blueprint/UserWidget.h"
#include "JIOceanWidget.h"
#include "JIChattingWidget.h"
#include "JIGeneralHUDWidget.h"

//서버헤더
#include "Sockets.h"
#include "PacketMgr.h"
#include "PacketMove.h"
#include "PacketLogin.h"
#include "PacketEnter.h"
#include "protocol.h"
#include "Map.h"

TMap<int, AJIGeneral*> g_mapPlayer;
TMap<int, AJIShip*> g_mapShip;
TMap<int, AJIBomb*> g_mapShell;
const static int BUF_SIZE = 255;
#define  MINIMUM_ZOOM_IN_OFFSET  15000.f
#define  MAXIMUM_ZOOM_IN_OFFSET  30000.f
#define SCREEN_SCROOL_SPEED 2.5f

#define JOSEON_INTERVAL 3000.f
#define JAPAN_INTERVAL 4000.f

AJIPlayerController::AJIPlayerController()
{
    bAutoManageActiveCameraTarget = true;
    PacketMgr::GetInst()->SetWorld(GetWorld());

    static ConstructorHelpers::FClassFinder<UJIHUDWidget> UI_HUD_C(TEXT("/Game/Custom/UI/UI_HUD"));
    if (UI_HUD_C.Succeeded())
        HUDWidgetClass = UI_HUD_C.Class;

    static ConstructorHelpers::FClassFinder<UJIMenuWidget> UI_MENU(TEXT("/Game/Custom/UI/UI_Menu"));
    if (UI_MENU.Succeeded())
        MenuWidgetClass = UI_MENU.Class;

    static ConstructorHelpers::FClassFinder<UJIMiniMapWidget> UI_MINIMAP(TEXT("/Game/Custom/UI/UI_Minimap"));
    if (UI_MINIMAP.Succeeded())
        MiniMapWidgetClass = UI_MINIMAP.Class;

    static ConstructorHelpers::FClassFinder<UJIVictoryWidget> UI_VECTORY(TEXT("/Game/Custom/UI/UI_Victory"));
    if (UI_VECTORY.Succeeded())
        VictoryWidgetClass = UI_VECTORY.Class;

    static ConstructorHelpers::FClassFinder<UJIDefeatWidget> UI_DEFEAT(TEXT("/Game/Custom/UI/UI_Defeat"));
    if (UI_DEFEAT.Succeeded())
        DefeatWidgetClass = UI_DEFEAT.Class;

    static ConstructorHelpers::FClassFinder<UJIOceanWidget> UI_Ocean(TEXT("/Game/Custom/UI/UI_Ocean"));
    if (UI_Ocean.Succeeded())
        OceanWidgetClass = UI_Ocean.Class;

    static ConstructorHelpers::FClassFinder<UJIChattingWidget> UI_Chatting(TEXT("/Game/Custom/UI/UI_Chatting"));
    if (UI_Chatting.Succeeded())
        ChattingWidgetClass = UI_Chatting.Class;

    static ConstructorHelpers::FClassFinder<UJIGeneralHUDWidget> UI_GeneralState(TEXT("/Game/Custom/UI/UI_CharacterState"));
    if (UI_GeneralState.Succeeded())
        GeneralHUDWidgetClass = UI_GeneralState.Class;


    Cursor = CreateDefaultSubobject<UDecalComponent>("Cursor");
    static ConstructorHelpers::FObjectFinder<UMaterial> MT_SHOOTDECAL(TEXT("/Game/Custom/Decal/M_ShootDecal"));
    if (MT_SHOOTDECAL.Succeeded())
        Cursor->SetDecalMaterial(MT_SHOOTDECAL.Object);
    Cursor->DecalSize = FVector(128.f, 256.0f, 256.0f);
    Cursor->SetRelativeRotation(FRotator(-90.0f, 0.0f, 0.0f));
    Cursor->SetRelativeScale3D(FVector(30.0f, 30.0f, 30.0f));

    BGMAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("BGM"));
    static ConstructorHelpers::FObjectFinder<USoundCue> SC_BGM(TEXT("/Game/Custom/Sound/HansanBGM_Cue"));
    if (SC_BGM.Succeeded())
        HansanSoundCue = SC_BGM.Object;

    UIAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
    static ConstructorHelpers::FObjectFinder<USoundCue> SC_Animation(TEXT("/Game/Custom/Sound/sound222/Animation_Cue"));
    if (SC_Animation.Succeeded())
        UISoundCue = SC_Animation.Object;

    ClickAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("ClickAudioComponent"));
    static ConstructorHelpers::FObjectFinder<USoundCue> SC_ClickUI(TEXT("/Game/Custom/Sound/sound222/Click_Cue"));
    if (SC_ClickUI.Succeeded())
        ClickSoundCue = SC_ClickUI.Object;

    CurrentView = EViewMode::TOPVIEW;
    DefaultMouseCursor = EMouseCursor::Default;
    DefaultClickTraceChannel = ECollisionChannel::ECC_Visibility;
    bShowMouseCursor = true;
    bClickMouse = false;
    bOnVictoryWidget = false;
    bOnDefeatWidget = false;
    bMouseOnMinimap = false;
    interval = 3000.f;
    ScrollSpeed = 200.f;
}

void AJIPlayerController::BeginPlay()
{
    Super::BeginPlay();

    ChangeInputMode(true);

    PacketMgr::GetInst()->SetPlayerController(this);

    char buf[BUF_SIZE]{};
    int32 received = 0;
    uint32 datasize;
    g_Socket->HasPendingData(datasize);

    auto result = g_Socket->Recv(reinterpret_cast<uint8*>(buf), BUF_SIZE, received);

    if (received > 0)
        PacketMgr::GetInst()->process_data(buf, received);

    HUDPtr = Cast<AJIHUD>(GetHUD());
    GameModePtr = Cast<AJIGameMode>(UGameplayStatics::GetGameMode(GetWorld()));

    TArray<AActor*> FoundOcean;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), AJIOcean::StaticClass(), FoundOcean);
    for (auto Actor : FoundOcean) {
        AJIOcean* FoundActor = Cast<AJIOcean>(Actor);
        if (FoundActor)
            OceanPtr = FoundActor;
    }

    if (BGMAudioComponent && HansanSoundCue)
        BGMAudioComponent->SetSound(HansanSoundCue);

    if (UIAudioComponent && UISoundCue)
        UIAudioComponent->SetSound(UISoundCue);

    if (ClickAudioComponent && ClickSoundCue)
        ClickAudioComponent->SetSound(ClickSoundCue);

    MiniMapWidget = CreateWidget<UJIMiniMapWidget>(this, MiniMapWidgetClass);
    JICHECK(nullptr != MiniMapWidget);
    MiniMapWidget->AddToViewport(1);

    HUDWidget = CreateWidget<UJIHUDWidget>(this, HUDWidgetClass);
    JICHECK(nullptr != HUDWidget);
    HUDWidget->AddToViewport(1);

    OceanWidget = CreateWidget<UJIOceanWidget>(this, OceanWidgetClass);
    JICHECK(nullptr != OceanWidget);
    OceanWidget->SetOceanPtr(OceanPtr);
    OceanWidget->AddToViewport(1);

    ChattingWidget = CreateWidget<UJIChattingWidget>(this, ChattingWidgetClass);
    JICHECK(nullptr != ChattingWidget);
    ChattingWidget->AddToViewport(1);

    GeneralHUDWidget = CreateWidget<UJIGeneralHUDWidget>(this, GeneralHUDWidgetClass);
    JICHECK(nullptr != GeneralHUDWidget);
    GeneralHUDWidget->AddToViewport(1);


    DefeatWidget = CreateWidget<UJIDefeatWidget>(this, DefeatWidgetClass);
    VictoryWidget = CreateWidget<UJIVictoryWidget>(this, VictoryWidgetClass);

    TArray<AActor*> FoundActors;
    switch (CurrentView)
    {
    case EViewMode::TOPVIEW:
        UGameplayStatics::GetAllActorsOfClass(GetWorld(), AJICameraPawn::StaticClass(), FoundActors);
        for (auto Actor : FoundActors) {
            AJICameraPawn* FoundActor = Cast<AJICameraPawn>(Actor);
            if (FoundActor)
            {
               // if (FoundActor->GetActorLabel() == FString("CameraPawn")) {
                    TopViewCamera = FoundActor;
                    Possess(TopViewCamera);
                    SetViewMode(EViewMode::TOPVIEW);
                    MiniMapWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible); 
                    GeneralHUDWidget->SetVisibility(ESlateVisibility::Hidden);
               // }
            }
        }
        break;
        //서버 부분
    case EViewMode::BACKVIEW:
        UGameplayStatics::GetAllActorsOfClass(GetWorld(), AJIGeneral::StaticClass(), FoundActors);
        for (auto Actor : FoundActors)
        {
            AJIGeneral* FoundActor = Cast<AJIGeneral>(Actor);
            if (FoundActor)
                SelctedGenerals.AddUnique(FoundActor);
        }

        for (int32 i = 0; i < SelctedGenerals.Num(); i++)
        {
            if (SelctedGenerals[i]->m_bIsOwner)
            {
                Possess(SelctedGenerals[i]);
                SetViewMode(EViewMode::BACKVIEW);
            }
        }
        break;
    }

    BGMAudioComponent->Play(0.f);
    MiniMapWidget->SetAudioComponent(UIAudioComponent);
    HUDWidget->SetAudioComponent(ClickAudioComponent);

    bLeftMouseButtonDown = false;
    bRightMouseButtonDown = false;
    clickCount = 0;
}

void AJIPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    InputComponent->BindAction("LeftMouseClick", IE_Pressed, this, &AJIPlayerController::OnSelectionPressed);
    InputComponent->BindAction("LeftMouseClick", IE_Released, this, &AJIPlayerController::OnSelectionReleased);
    InputComponent->BindAction("RightMouseClick", IE_Pressed, this, &AJIPlayerController::OnMovePressed);
    InputComponent->BindAction("RightMouseClick", IE_Released, this, &AJIPlayerController::OnMoveReleased);
    InputComponent->BindAction("ZoomIn", IE_Pressed, this, &AJIPlayerController::OnZoomIn);
    InputComponent->BindAction("ZoomIn", IE_Released, this, &AJIPlayerController::OnZoomInReleased);
    InputComponent->BindAction("ZoomOut", IE_Pressed, this, &AJIPlayerController::OnZoomOut);
    InputComponent->BindAction("ZoomOut", IE_Released, this, &AJIPlayerController::OnZoomOutReleased);


    InputComponent->BindAction("Spawn", IE_Pressed, this, &AJIPlayerController::OnSpawn);
    InputComponent->BindAction("Fire", IE_Pressed, this, &AJIPlayerController::OnFirePressed);
    InputComponent->BindAction("Fire", IE_Released, this, &AJIPlayerController::OnFireReleased);
    InputComponent->BindAction("Reload", IE_Pressed, this, &AJIPlayerController::OnReload);
    InputComponent->BindAction("Reload", IE_Released, this, &AJIPlayerController::OnReloadReleased);

    InputComponent->BindAction("WeaponChange", IE_Pressed, this, &AJIPlayerController::OnWeaponChange);
    InputComponent->BindAction("1", IE_Pressed, this, &AJIPlayerController::SetTatics1);
    InputComponent->BindAction("2", IE_Pressed, this, &AJIPlayerController::SetTatics2);

    InputComponent->BindAction("UnitDesignation1", IE_Pressed, this, &AJIPlayerController::UnitDesignation1);
    InputComponent->BindAction("UnitDesignation2", IE_Pressed, this, &AJIPlayerController::UnitDesignation2);
    InputComponent->BindAction("UnitDesignation3", IE_Pressed, this, &AJIPlayerController::UnitDesignation3);
    InputComponent->BindAction("UnitDesignation4", IE_Pressed, this, &AJIPlayerController::UnitDesignation4);
    InputComponent->BindAction("UnitDesignation5", IE_Pressed, this, &AJIPlayerController::UnitDesignation5);

    InputComponent->BindAction("LoadUnitDesignation1", IE_Pressed, this, &AJIPlayerController::LoadUnitDesignation1);
    InputComponent->BindAction("LoadUnitDesignation2", IE_Pressed, this, &AJIPlayerController::LoadUnitDesignation2);
    InputComponent->BindAction("LoadUnitDesignation3", IE_Pressed, this, &AJIPlayerController::LoadUnitDesignation3);
    InputComponent->BindAction("LoadUnitDesignation4", IE_Pressed, this, &AJIPlayerController::LoadUnitDesignation4);
    InputComponent->BindAction("LoadUnitDesignation5", IE_Pressed, this, &AJIPlayerController::LoadUnitDesignation5);

    InputComponent->BindAction("SaveScreen1", IE_Pressed, this, &AJIPlayerController::SaveScreen1);
    InputComponent->BindAction("SaveScreen2", IE_Pressed, this, &AJIPlayerController::SaveScreen2);
    InputComponent->BindAction("SaveScreen3", IE_Pressed, this, &AJIPlayerController::SaveScreen3);
    InputComponent->BindAction("LoadScreen1", IE_Pressed, this, &AJIPlayerController::LoadScreen1);
    InputComponent->BindAction("LoadScreen2", IE_Pressed, this, &AJIPlayerController::LoadScreen2);
    InputComponent->BindAction("LoadScreen3", IE_Pressed, this, &AJIPlayerController::LoadScreen3);

    InputComponent->BindAction("Pause", IE_Pressed, this, &AJIPlayerController::OnGamePause);
    InputComponent->BindAction("Pause", IE_Released, this, &AJIPlayerController::OnGamePauseReleased);

    InputComponent->BindAction("Stop", IE_Pressed, this, &AJIPlayerController::OnStopPressed);
    InputComponent->BindAction("Stop", IE_Released, this, &AJIPlayerController::OnStopReleased);

    InputComponent->BindAction("Capture", IE_Pressed, this, &AJIPlayerController::OnCapture);
    InputComponent->BindAction("SetCaptureLocation", IE_Pressed, this, &AJIPlayerController::OnSetCaptureLocation);

    InputComponent->BindAction("Map", IE_Pressed, this, &AJIPlayerController::OnMiniMap);
    InputComponent->BindAction("Map", IE_Released, this, &AJIPlayerController::OnMiniMapReleased);

    InputComponent->BindAction("Ocean", IE_Pressed, this, &AJIPlayerController::OnOceanButton);
    InputComponent->BindAction("Ocean", IE_Released, this, &AJIPlayerController::OnOceanButtonReleased);



    InputComponent->BindAction("Tab", IE_Pressed, this, &AJIPlayerController::OnTab);
    InputComponent->BindAction("SetClickCountZero", IE_Pressed, this, &AJIPlayerController::SetClickCountZero);

    InputComponent->BindAxis("MoveForward", this, &AJIPlayerController::MoveForward);
    InputComponent->BindAxis("MoveRight", this, &AJIPlayerController::MoveRight);
    InputComponent->BindAxis("LookUp", this, &AJIPlayerController::LookUp);
    InputComponent->BindAxis("Turn", this, &AJIPlayerController::Turn);
    InputComponent->BindAxis("Roll", this, &AJIPlayerController::Roll);
    InputComponent->BindAxis("MoveUpDown", this, &AJIPlayerController::MoveUpDown);
    InputComponent->BindAxis("MoveLeftRight", this, &AJIPlayerController::MoveLeftRight);
}

void AJIPlayerController::OnSetCaptureLocation()
{
    if (CurrentView == EViewMode::TOPVIEW)
        TopViewCamera->SetActorLocation(CaptureLocation);
}

void AJIPlayerController::OnCapture()
{
    if (CurrentView == EViewMode::TOPVIEW)
        CaptureLocation = TopViewCamera->GetActorLocation();
}

void AJIPlayerController::OnMiniMap()
{
    MiniMapWidget->OnMapClicked();
    HUDWidget->OnMapButtonClicked();
}

void AJIPlayerController::OnMiniMapReleased()
{
    HUDWidget->OnMapButtonReleased();
}

void AJIPlayerController::OnOceanButton()
{
    OceanWidget->OnOceanClicked();
    HUDWidget->OnOceanButtonClicked();
}

void AJIPlayerController::OnOceanButtonReleased()
{
    HUDWidget->OnOceanButtonReleased();
}

void AJIPlayerController::OnStopPressed()
{
    if (CurrentView == EViewMode::TOPVIEW) {
        for (int32 i = 0; i < SelctedActors.Num(); ++i)
            SelctedActors[i]->SetStop();
        HUDWidget->OnStopButtonClicked();
    }
}

void AJIPlayerController::OnStopReleased()
{
    HUDWidget->OnStopButtonReleased();
}

void AJIPlayerController::OnReload()
{
    for (int32 i = 0; i < SelctedActors.Num(); ++i)
        SelctedActors[i]->OnReload();
    HUDWidget->OnReloadButtonClicked();
}

void AJIPlayerController::OnReloadReleased()
{
    HUDWidget->OnReloadButtonReleased();
}


void AJIPlayerController::OnPause()
{
    UGameplayStatics::SetGamePaused(GetWorld(), true);
    if (MenuWidget != nullptr)
    {
        MenuWidget->AddToViewport();
        FInputModeUIOnly Mode;
        SetInputMode(Mode);
        bShowMouseCursor = true;
        bIsOnPause = true;
    }
}

void AJIPlayerController::OnWeaponChange()
{
    for (auto& Generals : g_mapPlayer)
    {
        if (Generals.Value->m_bIsOwner)
        {
            if (CurrentView == EViewMode::BACKVIEW)
            {
                Generals.Value->WeaponChange();
                if (Generals.Value->CurWeaponIndex == EWeaponType::SWORD)
                {
                    GeneralHUDWidget->SetBowImg();
                }
                else
                {
                    GeneralHUDWidget->SetSwordImg();

                }
            }
        }
    }
}

//부대지정
void AJIPlayerController::UnitDesignation1()
{
    FUnitDesignation temp;
    temp.Ship = SelctedActors;
    UnitDesignationMap.Add(1, temp);

}

void AJIPlayerController::UnitDesignation2()
{
    FUnitDesignation temp;
    temp.Ship = SelctedActors;
    UnitDesignationMap.Add(2, temp);
}


void AJIPlayerController::UnitDesignation3()
{
    FUnitDesignation temp;
    temp.Ship = SelctedActors;
    UnitDesignationMap.Add(3, temp);
}

void AJIPlayerController::UnitDesignation4()
{
    FUnitDesignation temp;
    temp.Ship = SelctedActors;
    UnitDesignationMap.Add(4, temp);
}

void AJIPlayerController::UnitDesignation5()
{
    FUnitDesignation temp;
    temp.Ship = SelctedActors;
    UnitDesignationMap.Add(5, temp);
}

//지정된 부대 불러오기
void AJIPlayerController::LoadUnitDesignation1()
{
    bool bHasKeyValue = UnitDesignationMap.Contains(1);
    for (int i = 0; i < SelctedActors.Num(); ++i)
    {
        if (SelctedActors[i] != nullptr)
            SelctedActors[i]->SetSelection(false);
    }
    if (bHasKeyValue)
    {
        SelctedActors = UnitDesignationMap[1].Ship;
        HUDWidget->SetSelectedShipImage(SelctedActors);
    }
    for (int i = 0; i < SelctedActors.Num(); ++i)
    {
        if (SelctedActors[i] != nullptr)
            SelctedActors[i]->SetSelection(true);
    }
    if (SelctedActors.Num() > 0)
    {
        PressedCnt1 += 1;
        if (PressedCnt1 >= 2)
        {
            TopViewCamera->SetActorLocation(SelctedActors[0]->GetActorLocation());
        }
        FTimerHandle DoublePress;
        GetWorldTimerManager().SetTimer(DoublePress, this, &AJIPlayerController::LoadUnitDesignationPos1, 0.3f, false);
    }
}

void AJIPlayerController::LoadUnitDesignation2()
{
    bool bHasKeyValue = UnitDesignationMap.Contains(2);
    for (int i = 0; i < SelctedActors.Num(); ++i)
    {
        if (SelctedActors[i] != nullptr)
            SelctedActors[i]->SetSelection(false);
    }
    if (bHasKeyValue)
    {
        SelctedActors = UnitDesignationMap[2].Ship;
        HUDWidget->SetSelectedShipImage(SelctedActors);
    }
    for (int i = 0; i < SelctedActors.Num(); ++i)
    {
        if (SelctedActors[i] != nullptr)
            SelctedActors[i]->SetSelection(true);
    }
    if (SelctedActors.Num() > 0)
    {
        PressedCnt2 += 1;
        if (PressedCnt2 >= 2)
        {
            TopViewCamera->SetActorLocation(SelctedActors[0]->GetActorLocation());
        }
        FTimerHandle DoublePress;
        GetWorldTimerManager().SetTimer(DoublePress, this, &AJIPlayerController::LoadUnitDesignationPos2, 0.3f, false);
    }
}

void AJIPlayerController::LoadUnitDesignation3()
{
    bool bHasKeyValue = UnitDesignationMap.Contains(3);
    for (int i = 0; i < SelctedActors.Num(); ++i)
    {
        if (SelctedActors[i] != nullptr)
            SelctedActors[i]->SetSelection(false);
    }
    if (bHasKeyValue)
    {
        SelctedActors = UnitDesignationMap[3].Ship;
        HUDWidget->SetSelectedShipImage(SelctedActors);
    }
    for (int i = 0; i < SelctedActors.Num(); ++i)
    {
        if (SelctedActors[i] != nullptr)
           SelctedActors[i]->SetSelection(true);
    }
    if (SelctedActors.Num() > 0)
    {
        PressedCnt3 += 1;
        if (PressedCnt3 >= 2)
        {
            TopViewCamera->SetActorLocation(SelctedActors[0]->GetActorLocation());
        }
        FTimerHandle DoublePress;
        GetWorldTimerManager().SetTimer(DoublePress, this, &AJIPlayerController::LoadUnitDesignationPos3, 0.3f, false);
    }
}

void AJIPlayerController::LoadUnitDesignation4()
{
    bool bHasKeyValue = UnitDesignationMap.Contains(4);
    for (int i = 0; i < SelctedActors.Num(); ++i)
    {
        if (SelctedActors[i] != nullptr)
           SelctedActors[i]->SetSelection(false);
    }
    if (bHasKeyValue)
    {
        SelctedActors = UnitDesignationMap[4].Ship;
        HUDWidget->SetSelectedShipImage(SelctedActors);
    }
    for (int i = 0; i < SelctedActors.Num(); ++i)
    {
        if (SelctedActors[i] != nullptr)
           SelctedActors[i]->SetSelection(true);
    }

    if (SelctedActors.Num() > 0)
    {
        PressedCnt4 += 1;
        if (PressedCnt4 >= 2)
        {
            TopViewCamera->SetActorLocation(SelctedActors[0]->GetActorLocation());
        }
        FTimerHandle DoublePress;
        GetWorldTimerManager().SetTimer(DoublePress, this, &AJIPlayerController::LoadUnitDesignationPos4, 0.3f, false);
    }
}

void AJIPlayerController::LoadUnitDesignation5()
{
    bool bHasKeyValue = UnitDesignationMap.Contains(5);
    for (int i = 0; i < SelctedActors.Num(); ++i)
    {
        if (SelctedActors[i] != nullptr)
            SelctedActors[i]->SetSelection(false);
    }
    if (bHasKeyValue)
    {
        SelctedActors = UnitDesignationMap[5].Ship;
        HUDWidget->SetSelectedShipImage(SelctedActors);
    }
    for (int i = 0; i < SelctedActors.Num(); ++i)
    {
        if (SelctedActors[i] != nullptr)
            SelctedActors[i]->SetSelection(true);
    }

    if (SelctedActors.Num() > 0)
    {
        PressedCnt5 += 1;
        if (PressedCnt5 >= 2)
        {
            TopViewCamera->SetActorLocation(SelctedActors[0]->GetActorLocation());
        }
        FTimerHandle DoublePress;
        GetWorldTimerManager().SetTimer(DoublePress, this, &AJIPlayerController::LoadUnitDesignationPos5, 0.3f, false);
    }
}

//지정된 부대 불러오기
void AJIPlayerController::LoadUnitDesignationPos1()
{
    PressedCnt1 = 0;
}

void AJIPlayerController::LoadUnitDesignationPos2()
{
    PressedCnt2 = 0;
}

void AJIPlayerController::LoadUnitDesignationPos3()
{
    PressedCnt3 = 0;
}

void AJIPlayerController::LoadUnitDesignationPos4()
{
    PressedCnt4 = 0;
}

void AJIPlayerController::LoadUnitDesignationPos5()
{
    PressedCnt5 = 0;
}

void AJIPlayerController::SaveScreen1()
{
    if (CurrentView == EViewMode::TOPVIEW)
        SaveScreenMap.Add(1, TopViewCamera->GetActorLocation());
}

void AJIPlayerController::SaveScreen2()
{
    if (CurrentView == EViewMode::TOPVIEW)
        SaveScreenMap.Add(2, TopViewCamera->GetActorLocation());
}

void AJIPlayerController::SaveScreen3()
{
    if (CurrentView == EViewMode::TOPVIEW)
        SaveScreenMap.Add(3, TopViewCamera->GetActorLocation());
}

void AJIPlayerController::LoadScreen1()
{
    bool bHasKeyValue = SaveScreenMap.Contains(1);
    if (CurrentView == EViewMode::TOPVIEW && bHasKeyValue)
        TopViewCamera->SetActorLocation(SaveScreenMap[1]);
}

void AJIPlayerController::LoadScreen2()
{
    bool bHasKeyValue = SaveScreenMap.Contains(2);
    if (CurrentView == EViewMode::TOPVIEW && bHasKeyValue)
        TopViewCamera->SetActorLocation(SaveScreenMap[2]);
}

void AJIPlayerController::LoadScreen3()
{
    bool bHasKeyValue = SaveScreenMap.Contains(3);
    if (CurrentView == EViewMode::TOPVIEW && bHasKeyValue)
        TopViewCamera->SetActorLocation(SaveScreenMap[3]);
}

void AJIPlayerController::SetTatics1()
{
    FVector MoveLocation = FVector::ZeroVector;
    if (SelctedActors.Num() > 0)
    {
        for (int32 i = 0; i < SelctedActors.Num(); ++i)
            MoveLocation += (SelctedActors[i]->GetActorLocation() / SelctedActors.Num());
    }

    if (SelctedActors.Num() > 0)
    {
        for (int32 i = 0; i < SelctedActors.Num(); ++i)
        {
            int x = SelctedActors.Num() / 2;
            if (SelctedActors.Num() % 2 == 1)//홀수일경우
            {
                FVector Dest = MoveLocation + FVector(0, (i - x) * interval, 0);
                SelctedActors[i]->SetArrival(Dest);
            }
        }
    }
}

void AJIPlayerController::SetTatics2()
{

}

void AJIPlayerController::OnSpawn()
{
    if (CurrentView == EViewMode::BACKVIEW)
    {
        for (int8 i = 0; i < 3; i++)
        {
            float x = FMath::RandRange(-500, 500);
            float y = FMath::RandRange(-500, 500);

            for (auto& Generals : g_mapPlayer)
            {
                if (Generals.Value->m_bIsOwner)
                {
                    GetWorld()->SpawnActor<AJIAICharacter>(Generals.Value->GetActorLocation() + FVector(x, y, 0.0f), FRotator::ZeroRotator);
                    GetWorld()->SpawnActor<AJICharacter>(Generals.Value->GetActorLocation() + FVector(x, y, 0.0f), FRotator::ZeroRotator);
                }
            }
        }
    }
}

void AJIPlayerController::OnFirePressed()
{
    bOnFire = true;
    if (SelctedActors.Num() > 0)
    {
        for (int32 i = 0; i < SelctedActors.Num(); ++i)
        {
            SelctedActors[i]->bOnRangeDecal = true;
        }
    }
}


void AJIPlayerController::OnFireReleased()
{
    bOnFire = false;
    if (SelctedActors.Num() > 0)
    {
        for (int32 i = 0; i < SelctedActors.Num(); ++i)
        {
            SelctedActors[i]->bOnRangeDecal = false;
        }
    }
}

void AJIPlayerController::OnVictoryWidget()
{
    if (!bOnVictoryWidget)
    {
        bOnVictoryWidget = true;
        UGameplayStatics::SetGamePaused(GetWorld(), true);
        if (VictoryWidget != nullptr)
        {
            VictoryWidget->AddToViewport(1);
            FInputModeUIOnly Mode;
            SetInputMode(Mode);
            bShowMouseCursor = true;
            bIsOnPause = true;
        }
    }
}

void AJIPlayerController::OnDefeatWidget()
{
    if (!bOnDefeatWidget)
    {
        bOnDefeatWidget = true;
        UGameplayStatics::SetGamePaused(GetWorld(), true);
        if (DefeatWidget != nullptr)
        {
            DefeatWidget->AddToViewport(1);
            FInputModeUIOnly Mode;
            SetInputMode(Mode);
            bShowMouseCursor = true;
            bIsOnPause = true;
        }
    }
}

FVector fCameraVec;
void AJIPlayerController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if (OceanWidget != nullptr)
        OceanWidget->SetOceanData();

    if (bOnFire)
        Cursor->SetVisibility(true);
    else
        Cursor->SetVisibility(false);
    float x, y;
    GetMousePosition(x, y);
    Cursor->SetWorldLocation(FVector(x, y, 3000));

    char buf[BUF_SIZE];
    int32 received = 0;
    uint32 datasize;
    g_Socket->HasPendingData(datasize);

    auto result = g_Socket->Recv(reinterpret_cast<uint8*>(buf), BUF_SIZE, received);

    if (received > 0)
    {
        PacketMgr::GetInst()->process_data(buf, received);
    }

    if (WasInputKeyJustPressed(EKeys::Tab)) // released 로 바꾸자
    {
        cs_packet_view packet;

        packet.type = C2S_VIEWMODE;
        packet.size = sizeof(cs_packet_view);
        if (CurrentView == EViewMode::BACKVIEW)
        {
            packet.viewmode = M_TOPVIEW;
            //OnTab();
        }
        else if (CurrentView == EViewMode::TOPVIEW)
        {
            packet.viewmode = M_BACKVIEW;
           // OnTab();
        }
        PacketMgr::GetInst()->send_packet(&packet);
    }


    if (WasInputKeyJustPressed(EKeys::F12)) // released 로 바꾸자
    {
        cs_quit_game packet;
        packet.type = C2S_CLIENT_QUIT_GAME;
        packet.size = sizeof(cs_quit_game);
        PacketMgr::GetInst()->send_packet(&packet);
    }

    if (CurrentView == EViewMode::BACKVIEW)
    {
        float fCamValue = 0.f;

        for (auto& Generals : g_mapPlayer)
        {
            if (Generals.Value->m_bIsOwner)
            {
                //Generals.Value->JIAnim->CurrentPawnSpeed = 1.f;
                if (WasInputKeyJustPressed(EKeys::LeftMouseButton))  // released 로 바꾸자 // 수정
                {
                    cs_packet_attack packet;

                    packet.type = C2S_ATTACK;
                    packet.size = sizeof(cs_packet_attack);
                    
                    if (Generals.Value->WeaponSocketName == (TEXT("LHand_Bow")))
                    {
                        packet.attack = A_BOW;
                        GeneralHUDWidget->SetBowImg();
                    }
                    else if (Generals.Value->WeaponSocketName == (TEXT("RHand_Sword")))
                    {
                        packet.attack = A_KNIFE;
                        GeneralHUDWidget->SetSwordImg();
                    }
                    packet.objtype = (char)OBJ_TYPE::GENERAL;
                    packet.id = Generals.Value->m_iId;
                    PacketMgr::GetInst()->send_packet(&packet);
                }
                if (IsInputKeyDown(EKeys::W) || IsInputKeyDown(EKeys::S))  // released 로 바꾸자 // 수정
                {
                    cs_packet_move packet;
                    packet.type = C2S_MOVE;
                    packet.size = sizeof(cs_packet_move);

                    FVector pos = Generals.Value->GetActorLocation();
                    FVector vel = Generals.Value->GetVelocity();

                    packet.x = pos.X;
                    packet.y = pos.Y;
                    packet.z = pos.Z;
                    packet.velx = vel.X;
                    packet.vely = vel.Y;
                    packet.velz = vel.Z;
                    packet.m_id = Generals.Value->m_iId;
                    packet.objtype = Generals.Value->m_eObjType;
                    packet.dir = D_UP;
                    packet.status = (char)GENERAL_STATUS::RUN;
                    //Generals.Value->GetMovementComponent()->UpdateComponentVelocity();

                    packet.TurnValue = Generals.Value->GetActorRotation().Yaw;
                    PacketMgr::GetInst()->send_packet(&packet);

                    //JILOG(Warning, TEXT("%d"), 0);
                    break;
                }
                else if (IsInputKeyDown(EKeys::A) || IsInputKeyDown(EKeys::D))
                {
                    cs_packet_move packet;
                    packet.type = C2S_MOVE;
                    packet.size = sizeof(cs_packet_move);

                    FVector pos = Generals.Value->GetActorLocation();
                    FVector vel = Generals.Value->GetVelocity();

                    packet.x = pos.X;
                    packet.y = pos.Y;
                    packet.z = pos.Z;
                    packet.velx = vel.X;
                    packet.vely = vel.Y;
                    packet.velz = vel.Z;
                    packet.m_id = Generals.Value->m_iId;
                    packet.objtype = Generals.Value->m_eObjType;
                    packet.dir = D_RIGHT;
                    packet.status = (char)GENERAL_STATUS::RUN;
                    //Generals.Value->GetMovementComponent()->UpdateComponentVelocity();

                    packet.TurnValue = Generals.Value->GetActorRotation().Yaw;
                    PacketMgr::GetInst()->send_packet(&packet);

                    // JILOG(Warning, TEXT("%d"), 0);
                    break;
                }

                if (m_eStatus == GENERAL_STATUS::RUN)
                {
                    cs_packet_move packet;
                    packet.type = C2S_MOVE;
                    packet.size = sizeof(cs_packet_move);

                    FVector pos = Generals.Value->GetActorLocation();
                    FVector vel = Generals.Value->GetVelocity();

                    packet.x = pos.X;
                    packet.y = pos.Y;
                    packet.z = pos.Z;
                    packet.velx = vel.X;
                    packet.vely = vel.Y;
                    packet.velz = vel.Z;
                    packet.m_id = Generals.Value->m_iId;
                    packet.objtype = Generals.Value->m_eObjType;
                    packet.TurnValue = GetControlRotation().Yaw;
                    //packet.RotValue = NewAxisValue;
                    packet.status = (char)GENERAL_STATUS::IDLE;
                    PacketMgr::GetInst()->send_packet(&packet);

                    // JILOG(Warning, TEXT("%d"), 0);
                    break;
                }
            }
        }
    }
}

void AJIPlayerController::MinimapClick(FVector2D location)
{
    bMouseOnMinimap = true;
    if (bLeftMouseButtonDown)
    {
       // JILOG(Warning, TEXT("bLeftMouseButtonDown, %f, %f"), location.X, location.Y);
        FVector Hit = FVector(location.X, location.Y, 1000.0f);
        GetWorld()->SpawnActor<AJIFireDecal>(Hit, FRotator::ZeroRotator);
        TopViewCamera->SetActorLocation(Hit);
    }
    if (bRightMouseButtonDown&& bMouseOnMinimap)
    {
        //JILOG(Warning, TEXT("bRightMouseButtonDown"), location.X, location.Y);
        SetInputModeGameOnly(true);
        FVector Hit = FVector(location.X, location.Y, 500.0f);
        GetWorld()->SpawnActor<AJIMousClickDecal>(Hit, FRotator::ZeroRotator);
        if (SelctedActors.Num() > 0)
        {
            clickCount -= 1;
            for (int32 i = 0; i < SelctedActors.Num(); ++i)
            {
                float RotationZ = GetControlRotation().Roll;
                int x = SelctedActors.Num() / 2;
                if (SelctedActors.Num() % 2 == 1)//홀수일경우
                {
                    if (SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JOSEON_1 || SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JOSEON_2)
                    {
                        FVector Dest = Hit + FVector((i - x), 0, 0) * JOSEON_INTERVAL;
                        SelctedActors[i]->SetArrival(Dest);
                    }
                    else if (SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JAPAN_1 || SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JAPAN_2)
                    {
                        FVector Dest = Hit + FVector((i - x), 0, 0) * JAPAN_INTERVAL;
                        SelctedActors[i]->SetArrival(Dest);
                    }
                }
                else//짝수일경우
                {
                    if (SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JOSEON_1 || SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JOSEON_2)
                    {
                        FVector Dest = Hit + FVector((i - x) * JOSEON_INTERVAL + 0.5 * JOSEON_INTERVAL, 0, 0);
                        SelctedActors[i]->SetArrival(Dest);
                    }
                    else if (SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JAPAN_1 || SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JAPAN_2)
                    {
                        FVector Dest = Hit + FVector((i - x) * JAPAN_INTERVAL + 0.5 * JAPAN_INTERVAL, 0, 0);
                        SelctedActors[i]->SetArrival(Dest);
                    }
                }
                HUDWidget->OnMoveButtonReleased();
            }
        }
        SetInputModeGameOnly(false);
    }
    bMouseOnMinimap = false;
}

void AJIPlayerController::SetInputModeGameOnly(bool InConsumeCaptureMouseDown)
{
    if (!bIsOnPause) {
        if (CurrentView == EViewMode::TOPVIEW) {
            FInputModeGameAndUI InputMode;
            SetInputMode(InputMode);
        }
        else if (CurrentView == EViewMode::BACKVIEW) {
            FInputModeGameOnly InputMode;
            InputMode.SetConsumeCaptureMouseDown(InConsumeCaptureMouseDown);
            SetInputMode(InputMode);
        }
    }
}

void AJIPlayerController::OnSelectionPressed()
{
    SetInputModeGameOnly(true);
    bLeftMouseButtonDown = true;
    if (bOnFire) {
        if (SelctedActors.Num() > 0) {
            for (int32 i = 0; i < SelctedActors.Num(); ++i) {
                FHitResult Hit;
                GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, false, Hit);
                GetWorld()->SpawnActor<AJIFireDecal>(Hit.Location, FRotator::ZeroRotator);
                if (SelctedActors[i]->IsInAttackRange(Hit.Location))
                {
                    SelctedActors[i]->LoadBomb(Hit.Location);
                    //SelctedActors[i]->Fire(Hit.Location);
                    HUDWidget->OnAttackButtonClicked();
                }
            }
        }
    }
    else {
        //서버부분
        //SelctedActors.Empty();

        if (CurrentView == EViewMode::TOPVIEW) {
            for (int32 i = 0; i < SelctedActors.Num(); ++i)
            {
                if (SelctedActors[i] != nullptr)
                    SelctedActors[i]->SetSelection(false);
            }
            HUDPtr->m_vInitialPoint = HUDPtr->GetMousePos2D();
            HUDPtr->m_bStartSelecting = true;
        }
        else if (CurrentView == EViewMode::BACKVIEW)
        {
            //서버부분
               //for (auto& Generals : g_mapPlayer)
               //{
               //   if(Generals.Value->m_bIsOwner)
               //      Generals.Value->Attack();
               //}
              // if (General != nullptr)
              // {
              //    General->Attack();
              // }
               //클라부분
               //if( General != nullptr)
               //   General->Attack();   
        }
    }
    SetInputModeGameOnly(false);
}

void AJIPlayerController::OnSelectionReleased()
{
    SetInputModeGameOnly(true);
    bLeftMouseButtonDown = false;
    HUDWidget->OnAttackButtonReleased();
    if (CurrentView == EViewMode::TOPVIEW)
    {
        SelctedActors.Empty();
        HUDPtr->m_bStartSelecting = false;
        TArray<AJIShip*> tarray = HUDPtr->FoundActors;

        for (auto& player : g_mapPlayer)
        {
            if (player.Value->m_bIsOwner)
            {
                for (int i = 0; i < tarray.Num(); ++i)
                {
                    if (tarray[i] != nullptr)
                    {
                        if (tarray[i]->m_eTeamColor == player.Value->m_eTeamColor && tarray[i]->ShipStat->CurrentHP > 0)
                        {
                            SelctedActors.Emplace(tarray[i]);
                        }
                    }
                }
                HUDWidget->SetSelectedShipImage(SelctedActors);
            }
        }
    }
    for (int32 i = 0; i < SelctedActors.Num(); ++i)
    {
        if(SelctedActors[i] != nullptr)
             SelctedActors[i]->SetSelection(true);
    }
    SetInputModeGameOnly(false);
}

void AJIPlayerController::OnMovePressed()
{
    SetInputModeGameOnly(true);
    if(clickCount==0)
        clickCount += 1;
    bRightMouseButtonDown = true;
    HUDWidget->OnMoveButtonClicked();
    SetInputModeGameOnly(false);
}

void AJIPlayerController::OnMoveReleased()
{
    SetInputModeGameOnly(true);
    bRightMouseButtonDown = false;
    if (clickCount == 1&& !bMouseOnMinimap)
    {
        clickCount -= 1;
       // JILOG(Warning, TEXT("OnMoveReleased"));
        FHitResult Hit;
        GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, false, Hit);
        GetWorld()->SpawnActor<AJIMousClickDecal>(Hit.Location, FRotator::ZeroRotator);
        FVector MoveLocation = FVector::ZeroVector;
        MoveLocation = Hit.Location;
        if (SelctedActors.Num() > 0)
        {
            //JILOG(Warning, TEXT("목적지 X : %f,목적지 Y : %f,목적지 Z : %f"), Hit.Location.X, Hit.Location.Y, Hit.Location.Z);
            for (int32 i = 0; i < SelctedActors.Num(); ++i)
            {
                float RotationZ = GetControlRotation().Roll;
                int x = SelctedActors.Num() / 2;
                if (SelctedActors.Num() % 2 == 1)//홀수일경우
                {
                    if (SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JOSEON_1 || SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JOSEON_2)
                    {
                        FVector Dest = MoveLocation + FVector((i - x), 0, 0) * JOSEON_INTERVAL;
                        SelctedActors[i]->SetArrival(Dest);
                    }
                    else if (SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JAPAN_1 || SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JAPAN_2)
                    {
                        FVector Dest = MoveLocation + FVector((i - x), 0, 0) * JAPAN_INTERVAL;
                        SelctedActors[i]->SetArrival(Dest);
                    }
                }
                else//짝수일경우
                {
                    if (SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JOSEON_1 || SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JOSEON_2)
                    {
                        FVector Dest = MoveLocation + FVector((i - x) * JOSEON_INTERVAL + 0.5 * JOSEON_INTERVAL, 0, 0);
                        SelctedActors[i]->SetArrival(Dest);
                    }
                    else if (SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JAPAN_1 || SelctedActors[i]->m_eTeamColor == TEAM_COLOR::JAPAN_2)
                    {
                        FVector Dest = MoveLocation + FVector((i - x) * JAPAN_INTERVAL + 0.5 * JAPAN_INTERVAL, 0, 0);
                        SelctedActors[i]->SetArrival(Dest);
                    }
                }
                HUDWidget->OnMoveButtonReleased();
            }
        }
    }
    SetInputModeGameOnly(false);
}

void AJIPlayerController::SetClickCountZero()
{
    JILOG(Warning, TEXT("SetClickCountZero"));
    clickCount = 0;
}

void AJIPlayerController::OnTab()
{
    TArray<AActor*> FoundActors;
    switch (CurrentView)
    {
    case EViewMode::TOPVIEW:
        UGameplayStatics::GetAllActorsOfClass(GetWorld(), AJIGeneral::StaticClass(), FoundActors);
        for (auto Actor : FoundActors)
        {
            AJIGeneral* FoundActor = Cast<AJIGeneral>(Actor);
            if (FoundActor)
            {
                if (FoundActor->m_bIsOwner)
                {
                    Possess(FoundActor);
                    SetViewMode(EViewMode::BACKVIEW);
                }
            }
        }
        break;

    case EViewMode::BACKVIEW:
        UGameplayStatics::GetAllActorsOfClass(GetWorld(), AJICameraPawn::StaticClass(), FoundActors);
        for (auto Actor : FoundActors)
        {
            AJICameraPawn* FoundActor = Cast<AJICameraPawn>(Actor);
            if (FoundActor)
            {
                //if (FoundActor->GetActorLabel() == FString("CameraPawn"))
                //{
                    TopViewCamera = FoundActor;
                    Possess(TopViewCamera);
                    SetViewMode(EViewMode::TOPVIEW);
                    TopViewCamera->CurrentLocationIndicatorInMinimap->SetVisibility(false);
                //}
            }
        }
        break;
    }
}

void AJIPlayerController::OnZoomIn()
{
    switch (CurrentView)
    {
    case EViewMode::TOPVIEW:
        if (TopViewCamera != nullptr)
        {
            float NewSpringArm = 0;
            NewSpringArm = TopViewCamera->GetSpringArm()->TargetArmLength - 2000.f;
            TopViewCamera->GetSpringArm()->TargetArmLength = FMath::Clamp(NewSpringArm, 15000.f, 30000.f);
            HUDWidget->OnZoomButtonClicked();
        }
        break;
    case EViewMode::BACKVIEW:/*
        if (General != nullptr)
        {
            float NewSpringArm = 0;
            NewSpringArm = General->GetSpringArm()->TargetArmLength - 100.f;
            General->GetSpringArm()->TargetArmLength = FMath::Clamp(NewSpringArm, 100.0f, 700.f);
        }*/
        break;
    }
}

void AJIPlayerController::OnZoomInReleased()
{
    HUDWidget->OnZoomButtonReleased();
   
}

void AJIPlayerController::OnZoomOut()
{
    switch (CurrentView)
    {
    case EViewMode::TOPVIEW:
        if (TopViewCamera != nullptr)
        {
            float NewSpringArm = 0;
            NewSpringArm = TopViewCamera->GetSpringArm()->TargetArmLength + 2000.f;
            TopViewCamera->GetSpringArm()->TargetArmLength = FMath::Clamp(NewSpringArm, 15000.f, 30000.f);
            HUDWidget->OnZoomButtonClicked();
        }
        break;
    case EViewMode::BACKVIEW:

        break;
    }
}

void AJIPlayerController::OnZoomOutReleased()
{
    HUDWidget->OnZoomButtonReleased();
}

void AJIPlayerController::MoveForward(float NewAxisValue)
{
    switch (CurrentView)
    {
    case EViewMode::TOPVIEW:
        if (TopViewCamera != nullptr)
            TopViewCamera->MoveForward(NewAxisValue);
        break;
    case EViewMode::BACKVIEW:
        for (auto& Generals : g_mapPlayer)
        {
            if (CurrentView == EViewMode::BACKVIEW && Generals.Value->m_bIsOwner)
            {
                Generals.Value->MoveForward(NewAxisValue);

                //cs_packet_move packet;
                //packet.type = C2S_MOVE;
                //packet.size = sizeof(cs_packet_move);
                //
                //FVector pos = Generals.Value->GetActorLocation();
                //FVector vel = Generals.Value->GetVelocity();
                //
                //packet.x = pos.X;
                //packet.y = pos.Y;
                //packet.z = pos.Z;
                //packet.velx = vel.X;
                //packet.vely = vel.Y;
                //packet.velz = vel.Z;
                //packet.m_id = Generals.Value->m_iId;
                //packet.objtype = Generals.Value->m_eObjType;
                //packet.dir = D_UP;
                //packet.TurnValue = GetControlRotation().Yaw;
                //packet.RotValue = NewAxisValue;
                //PacketMgr::GetInst()->send_packet(&packet);
            }
        }
        break;
    }
}

void AJIPlayerController::MoveRight(float NewAxisValue)
{
    switch (CurrentView)
    {
    case EViewMode::TOPVIEW:
        if (TopViewCamera != nullptr)
            TopViewCamera->AddMovementInput(AActor::GetActorRightVector(), NewAxisValue);
        break;
    case EViewMode::BACKVIEW:
        for (auto& Generals : g_mapPlayer)
        {
            if (CurrentView == EViewMode::BACKVIEW && Generals.Value->m_bIsOwner)
            {
                Generals.Value->MoveRight(NewAxisValue);

                //cs_packet_move packet;
                //packet.type = C2S_MOVE;
                //packet.size = sizeof(cs_packet_move);
                //
                //FVector pos = Generals.Value->GetActorLocation();
                //FVector vel = Generals.Value->GetVelocity();
                //
                //packet.x = pos.X;
                //packet.y = pos.Y;
                //packet.z = pos.Z;
                //packet.velx = vel.X;
                //packet.vely = vel.Y;
                //packet.velz = vel.Z;
                //packet.m_id = Generals.Value->m_iId;
                //packet.objtype = Generals.Value->m_eObjType;
                //packet.dir = D_RIGHT;
                //packet.TurnValue = GetControlRotation().Yaw;
                //packet.RotValue = NewAxisValue;
                //PacketMgr::GetInst()->send_packet(&packet);
                //
                //JILOG(Warning, TEXT("%d"), 0);


                //AddMovementInput(FRotationMatrix(FRotator(0.0f, GetControlRotation().Yaw, 0.0f)).GetUnitAxis(EAxis::X), NewAxisValue);
            }
        }
        break;
    }
}

void AJIPlayerController::Roll(float NewAxisValue)
{
    switch (CurrentView)
    {
    case EViewMode::TOPVIEW:
        if (TopViewCamera != nullptr)
            TopViewCamera->Roll(NewAxisValue);
        break;
    }
}


void AJIPlayerController::LookUp(float NewAxisValue)
{
    for (auto& Generals : g_mapPlayer)
    {
        if (CurrentView == EViewMode::BACKVIEW && Generals.Value->m_bIsOwner)
            Generals.Value->LookUp(NewAxisValue);
    }

    /*if (CurrentView == EViewMode::BACKVIEW && General != nullptr)
    {
       General->LookUp(NewAxisValue);
    }*/
}

void AJIPlayerController::Turn(float NewAxisValue)
{
    //서버 부분
    for (auto& Generals : g_mapPlayer)
    {
        if (CurrentView == EViewMode::BACKVIEW && Generals.Value->m_bIsOwner)
        {
            Generals.Value->Turn(NewAxisValue);
            Generals.Value->m_fTurnValue = NewAxisValue;
        }
    }

    //클라부분
    //if (CurrentView == EViewMode::BACKVIEW && General != nullptr)
    //{
    //   General->Turn(NewAxisValue);
    //}
}


void  AJIPlayerController::MoveLeftRight(float NewAxisValue)
{
    if (CurrentView == EViewMode::TOPVIEW && TopViewCamera != nullptr)
    {
        float x, y;
        GetMousePosition(x, y);
        FVector MouseDir = EdgeScoll(x, y);
        FVector Axis = FVector(0.0f, SCREEN_SCROOL_SPEED, 0.0f);
        TopViewCamera->AddActorLocalOffset(MouseDir * Axis );
    }
}

void AJIPlayerController::MoveUpDown(float NewAxisValue)
{
    if (CurrentView == EViewMode::TOPVIEW && TopViewCamera != nullptr)
    {
        float x, y;
        GetMousePosition(x, y);
        FVector MouseDir = EdgeScoll(x, y);
        //수직으로 내려다볼때
        //FVector Axis = FVector(0.0f, 0.0f, 1.0f);
        //45도로 볼때
        FVector Axis = FVector(SCREEN_SCROOL_SPEED, 0.0f, SCREEN_SCROOL_SPEED);
        TopViewCamera->AddActorLocalOffset(MouseDir * Axis);
       //TopViewCamera->CurrentLocationIndicatorInMinimap->AddLocalOffset(MouseDir * Axis);
    }
}

void AJIPlayerController::SetViewMode(EViewMode NewViewMode)
{
    CurrentView = NewViewMode;
    switch (CurrentView)
    {
    case EViewMode::TOPVIEW:
        bShowMouseCursor = true;
        if (TopViewCamera != nullptr)
        {
            USpringArmComponent* TopViewCameralSpringArm = TopViewCamera->GetSpringArm();
            TopViewCamera->ResetTargetArmLengthDefault();
            MiniMapWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
            HUDWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
            OceanWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
            ChattingWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
            GeneralHUDWidget->SetVisibility(ESlateVisibility::Hidden);
             for (auto& Japans : g_mapJapanSoldier)
             {
                 if (Japans.Value != nullptr)
                     Japans.Value->HPBarWidget->SetHiddenInGame(true);
             }
             for (auto& Joseons : g_mapJoseonSoldier)
             {
                 if (Joseons.Value != nullptr)
                     Joseons.Value->HPBarWidget->SetHiddenInGame(true);
             }
        }
        break;
    case EViewMode::BACKVIEW:
        bShowMouseCursor = false;
        //서버 부분
        for (auto& Generals : g_mapPlayer)
        {
            if (Generals.Value != nullptr && Generals.Value->m_bIsOwner)
            {
                USpringArmComponent* GeneralSpringArm = Generals.Value->GetSpringArm();
                GeneralSpringArm->TargetArmLength = 300.f;
                GeneralSpringArm->SetRelativeRotation(FRotator::ZeroRotator);
                GeneralSpringArm->bUsePawnControlRotation = true;
                GeneralSpringArm->bInheritPitch = true;
                GeneralSpringArm->bInheritYaw = true;
                GeneralSpringArm->bInheritRoll = true;
                GeneralSpringArm->bDoCollisionTest = true;
                Generals.Value->bUseControllerRotationYaw = false;
                Generals.Value->HPBarWidget->SetHiddenInGame(false);

                Generals.Value->ResetTargetArmLengthDefault();
                Generals.Value->GetCharacterMovement()->bOrientRotationToMovement = true;
                Generals.Value->GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.f, 0.0f);
                //위젯
                MiniMapWidget->SetVisibility(ESlateVisibility::Hidden);
                HUDWidget->SetVisibility(ESlateVisibility::Hidden);
                OceanWidget->SetVisibility(ESlateVisibility::Hidden);
                ChattingWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
                GeneralHUDWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
            }
        }
       
        for (auto& Japans : g_mapJapanSoldier)
        {
            if (Japans.Value != nullptr)
                Japans.Value->HPBarWidget->SetHiddenInGame(false);
        }
        for (auto& Joseons : g_mapJoseonSoldier)
        {
            if (Joseons.Value != nullptr)
                Joseons.Value->HPBarWidget->SetHiddenInGame(false);
        }
        break;
    }
}

//void AJIPlayerController::SetViewMode(EViewMode NewViewMode)
//{
//    CurrentView = NewViewMode;
//    switch (CurrentView)
//    {
//    case EViewMode::TOPVIEW:
//        bShowMouseCursor = true;
//        if (TopViewCamera != nullptr)
//        {
//            USpringArmComponent* TopViewCameralSpringArm = TopViewCamera->GetSpringArm();
//            TopViewCamera->ResetTargetArmLengthDefault();
//            MiniMapWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
//            HUDWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
//            OceanWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
//            ChattingWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
//            GeneralHUDWidget->SetVisibility(ESlateVisibility::Hidden);
//
//          // for (auto& Generals : g_mapPlayer)
//          // {
//          //     if (Generals.Value != nullptr)
//          //         Generals.Value->HPBarWidget->SetHiddenInGame(true);
//          // }
//        }
//        break;
//    case EViewMode::BACKVIEW:
//        bShowMouseCursor = false;
//        //서버 부분
//        for (auto& Generals : g_mapPlayer)
//        {
//            if (Generals.Value != nullptr)
//            {
//                USpringArmComponent* GeneralSpringArm = Generals.Value->GetSpringArm();
//                GeneralSpringArm->TargetArmLength = 300.f;
//                GeneralSpringArm->SetRelativeRotation(FRotator::ZeroRotator);
//                GeneralSpringArm->bUsePawnControlRotation = true;
//                GeneralSpringArm->bInheritPitch = true;
//                GeneralSpringArm->bInheritYaw = true;
//                GeneralSpringArm->bInheritRoll = true;
//                GeneralSpringArm->bDoCollisionTest = true;
//                Generals.Value->bUseControllerRotationYaw = false;
//                Generals.Value->HPBarWidget->SetHiddenInGame(false);
//
//                Generals.Value->ResetTargetArmLengthDefault();
//               // Generals.Value->GetCharacterMovement()->bOrientRotationToMovement = true;
//               // Generals.Value->GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.f, 0.0f);
//
//                //위젯
//                MiniMapWidget->SetVisibility(ESlateVisibility::Hidden);
//                HUDWidget->SetVisibility(ESlateVisibility::Hidden);
//                OceanWidget->SetVisibility(ESlateVisibility::Hidden);
//                ChattingWidget->SetVisibility(ESlateVisibility::Hidden);
//                GeneralHUDWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
//            }
//        }
//        break;
//    }
//}


FVector AJIPlayerController::EdgeScoll(float MouseX, float MouseY)
{
    float LocationX, LocationY;
    float EdgeScrollSpeedX, EdgeScrollSpeedY;
    float Roll = GetControlRotation().Roll;
    int32 ViewportSizeX, ViewportSizeY;

    GetMousePosition(LocationX, LocationY);
    GetViewportSize(ViewportSizeX, ViewportSizeY);

    if (LocationX / ViewportSizeX >= 0.99)
        EdgeScrollSpeedX = ScrollSpeed;
    else if (LocationX / ViewportSizeX <= 0.01)
        EdgeScrollSpeedX = -ScrollSpeed;
    else
        EdgeScrollSpeedX = 0.0f;

    if (LocationY / ViewportSizeY >= 0.99)
        EdgeScrollSpeedY = -ScrollSpeed;
    else if (LocationY / ViewportSizeY <= 0.01)
        EdgeScrollSpeedY = ScrollSpeed;
    else
        EdgeScrollSpeedY = 0.0f;

    //90도에서 내려다볼때 
    //FVector Dir = FVector(0.0f, EdgeScrollSpeedX, EdgeScrollSpeedY);

    //45도로 기울였을때 
    FVector Dir = FVector(EdgeScrollSpeedY / 2, EdgeScrollSpeedX, EdgeScrollSpeedY / 2);
    return Dir;
}

void AJIPlayerController::OnGamePause()
{
    cs_packet_pause packet;
    packet.size = sizeof(cs_packet_pause);
    packet.type = C2S_PAUSE;
    PacketMgr::GetInst()->send_packet(&packet);
    HUDWidget->OnOptionButtonClicked();
}

void AJIPlayerController::OnGamePauseReleased()
{
    HUDWidget->OnOptionButtonReleased();
}

void AJIPlayerController::ChangeInputMode(bool bGameMode)
{
    if (bGameMode)
    {
        SetInputMode(GameAndUIInputMode);
        bShowMouseCursor = true;
    }
    else
    {
        SetInputMode(UIInputMode);
        bShowMouseCursor = true;
    }
}

void AJIPlayerController::Pause()
{
     MenuWidget = CreateWidget<UJIMenuWidget>(this, MenuWidgetClass);
     JICHECK(nullptr != MenuWidget);
     MenuWidget->AddToViewport(2);
     SetPause(true);
     ChangeInputMode(false);
}

void AJIPlayerController::Resume()
{
    if (MenuWidget != nullptr)
        MenuWidget->Resume();
    HUDWidget->OnOptionButtonReleased();
}
void AJIPlayerController::GoToTitle()
{
    UGameplayStatics::OpenLevel(GetWorld(), TEXT("Title"));
}