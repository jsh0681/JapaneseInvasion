#include "JIReEntryWidget.h"
#include "Components/Button.h"
class AJIUIPlayerController;

void UJIReEntryWidget::NativeConstruct()
{
	OnBtnConfirm = Cast<UButton>(GetWidgetFromName(TEXT("btnConfirm")));
	if (OnBtnConfirm != nullptr)
	{
		OnBtnConfirm->OnHovered.AddDynamic(this, &UJIReEntryWidget::OnBtnConfirmHovered);
		OnBtnConfirm->OnClicked.AddDynamic(this, &UJIReEntryWidget::OnBtnConfirmClicked);
	}
}

void UJIReEntryWidget::OnBtnConfirmHovered()
{
	//JILOG(Warning, TEXT("OnBtnConfirmHovered"));
	if(HoveredAudioComponent!=nullptr)
		HoveredAudioComponent->Play(0.f);
}

void UJIReEntryWidget::OnBtnConfirmClicked()
{
	//JILOG(Warning, TEXT("OnBtnConfirmClicked"));
	UGameplayStatics::OpenLevel(GetWorld(), "Title", true);
}

void UJIReEntryWidget::SetAudioComponent(UAudioComponent* Audio)
{
	HoveredAudioComponent = Audio;
}