﻿#include "JIShip.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetInputLibrary.h"
#include "JIKoreaShipStatComponent.h"
#include "JIKoreaShipWidget.h"
#include "JIReloadWidget.h"
#include "JIPlayerController.h"
#include "JIWeapon.h"
#include "Kismet/GameplayStatics.h"
#include "JIOcean.h"
#include "JIBullet.h"
#include "JIBomb.h"
#include "Components/WidgetComponent.h"
#include "JIShipSetting.h"
#include "JIGameInstance.h"
#include "PacketMgr.h"

#define JOSEON_BOMB_RANGE 30000
#define JAPAN_BULLET_RANGE 20000

FVector TempShootLocation;

AJIShip::AJIShip() :ShootRange(20000.f)
{
    PrimaryActorTick.bCanEverTick = true;
    

    Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Ship");
    Mesh->SetNotifyRigidBodyCollision(true);
    RootComponent = Mesh;

    /*이동을 위한 컴퍼넌트인 UFloatingPawnMovement를 추가해준다*/
    Movement = CreateDefaultSubobject<UFloatingPawnMovement>("Movement");
    /////////////////////////////////////////////////////////////////////////
    ShipStat = CreateDefaultSubobject<UJIKoreaShipStatComponent>(TEXT("SHIPSTAT"));
    /////////////////////////////////////////////////////////////////////////
    ShipBarWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("SHIPBARWIDGET"));
    ShipBarWidget->SetupAttachment(Mesh);
    ShipBarWidget->SetRelativeLocation(FVector(0.0f, 0.f, 2000.0f));
    ShipBarWidget->SetWidgetSpace(EWidgetSpace::Screen);
    ShipBarWidget->SetCollisionProfileName(TEXT("NoCollision"));
    static ConstructorHelpers::FClassFinder<UUserWidget> UI_BAR(TEXT("/Game/Custom/UI/UI_ShipBar"));
    if (UI_BAR.Succeeded())
    {
        ShipBarWidget->SetWidgetClass(UI_BAR.Class);
        ShipBarWidget->SetDrawSize(FVector2D(80.f, 26.f));
    }

    /*장전중을 보여주기 위한 컴퍼넌트인 JIReloadWidget를 추가해준다*/
    ReloadWidget = CreateDefaultSubobject<UJIReloadWidget>(TEXT("RELOADWIDGET"));
    ReloadBarWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("RELOADBARWIDGET"));
    ReloadBarWidget->SetupAttachment(RootComponent);
    ReloadBarWidget->SetRelativeLocation(FVector(0.0f, 0.0f, 1000.0f));
    ReloadBarWidget->SetWidgetSpace(EWidgetSpace::Screen);
    ReloadBarWidget->SetCollisionProfileName(TEXT("NoCollision"));
    ReloadBarWidget->SetVisibility(false);
    static ConstructorHelpers::FClassFinder<UUserWidget> UI_RELOAD(TEXT("/Game/Custom/UI/UI_Reload"));
    if (UI_RELOAD.Succeeded())
    {
        ReloadBarWidget->SetWidgetClass(UI_RELOAD.Class);
        ReloadBarWidget->SetDrawSize(FVector2D(100.f, 100.f));
    }

    /*부력을 위한 컴퍼넌트인 UBuoyancyForceComponent를 추가해준다*/
    BuoyancyForce = CreateDefaultSubobject<UBuoyancyForceComponent>("BuoyancyForce");
    BuoyancyForce->SetupAttachment(RootComponent);
    BuoyancyForce->TestPoints.Add(FVector(-1400.f, -500.0f, 50.f));
    BuoyancyForce->TestPoints.Add(FVector(1400.f, -500.0f, 50.f));
    BuoyancyForce->TestPoints.Add(FVector(1400.f, 500.0f, 50.f));
    BuoyancyForce->TestPoints.Add(FVector(-1400.f, 500.0f, 50.f));
    BuoyancyForce->TestPoints.Add(FVector(0.0f, 500.0f, 50.f));
    BuoyancyForce->TestPoints.Add(FVector(0.0f, -500.0f, 50.f));

    /*포탄의 발사가 가능한 범위를 알려주는 Decal*/
    fireRange = CreateDefaultSubobject<UStaticMeshComponent>("FireRange");
    fireRange->SetupAttachment(RootComponent);

    static ConstructorHelpers::FObjectFinder<UStaticMesh> FireRangeMesh(TEXT("/Game/Custom/StaticMesh/FireRange"));
    if (FireRangeMesh.Succeeded())
        fireRange->SetStaticMesh(FireRangeMesh.Object);

    fireRange->SetRelativeLocation(FVector(0.0f, 0.0f, 300.f));
    fireRange->SetRelativeScale3D(FVector(400.0f, 400.f, 1.f));
    fireRange->SetVisibility(true);
    fireRange->SetupAttachment(RootComponent);
    fireRange->SetCollisionProfileName(TEXT("NoCollision"));
    fireRange->bAbsoluteRotation = true;

    ///////////////////////////////////////////////////////////////////////////
    SelectionCircle = CreateDefaultSubobject<UStaticMeshComponent>("SelectionCircle");
    SelectionCircle->SetupAttachment(RootComponent);

    static ConstructorHelpers::FObjectFinder<UStaticMesh> MT_CIRCLE(TEXT("/Game/Custom/StaticMesh/SelectionMesh"));
    if (MT_CIRCLE.Succeeded())
        SelectionCircle->SetStaticMesh(MT_CIRCLE.Object);
    SelectionCircle->SetRelativeScale3D(FVector(15.0f, 15.0f, 1.0f));
    SelectionCircle->SetRelativeLocation(FVector(0.0f, 0.0f, 200.f));
    SelectionCircle->bAbsoluteRotation = true;

    ///////////////////////////////////////////////////////////////////////////
    MinimapVisualPoint = CreateDefaultSubobject<UStaticMeshComponent>("MinimapPoint");
    static ConstructorHelpers::FObjectFinder<UStaticMesh> ST_MINIMAPMESH(TEXT("/Game/Custom/StaticMesh/MinimapSphere"));
    if (ST_MINIMAPMESH.Succeeded())
        MinimapVisualPoint->SetStaticMesh(ST_MINIMAPMESH.Object);
    MinimapVisualPoint->SetupAttachment(RootComponent);
    MinimapVisualPoint->SetRelativeLocation(FVector(0.0f, 0.0f, 100000.f));
    MinimapVisualPoint->SetRelativeScale3D(FVector(100.f, 100.f, 100.f));
    MinimapVisualPoint->SetCollisionProfileName(TEXT("NoCollision"));
    MinimapVisualPoint->bAbsoluteRotation = true;

    ///////////////////////////////////////////////////////////////////////////
    FireAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("FireAudioComponent"));
    FireAudioComponent->SetupAttachment(RootComponent);
    FireAudioComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 300.0f));
    static ConstructorHelpers::FObjectFinder<USoundCue> SC_ExplosionCue(TEXT("/Game/Custom/Sound/Fire_Cue"));
    if (SC_ExplosionCue.Succeeded())
        FireSoundCue = SC_ExplosionCue.Object;
    ///////////////////////////////////////////////////////////////////////////
    BombLoadAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("BombLoadAudioComponent"));
    BombLoadAudioComponent->SetupAttachment(RootComponent);
    BombLoadAudioComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 300.0f));
    static ConstructorHelpers::FObjectFinder<USoundCue> SC_BombLoad(TEXT("SoundCue'/Game/Custom/Sound/BombLoad_Cue.BombLoad_Cue'"));
    if (SC_BombLoad.Succeeded())
        BombLoadSoundCue = SC_BombLoad.Object;

    ///////////////////////////////////////////////////////////////////////////
    static ConstructorHelpers::FObjectFinder<USoundCue> SC_GunFireCue(TEXT("/Game/Custom/Sound/GunFire_Cue"));
    if (SC_GunFireCue.Succeeded())
        GunFireSoundCue = SC_GunFireCue.Object;
    ///////////////////////////////////////////////////////////////////////////
    RangeOutAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("RangeOutAudioComponent"));
    RangeOutAudioComponent->SetupAttachment(RootComponent);
    static ConstructorHelpers::FObjectFinder<USoundCue> SC_RangeOutCue(TEXT("/Game/Custom/Sound/sound222/RangeOut_Cue"));
    if (SC_RangeOutCue.Succeeded())
        RangeOutSoundCue = SC_RangeOutCue.Object;

    ///////////////////////////////////////////////////////////////////////////
    GunEmptyAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("GunEmptyAudioComponent"));
    GunEmptyAudioComponent->SetupAttachment(RootComponent);
    static ConstructorHelpers::FObjectFinder<USoundCue> SC_GunEmpty(TEXT("/Game/Custom/Sound/sound222/GunEmpty_Cue"));
    if (SC_GunEmpty.Succeeded())
        GunEmptySoundCue = SC_GunEmpty.Object;

    ///////////////////////////////////////////////////////////////////////////
    GunReloadAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("GunReloadAudioComponent"));
    GunReloadAudioComponent->SetupAttachment(RootComponent);
    static ConstructorHelpers::FObjectFinder<USoundCue> SC_GunReload(TEXT("/Game/Custom/Sound/sound222/GunReload_Cue"));
    if (SC_GunReload.Succeeded())
        GunReloadSoundCue = SC_GunReload.Object;

    ///////////////////////////////////////////////////////////////////////////
    loadedAmmo = 30;
    ammoPool = 30;
    bOnce = true;
    m_fShipSpeed = 0.f;
    bIsMoving = false;
    bCanSend = false;
    bCanFire = false;
}

UUserWidget* AJIShip::GetShipWidget()
{
    return ShipBarWidget->GetUserWidgetObject();
}

void AJIShip::SetAsset()
{
    //동적으로 에셋 할당
    auto DefaultSetting = GetDefault<UJIShipSetting>();
    ShipAssetToLoad = DefaultSetting->ShipAssets[AssetIndex];
    auto JIGameInstance = Cast<UJIGameInstance>(GetGameInstance());
    if (JIGameInstance != nullptr)
        AssetStreamingHandle = JIGameInstance->StreamableManager.RequestAsyncLoad(ShipAssetToLoad, FStreamableDelegate::CreateUObject(this, &AJIShip::OnAssetLoadCompleted));
}

void AJIShip::BeginPlay()
{
    Super::BeginPlay();
    Dest = FVector::ZeroVector;

    auto ShipWidget = Cast<UJIKoreaShipWidget>(ShipBarWidget->GetUserWidgetObject());
    if (ShipWidget != nullptr)
        ShipWidget->BindKoreaShipStat(ShipStat);

    TArray<AActor*> FoundActors;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), AJIOcean::StaticClass(), FoundActors);
    for (auto Actor : FoundActors) {
        AJIOcean* FoundActor = Cast<AJIOcean>(Actor);
        if (FoundActor)
            OceanPtr = FoundActor;
    }

    if (RangeOutAudioComponent && RangeOutSoundCue)
        RangeOutAudioComponent->SetSound(RangeOutSoundCue);

    if (GunEmptyAudioComponent && GunEmptySoundCue)
        GunEmptyAudioComponent->SetSound(GunEmptySoundCue);

    if (GunReloadAudioComponent && GunReloadSoundCue)
        GunReloadAudioComponent->SetSound(GunReloadSoundCue);

    if (BombLoadAudioComponent && BombLoadSoundCue)
        BombLoadAudioComponent->SetSound(BombLoadSoundCue);
}

void AJIShip::SetDetailOption()
{
    if (m_eTeamColor == TEAM_COLOR::JOSEON_1 || m_eTeamColor == TEAM_COLOR::JOSEON_2)
    {
        FireAudioComponent->SetSound(FireSoundCue);
    }
    else if (m_eTeamColor == TEAM_COLOR::JAPAN_1 || m_eTeamColor == TEAM_COLOR::JAPAN_2)
    {
        FireAudioComponent->SetSound(GunFireSoundCue);
    }
    FireAudioComponent->FadeOut(10.f, 1000000.f);
}

void AJIShip::OnAssetLoadCompleted()
{
    AssetStreamingHandle->ReleaseHandle();
    TSoftObjectPtr<UStaticMesh> LoadedAssetPath(ShipAssetToLoad);
    if (LoadedAssetPath.IsValid())
        Mesh->SetStaticMesh(LoadedAssetPath.Get());
}

void AJIShip::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if (m_eTeamColor == TEAM_COLOR::JOSEON_1 || m_eTeamColor == TEAM_COLOR::JOSEON_2)
        fireRange->SetRelativeScale3D(FVector(600.0f, 600.f, 1.f));
    else if (m_eTeamColor == TEAM_COLOR::JAPAN_1 || m_eTeamColor == TEAM_COLOR::JAPAN_2)
        fireRange->SetRelativeScale3D(FVector(400.0f, 400.0f, 1.f));

    if (bIsMoving)
    {
        Mesh->SetSimulatePhysics(true);
        BuoyancyForce->FluidLinearDamping = 1.f;
        BuoyancyForce->FluidAngularDamping = 1.f;
    }
    else //if (!bIsMoving && !bIsBackView)
    {
        Mesh->SetSimulatePhysics(false);
        BuoyancyForce->FluidLinearDamping = 1.0f;//25
        BuoyancyForce->FluidAngularDamping = 1.0f;
    }
    float Distance = 0.f;

    if (ShipStat->CurrentHP <= 0)
    {
        g_mapShip.Remove(m_iId);
        return;
    }

    EViewMode CurrentView = Cast<AJIPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0))->GetViewMode();

    if (CurrentView == EViewMode::TOPVIEW) {
        MinimapVisualPoint->SetVisibility(true);
        if (ShipBarWidget != nullptr)
            ShipBarWidget->SetVisibility(true);

        if (bIsSelection)
            SelectionCircle->SetVisibility(true);
        else
            SelectionCircle->SetVisibility(false);
    }
    else if (CurrentView == EViewMode::BACKVIEW) {
        MinimapVisualPoint->SetVisibility(false);
        if (ShipBarWidget != nullptr)
            ShipBarWidget->SetVisibility(false);
    }
    if (bOnRangeDecal)
        fireRange->SetVisibility(true);
    else
        fireRange->SetVisibility(false);

    if (bIsMoving)
    {
        Start = GetActorLocation();
        Dest.Z = GetActorLocation().Z;
        Start.Z = 0.0f;
        FVector Direction = Dest - Start;
        FVector GoalDir = Direction.GetSafeNormal();
        GoalDir.Z = 0.0f;

        float dot = FVector::DotProduct(GetActorForwardVector(), GoalDir);
        float AcosAngle = FMath::Acos(dot);
        float angle = FMath::RadiansToDegrees(AcosAngle);
        // dot한 값을 아크코사인 계산해 주면 0 ~ 180도 사이의 값 (0 ~ 1)의 양수 값만 나온다.
        //그값은 degrees 값인데 이것에 1라디안을 곱해주면 60분법의 도가 나온다.
        //여기서 두 백터를 크로스 하여 회전할 축을 얻게 된다.
        //이 크로스 백터는 Axis회전의 회전축이 되며 , 그 양수 음수로 회전 방향 왼쪽(음수), 오른쪽(양수)를 알수 있다.

        FVector cross = FVector::CrossProduct(GetActorForwardVector(), GoalDir);
        float TurnAngle = 0.0f;
        if (cross.Z > 0)
            TurnAngle = angle * DeltaTime * 4;
        else if (cross.Z < 0)
            TurnAngle = -angle * DeltaTime * 4;

        FRotator deltaRotation = FRotator(0, TurnAngle, 0);   //Yaw
        Distance = FVector::Dist(Dest, Start);
        if (Distance >= 200.0f)
        {
            //RootComponent->AddWorldRotation(deltaRotation);
            //SetActorLocation(Start + (GoalDir * m_fShipSpeed)+FVector((OceanPtr->GlobalWaveDirection*OceanPtr->GlobalWaveSpeed),0.0f));
            //FVector temp = (GoalDir * m_fShipSpeed) + FVector((OceanPtr->GlobalWaveDirection * OceanPtr->GlobalWaveSpeed), 0.0f));

            //JILOG(Warning, TEXT("배의 속도 : %f, %f"), (GoalDir * m_fShipSpeed).X, (GoalDir * m_fShipSpeed).Y);
            //JILOG(Warning, TEXT("바다 속도 : %f, %f"), FVector((OceanPtr->GlobalWaveDirection * OceanPtr->GlobalWaveSpeed), 0.0f).X, FVector((OceanPtr->GlobalWaveDirection * OceanPtr->GlobalWaveSpeed), 0.0f).Y);
            //JILOG(Warning, TEXT("현재 속도 : %f, %f"), temp.X, temp.Y)
            //서버부분
            if (m_iId == -1)
            {
                RootComponent->AddWorldRotation(deltaRotation);
                SetActorLocation(Start + (GoalDir * m_fShipSpeed) + FVector((OceanPtr->GlobalWaveDirection * OceanPtr->GlobalWaveSpeed) * 5, 0.0f));
            }
            else if (!bCanSend)
            {
                Timer();
                FVector vShip = Start + (GoalDir * m_fShipSpeed) + (FVector((OceanPtr->GlobalWaveDirection * OceanPtr->GlobalWaveSpeed) * 5, 0.0f));
                cs_packet_ship_move packet;

                packet.type = C2S_SHIP_MOVE;
                packet.size = sizeof(cs_packet_ship_move);
                packet.x = vShip.X;
                packet.y = vShip.Y;
                packet.z = vShip.Z;
                packet.TurnValue = TurnAngle;
                packet.objtype = OBJ_TYPE::SHIP;
                packet.m_id = m_iId;

                PacketMgr::GetInst()->send_packet(&packet);
            }
        }
        else
            bIsMoving = false;
    }
}

void AJIShip::Timer()
{
    bCanSend = true;
    GetWorldTimerManager().SetTimer(TimerHandle, this, &AJIShip::TimeAdd, 0.04f, false);
}

void AJIShip::TimeAdd()
{
    bCanSend = false;
}

void AJIShip::FireCoolTimer()
{
    bCanFire = true;
    GetWorldTimerManager().SetTimer(FireTimerHandle, this, &AJIShip::FireCoolTimeAdd, 0.5f, false);
}

void AJIShip::FireCoolTimeAdd()
{
    bCanFire = false;
}

void AJIShip::SetSelection(bool tf)
{
    bIsSelection = tf;
}

void AJIShip::SetIsMoving(bool tf)
{
    bIsMoving = tf;
}

void AJIShip::PostInitializeComponents()
{
    Super::PostInitializeComponents();
    ShipStat->OnHPIsZero.AddLambda([this]() ->void {
        //JILOG(Warning, TEXT("Ship Dead"));
        Mesh->SetCollisionProfileName("NoCollision");
        SetLifeSpan(2.0f);
        ShipBarWidget->SetVisibility(false);
        });
}

float AJIShip::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
    //JILOG(Warning, TEXT("AJIShip TakeDamage"));
    float FinalDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

    cs_packet_set_hp packet;
    packet.size = sizeof(cs_packet_set_hp);
    packet.type = C2S_SET_HP;
    packet.id = m_iId;
    packet.objtype = m_eObjType;

    if (DamageCauser->ActorHasTag(TEXT("BULLET")))
        packet.damagecauser = HIT_OBJ::BULLET;
    else if (DamageCauser->ActorHasTag(TEXT("SHELL")))
        packet.damagecauser = HIT_OBJ::SHELL;
    else
        return 0.f;

    PacketMgr::GetInst()->send_packet(&packet);

    return 0.f;
}

void AJIShip::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AJIShip::LoadBomb(FVector ShootLocation)
{
    if (m_eTeamColor == TEAM_COLOR::JOSEON_1 || m_eTeamColor == TEAM_COLOR::JOSEON_2)
    {
        if (bIsShellLoading)
        {
            GunEmptyAudioComponent->Play(0.f);
            return;
        }
        BombLoadAudioComponent->Play(0.4f);
        TempShootLocation = ShootLocation;
        GetWorldTimerManager().SetTimer(LoadShellHandle, this, &AJIShip::Fire, 1.0f, false);
        bIsShellLoading = true;
    }
    else if (m_eTeamColor == TEAM_COLOR::JAPAN_1 || m_eTeamColor == TEAM_COLOR::JAPAN_2)
    {
        TempShootLocation = ShootLocation;
        GetWorldTimerManager().SetTimer(LoadShellHandle, this, &AJIShip::Fire, 0.1f, false);
    }
}

void AJIShip::Fire()
{
    //if (bCanFire) 
    //   return;
    //FireCoolTimer();

    FVector ShootLocation = TempShootLocation;
    FVector CurrentMouseLocation = FVector(ShootLocation.X, ShootLocation.Y, ShootLocation.Z);
    FVector CurrentShipLocation = GetActorLocation();

    float Distance = FVector::Dist(CurrentShipLocation, CurrentMouseLocation);
    FVector Direction = (CurrentMouseLocation - CurrentShipLocation).GetSafeNormal(); //방향을 얻었다.

    float dot = FVector::DotProduct(GetActorForwardVector(), Direction);
    float AcosAngle = FMath::Acos(dot);    // dot한 값을 아크코사인 계산해 주면 0 ~ 180도 사이의 값 (0 ~ 1)의 양수 값만 나온다.
    float angle = FMath::RadiansToDegrees(AcosAngle); //그값은 degrees 값인데 이것에 1라디안을 곱해주면 60분법의 도가 나온다.
    //여기서 두 백터를 크로스 하여 회전할 축을 얻게 된다.
    //이 크로스 백터는 Axis회전의 회전축이 되며 , 그 양수 음수로 회전 방향 왼쪽(음수), 오른쪽(양수)를 알수 있다.
    FVector cross = FVector::CrossProduct(GetActorForwardVector(), Direction);
    float TurnAngle = 0.f;

    if (cross.Z > 0)
        TurnAngle = angle;
    else if (cross.Z < 0)
        TurnAngle = -angle;//TurnAngle = 360 - angle; //360에서 뺴게되면 양수로 각을 리턴하게 된다.

    //Mesh->AddImpulseAtLocation(Direction * 1000.f, GetActorLocation());

    if (m_eTeamColor == TEAM_COLOR::JOSEON_1 || m_eTeamColor == TEAM_COLOR::JOSEON_2)
    {
        float arcValue = 0.95f;                       // ArcParam (0.0-1.0)
        FVector ArrowVelocity = FVector::ZeroVector;   // 결과 Velocity
        //서버부분
        if (UGameplayStatics::SuggestProjectileVelocity_CustomArc(this, ArrowVelocity, GetActorLocation() + FVector(0.f, 0.0f, 300.0f), GetActorLocation() + Direction * Distance, GetWorld()->GetGravityZ(), arcValue))
        {
            // 수정
            //FireAudioComponent->Play(0.f);

            FRotator vRot = GetActorRotation().Add(0.0f, TurnAngle, 0.0f);
            FVector vPos = GetActorLocation() + FVector(0.f, 0.0f, 300.0f);

            // 위치 12, 회전 12, turn 4, arrowvelocity 12
            cs_packet_shell_spawn packet;
            packet.size = sizeof(cs_packet_shell_spawn);
            packet.type = C2S_SHELL_SPAWN;
            packet.posx = vPos.X;
            packet.posy = vPos.Y;
            packet.posz = vPos.Z;
            packet.rotx = vRot.Pitch;
            packet.roty = vRot.Yaw;
            packet.rotz = vRot.Roll;
            packet.velx = ArrowVelocity.X;
            packet.vely = ArrowVelocity.Y;
            packet.velz = ArrowVelocity.Z;
            packet.id = m_iId;
            PacketMgr::GetInst()->send_packet(&packet);
            bIsShellLoading = false;
        }
    }
    else if (m_eTeamColor == TEAM_COLOR::JAPAN_1 || m_eTeamColor == TEAM_COLOR::JAPAN_2)
    {
        float arcValue = 0.98f;                       // ArcParam (0.0-1.0)
        FVector ArrowVelocity = FVector::ZeroVector;   // 결과 Velocity
        //서버부분
        if (UGameplayStatics::SuggestProjectileVelocity_CustomArc(this, ArrowVelocity, GetActorLocation() + FVector(0.f, 0.0f, 500.0f), GetActorLocation() + Direction * Distance, GetWorld()->GetGravityZ(), arcValue))
        {
            // 수정
            //FireAudioComponent->Play(0.f);

            FRotator vRot = GetActorRotation().Add(0.0f, TurnAngle, 0.0f);
            FVector vPos = GetActorLocation() + FVector(0.f, 0.0f, 500.0f);

            // 위치 12, 회전 12, turn 4, arrowvelocity 12
            cs_packet_bullet_spawn packet;
            packet.size = sizeof(cs_packet_bullet_spawn);
            packet.type = C2S_BULLET_SPAWN;
            packet.posx = vPos.X;
            packet.posy = vPos.Y;
            packet.posz = vPos.Z;
            packet.rotx = vRot.Pitch;
            packet.roty = vRot.Yaw;
            packet.rotz = vRot.Roll;
            packet.velx = ArrowVelocity.X;
            packet.vely = ArrowVelocity.Y;
            packet.velz = ArrowVelocity.Z;
            packet.id = m_iId;
            PacketMgr::GetInst()->send_packet(&packet);
        }
    }
}

void AJIShip::OnReload()
{
    if (ammoPool <= 0 || loadedAmmo >= 30)
        return;

    GunReloadAudioComponent->Play(0.f);
    ReloadBarWidget->SetVisibility(true);
    loadedAmmo = 30;
    GetWorldTimerManager().SetTimer(UnusedHandle, this, &AJIShip::Reloading, 3.0f, false);
}

void AJIShip::Reloading()
{
    ReloadBarWidget->SetVisibility(false);
    ShipStat->SetReload(loadedAmmo);
}

bool AJIShip::IsInAttackRange(FVector Pos)
{
    float distance = FVector::Dist(GetActorLocation(), Pos);
    if (m_eTeamColor == TEAM_COLOR::JOSEON_1 || m_eTeamColor == TEAM_COLOR::JOSEON_2)
    {
        if (distance < JOSEON_BOMB_RANGE)
            return true;
    }
    else if (m_eTeamColor == TEAM_COLOR::JAPAN_1 || m_eTeamColor == TEAM_COLOR::JAPAN_2)
    {
        if (distance < JAPAN_BULLET_RANGE)
            return true;
    }
    if (RangeOutAudioComponent && RangeOutSoundCue)
        RangeOutAudioComponent->Play(0.f);
    return false;
}

void AJIShip::SetArrival(FVector MoveLocation)
{
    bIsMoving = true;
    Dest = FVector(MoveLocation.X, MoveLocation.Y, 0.0f);
}

void AJIShip::PossessedBy(AController* NewController)
{
    Super::PossessedBy(NewController);
}

void AJIShip::NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{

    //if (Other != nullptr&& bOnce)
    //{
    //   FDamageEvent DamageEvent;

    //   if (Other->ActorHasTag(TEXT("Japan")))//Actor
    //   {
    //      JILOG(Warning, TEXT("Notify Hit"));
    //      bIsMoving = false;
    //      Cast<AJIPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0))->OnTab();
    //      //Mesh->SetSimulatePhysics(false);
    //      //(Cast<AJIShip>(Other))->Mesh->SetSimulatePhysics(false);
    //      bOnce = false;
    //      for (auto& FoundShip : FoundActors)
    //      {
    //         AJIShip* Ship = Cast<AJIShip>(FoundShip);
    //         Ship->SetIsMoving(false);
    //         Ship->Mesh->SetSimulatePhysics(false);
    //      }
    //   }
    //}
    //JILOG(Warning, TEXT("Notify Hit"));

    TArray<AActor*> FoundActors;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), AJIShip::StaticClass(), FoundActors);
    //서버 부분
    if (Other != nullptr && bOnce)
    {
        FDamageEvent DamageEvent;

        if (((Other->ActorHasTag(TEXT("JOSEON_1")) || Other->ActorHasTag(TEXT("JOSEON_2"))) && (this->ActorHasTag(TEXT("JAPAN_1")) || this->ActorHasTag(TEXT("JAPAN_2")))) ||
            ((Other->ActorHasTag(TEXT("JAPAN_1")) || Other->ActorHasTag(TEXT("JAPAN_2"))) && (this->ActorHasTag(TEXT("JOSEON_1")) || this->ActorHasTag(TEXT("JOSEON_2")))))
        {

            Cast<AJIShip>(Other)->Mesh->SetSimulatePhysics(false);
            this->Mesh->SetSimulatePhysics(false);

            //JILOG(Warning, TEXT("Notify Hit"));
            bIsMoving = false;
            //Cast<AJIPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0))->OnTab();
            //Mesh->SetSimulatePhysics(false);
            //(Cast<AJIShip>(Other))->Mesh->SetSimulatePhysics(false);
            bOnce = false;
            for (auto& FoundShip : FoundActors)
            {
                AJIShip* Ship = Cast<AJIShip>(FoundShip);
                Ship->SetIsMoving(false);
                Ship->Mesh->SetSimulatePhysics(false);
                Ship->bIsBackView = true;
            }

            // 충돌한 놈들만 뷰전환 되야 한다.


            int other_id = -1;
            int my_id = -1;

            for (auto& Generals : g_mapPlayer)
            {
                if (Generals.Value->m_eTeamColor == m_eTeamColor) // 나
                {
                    my_id = Generals.Value->m_iId;
                    for (auto& U_Generals : g_mapPlayer)
                    {
                        if (U_Generals.Value->m_eTeamColor == Cast<AJIShip>(Other)->m_eTeamColor)
                        {
                            other_id = U_Generals.Value->m_iId;
                            break;
                        }
                    }
                }
            }

            for (auto& Generals : g_mapPlayer)
            {
                if (other_id != -1 && my_id != -1)
                {
                    //JILOG(Warning, TEXT("out general teamcolor : %d, ship teamcolor : %d"), (int)Generals.Value->m_eTeamColor, (int)m_eTeamColor);
                    if (Generals.Value->m_eTeamColor == m_eTeamColor && Generals.Value->m_bIsOwner)
                    {
                        //JILOG(Warning, TEXT("generals.id : %d, my.id : %d, other.id : %d"), (int)Generals.Value->m_iId, my_id, other_id);

                        cs_packet_soldier_gen sg_packet;
                        sg_packet.type = C2S_SOLDIER_GEN;
                        sg_packet.size = sizeof(cs_packet_soldier_gen);
                        sg_packet.my_general_id = my_id;
                        sg_packet.other_general_id = other_id;
                        sg_packet.ship_id = m_iId;
                        PacketMgr::GetInst()->send_packet(&sg_packet);

                        //Cast<AJIPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0))->OnTab();
                        cs_packet_view packet;
                        packet.type = C2S_VIEWMODE;
                        packet.size = sizeof(cs_packet_view);
                        packet.viewmode = M_BACKVIEW;
                        packet.teamcolor = (char)m_eTeamColor;
                        PacketMgr::GetInst()->send_packet(&packet);
                    }
                }
            }
        }
    }
}