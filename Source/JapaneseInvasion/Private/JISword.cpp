#include "JISword.h"

AJISword::AJISword()
{
	PrimaryActorTick.bCanEverTick = false;

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_SWORD(TEXT("/Game/Custom/StaticMesh/Weapon/Sword/SK_Sword"));
	if (SK_SWORD.Succeeded())
	{
		Weapon->SetSkeletalMesh(SK_SWORD.Object);
	}
	Weapon->SetCollisionProfileName(TEXT("NoCollision"));
	Weapon->SetRelativeLocation(FVector(0.0f, -50.f, 0.0f));
}

void AJISword::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void AJISword::BeginPlay()
{
	Super::BeginPlay();

	Weapon->SetAnimationMode(EAnimationMode::AnimationSingleNode);
	AnimAsset = LoadObject<UAnimationAsset>(nullptr, TEXT("/Game/Custom/StaticMesh/Weapon/Sword/SK_Sword_Anim"));
	if (AnimAsset != nullptr)
		Weapon->PlayAnimation(AnimAsset, true);

}

void AJISword::PlayAnimForTrail()
{
	Weapon->PlayAnimation(AnimAsset, true);
}