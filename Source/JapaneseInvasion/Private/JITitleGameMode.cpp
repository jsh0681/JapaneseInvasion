#include "JITitleGameMode.h"
#include "JIUIPlayerController.h"
#include "Sockets.h"
#include "SocketSubsystem.h"
#include "Networking/Public/Interfaces/IPv4/IPv4Address.h"
#include "protocol.h"
#include "PacketMgr.h"

AJITitleGameMode::AJITitleGameMode()
{
	PlayerControllerClass = AJIUIPlayerController::StaticClass();
}