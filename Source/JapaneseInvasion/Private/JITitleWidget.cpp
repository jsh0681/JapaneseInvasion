#include "JITitleWidget.h"
#include "Components/Button.h"
#include "JIPlayerController.h"
#include "Components/EditableTextBox.h"
#include "JICameraPawn.h"
#include "JIHUD.h"
#include "EngineUtils.h"

#include "Sockets.h"
#include "SocketSubsystem.h"
#include "Networking/Public/Interfaces/IPv4/IPv4Address.h"
#include "protocol.h"
#include "PacketMgr.h"

TCHAR* UserID;
FString CharacterName;
FString IPAddress;

extern bool g_bLogin;

UJITitleWidget::UJITitleWidget(const FObjectInitializer& ObjectInitializer) : UUserWidget(ObjectInitializer)
{
	g_Socket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(NAME_Stream, TEXT("default"), false);
	g_bLogin = false;
}

void UJITitleWidget::SetReEntryWidget(UJIReEntryWidget* widget)
{
	ReEntryWidget = widget;
}

void UJITitleWidget::NativeConstruct()
{
	Super::NativeConstruct();

	TextBox = Cast<UEditableTextBox>(GetWidgetFromName(TEXT("EditID")));
	JICHECK(TextBox != nullptr);
	TextBox->ClearKeyboardFocusOnCommit = true;
	TextBox->OnTextCommitted.AddDynamic(this, &UJITitleWidget::OnIDCommitted);

	TextIP = Cast<UEditableTextBox>(GetWidgetFromName(TEXT("EditIP")));
	JICHECK(TextIP != nullptr);

	OnBtnNewGame = Cast<UButton>(GetWidgetFromName(TEXT("btnNewGame")));
	if (OnBtnNewGame != nullptr)
	{
		OnBtnNewGame->OnHovered.AddDynamic(this, &UJITitleWidget::OnBtnNewGameHovered);
		OnBtnNewGame->OnClicked.AddDynamic(this, &UJITitleWidget::OnBtnNewGameClicked);
	}

	OnBtnContinue = Cast<UButton>(GetWidgetFromName(TEXT("btnContinue")));
	if (OnBtnContinue != nullptr)
	{
		OnBtnContinue->OnHovered.AddDynamic(this, &UJITitleWidget::OnBtnContinueHovered);
		OnBtnContinue->OnClicked.AddDynamic(this, &UJITitleWidget::OnBtnContinueClicked);
	}

	OnBtnExit = Cast<UButton>(GetWidgetFromName(TEXT("btnExit")));
	if (OnBtnExit != nullptr)
	{
		OnBtnExit->OnHovered.AddDynamic(this, &UJITitleWidget::OnBtnExitHovered);
		OnBtnExit->OnClicked.AddDynamic(this, &UJITitleWidget::OnBtnExitClicked);
	}
}

void UJITitleWidget::SetAudioComponent(UAudioComponent* Audio)
{
	HoveredAudioComponent = Audio;
}

bool connected = false;
void UJITitleWidget::OnBtnNewGameClicked()
{
	//IP , 들어가고자 하는 컴퓨터의 IP주소르 적는다.
	IPAddress = TextIP->GetText().ToString();
	if (IPAddress.Len() <= 0 || IPAddress.Len() > 20)
		return;
	FIPv4Address ip;
	FIPv4Address::Parse(IPAddress, ip);

	//ID
	CharacterName = TextBox->GetText().ToString();
	if (CharacterName.Len() <= 0 || CharacterName.Len() > 10)
	{
		UGameplayStatics::SetGamePaused(GetWorld(), true);
		if (ReEntryWidget != nullptr)
		{
			ReEntryWidget->SetAudioComponent(HoveredAudioComponent);
			ReEntryWidget->AddToViewport();
		}
		return;
	}
	UserID = CharacterName.GetCharArray().GetData();

	TSharedRef<FInternetAddr> addr = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	addr->SetIp(ip.Value); addr->SetPort(SERVER_PORT);
	g_Socket->SetNonBlocking();

	connected = g_Socket->Connect(*addr);
	//if (!connected)
	//	return;

	cs_packet_login l_packet2;
	l_packet2.size = sizeof(cs_packet_login);
	l_packet2.type = C2S_LOGIN;
	wcscpy_s(l_packet2.name, UserID);
	PacketMgr::GetInst()->send_packet(&l_packet2);
}

void UJITitleWidget::OnBtnContinueClicked()
{
	UGameplayStatics::SetGamePaused(GetWorld(), true);
	UGameplayStatics::OpenLevel(GetWorld(), "Hansan", true);
}

void UJITitleWidget::OnBtnExitClicked()
{
	UKismetSystemLibrary::QuitGame(GetWorld(), GetOwningPlayer(), EQuitPreference::Quit, false);
}

void UJITitleWidget::OnBtnNewGameHovered()
{
	HoveredAudioComponent->Play(0.f);
}

void UJITitleWidget::OnBtnContinueHovered()
{
	HoveredAudioComponent->Play(0.f);
}

void UJITitleWidget::OnBtnExitHovered()
{
	HoveredAudioComponent->Play(0.f);
}
void UJITitleWidget::OnIDCommitted(const FText& InText, ETextCommit::Type InCommitType)
{
	if (!g_bLogin)
	{
		g_bLogin = true;
		JILOG(Warning, TEXT("OnChattingCommitted"));
		OnBtnNewGameClicked();
	}
}