#include "JIUIPlayerController.h"
#include "JITitleWidget.h"
#include "JIReEntryWidget.h"
#include "Blueprint/UserWidget.h"
#include "PacketMgr.h"

struct FUserInfo
{
	short lobbynum;
	char owner;
};

TMap<char, FUserInfo> UserList;

AJIUIPlayerController::AJIUIPlayerController()
{
	static ConstructorHelpers::FClassFinder<UJITitleWidget> UI_TITLE(TEXT("/Game/Custom/UI/UI_Title"));
	if (UI_TITLE.Succeeded())
		TitleWidgetClass = UI_TITLE.Class;

	static ConstructorHelpers::FClassFinder<UJIReEntryWidget> UI_ReEntry(TEXT("/Game/Custom/UI/UI_Re_Entry"));
	if (UI_ReEntry.Succeeded())
		ReEntryWidgetClass = UI_ReEntry.Class;

	BGMAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("TitleBGMComponent"));
	static ConstructorHelpers::FObjectFinder<USoundCue> SC_TitleBGM(TEXT("/Game/Custom/Sound/MyeongRyangBGM_Cue"));
	if (SC_TitleBGM.Succeeded())
		BGMSoundCue = SC_TitleBGM.Object;

	HoveredAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("HoveredAudioComponent"));
	static ConstructorHelpers::FObjectFinder<USoundCue> SC_Hovered(TEXT("/Game/Custom/Sound/sound222/Hovered_Cue"));
	if (SC_Hovered.Succeeded())
		HoveredSoundCue = SC_Hovered.Object;
}

void AJIUIPlayerController::BeginPlay()
{
	Super::BeginPlay();

	PacketMgr::GetInst()->SetWorld(GetWorld());

	TitleWidget = CreateWidget<UJITitleWidget>(this, TitleWidgetClass);
	JICHECK(TitleWidget != nullptr);

	ReEntryWidget = CreateWidget<UJIReEntryWidget>(this, ReEntryWidgetClass);
	JICHECK(ReEntryWidget != nullptr);

	if (HoveredAudioComponent && HoveredSoundCue)
		HoveredAudioComponent->SetSound(HoveredSoundCue);

	TitleWidget->SetAudioComponent(HoveredAudioComponent);
	TitleWidget->SetReEntryWidget(ReEntryWidget);
	TitleWidget->AddToViewport();

	FInputModeUIOnly Mode;
	Mode.SetWidgetToFocus(TitleWidget->GetCachedWidget());
	SetInputMode(Mode);
	bShowMouseCursor = true;

	if (BGMAudioComponent && BGMSoundCue)
	{
		BGMAudioComponent->SetSound(BGMSoundCue);
		BGMAudioComponent->Play(10.f);
	}
	//서버 부분
	PacketMgr::GetInst()->SetUIController(this);

	FInputModeGameAndUI GameAndUIInputMode;
	SetInputMode(GameAndUIInputMode);
}

const int buf_sizee = 255;
void AJIUIPlayerController::Tick(float DeltaTime)
{
	//서버 부분
	char buf[buf_sizee];
	int32 received = 0;
	uint32 datasize;
	g_Socket->HasPendingData(datasize);

	auto result = g_Socket->Recv(reinterpret_cast<uint8*>(buf), buf_sizee, received);


	if (received > 0)
		PacketMgr::GetInst()->process_data(buf, received);
}


void AJIUIPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAction("Login", IE_Pressed, this, &AJIUIPlayerController::OnEnter);
}

void AJIUIPlayerController::OnEnter()
{
	//JILOG(Warning, TEXT("OnEnter"));
	TitleWidget->TextBox->ResetCursor();
	TitleWidget->OnBtnNewGameClicked();
}