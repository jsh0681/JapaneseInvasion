#include "JIVictoryWidget.h"
#include "Components/Button.h"

void UJIVictoryWidget::NativeConstruct()
{
	Super::NativeConstruct();

	btnReturnToTitle = Cast<UButton>(GetWidgetFromName(TEXT("BTN_ReturnToTitle")));
	if (btnReturnToTitle != nullptr)
		btnReturnToTitle->OnClicked.AddDynamic(this, &UJIVictoryWidget::OnBtnReturnToTitle);

	btnQuit = Cast<UButton>(GetWidgetFromName(TEXT("BTN_Quit")));
	if (btnQuit != nullptr)
		btnQuit->OnClicked.AddDynamic(this, &UJIVictoryWidget::OnBtnQuit);

}

void UJIVictoryWidget::OnBtnReturnToTitle()
{
	//JILOG(Warning, TEXT("OnBtnReturnToTitle"));
	//UGameplayStatics::OpenLevel(GetWorld(), "Title",  true);
}

void UJIVictoryWidget::OnBtnQuit()
{
	//JILOG(Warning, TEXT("OnBtnQuit"));
	//UKismetSystemLibrary::QuitGame(GetWorld(), GetOwningPlayer(), EQuitPreference::Quit, false);
}
