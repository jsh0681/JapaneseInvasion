#include "JIWeapon.h"

AJIWeapon::AJIWeapon()
{
	PrimaryActorTick.bCanEverTick = true;
	Weapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SK_WEAPON"));
}

void AJIWeapon::BeginPlay()
{
	Super::BeginPlay();
}
void AJIWeapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void AJIWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AJIWeapon::AttachMeshToPawn(USkeletalMeshComponent* mesh, FName WeaponSocketName)
{
	if (Weapon != nullptr)
	{
		AttachToComponent(mesh, FAttachmentTransformRules::SnapToTargetIncludingScale, WeaponSocketName);
		SetActorEnableCollision(true);
		SetActorHiddenInGame(false);
	}
}

void AJIWeapon::DetachMeshFromPawn()
{
	if (Weapon != nullptr)
	{
		DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		SetActorEnableCollision(false);
		SetActorHiddenInGame(true);
	}
}

