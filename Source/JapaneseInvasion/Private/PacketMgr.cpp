// Fill out your copyright notice in the Description page of Project Settings.


#include "PacketMgr.h"
#include "PacketMove.h"
#include "PacketLogin.h"
#include "PacketEnter.h"
//#include "PacketHandler.h"

#include "JIPlayerController.h"
#include "JIAICharacter.h"
#include "NavigationSystem.h"
#include "JICharacter.h"
#include "DrawDebugHelpers.h"
#include "JIHUDWidget.h"
#include "JIMenuWidget.h"
#include "JITitleWidget.h"
#include "JILobbyWIdget.h"
#include "JIChatBox.h"
#include "Blueprint/UserWidget.h"
#include "JIChattingWidget.h"
#include "JIAIAnimInstance.h"
#include "JIJapanAIAnimInstance.h"

#include "JIUIPlayerController.h"
#include "JICharacterWidget.h"
#include "JICharacterStatComponent.h"
#include "JIKoreaShipStatComponent.h"
#include "JIGeneral.h"
#include "JIShip.h"
#include "JIBomb.h"
#include "JIBow.h"
#include "JISword.h"
#include "JIBullet.h"
#include "JIArrow.h"
#include "Blueprint/UserWidget.h"

#include "protocol.h"

TMap<int, AJICharacter*> g_mapJoseonSoldier;
TMap<int, AJIAICharacter*> g_mapJapanSoldier;
TMap<int, AJIBullet*> g_mapBullet;
TMap<int, AJIArrow*> g_mapArrow;

const int PACKET_BUF_SIZE = 255;
int joseon_team_num = 0;
int japan_team_num = 0;
int each_team_num = 0;
TEAM_COLOR client_teamcolor;


extern bool bGeneralSpawn;

// event select 구조로 짜야겠음
sc_packet_login_ok* s;
sc_packet_enter* sd;
sc_packet_move* move;
sc_packet_quit_lobby* lobby;

FVector playerpos;

extern FString CharacterName;

#define PANOK_ASSET 0
#define TURTLE_ASSET 1
#define ANTAKE_ASSET 2

#define JOSEON_GENERAL_ASSET 0
#define JAPAN_GENERAL_ASSET 1


void PacketMgr::process_packet(char* buf)
{
    //PacketHandler::

    switch (buf[1])
    {
    case S2C_CERTIFIFY_OK:
    {
        sc_packet_certifify_ok* packet = reinterpret_cast<sc_packet_certifify_ok*>(buf);
        // 클라이언트 id  = packet->name;

        UGameplayStatics::SetGamePaused(m_pUWorld, true);
        UGameplayStatics::OpenLevel(m_pUWorld, "Lobby", true);
    }
    break;
    case S2C_CERTIFIFY_FAIL:
    {
        sc_packet_certifify_fail* packet = reinterpret_cast<sc_packet_certifify_fail*>(buf);
        if (packet->failtype == (char)CERTIFIFY_FAIL::NO_ID)
            m_pUIController->ReEntryWidget->AddToViewport();
        else if (packet->failtype == (char)CERTIFIFY_FAIL::ALREADY_LOGIN)
        {
            UGameplayStatics::SetGamePaused(m_pUWorld, true);
            UGameplayStatics::OpenLevel(m_pUWorld, "Title", true);
        }

    }
    break;
    case S2C_SELECT_LOBBY:
    {
        bGeneralSpawn = false;
        sc_packet_select_lobby* packet = reinterpret_cast<sc_packet_select_lobby*>(buf);
        int cl_id = packet->id;
        japan_team_num = packet->japan_num;
        joseon_team_num = packet->joseon_num;

        //방장인 놈의 인덱스가 몇인지
        if ((TEAM_COLOR)packet->team == TEAM_COLOR::JOSEON_1)
        {
            m_pLobbyWidget->TeamJoseonImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->JoseonFlagTex, 575, 250));
            m_pLobbyWidget->TeamJoseonTxtBlock[0]->SetText(FText::FromString(packet->name));
            //if ((bool)packet->owner == true)
            //    m_pLobbyWidget->CrownImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->CrownTex, 900, 900));
            //else if ((bool)packet->owner == false)
            //    m_pLobbyWidget->CrownImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->nullBack, 900, 900));
        }
        else if ((TEAM_COLOR)packet->team == TEAM_COLOR::JOSEON_2)
        {
            m_pLobbyWidget->TeamJoseonImg[1]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->JoseonFlagTex, 575, 250));
            m_pLobbyWidget->TeamJoseonTxtBlock[1]->SetText(FText::FromString(packet->name));
            //if ((bool)packet->owner == true)
            //    m_pLobbyWidget->CrownImg[1]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->CrownTex, 900, 900));
            //else if ((bool)packet->owner == false)
            //    m_pLobbyWidget->CrownImg[1]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->nullBack, 900, 900));
        }
        if ((TEAM_COLOR)packet->team == TEAM_COLOR::JAPAN_1)
        {
            m_pLobbyWidget->TeamJapanImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->JapanFlagTex, 575, 250));
            m_pLobbyWidget->TeamJapanTxtBlock[0]->SetText(FText::FromString(packet->name));
            //if ((bool)packet->owner == true)
            //    m_pLobbyWidget->CrownImg[2]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->CrownTex, 900, 900));
            //else if ((bool)packet->owner == false)
            //    m_pLobbyWidget->CrownImg[2]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->nullBack, 900, 900));
        }
        else if ((TEAM_COLOR)packet->team == TEAM_COLOR::JAPAN_2)
        {
            m_pLobbyWidget->TeamJapanImg[1]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->JapanFlagTex, 575, 250));
            m_pLobbyWidget->TeamJapanTxtBlock[1]->SetText(FText::FromString(packet->name));
            //if ((bool)packet->owner == true)
            //    m_pLobbyWidget->CrownImg[3]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->CrownTex, 900, 900));
            //else if ((bool)packet->owner == false)
            //    m_pLobbyWidget->CrownImg[3]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->nullBack, 900, 900));
        }

        if (packet->order == 0)
        {
            m_pLobbyWidget->CurrentUserEnterTxtBlock[0]->SetText(FText::FromString(packet->name));
            if ((bool)packet->owner == true)
                m_pLobbyWidget->CrownImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->CrownTex, 900, 900));
            else if ((bool)packet->owner == false)
                m_pLobbyWidget->CrownImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->nullBack, 900, 900));

        }
        else if (packet->order == 1)
        {
            m_pLobbyWidget->CurrentUserEnterTxtBlock[1]->SetText(FText::FromString(packet->name));
            if ((bool)packet->owner == true)
                m_pLobbyWidget->CrownImg[1]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->CrownTex, 900, 900));
            else if ((bool)packet->owner == false)
                m_pLobbyWidget->CrownImg[1]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->nullBack, 900, 900));
        }
        else if (packet->order == 2)
        {
            m_pLobbyWidget->CurrentUserEnterTxtBlock[2]->SetText(FText::FromString(packet->name));
            if ((bool)packet->owner == true)
                m_pLobbyWidget->CrownImg[2]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->CrownTex, 900, 900));
            else if ((bool)packet->owner == false)
                m_pLobbyWidget->CrownImg[2]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->nullBack, 900, 900));
        }
        else if (packet->order == 3)
        {
            m_pLobbyWidget->CurrentUserEnterTxtBlock[3]->SetText(FText::FromString(packet->name));
            if ((bool)packet->owner == true)
                m_pLobbyWidget->CrownImg[3]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->CrownTex, 900, 900));
            else if ((bool)packet->owner == false)
                m_pLobbyWidget->CrownImg[3]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->nullBack, 900, 900));
        }
    }
    break;
    case S2C_QUIT_LOBBY:
    {
        sc_packet_quit_lobby* packet = reinterpret_cast<sc_packet_quit_lobby*>(buf);

        lobby = packet;

        ///////////////////////////////////////////
        if ((TEAM_COLOR)packet->prevteam == TEAM_COLOR::JOSEON_1)
        {
            m_pLobbyWidget->TeamJoseonImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->NullTexture, 575, 250));
            FString nullstr = TEXT(" ");
            m_pLobbyWidget->TeamJoseonTxtBlock[0]->SetText(FText::FromString(nullstr));
        }
        else if ((TEAM_COLOR)packet->prevteam == TEAM_COLOR::JOSEON_2)
        {
            m_pLobbyWidget->TeamJoseonImg[1]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->NullTexture, 575, 250));
            FString nullstr = TEXT(" ");
            m_pLobbyWidget->TeamJoseonTxtBlock[1]->SetText(FText::FromString(nullstr));
        }
        else if ((TEAM_COLOR)packet->prevteam == TEAM_COLOR::JAPAN_1)
        {
            m_pLobbyWidget->TeamJapanImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->NullTexture, 575, 250));
            FString nullstr = TEXT(" ");
            m_pLobbyWidget->TeamJapanTxtBlock[0]->SetText(FText::FromString(nullstr));
        }
        else if ((TEAM_COLOR)packet->prevteam == TEAM_COLOR::JAPAN_2)
        {
            m_pLobbyWidget->TeamJapanImg[1]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->NullTexture, 575, 250));
            FString nullstr = TEXT(" ");
            m_pLobbyWidget->TeamJapanTxtBlock[1]->SetText(FText::FromString(nullstr));
        }

        FString nullstr = TEXT(" ");
        if (packet->prevquitorder < 4 && packet->prevquitorder >= 0)
            m_pLobbyWidget->CurrentUserEnterTxtBlock[packet->prevquitorder]->SetText(FText::FromString(nullstr));

        if (packet->prevquitorder != -1)
            m_pLobbyWidget->CrownImg[packet->prevquitorder]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->nullBack, 900, 900));


        if (packet->nextownerid != -1)
            m_pLobbyWidget->CrownImg[packet->nextownerid]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->CrownTex, 900, 900));


        japan_team_num = 0;
        joseon_team_num = 0;
    }
    break;

    case S2C_START_GAME:
    {
        sc_packet_start_game* packet = reinterpret_cast<sc_packet_start_game*>(buf);
        // 게임 시작시 login_ok 처리시 하는 것을 해야 함.

        UGameplayStatics::SetGamePaused(m_pUWorld, true);
        UGameplayStatics::OpenLevel(m_pUWorld, "Hansan", true);
    }
    break;
    case S2C_START_GAME_FAIL:
    {
        sc_packet_start_game_fail* packet = reinterpret_cast<sc_packet_start_game_fail*>(buf);
    }
    break;
    case S2C_SELECT_TEAM:
    {
        sc_packet_select_team* packet = reinterpret_cast<sc_packet_select_team*>(buf);
        if ((TEAM_COLOR)packet->team == TEAM_COLOR::JOSEON_1 || (TEAM_COLOR)packet->team == TEAM_COLOR::JOSEON_2)
        {
            joseon_team_num = packet->joseon_team_num;
            japan_team_num = packet->japan_team_num;
        }
        else if ((TEAM_COLOR)packet->team == TEAM_COLOR::JAPAN_1 || (TEAM_COLOR)packet->team == TEAM_COLOR::JAPAN_2)
        {
            joseon_team_num = packet->joseon_team_num;
            japan_team_num = packet->japan_team_num;
        }
        // cl_id클라이언트 = packet->team;
        // japan_team_num 이 1인데 0 텍스쳐가 차있으면 1번 텍스쳐에 넣는다.
        // joseon도 마찬가지   

        const FName ImgJoseonName = m_pLobbyWidget->TeamJoseonImg[0]->Brush.GetResourceName();
        const FName ImgJapanName = m_pLobbyWidget->TeamJapanImg[0]->Brush.GetResourceName();


        ///////////////////////////////////////////
        if ((TEAM_COLOR)packet->prevteam == TEAM_COLOR::JOSEON_1)
        {
            m_pLobbyWidget->TeamJoseonImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->NullTexture, 575, 250));
            FString nullstr = TEXT(" ");
            m_pLobbyWidget->TeamJoseonTxtBlock[0]->SetText(FText::FromString(nullstr));
        }
        else if ((TEAM_COLOR)packet->prevteam == TEAM_COLOR::JOSEON_2)
        {
            m_pLobbyWidget->TeamJoseonImg[1]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->NullTexture, 575, 250));
            FString nullstr = TEXT(" ");
            m_pLobbyWidget->TeamJoseonTxtBlock[1]->SetText(FText::FromString(nullstr));
        }
        else if ((TEAM_COLOR)packet->prevteam == TEAM_COLOR::JAPAN_1)
        {
            m_pLobbyWidget->TeamJapanImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->NullTexture, 575, 250));
            FString nullstr = TEXT(" ");
            m_pLobbyWidget->TeamJapanTxtBlock[0]->SetText(FText::FromString(nullstr));
        }
        else if ((TEAM_COLOR)packet->prevteam == TEAM_COLOR::JAPAN_2)
        {
            m_pLobbyWidget->TeamJapanImg[1]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->NullTexture, 575, 250));
            FString nullstr = TEXT(" ");
            m_pLobbyWidget->TeamJapanTxtBlock[1]->SetText(FText::FromString(nullstr));
        }


        if ((TEAM_COLOR)packet->team == TEAM_COLOR::JOSEON_1 || (TEAM_COLOR)packet->team == TEAM_COLOR::JOSEON_2 && joseon_team_num == 2 && ImgJoseonName == TEXT("image_null"))
        {
            m_pLobbyWidget->TeamJoseonImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->JoseonFlagTex, 575, 250));
            m_pLobbyWidget->TeamJoseonTxtBlock[0]->SetText(FText::FromString(packet->name));
        }
        else if ((TEAM_COLOR)packet->team == TEAM_COLOR::JAPAN_1 || (TEAM_COLOR)packet->team == TEAM_COLOR::JAPAN_2 && japan_team_num == 2 && ImgJapanName == TEXT("image_null"))
        {
            m_pLobbyWidget->TeamJapanImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->JapanFlagTex, 575, 250));
            m_pLobbyWidget->TeamJapanTxtBlock[0]->SetText(FText::FromString(packet->name));
        }
        ///////////////////////////////////////////
        else if ((TEAM_COLOR)packet->team == TEAM_COLOR::JOSEON_1)
        {
            m_pLobbyWidget->TeamJoseonImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->JoseonFlagTex, 575, 250));
            m_pLobbyWidget->TeamJoseonTxtBlock[0]->SetText(FText::FromString(packet->name));
        }
        else if ((TEAM_COLOR)packet->team == TEAM_COLOR::JOSEON_2)
        {
            m_pLobbyWidget->TeamJoseonImg[1]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->JoseonFlagTex, 575, 250));
            m_pLobbyWidget->TeamJoseonTxtBlock[1]->SetText(FText::FromString(packet->name));
        }
        else if ((TEAM_COLOR)packet->team == TEAM_COLOR::JAPAN_1)
        {
            m_pLobbyWidget->TeamJapanImg[0]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->JapanFlagTex, 575, 250));
            m_pLobbyWidget->TeamJapanTxtBlock[0]->SetText(FText::FromString(packet->name));
        }
        else if ((TEAM_COLOR)packet->team == TEAM_COLOR::JAPAN_2)
        {
            m_pLobbyWidget->TeamJapanImg[1]->SetBrush(m_pLobbyWidget->MakeBrush(m_pLobbyWidget->JapanFlagTex, 575, 250));
            m_pLobbyWidget->TeamJapanTxtBlock[1]->SetText(FText::FromString(packet->name));
        }
    }
    break;

    case S2C_LOGIN_OK:
    {
        sc_packet_login_ok* packet = reinterpret_cast<sc_packet_login_ok*>(buf);
        if (packet->objtype == OBJ_TYPE::GENERAL)
        {
            AJIGeneral* p = m_pUWorld->SpawnActor<AJIGeneral>(FVector(packet->x, packet->y, packet->z), FRotator::ZeroRotator);
            s = packet;
            if (p == nullptr)
                break;

            p->m_iId = packet->id;
            p->CharacterStat->SetHP(packet->hp);

            p->m_eTeamColor = packet->teamcolor;
            if (packet->owner)
            {
                p->m_bIsOwner = true;
                client_teamcolor = packet->teamcolor;
            }

            if (p->m_eTeamColor == TEAM_COLOR::JOSEON_1 || p->m_eTeamColor == TEAM_COLOR::JOSEON_2)
            {
                p->ActorTag.Add("JOSEON");
                p->ActorTag.Add("JoseonGeneral");
                p->AssetIndex = JOSEON_GENERAL_ASSET;
                p->SetAsset();
                p->CharacterStat->SetObejctType(EObjectType::JOSEON_GENERAL);
            }
            else if (p->m_eTeamColor == TEAM_COLOR::JAPAN_1 || p->m_eTeamColor == TEAM_COLOR::JAPAN_2)
            {
                p->ActorTag.Add("JAPAN");
                p->ActorTag.Add("JapanGeneral");
                p->AssetIndex = JAPAN_GENERAL_ASSET;
                p->SetAsset();
                p->CharacterStat->SetObejctType(EObjectType::JAPAN_GENERAL);
            }
            //p->m_iId = packet->general_id; // 문제 여기서 생길수도
            p->Tags = p->ActorTag;
            g_mapPlayer.Add(packet->general_id, p);
            if (p->m_bIsOwner)
                bGeneralSpawn = true;
            
        }
        else if (packet->objtype == OBJ_TYPE::SHIP)
        {

            AJIShip* ship = m_pUWorld->SpawnActor<AJIShip>(FVector(packet->x, packet->y, packet->z), FRotator::ZeroRotator);
            s = packet;
            if (ship == nullptr)
                break;

            ship->ShipStat->SetHP(packet->hp);

            ship->m_eTeamColor = packet->teamcolor;
            if (packet->teamcolor == TEAM_COLOR::JOSEON_1)
            {
                ship->ActorTag.Add("JOSEON_1");
                ship->Mesh->SetCollisionProfileName("JoseonShip");
                ship->SetDetailOption();
                if (packet->shiptype == (SHIP_TYPE)1)
                {
                    ship->AssetIndex = PANOK_ASSET;
                    ship->SetAsset();
                    ship->ShipStat->SetNewLevel(1);
                }
                else if (packet->shiptype == (SHIP_TYPE)0)
                {
                    ship->AssetIndex = TURTLE_ASSET;
                    ship->SetAsset();
                    ship->ShipStat->SetNewLevel(2);
                }
            }
            else if (packet->teamcolor == TEAM_COLOR::JOSEON_2)
            {
                ship->ActorTag.Add("JOSEON_2");
                ship->Mesh->SetCollisionProfileName("JoseonShip");
                ship->SetDetailOption();
                if (packet->shiptype == (SHIP_TYPE)1)
                {
                    ship->AssetIndex = PANOK_ASSET;
                    ship->SetAsset();
                    ship->ShipStat->SetNewLevel(1);
                }
                else if (packet->shiptype == (SHIP_TYPE)0)
                {
                    ship->AssetIndex = TURTLE_ASSET;
                    ship->SetAsset();
                    ship->ShipStat->SetNewLevel(2);
                }
            }
            else if (packet->teamcolor == TEAM_COLOR::JAPAN_1)
            {
                ship->ActorTag.Add("JAPAN_1");
                ship->AssetIndex = ANTAKE_ASSET;
                ship->SetAsset();
                ship->Mesh->SetCollisionProfileName("JapanShip");
                ship->ShipStat->SetNewLevel(3);
                ship->SetDetailOption();
            }
            else if (packet->teamcolor == TEAM_COLOR::JAPAN_2)
            {
                ship->ActorTag.Add("JAPAN_2");
                ship->AssetIndex = ANTAKE_ASSET;
                ship->SetAsset();
                ship->Mesh->SetCollisionProfileName("JapanShip");
                ship->ShipStat->SetNewLevel(3);
                ship->SetDetailOption();
            }

            if (packet->shiptype == SHIP_TYPE(2))
            {
                ship->ActorTag.Add("ANTAEK");
            }
            else if (packet->shiptype == SHIP_TYPE(1))
            {
                ship->ActorTag.Add("PANOK");
            }
            else if (packet->shiptype == SHIP_TYPE(0))
            {
                ship->ActorTag.Add("TURTLE");
            }
            ship->SetActorLocation(FVector(packet->x, packet->y, packet->z), true);
            ship->m_fShipSpeed = packet->speed;
            ship->Tags = ship->ActorTag;
            ship->m_iId = packet->id;
            g_mapShip.Add(ship->m_iId, ship);
        }
    }
    break;
    case S2C_ENTER:
    {
        sc_packet_enter* packet = reinterpret_cast<sc_packet_enter*>(buf);

        for (auto& Generals : g_mapPlayer)
        {
            //JILOG(Warning, TEXT("m_iId : %d, id : %d"), Generals.Value->m_iId, packet->id);
            if (Generals.Value->m_iId == packet->id)
            {
                wcscpy_s(Generals.Value->m_strID, packet->name);
            }
        }

    }
    break;

    case S2C_MOVE:
    {
        sc_packet_move* packet = reinterpret_cast<sc_packet_move*>(buf);

        if (packet->objtype == OBJ_TYPE::GENERAL)
        {
            for (auto& Generals : g_mapPlayer)
            {
                if (packet->id == Generals.Key)
                {
                    playerpos.X = packet->x;
                    playerpos.Y = packet->y;
                    playerpos.Z = packet->z;

                    Generals.Value->m_iId = packet->id;
                    Generals.Value->m_Dir = packet->dir;
                    Generals.Value->m_fCamValue = packet->CamValue;
                    Generals.Value->m_vPosX = packet->x;
                    Generals.Value->m_vPosY = packet->y;
                    Generals.Value->m_vPosZ = packet->z;
                    Generals.Value->m_fRotValue = packet->RotValue;

                    Generals.Value->m_fTurnValue = packet->TurnValue;

                    Generals.Value->m_fCamVec.X = packet->x;
                    Generals.Value->m_fCamVec.Y = packet->y;
                    Generals.Value->m_fCamVec.Z = packet->z;

                    FVector pos = Generals.Value->GetActorLocation();
                    Generals.Value->GetRootComponent()->SetWorldLocation(FVector(packet->x, packet->y, packet->z)); //SetActorLocation(, true);

                    FRotator deltaRotation = FRotator(0, packet->TurnValue, 0);
                    Generals.Value->GetRootComponent()->SetWorldRotation(deltaRotation);

                    FVector vel;
                    vel.X = packet->velx;
                    vel.Y = packet->vely;
                    vel.Z = packet->velz;

                    GENERAL_STATUS status = (GENERAL_STATUS)packet->status;
                    Generals.Value->m_eStatus = status;

                    FVector pvel = Generals.Value->GetVelocity() = vel;

                    if (!Generals.Value->m_bIsOwner)
                    {
                        if (status == GENERAL_STATUS::IDLE) // 아이들 상태                                                    
                            Generals.Value->JIAnim->PlayIdleMontage();
                        else if (status == GENERAL_STATUS::RUN) // 뛰는 상태
                            Generals.Value->JIAnim->PlayRunMontage();
                    }

                }
            }
        }
        else if (packet->objtype == OBJ_TYPE::SOLDIER) // 조선 병사인지 일본 병사인지 구분해야 함.
        {
            if (packet->dir == (char)TEAM_COLOR::JOSEON_1)
            {
                for (auto& JoseonS : g_mapJoseonSoldier)
                {
                    if (packet->id == JoseonS.Key)
                    {
                        FVector pos = JoseonS.Value->GetActorLocation();

                        JoseonS.Value->SetActorLocation(FVector(packet->x, packet->y, packet->z), true);
                       // JoseonS.Value->GetRootComponent()->SetWorldLocation(FVector(packet->x, packet->y, packet->z)); //SetActorLocation(, true);

                        FRotator deltaRotation = FRotator(0, packet->TurnValue, 0);
                        JoseonS.Value->GetRootComponent()->SetWorldRotation(deltaRotation);

                        FVector vel;
                        vel.X = packet->velx;
                        vel.Y = packet->vely;
                        vel.Z = packet->velz;

                        GENERAL_STATUS status = (GENERAL_STATUS)packet->status;
                        JoseonS.Value->m_eStatus = status;

                        FVector pvel = JoseonS.Value->GetVelocity() = vel;

                        if (status == GENERAL_STATUS::IDLE) // 아이들 상태                                                    
                            JoseonS.Value->JIAnim->PlayIdleMontage();
                        else if (status == GENERAL_STATUS::RUN) // 뛰는 상태
                            JoseonS.Value->JIAnim->PlayRunMontage();

                        // JILOG(Warning, TEXT("id : %d"), packet->id);
                    }
                }
            }
            else if (packet->dir == (char)TEAM_COLOR::JAPAN_1)
            {
                for (auto& JapanS : g_mapJapanSoldier)
                {
                    if (packet->id == JapanS.Key)
                    {
                        FVector pos = JapanS.Value->GetActorLocation();
                        JapanS.Value->SetActorLocation(FVector(packet->x, packet->y, packet->z), true);
                        //JapanS.Value->GetRootComponent()->SetWorldLocation(FVector(packet->x, packet->y, packet->z)); //SetActorLocation(, true);

                        FRotator deltaRotation = FRotator(0, packet->TurnValue, 0);
                        JapanS.Value->GetRootComponent()->SetWorldRotation(deltaRotation);

                        FVector vel;
                        vel.X = packet->velx;
                        vel.Y = packet->vely;
                        vel.Z = packet->velz;

                        GENERAL_STATUS status = (GENERAL_STATUS)packet->status;
                        JapanS.Value->m_eStatus = status;

                        FVector pvel = JapanS.Value->GetVelocity() = vel;

                        if (status == GENERAL_STATUS::IDLE) // 아이들 상태                                                    
                            JapanS.Value->JIAnim->PlayIdleMontage();
                        else if (status == GENERAL_STATUS::RUN) // 뛰는 상태
                            JapanS.Value->JIAnim->PlayRunMontage();
                        // JILOG(Warning, TEXT("id : %d"), packet->id);
                    }
                }
            }
        }
    }
    break;
    case S2C_SHIP_MOVE:
    {
        sc_packet_ship_move* packet = reinterpret_cast<sc_packet_ship_move*>(buf);
        for (auto& Ships : g_mapShip)
        {
            if (packet->m_id == Ships.Key)
            {
                FRotator deltaRotation = FRotator(0, packet->TurnValue, 0);
                Ships.Value->GetRootComponent()->AddWorldRotation(deltaRotation);
                Ships.Value->SetActorLocation(FVector(packet->x, packet->y, packet->z),true);

            }
        }
    }
    break;
    case S2C_ATTACK:
    {
        sc_packet_attack* packet = reinterpret_cast<sc_packet_attack*>(buf);

        if (packet->objtype == (char)OBJ_TYPE::GENERAL)
        {
            for (auto& Generals : g_mapPlayer)
            {
                if (packet->id == Generals.Key)
                {
                    Generals.Value->Attack();
                }
            }
        }
        else if (packet->objtype == (char)OBJ_TYPE::SOLDIER)
        {
            TEAM_COLOR teamcolor = (TEAM_COLOR)packet->attack;
            if (teamcolor == TEAM_COLOR::JAPAN_1)
            {
                for (auto& JapanS : g_mapJapanSoldier)
                {
                    //JILOG(Warning, TEXT("packet->id : %d , JapanS.Value->m_id : %d "), packet->id == JapanS.Value->m_id);
                    if (packet->id == JapanS.Value->m_id)
                    {
                        JapanS.Value->Attack();
                    }
                }
            }
            else if (teamcolor == TEAM_COLOR::JOSEON_1)
            {
                for (auto& JoseonS : g_mapJoseonSoldier)
                    if (packet->id == JoseonS.Value->m_id)
                    {
                        // JILOG(Warning, TEXT("JoseonS Attack"));
                        JoseonS.Value->Attack();
                    }
            }
        }
    }
    break;
    case S2C_SHELL_SPAWN:
    {
        sc_packet_shell_spawn* packet = reinterpret_cast<sc_packet_shell_spawn*>(buf);

        FVector vPos(packet->posx, packet->posy, packet->posz);
        FRotator vRot(packet->rotx, packet->roty, packet->rotz);
        FVector vVel(packet->velx, packet->vely, packet->velz);

        for (auto& Ships : g_mapShip)
        {
            if (Ships.Value->m_iId == packet->shipid)
            {
                if (Ships.Value->loadedAmmo <= 0)
                {
                    Ships.Value->GunEmptyAudioComponent->Play(0.f);
                    return;
                }
                Ships.Value->loadedAmmo = Ships.Value->loadedAmmo - 1;
                Ships.Value->ShipStat->SetReload(Ships.Value->loadedAmmo);
                Ships.Value->FireAudioComponent->Play(0.0f);
            }
        }

        AJIBomb* Shell = m_pUWorld->SpawnActor<AJIBomb>(AJIBomb::StaticClass(), vPos, vRot);
        Cast<AJIBomb>(Shell)->SetVelocity(vVel);
        Shell->m_iId = packet->id;
        Shell->ActorTag.Add("SHELL");
        Shell->Tags = Shell->ActorTag;
        g_mapShell.Add(packet->id, Shell);
    }
    break;
    case S2C_BULLET_SPAWN:
    {
        sc_packet_bullet_spawn* packet = reinterpret_cast<sc_packet_bullet_spawn*>(buf);

        FVector vPos(packet->posx, packet->posy, packet->posz);
        FRotator vRot(packet->rotx, packet->roty, packet->rotz);
        FVector vVel(packet->velx, packet->vely, packet->velz);

        for (auto& Ships : g_mapShip)
        {
            if (Ships.Value->m_iId == packet->shipid)
            {
                if (Ships.Value->loadedAmmo <= 0)
                {
                    Ships.Value->GunEmptyAudioComponent->Play(0.f);
                    return;
                }
                Ships.Value->loadedAmmo = Ships.Value->loadedAmmo - 1;
                Ships.Value->ShipStat->SetReload(Ships.Value->loadedAmmo);
                Ships.Value->FireAudioComponent->Play(0.0f);
            }
        }

        AJIBullet* Bullet = m_pUWorld->SpawnActor<AJIBullet>(AJIBullet::StaticClass(), vPos, vRot);
        Cast<AJIBullet>(Bullet)->SetVelocity(vVel);
        Bullet->m_iId = packet->id;
        Bullet->ActorTag.Add("BULLET");
        Bullet->Tags = Bullet->ActorTag;
        g_mapBullet.Add(packet->id, Bullet);
    }
    break;
    case S2C_ARROW_SPAWN:
    {
        sc_packet_arrow_spawn* packet = reinterpret_cast<sc_packet_arrow_spawn*>(buf);
        FVector pos = FVector(packet->posx, packet->posy, packet->posz);
        FRotator rot = FRotator(packet->rotx, packet->roty, packet->rotz);
        FRotator controllerRot = FRotator(packet->controllerRotx, packet->controllerRoty, packet->controllerRotz);
        FVector velocity = FVector(packet->velx, packet->vely, packet->velz);
        //SetActorRotation(rot);
        AJIArrow* Arrow = m_pUWorld->SpawnActor<AJIArrow>(pos, rot.Add(controllerRot.GetComponentForAxis(EAxis::Y), 0.0f, 0.0f));
        Cast<AJIArrow>(Arrow)->SetVelocity(velocity);
        Arrow->m_id = packet->id;
        Arrow->ActorTag.Add("ARROW");
        Arrow->Tags = Arrow->ActorTag;
        g_mapArrow.Add(packet->id, Arrow);
    }
    break;
    case S2C_SHELL_MOVE:
    {
        sc_packet_shell_move* packet = reinterpret_cast<sc_packet_shell_move*>(buf);

        for (auto& Shell : g_mapShell)
        {
            if (packet->id == Shell.Key)
            {
                Shell.Value->SetActorLocation(FVector(packet->x, packet->y, packet->z));
            }
        }
    }
    break;
    case S2C_BULLET_MOVE:
    {
        sc_packet_bullet_move* packet = reinterpret_cast<sc_packet_bullet_move*>(buf);

        for (auto& Bullet : g_mapBullet)
        {
            if (packet->id == Bullet.Key)
            {
                Bullet.Value->SetActorLocation(FVector(packet->x, packet->y, packet->z));
            }
        }
    }
    break;
    case S2C_ARROW_MOVE:
    {
        sc_packet_arrow_move* packet = reinterpret_cast<sc_packet_arrow_move*>(buf);

        for (auto& Arrow : g_mapArrow)
        {
            if (packet->id == Arrow.Key)
            {
                Arrow.Value->SetActorLocation(FVector(packet->x, packet->y, packet->z));
            }
        }
    }
    break;
    case S2C_SET_HP:
    {
        sc_packet_set_hp* packet = reinterpret_cast<sc_packet_set_hp*>(buf);

        if (packet->objtype == OBJ_TYPE::GENERAL)
        {
            for (auto& General : g_mapPlayer)
            {
                if (General.Value->m_iId == packet->id)
                    General.Value->CharacterStat->SetHP(packet->hp);
            }
        }
        else if (packet->objtype == OBJ_TYPE::SHIP)
        {
            for (auto& Ship : g_mapShip)
            {
                if (Ship.Value->m_iId == packet->id)
                    Ship.Value->ShipStat->SetHP(packet->hp);
            }
        }
        else if (packet->objtype == OBJ_TYPE::SOLDIER)
        {
           
                for (auto& JoseonS : g_mapJoseonSoldier)
                {
                    if (JoseonS.Value->m_id == packet->id)
                        JoseonS.Value->CharacterStat->SetHP(packet->hp);
                }
                for (auto& JapanS : g_mapJapanSoldier)
                {
                    if (JapanS.Value->m_id == packet->id)
                        JapanS.Value->CharacterStat->SetHP(packet->hp);
                }
            
        }
    }
    break;
    case S2C_VIEWMODE:
    {
        sc_packet_view* packet = reinterpret_cast<sc_packet_view*>(buf);

        // c = packet->viewmode;

        for (auto& Generals : g_mapPlayer)
        {
            if (Generals.Value->m_bIsOwner)
            {
                TArray<AActor*> FoundActors;

                if ((EViewMode)packet->viewmode == EViewMode::TOPVIEW)
                {
                    m_pController->Possess(m_pController->TopViewCamera);
                    m_pController->SetViewMode(EViewMode::TOPVIEW);
                }
                else if ((EViewMode)packet->viewmode == EViewMode::BACKVIEW)
                {
                    m_pController->Possess(Generals.Value);
                    m_pController->SetViewMode(EViewMode::BACKVIEW);
                }

                if (packet->cause == CHANGE_VIEW_TYPE::COMBAT)
                {
                    g_mapJoseonSoldier.Reset();
                    g_mapJapanSoldier.Reset();
                }

            }
        }
    }
    break;
    case S2C_CHANGE_WEAPON:
    {
        sc_packet_change_weapon* packet = reinterpret_cast<sc_packet_change_weapon*>(buf);

        for (auto& Generals : g_mapPlayer)
        {
            if (Generals.Value->m_iId == packet->general_id)
            {
                if (packet->weapon == A_BOW)
                {

                    if (Generals.Value->Weapon != nullptr)
                    {
                        Generals.Value->Weapon->DetachMeshFromPawn();
                        Generals.Value->Weapon->Destroy(true);
                    }
                    Generals.Value->WeaponSocketName = (TEXT("LHand_Bow"));
                    Generals.Value->Weapon = m_pUWorld->SpawnActor<AJIBow>(AJIBow::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
                    Generals.Value->Weapon->AttachMeshToPawn(Generals.Value->GetMesh(), Generals.Value->WeaponSocketName);
                    Generals.Value->Weapon->SetOwner(Generals.Value);
                    Generals.Value->CurWeaponIndex = EWeaponType::BOW;
                }
                else if (packet->weapon == A_KNIFE)
                {
                    if (Generals.Value->Weapon != nullptr)
                    {
                        Generals.Value->Weapon->DetachMeshFromPawn();
                        Generals.Value->Weapon->Destroy(true);
                    }
                    Generals.Value->WeaponSocketName = (TEXT("RHand_Sword"));
                    Generals.Value->Weapon = m_pUWorld->SpawnActor<AJISword>(AJISword::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
                    Generals.Value->Weapon->AttachMeshToPawn(Generals.Value->GetMesh(), Generals.Value->WeaponSocketName);
                    Generals.Value->Weapon->SetOwner(Generals.Value);
                    Generals.Value->CurWeaponIndex = EWeaponType::SWORD;
                }
            }
        }
    }
    break;
    case S2C_SOLDIER_GEN:
    {
        sc_packet_soldier_gen* packet = reinterpret_cast<sc_packet_soldier_gen*>(buf);
        // = packet->soldier_id
        if (packet->teamcolor == TEAM_COLOR::JAPAN_1 || packet->teamcolor == TEAM_COLOR::JAPAN_2)
        {
            AJIAICharacter* JapanSoldier = m_pUWorld->SpawnActor<AJIAICharacter>(FVector(packet->x, packet->y, packet->z), FRotator::ZeroRotator);
            if (JapanSoldier == nullptr)
                return;
            JapanSoldier->SetActorLocation(FVector(packet->x, packet->y, packet->z), true);
            JapanSoldier->m_id = packet->soldier_id;
            //JapanSoldier->CharacterStat->SetHP(packet->hp);
            JapanSoldier->m_teamcolor = packet->teamcolor;
            JapanSoldier->ActorTag.Add("JAPAN");
            JapanSoldier->ActorTag.Add("JapanSoldier");
            JapanSoldier->Tags = JapanSoldier->ActorTag;
            JapanSoldier->CharacterStat->SetObejctType(EObjectType::JAPAN_SOLDIER);
            g_mapJapanSoldier.Add(packet->soldier_id, JapanSoldier);

        }
        else if ((packet->teamcolor == TEAM_COLOR::JOSEON_1 || packet->teamcolor == TEAM_COLOR::JOSEON_2))
        {
            AJICharacter* JoseonSoldier = m_pUWorld->SpawnActor<AJICharacter>(FVector(packet->x, packet->y, packet->z), FRotator::ZeroRotator);
            if (JoseonSoldier == nullptr)
                return;

            JoseonSoldier->SetActorLocation(FVector(packet->x, packet->y, packet->z), true);
            JoseonSoldier->m_id = packet->soldier_id;
            //JoseonSoldier->CharacterStat->SetHP(packet->hp);
            JoseonSoldier->m_teamcolor = packet->teamcolor;
            JoseonSoldier->ActorTag.Add("JOSEON");
            JoseonSoldier->ActorTag.Add("JoseonSoldier");
            JoseonSoldier->Tags = JoseonSoldier->ActorTag;
            JoseonSoldier->CharacterStat->SetObejctType(EObjectType::JOSEON_SOLDIER);
            g_mapJoseonSoldier.Add(packet->soldier_id, JoseonSoldier);
        }
    }
    break;
    case S2C_CHAT:
    {
        sc_packet_chat* packet = reinterpret_cast<sc_packet_chat*>(buf);
        UJIChatBox* ChatBoxWidget = m_pChattingWidget->MakeChatBox();

        FString ID;
        for (auto& General : g_mapPlayer)
        {
            //JILOG(Warning, TEXT("m_iId : %d, sender_id : %d"), General.Value->m_iId, packet->sender_id);
            if (General.Value->m_iId == packet->sender_id)
            {
                ID = General.Value->m_strID;
            }
        }

        FString Message = packet->chat;
        FString Dot = TEXT(" : ");
        FString IDCombineDot = ID.Append(Dot);
        FString IDCombineMessage = IDCombineDot.Append(Message);

        ChatBoxWidget->MessageBlock->SetText(FText::AsCultureInvariant(IDCombineMessage));
        m_pChattingWidget->ScrollBoxChattingLog->AddChild(ChatBoxWidget);
        m_pChattingWidget->ScrollBoxChattingLog->ScrollToEnd();

    }
    break;
    case S2C_PAUSE:
    {
        sc_packet_pause* packet = reinterpret_cast<sc_packet_pause*>(buf);
        for (auto& General : g_mapPlayer)
        {
            if (General.Value->m_bIsOwner)
            {
                m_pController->Pause();
            }
        }
    }
    break;
    case S2C_RESUME:
    {
        sc_packet_resume* packet = reinterpret_cast<sc_packet_resume*>(buf);
        for (auto& General : g_mapPlayer)
        {
            if (General.Value->m_bIsOwner)
            {
                m_pController->Resume();
            }
        }
    }
    break;
    case S2C_FINISH_GAME:
    {
        sc_packet_finish_game* packet = reinterpret_cast<sc_packet_finish_game*>(buf);
        if (GAME_RESULT::WIN == (GAME_RESULT)packet->result)
            m_pController->OnVictoryWidget();
        else if (GAME_RESULT::LOSE == (GAME_RESULT)packet->result)
            m_pController->OnDefeatWidget();
    }
    break;
    case S2C_EXIT:
    {
        sc_packet_exit* packet = reinterpret_cast<sc_packet_exit*>(buf);
        for (auto& General : g_mapPlayer)
        {
            if (General.Value->m_bIsOwner)
            {
                if (General.Value->m_iId == packet->id)
                {
                    m_pController->GoToTitle();
                }
            }
        }
    }
    break;
    case S2C_LEAVE:
    {
        sc_packet_leave* packet = reinterpret_cast<sc_packet_leave*>(buf);

        //UGameplayStatics::SetGamePaused(m_pUWorld, true);
        //UGameplayStatics::OpenLevel(m_pUWorld, "Title", true);

        //int other_id = packet->id;
        //for (auto& General : g_mapPlayer)
        //{
        //   if (General.Value->m_bIsOwner)
        //   {
        //   }
        //}
    }
    break;
    default:
    {
        //cout << "Unkown packet type error!\n";
        //DebugBreak(); // 비쥬얼 스튜디오 상에서 멈추고 상태를 표시하라
        //exit(-1);
    }
    }
}

void PacketMgr::process_data(char* buf, uint8 io_byte)
{
    char* ptr = buf;
    static size_t in_packet_size = 0;
    static size_t saved_packet_size = 0;
    static char packet_buffer[PACKET_BUF_SIZE];

    while (0 != io_byte)
    {
        if (0 == in_packet_size) in_packet_size = ptr[0];
        if (io_byte + saved_packet_size >= in_packet_size)
        {
            memcpy(packet_buffer + saved_packet_size, ptr, in_packet_size - saved_packet_size);
            process_packet(packet_buffer);
            ptr += in_packet_size - saved_packet_size;
            io_byte -= in_packet_size - saved_packet_size;
            in_packet_size = 0;
            saved_packet_size = 0;
        }
        else
        {
            memcpy(packet_buffer + saved_packet_size, ptr, io_byte);
            saved_packet_size += io_byte;
            io_byte = 0;
        }
    }
}

int size;
void PacketMgr::send_packet(void* packet) // possess된 놈이 배인지 장군인지도 구분해야할듯
{
    char* buf;
    buf = reinterpret_cast<char*>(packet);
    int32 sent;

    size = buf[0];
    //m_Socket->Send(reinterpret_cast<uint8*>(buf), buf[0], sent);

    g_Socket->Send(reinterpret_cast<uint8*>(buf), buf[0], sent);
}


//if (packet->dir == D_LEFT)
//{
//    if (Generals.Value->CurWeaponIndex == EWeaponType::SWORD)
//        Generals.Value->AddMovementInput(FRotationMatrix(FRotator(0.0f, packet->RotValue, 0.0f)).GetUnitAxis(EAxis::Y), packet->CamValue);
//    Generals.Value->AddControllerYawInput(packet->TurnValue);
//}
//else if (packet->dir == D_RIGHT)
//{
//    if (Generals.Value->CurWeaponIndex == EWeaponType::SWORD)
//        Generals.Value->AddMovementInput(FRotationMatrix(FRotator(0.0f, packet->RotValue, 0.0f)).GetUnitAxis(EAxis::Y), packet->CamValue);
//    Generals.Value->AddControllerYawInput(packet->TurnValue);
//}
//else if (packet->dir == D_UP)
//{
//    Generals.Value->AddMovementInput(FRotationMatrix(FRotator(0.0f, packet->RotValue, 0.0f)).GetUnitAxis(EAxis::X), packet->CamValue);;
//    Generals.Value->AddControllerYawInput(packet->TurnValue);
//}
//else if (packet->dir == D_DOWN)
//{
//    Generals.Value->AddMovementInput(FRotationMatrix(FRotator(0.0f, packet->RotValue, 0.0f)).GetUnitAxis(EAxis::X), packet->CamValue);
//    Generals.Value->AddControllerYawInput(packet->TurnValue);
//}


     //AJIGeneral* p = m_pUWorld->SpawnActor<AJIGeneral>(FVector(packet->x, packet->y, packet->z), FRotator::ZeroRotator);
     //s = packet;
     //if (p == nullptr)
     //   break;
     //
     //p->m_iId = packet->id;
     //p->CharacterStat->SetHP(packet->hp);
     //JILOG(Warning, TEXT("id : %d, generalid : %d"), p->m_iId, packet->general_id);
     //
     //if (p->m_iId == packet->general_id)
     //{
     //
     //   p->m_bIsOwner = true;
     //   p->m_eTeamColor = packet->teamcolor;
     //}
     //p->m_iId = packet->general_id;
     //g_mapPlayer.Add(packet->general_id, p);


     //TEAM_COLOR team = (TEAM_COLOR)packet->team;
     //cl_id 클라이언트의 로비 번호 = packet->lobbynum ;
     //cl_id 클라이언트의 로비 안에서의 번호 = packet->order;
     //cl_id 클라이언트의 로비에서 방장인지 아닌지 = packet->owner;

     // 로비 선택할 시 자기 위치에 따라 위치 고정
     // 클라가 일본군인지 조선군인지 선택하는것 처리 반응 어디서 할지 정해야 함.

     // select lobby button 눌렀을때 서버로 보내야 하는 패킷
     // cs_packet_select_lobby packet;
     // packet.size = sizeof(cs_packet_select_lobby);
     // packet.type = C2S_SELECT_LOBBY;
     // PacketMgr::GetInst()->send_packet(&packet);


     // start button 눌렀을때 서버로 보내야 하는 패킷
     // cs_packet_start_game packet;
     // packet.size = sizeof(cs_packet_start_game);
     // packet.type = C2S_START_GAME;
     // PacketMgr::GetInst()->send_packet(&packet);

     // 다시 로비하는 화면으로 이동 및 서버측으로부터 순서 리셋 받음

     //cl_id 클라이언트의 로비 번호 = packet->lobbynum;
     //cl_id 클라이언트의 로비 안에서의 번호 = packet->order;
     //cl_id 클라이언트의 로비에서 방장인지 아닌지 = packet->owner;