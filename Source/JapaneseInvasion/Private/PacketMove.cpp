// Fill out your copyright notice in the Description page of Project Settings.


#include "PacketMove.h"

#include "PacketMgr.h"

PacketMove::PacketMove()
{
}

PacketMove::~PacketMove()
{
}

void PacketMove::move(int dir)
{
}

void PacketMove::send_move_packet(unsigned char dir, OBJ_TYPE eType, FVector vec, float camvalue, float rotvalue, float turnvalue)
{
	cs_packet_move p_move;

	p_move.type = C2S_MOVE;
	p_move.size = sizeof(cs_packet_move);
	p_move.dir = dir;
	p_move.x = vec.X;
	p_move.y = vec.Y;
	p_move.z = vec.Z;
	p_move.CamValue = camvalue;
	p_move.RotValue = rotvalue;
	p_move.TurnValue = turnvalue;
	p_move.objtype = eType;
	PacketMgr::GetInst()->send_packet(&p_move);
}
	