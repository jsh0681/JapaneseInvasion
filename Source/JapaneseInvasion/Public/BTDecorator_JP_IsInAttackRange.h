#pragma once
#include "JapaneseInvasion.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTDecorator_JP_IsInAttackRange.generated.h"

UCLASS()
class JAPANESEINVASION_API UBTDecorator_JP_IsInAttackRange : public UBTDecorator
{
	GENERATED_BODY()
public:
	UBTDecorator_JP_IsInAttackRange();

protected:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
