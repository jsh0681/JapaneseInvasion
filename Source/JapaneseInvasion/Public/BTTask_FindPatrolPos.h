#pragma once

#include "JapaneseInvasion.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_FindPatrolPos.generated.h"
//LNK2001 "public: virtual void __cdecl IGameplayTaskOwnerInterface::OnGameplayTaskActivated(class UGameplayTask &)" (?OnGameplayTaskActivated@IGameplayTaskOwnerInterface@@UEAAXAEAVUGameplayTask@@@Z) 외부 기호를 확인할 수 없습니다.
//위의 오류는 Build.cs 폴더에 GameplayTasks를 추가해주지 않아서 생기는 오류다.

UCLASS()
class JAPANESEINVASION_API UBTTask_FindPatrolPos : public UBTTaskNode
{
	GENERATED_BODY()
public:
	UBTTask_FindPatrolPos();

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;


};
