#pragma once
#include "JapaneseInvasion.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_JP_Attack.generated.h"


UCLASS()
class JAPANESEINVASION_API UBTTask_JP_Attack : public UBTTaskNode
{
	GENERATED_BODY()
public:
	UBTTask_JP_Attack();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)override;
	bool IsAttacking = false;

	
};
