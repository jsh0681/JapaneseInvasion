#pragma once
#include "JapaneseInvasion.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_JP_TurnToTarget.generated.h"

UCLASS()
class JAPANESEINVASION_API UBTTask_JP_TurnToTarget : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTTask_JP_TurnToTarget();

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
