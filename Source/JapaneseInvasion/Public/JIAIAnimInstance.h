#pragma once
#include "JapaneseInvasion.h"
#include "Animation/AnimInstance.h"
#include "JIAIAnimInstance.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnNextAttackCheckDelegate);
DECLARE_MULTICAST_DELEGATE(FOnAttackHitCheckDelegate);
UCLASS()
class JAPANESEINVASION_API UJIAIAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	UJIAIAnimInstance();
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
	void PlayAttackMontage();
	void PlayRunMontage();
	void PlayIdleMontage();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pawn", Meta = (AllowPrivateAccess = true))
		float CurrentPawnSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pawn", Meta = (AllowPrivateAccess = true))
		bool IsInAir;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pawn", meta = (AllowPrivateAccess = true))
		bool IsCanDash;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pawn", meta = (AllowPrivateAccess = true))
		bool IsShooting;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
		bool IsDead;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Attack", Meta = (AllowPrivateAccess = true))
		UAnimMontage* AttackMontage;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Run", meta = (AllowPrivateAccess = true))
		UAnimMontage* RunMontage;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Idle", meta = (AllowPrivateAccess = true))
		UAnimMontage* IdleMontage;

	UFUNCTION()
		void AnimNotify_AttackHitCheck();

	UFUNCTION()
		void AnimNotify_NextAttackCheck();

	UFUNCTION()
		FName GetAttackMontageSectionName(int32 Section);
private:

public:
	void JumpToAttackMontageSection(int32 NewSection);
	void SetDeadAnim() { IsDead = true; };

	FOnAttackHitCheckDelegate OnAttackHitCheck;
	FOnNextAttackCheckDelegate OnNextAttackCheck;
};
