#pragma once
#include "JapaneseInvasion.h"
#include "GameplayTagContainer.h"
#include "GameFramework/Character.h"
#include "protocol.h"
#include "JIAICharacter.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnAttackEndDelegate);

UCLASS()
class JAPANESEINVASION_API AJIAICharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AJIAICharacter();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void PostInitializeComponents() override;
	virtual void PossessedBy(AController* NewController) override;
	void SetTargetingCount();
	uint8 GetTargetingCount() { return TargetingCount; };

	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser);
	void AttackCheck();
public:

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = true))
		bool IsAttacking;

	UFUNCTION()
		void OnAttackMontageEnded(UAnimMontage* Montage, bool bInterrupted);
public:
	UPROPERTY(VisibleAnywhere, Category = Stat)
		class UJICharacterStatComponent* CharacterStat;

	EWeaponType CurWeaponIndex = EWeaponType::SWORD;

	UPROPERTY()
		class UJIJapanAIAnimInstance* JIAnim;

	UPROPERTY(VisibleAnywhere, Category = Weapon)
		class AJIWeapon* Weapon;

	UPROPERTY(VisibleAnywhere, Category = UI)
		class UWidgetComponent* HPBarWidget;


	FName WeaponSocketName;

	UPROPERTY(VisibleAnywhere, Category = Tags)
		TArray<FName> ActorTag;

	UFUNCTION()
		void Attack();

	uint8 TargetingCount = 0;

	FOnAttackEndDelegate OnAttackEnd;

	int m_id;
	TEAM_COLOR m_teamcolor;
	OBJ_TYPE objtype = OBJ_TYPE::SOLDIER;
	GENERAL_STATUS m_eStatus;
};
