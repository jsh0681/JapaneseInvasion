#pragma once
#include "JapaneseInvasion.h"
#include "AIController.h"
#include "JIAIController.generated.h"

UCLASS()
class JAPANESEINVASION_API AJIAIController : public AAIController
{
	GENERATED_BODY()

public:
	AJIAIController();
	virtual void OnPossess(APawn* pawn) override;
private:
	UPROPERTY()
		class UBehaviorTree* BTAsset;
	UPROPERTY()
		class UBlackboardData* BBAsset;
public:
	static const FName HomePosKey;
	static const FName PatrolPosKey;
	static const FName TargetKey;
};
