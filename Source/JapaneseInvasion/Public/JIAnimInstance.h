#pragma once

#include "JapaneseInvasion.h"
#include "Animation/AnimInstance.h"
#include "JIAnimInstance.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnNextAttackCheckDelegate);
DECLARE_MULTICAST_DELEGATE(FOnAttackHitCheckDelegate);
DECLARE_MULTICAST_DELEGATE(FOnShootStartCheckDelegate);
DECLARE_MULTICAST_DELEGATE(FOnShootEndCheckDelegate);

UCLASS()
class JAPANESEINVASION_API UJIAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	UJIAnimInstance();


	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	void PlayAttackMontage();
	void PlayShootMontage();
	void PlayRunMontage();
	void PlayIdleMontage();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pawn", Meta = (AllowPrivateAccess = true))
		float CurrentPawnSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pawn", Meta = (AllowPrivateAccess = true))
		bool IsInAir;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pawn", meta = (AllowPrivateAccess = true))
		bool IsCanDash;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pawn", meta = (AllowPrivateAccess = true))
		bool IsShooting;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Shoot", Meta = (AllowPrivateAccess = true))
		UAnimMontage* ShootMontage;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Attack", Meta = (AllowPrivateAccess = true))
		UAnimMontage* AttackMontage;


	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Run", meta = (AllowPrivateAccess = true))
		UAnimMontage* RunMontage;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Idle", meta = (AllowPrivateAccess = true))
		UAnimMontage* IdleMontage;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
		bool IsDead;

	UFUNCTION()
		void AnimNotify_AttackHitCheck();

	UFUNCTION()
		void AnimNotify_NextAttackCheck();

	UFUNCTION()
		void AnimNotify_ShootStartCheck();

	UFUNCTION()
		void AnimNotify_ShootEndCheck();

	UFUNCTION()
		FName GetAttackMontageSectionName(int32 Section);

private:

public:
	void JumpToAttackMontageSection(int32 NewSection);
	void SetDeadAnim() { IsDead = true; };

	FOnAttackHitCheckDelegate OnAttackHitCheck;
	FOnNextAttackCheckDelegate OnNextAttackCheck;
	FOnShootStartCheckDelegate OnShootStartCheck;
	FOnShootEndCheckDelegate OnShootEndCheck;


	//����
	UPROPERTY(BlueprintReadOnly, Category = "Audio")
		UAudioComponent* SwordAudioComponent;

	UPROPERTY(BlueprintReadOnly, Category = "Audio")
		USoundCue* SwordSoundCue;
};
