// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JIWeapon.h"
#include "JIArrow.generated.h"

/**
 * 
 */
UCLASS()
class JAPANESEINVASION_API AJIArrow : public AJIWeapon
{
	GENERATED_BODY()
public:
	AJIArrow();
	virtual void Tick(float DeltaTime)override;
	virtual void PostInitializeComponents() override;
	//void AttackCheck();
protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateaAccess = true))
		UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(VisibleAnywhere, Category = Effect)
		USkeletalMeshComponent* TrailMesh;

	UFUNCTION()
		void SetVelocity(FVector Vel) { ProjectileMovement->Velocity = Vel; };

	UFUNCTION()
		void FireInDirection(const FVector& ShootDirection);

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	bool bCanSend = false;

	void Timer();
	void TimeAdd();

	FTimerHandle TimerHandle;

	int m_id;

	UPROPERTY(VisibleAnywhere, Category = Tags)
		TArray<FName> ActorTag;
};