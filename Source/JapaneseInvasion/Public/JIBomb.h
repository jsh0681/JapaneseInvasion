#pragma once
#include "JapaneseInvasion.h"
#include "GameFramework/Actor.h"
#include "JIBomb.generated.h"

UCLASS()
class JAPANESEINVASION_API AJIBomb : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AJIBomb();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION()
		void OnEffectFinished(class UParticleSystemComponent* PSystem);

public://Custom Function
	UFUNCTION()
		void SetVelocity(FVector Vel) { ProjectileMovement->Velocity = Vel; };

	UPROPERTY(BlueprintReadOnly, Category = "Audio")
		UAudioComponent* ExplosionAudioComponent;

	UPROPERTY(BlueprintReadOnly, Category = "Audio")
		USoundCue* ExplosionSoundCue;
private:

	UPROPERTY(VisibleAnywhere, Category = Movement, meta = (AllowPrivateaAccess = true))
		UStaticMeshComponent* Shell;

	UPROPERTY(VisibleAnywhere, Category = Movement, meta = (AllowPrivateaAccess = true))
		UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(VisibleAnywhere, Category = Effect)
		UParticleSystemComponent* ExplosionEffect;

	UPROPERTY(VisibleAnywhere, Category = Effect)
		USkeletalMeshComponent* TrailMesh;

	UPROPERTY(VisibleAnywhere, Category = Collisionm, meta = (AllowPrivateaAccess = true))
		UCapsuleComponent* Collision;
public:
	bool bCanSend = false;

	void Timer();
	void TimeAdd();

	FTimerHandle TimerHandle;

	int m_iId;

	UPROPERTY(VisibleAnywhere, Category = Tags)
		TArray<FName> ActorTag;
};