// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JIWeapon.h"
#include "JIBow.generated.h"

/**
 * 
 */
UCLASS()
class JAPANESEINVASION_API AJIBow : public AJIWeapon
{
	GENERATED_BODY()
public:
	AJIBow();

public:
	virtual void Tick(float DeltaTime);
	virtual void PostInitializeComponents() override;
protected:
	virtual void BeginPlay() override;
};
