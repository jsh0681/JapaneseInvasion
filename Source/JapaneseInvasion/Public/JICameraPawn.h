// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "JICameraPawn.generated.h"

UCLASS()
class JAPANESEINVASION_API AJICameraPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AJICameraPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override; 
	
	UFUNCTION()
		void MoveForward(float NewAxisValue);

	UFUNCTION()
		void MoveRight(float NewAxisValue);

	UFUNCTION()
		void Roll(float NewAxisValue);

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:
	/** Returns Camera subobject **/
	FORCEINLINE class UCameraComponent* GetCamera() const { return Camera; }
	/** Returns SpringArm subobject **/
	FORCEINLINE class USpringArmComponent* GetSpringArm() const { return SpringArm; }

	void ResetTargetArmLengthDefault() { SpringArm->TargetArmLength = 30000.f; };
public:
	UPROPERTY(VisibleAnywhere, Category = Root)
		UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleAnywhere, Category = Root)
		UStaticMeshComponent* CurrentLocationIndicatorInMinimap;

	UPROPERTY(VisibleAnywhere, Category = Movement)
		UFloatingPawnMovement* Movement;

	UPROPERTY(VisibleAnywhere, Category = SpringArm)
		USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere, Category = Camera)
		UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, Category = Tags)
		TArray<FName> ActorTag;




	FVector CameraInput;
	FVector MoveDirection;
	float CurrentVelocity;

	bool bLogin;
};
