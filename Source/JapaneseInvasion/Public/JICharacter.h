#pragma once
#include "JapaneseInvasion.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "GameFramework/Character.h"
#include "WidgetComponent.h"
#include "protocol.h"
#include "JICharacter.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnAttackEndDelegate);

UCLASS()
class JAPANESEINVASION_API AJICharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AJICharacter();

protected:
	virtual void BeginPlay() override;

public:
	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	virtual void PostInitializeComponents() override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)override;
	virtual void PossessedBy(AController* NewController) override;

	void SetTargetingCount();
	uint8 GetTargetingCount() { return TargetingCount; };
private:
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = true))
		bool IsAttacking;

public:
	UFUNCTION()
		void Attack();
	void AttackCheck();

	UFUNCTION()
		void OnAttackMontageEnded(UAnimMontage* Montage, bool bInterrupted);

public:
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
private:

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = "true"))
		uint8 TargetingCount;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = "true"))
		bool CanNextCombo;
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = "true"))
		bool IsComboInputOn;
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = "true"))
		int32 CurrentCombo;
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = "true"))
		int32 MaxCombo;

	UPROPERTY(VisibleAnywhere, Category = Weapon)
		class AJIWeapon* Weapon;

public:
	UPROPERTY(VisibleAnywhere, Category = UI)
		class UWidgetComponent* HPBarWidget;


	EWeaponType CurWeaponIndex = EWeaponType::SWORD;

public:
	UPROPERTY(VisibleAnywhere, Category = Tags)
		TArray<FName> ActorTag;

	FOnAttackEndDelegate OnAttackEnd;

	UPROPERTY(VisibleAnywhere, Category = Stat)
		class UJICharacterStatComponent* CharacterStat;

	UPROPERTY()
		class UJIAIAnimInstance* JIAnim;

	int m_id;
	TEAM_COLOR m_teamcolor;
	GENERAL_STATUS m_eStatus;
};