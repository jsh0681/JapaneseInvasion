#pragma once
#include "JapaneseInvasion.h"
#include "Components/ActorComponent.h"
#include "JICharacterStatComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnHPIsZeroDelegate);
DECLARE_MULTICAST_DELEGATE(FOnHPChangedDelegate);


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class JAPANESEINVASION_API UJICharacterStatComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UJICharacterStatComponent();

protected:
	virtual void BeginPlay() override;
	virtual void InitializeComponent() override;
public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void SetObejctType(EObjectType type);
	void SetDamage(float NewDamage);
	void SetHP(float NewHP);
	float GetSwordAttack();
	float GetArrowAttack();
	float GetHPRatio();

public:
	FOnHPIsZeroDelegate OnHPIsZero;
	FOnHPChangedDelegate OnHPChanged;
public:
	UPROPERTY(Transient, VisibleInstanceOnly, Category = Stat, Meta = (AllowPrivateAccess = true))
		float CurrentHP;

private:
	struct FJICharacterData* CurrentStatData = nullptr;

	UPROPERTY(EditInstanceOnly, Category = Type, Meta = (AllowPrivateAccess = true))
		EObjectType CharacterType;
};
