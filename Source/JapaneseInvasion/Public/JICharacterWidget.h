// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "JICharacterWidget.generated.h"

/**
 * 
 */
UCLASS()
class JAPANESEINVASION_API UJICharacterWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void BindCharacterStat(class UJICharacterStatComponent* NewCharacterStat);

protected:
	virtual void NativeConstruct() override;
	void UpdateHPWidget();
private:
	TWeakObjectPtr<class UJICharacterStatComponent> CurrentCharacterStat;

	UPROPERTY()
		class UProgressBar* HPProgressBar;
public:
	void SetHPBarJoseonColor();
	void SetHPBarJapanColor();

};
