// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "JIChatBox.generated.h"

/**
 * 
 */
UCLASS()
class JAPANESEINVASION_API UJIChatBox : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UJIChatBox(const FObjectInitializer& ObjectInitializer);

public:
	virtual void NativeConstruct() override;

	UPROPERTY()
		class UTextBlock* MessageBlock;

};
