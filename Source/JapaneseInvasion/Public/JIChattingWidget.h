#pragma once

#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "JIChattingWidget.generated.h"
class UJIChatBox;

UCLASS()
class JAPANESEINVASION_API UJIChattingWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UJIChattingWidget(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void NativeConstruct() override;

public:

	UJIChatBox* MakeChatBox();

	UPROPERTY()
		class UEditableTextBox* TextChattingBox;

	UPROPERTY()
		class UScrollBox* ScrollBoxChattingLog;

	UPROPERTY()
		class UButton* btnSendMessage;

	UPROPERTY()
		class UTextBlock* MessageTextBlock;

	UFUNCTION()
		void OnBtnSendMessageClicked();

	UPROPERTY()
		TArray<FString> ChatLogArray;

	UFUNCTION()
		void OnChattingCommitted(const FText& InText, ETextCommit::Type InCommitType);


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = UI)
		TSubclassOf<class UJIChatBox> ChatBoxClass;

};
