// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "Engine/DataTable.h"
#include "Engine/GameInstance.h"
#include "Engine/StreamableManager.h"
#include "JIGameInstance.generated.h"

USTRUCT(BlueprintType)
struct FJICharacterData : public FTableRowBase
{
	GENERATED_BODY()

public:
	FJICharacterData() : Level(1), Type("Null"), HP(100), MaxHP(100), Sword(10), Arrow(10) {}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		int32 Level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		FString Type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float HP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float MaxHP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float Sword;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float Arrow;
};


USTRUCT(BlueprintType)
struct FJIKoreaShipData : public FTableRowBase
{
	GENERATED_BODY()

public:
	FJIKoreaShipData() : Level(1), MaxHP(100), Attack(10), Reload(3),Velocity(50),DropExp(10),NextExp(30) {}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		int32 Level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float MaxHP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float Attack;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float Reload;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float Velocity;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		int32 DropExp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		int32 NextExp;
};


USTRUCT(BlueprintType)
struct FOceanValue : public FTableRowBase
{
	GENERATED_BODY()

public:
	FOceanValue() : Time(1), WaveSpeed(0), WaveDirectionX(0),WaveDirectionY(0), WaveAmplitube(0) {}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		int32 Time;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float WaveSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float WaveDirectionX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float WaveDirectionY;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float WaveAmplitube;
};

UCLASS()
class JAPANESEINVASION_API UJIGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UJIGameInstance();
	virtual void Init() override;
	FJICharacterData* GetJICharacterData(EObjectType ObjectType);

	FJIKoreaShipData* GetJIKoreaShipData(int32 Level);

	FOceanValue* GetOceanValue(int32 Time);
private:
	UPROPERTY()
		class UDataTable* JICharacterTable;

	UPROPERTY()
		class UDataTable* JIKoreaShipTable;

	UPROPERTY()
		class UDataTable* JIOceanTable;
public:

	FStreamableManager StreamableManager;
};

