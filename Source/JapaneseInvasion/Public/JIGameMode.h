// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "GameFramework/GameModeBase.h"
#include "JIGameMode.generated.h"

/**
 * 
 */
UCLASS()
class JAPANESEINVASION_API AJIGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AJIGameMode();
	virtual void PostLogin(APlayerController* NewPlayer) override;
};
