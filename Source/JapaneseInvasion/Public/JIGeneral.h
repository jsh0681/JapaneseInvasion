#pragma once
#include "JapaneseInvasion.h"
#include "JIWeapon.h"  
#include "JIAnimInstance.h"
#include "GameFramework/Character.h"
#include "protocol.h"
#include "JIGeneral.generated.h"

UCLASS()
class JAPANESEINVASION_API AJIGeneral : public ACharacter
{
    GENERATED_BODY()
public:
    // Sets default values for this character's properties
    AJIGeneral();
protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:
    virtual void Tick(float DeltaTime) override;
    virtual void PostInitializeComponents() override;
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
    UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = true))
        bool IsAttacking;

    UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Shoot, meta = (AllowPrivateAccess = true))
        bool IsShooting;

public:
    //Getter
    FORCEINLINE class UCameraComponent* GetCamera() const { return Camera; }
    FORCEINLINE class USpringArmComponent* GetSpringArm() const { return SpringArm; }

public:

    UPROPERTY()
        FTimerHandle UnusedHandle;

public:

    UFUNCTION()
        void Attack();

    UFUNCTION()
        void OnAttackMontageEnded(UAnimMontage* Montage, bool bInterrupted);

    UFUNCTION()
        void WeaponChange();

    UFUNCTION()
        void Shoot();

    UFUNCTION()
        FName GetWeaponSocketName() { return WeaponSocketName; };

    UFUNCTION()
        void MoveRight(float NewAxisValue);

    UFUNCTION()
        void MoveForward(float NewAxisValue);

    virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

    UFUNCTION()
        void Turn(float NewAxisValue);
    UFUNCTION()
        void LookUp(float NewAxisValue);

public:
    void AttackStartComboState();
    void AttackEndComboState();
    void AttackCheck();
    void ResetTargetArmLengthDefault() { SpringArm->TargetArmLength = 400.0f; };
    void SetID(int id) { m_iId = id; }
    const int& GetID() { return m_iId; }
    virtual void PossessedBy(AController* NewController) override;
public:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
        class UCameraComponent* Camera;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
        class USpringArmComponent* SpringArm;


    UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = "true"))
        bool CanNextCombo;

    UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = "true"))
        bool IsComboInputOn;

    UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = "true"))
        int32 CurrentCombo;

    UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = "true"))
        int32 MaxCombo;

    UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = "true"))
        float AttackRange;

    UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attack, meta = (AllowPrivateAccess = "true"))
        float AttackRadius;

    UPROPERTY(VisibleAnywhere, Category = Stat)
        class UJICharacterStatComponent* CharacterStat;

    EWeaponType CurWeaponIndex = EWeaponType::SWORD;

    UPROPERTY()
        class UJIAnimInstance* JIAnim;

    UPROPERTY(VisibleAnywhere, Category = Weapon)
        class AJIWeapon* Weapon;

    UPROPERTY(VisibleAnywhere, Category = UI)
        class UWidgetComponent* HPBarWidget;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = UI)
        TSubclassOf<class UJIGeneralHUDWidget> GeneralHUDWidgetClass;
    UPROPERTY()
        class UJIGeneralHUDWidget* GeneralHUDWidget;


    UPROPERTY(VisibleAnywhere, Category = Tags)
        TArray<FName> ActorTag;

    bool IsTargeting = false;

    FVector ArrowVelocity;
    FName WeaponSocketName = TEXT("RHand_Sword");

    //서버부분
    FVector   m_vPrevPos;
    FVector   m_vCurPos;

    float m_vPosX;
    float m_vPosY;
    float m_vPosZ;
    float m_fTurnAxisValue;

    float m_fTurnValue;
    float m_fCamValue;
    float m_fRotValue;

    FVector m_fCamVec;

    unsigned char m_Dir;

    int m_iId = -1;
    bool m_bIsOwner = false;

    OBJ_TYPE m_eObjType = OBJ_TYPE::GENERAL;
    TEAM_COLOR m_eTeamColor;

    GENERAL_STATUS m_eStatus;

    wchar_t m_strID[MAX_STR_LEN];

public:
    //UPROPERTY(VisibleAnywhere, Category = UI)
     //   class UJICharacterWidget* CharacterWidget;
    void OnAssetLoadCompleted();
    FSoftObjectPath GeneralAssetToLoad = FSoftObjectPath(nullptr);
    TSharedPtr<struct FStreamableHandle> AssetStreamingHandle;
    void SetAsset();
    int AssetIndex=0;

    //사운드
    UPROPERTY(BlueprintReadOnly, Category = "Audio")
        UAudioComponent* SwordAudioComponent;

    UPROPERTY(BlueprintReadOnly, Category = "Audio")
        USoundCue* SwordSoundCue;
    

    UPROPERTY(BlueprintReadOnly, Category = "Audio")
        UAudioComponent* SwordFinalAudioComponent;
    UPROPERTY(BlueprintReadOnly, Category = "Audio")
        USoundCue* SwordFinalSoundCue;



    //사운드
    UPROPERTY(BlueprintReadOnly, Category = "Audio")
        UAudioComponent* ArrowAudioComponent;

    UPROPERTY(BlueprintReadOnly, Category = "Audio")
        USoundCue* ArrowSoundCue;

};