#pragma once
#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "JIGeneralHUDWidget.generated.h"

UCLASS()
class JAPANESEINVASION_API UJIGeneralHUDWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UJIGeneralHUDWidget(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void NativeConstruct() override;
public:
	UPROPERTY()
		class UTexture2D* GeneralStateBarTex;

	UPROPERTY()
		class UTexture2D* BowTex;

	UPROPERTY()
		class UTexture2D* SwordTex;

	UPROPERTY()
		class UImage* GeneralStateImg;

	UPROPERTY()
		class UImage* WeaponStateImg;


	FSlateBrush MakeBrush(UTexture2D* inputTexture, int x, int y);

	void SetBowImg();
	
	void SetSwordImg();



};
