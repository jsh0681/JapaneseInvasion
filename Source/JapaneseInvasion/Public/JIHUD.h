#pragma once
#include "JapaneseInvasion.h"
#include "JIShip.h"
#include "JIHUD.generated.h"

UCLASS()
class JAPANESEINVASION_API AJIHUD : public AHUD
{
	GENERATED_BODY()
public:
	virtual void DrawHUD() override;
	FVector2D GetMousePos2D(); 

public:
	FVector2D m_vInitialPoint;

	FVector2D m_vCurrentPoint;

	UPROPERTY()
		bool m_bStartSelecting = false;

	UPROPERTY()
		TArray<AJIShip*> FoundActors;
};
