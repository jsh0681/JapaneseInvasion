#pragma once
#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"
#include "JIHUDWidget.generated.h"

UCLASS()
class JAPANESEINVASION_API UJIHUDWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UJIHUDWidget(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void NativeConstruct() override;
public:

	UPROPERTY(VisibleAnywhere, Category = "Button")
		class UButton* MoveButton;
	
	UPROPERTY(VisibleAnywhere, Category = "Button")
		class UButton* AttackButton;

	UPROPERTY(VisibleAnywhere, Category = "Button")
		class UButton* StopButton;

	UPROPERTY(VisibleAnywhere, Category = "Button")
		class UButton* ZoomButton;

	UPROPERTY(VisibleAnywhere, Category = "Button")
		class UButton* MapButton;

	UPROPERTY(VisibleAnywhere, Category = "Button")
		class UButton* OceanButton;

	UPROPERTY(VisibleAnywhere, Category = "Button")
		class UButton* OptionButton;

	UPROPERTY(VisibleAnywhere, Category = "Button")
		class UButton* ReloadButton;





	UPROPERTY(VisibleAnywhere, Category = "Widget Switcher")
		class UButton* Img_Button;

	UPROPERTY(VisibleAnywhere, Category = "Widget Switcher")
		class UWidgetSwitcher* WS_HUD;

	UPROPERTY(VisibleAnywhere, Category = "Image")
		TArray<UImage*> Img;

	UPROPERTY(VisibleAnywhere, Category = "Button")
		TArray<UButton*> Btn;

	UPROPERTY()
		TArray<class AJIShip*> SelectedShip;

	// C++ 불완전한 클래스 형식에 대한 포인터는 사용할 수 없습니다. 
	// 이런 오류 뜨면 선언 앞에 class 써주면 해결됩니다.

	UPROPERTY()
		class UTexture2D* PanokTex;
	UPROPERTY()
		class UTexture2D* PanokTexGreen;
	UPROPERTY()
		class UTexture2D* PanokTexRed;
	UPROPERTY()
		class UTexture2D* PanokTexYellow;

	UPROPERTY()
		class UTexture2D* TurtleTex;
	UPROPERTY()
		class UTexture2D* TurtleTexGreen;
	UPROPERTY()
		class UTexture2D* TurtleTexRed;
	UPROPERTY()
		class UTexture2D* TurtleTexYellow;


	UPROPERTY()
		class UTexture2D* AntakeTexGreen;
	UPROPERTY()
		class UTexture2D* AntakeTexRed;
	UPROPERTY()
		class UTexture2D* AntakeTexYellow;


	UPROPERTY()
		class UTexture2D* NullTex;

	UPROPERTY()
		class UTexture2D* MoveNoramlTex;
	UPROPERTY()
		class UTexture2D* MovePressedTex;

	UPROPERTY()
		class UTexture2D* StopNoramlTex;
	UPROPERTY()
		class UTexture2D* StopPressedTex;

	UPROPERTY()
		class UTexture2D* AttackNoramlTex;
	UPROPERTY()
		class UTexture2D* AttackPressedTex;

	UPROPERTY()
		class UTexture2D* ZoomNoramlTex;
	UPROPERTY()
		class UTexture2D* ZoomPressedTex;

	UPROPERTY()
		class UTexture2D* OceanNoramlTex;
	UPROPERTY()
		class UTexture2D* OceanPressedTex;

	UPROPERTY()
		class UTexture2D* MapNoramlTex;
	UPROPERTY()
		class UTexture2D* MapPressedTex;

	UPROPERTY()
		class UTexture2D* ReloadNoramlTex;
	UPROPERTY()
		class UTexture2D* ReloadPressedTex;

	UPROPERTY()
		class UTexture2D* OptionNoramlTex;
	UPROPERTY()
		class UTexture2D* OptionPressedTex;




	UPROPERTY()
		class UTextBlock* CurrentHP;
	UPROPERTY()
		class UTextBlock* MaxHP;

	UPROPERTY()
		class UTextBlock* ResidualShell;

	UPROPERTY()
		class UTextBlock* MaxShell;


	UPROPERTY()
		class UTextBlock* Damage;

	UPROPERTY()
		class UTextBlock* Speed;

	UPROPERTY()
		class UTextBlock* Kinds;

	UFUNCTION()
		void SetShipStat(AJIShip* ship);

	UFUNCTION()
		void SetMakeButtonStyle(int index);

	UFUNCTION()
		void SetSelectedShipImage(TArray<AJIShip*>& arr);

	UFUNCTION()
		FSlateBrush MakeBrush(UTexture2D* inputTexture, int x, int y);

	UFUNCTION()
		FButtonStyle MakeButtonStyle(const FSlateBrush brush);

	UFUNCTION()
		void OnMoveButtonClicked();
	UFUNCTION()
		void OnMoveButtonReleased();

	UFUNCTION()
		void OnStopButtonClicked();
	UFUNCTION()
		void OnStopButtonReleased();

	UFUNCTION()
		void OnAttackButtonClicked();
	UFUNCTION()
		void OnAttackButtonReleased();

	UFUNCTION()
		void OnZoomButtonClicked();
	UFUNCTION()
		void OnZoomButtonReleased();

	UFUNCTION()
		void OnOptionButtonClicked();
	UFUNCTION()
		void OnOptionButtonReleased();

	UFUNCTION()
		void OnReloadButtonClicked();
	UFUNCTION()
		void OnReloadButtonReleased();

	UFUNCTION()
		void OnOceanButtonClicked();
	UFUNCTION()
		void OnOceanButtonReleased();

	UFUNCTION()
		void OnMapButtonClicked();
	UFUNCTION()
		void OnMapButtonReleased();



	UFUNCTION()
		void OnShipButtonReleased0();
	UFUNCTION()
		void OnShipButtonReleased1();
	UFUNCTION()
		void OnShipButtonReleased2();
	UFUNCTION()
		void OnShipButtonReleased3();
	UFUNCTION()
		void OnShipButtonReleased4();
	UFUNCTION()
		void OnShipButtonReleased5();
	UFUNCTION()
		void OnShipButtonReleased6();
	UFUNCTION()
		void OnShipButtonReleased7();
	UFUNCTION()
		void OnShipButtonReleased8();
	UFUNCTION()
		void OnShipButtonReleased9();
	UFUNCTION()
		void OnShipButtonReleased10();
	UFUNCTION()
		void OnShipButtonReleased11();

	UFUNCTION(BlueprintCallable)
		UWidgetAnimation* GetAnimationByName(FName AnimationName) const;

/*  @brief 이름으로 애니메이션 재생
 *  @date 2020-04-13
 *  @return bool : 재생 여부
 *  @param AnimationName : 애니메이션 이름
 *  @param StartAtTime : 시작 시간 위치
 *  @param NumLoopsToPlay : 루프 횟수
 *  @param PlayMode : 재생 모드
 *  @param PlaybackSpeed : 재생 속도 */

UFUNCTION(BlueprintCallable)
bool PlayAnimationByName(FName AnimationName,float StartAtTime = 0.0f,int32 NumLoopsToPlay = 1,EUMGSequencePlayMode::Type PlayMode = EUMGSequencePlayMode::Forward, float PlaybackSpeed = 1.0f);

protected:
	void FillAnimationMap();

protected:
	// 애니메이션 맵
	TMap<FName, UWidgetAnimation*> AnimationsMap;

public:
	void SetAudioComponent(UAudioComponent* AudioComponent);
	void ClickOnShip();
	//사운드
private:
	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* ClickAudioComponent;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		USoundCue* ClickSoundCue;
};
