#pragma once
#include "JapaneseInvasion.h"
#include "AIController.h"
#include "JIJapanAIController.generated.h"

UCLASS()
class JAPANESEINVASION_API AJIJapanAIController : public AAIController
{
	GENERATED_BODY()

public:
	AJIJapanAIController();
	virtual void OnPossess(APawn* pawn) override;
private:
	UPROPERTY()
		class UBehaviorTree* BTAsset;
	UPROPERTY()
		class UBlackboardData* BBAsset;
public:
	static const FName HomePosKey;
	static const FName PatrolPosKey;
	static const FName TargetKey;
};
