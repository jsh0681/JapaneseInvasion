// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "Components/ActorComponent.h"
#include "JIKoreaShipStatComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnHPIsZeroDelegate);
DECLARE_MULTICAST_DELEGATE(FOnHPChangedDelegate);
DECLARE_MULTICAST_DELEGATE(FOnReloadDelegate);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class JAPANESEINVASION_API UJIKoreaShipStatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UJIKoreaShipStatComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void InitializeComponent() override;

public:
	void SetNewLevel(int32 NewLevel);
	void SetDamage(float NewDamage);
	void SetHP(float NewHP);
	void SetReload(float CurrentReload);
	float GetAttack();
	float GetHPRatio();
	float GetReload();
	float GetCurrentHP();
	float GetMaxHP();
	float GetResidualShell();
	float GetMaxShell();
	float GetDamage();

	void SetReloadBullet();

	FOnHPChangedDelegate OnHPChanged;
	FOnHPIsZeroDelegate OnHPIsZero;
	FOnReloadDelegate OnReloadOn; 
public: //����
	UPROPERTY(Transient, VisibleAnywhere, Category = "Stat", Meta = (AllowPrivateAccess = true))
		float CurrentHP;
private:
	struct FJIKoreaShipData* CurrentStatData = nullptr;
	UPROPERTY(EditInstanceOnly, Category = "Stat", Meta = (AllowPrivateAccess = true))
		int32 Level;

	UPROPERTY(Transient, VisibleAnywhere, Category = "Stat", Meta = (AllowPrivateAccess = true))
		float CurrentReload;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
