// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "JIKoreaShipWidget.generated.h"

/**
 * 
 */
UCLASS()
class JAPANESEINVASION_API UJIKoreaShipWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void BindKoreaShipStat(class UJIKoreaShipStatComponent* NewKoreaShipStat);

protected:
	virtual void NativeConstruct() override;
	void UpdateHPWidget();
	void UpdateReloadWidget();

private:
	TWeakObjectPtr<class UJIKoreaShipStatComponent> CurrentKoreaShipStat;
	
	UPROPERTY()
		class UProgressBar* HPProgressBar;

	UPROPERTY()
		class UProgressBar* ReloadProgressBar;

};
