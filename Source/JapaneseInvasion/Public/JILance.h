#pragma once

#include "JIWeapon.h"
#include "JILance.generated.h"

UCLASS()
class JAPANESEINVASION_API AJILance : public AJIWeapon
{
	GENERATED_BODY()

public:
	AJILance();
	virtual void PostInitializeComponents() override;
	void PlayAnimForTrail();

protected:
	virtual void BeginPlay() override;

	UAnimationAsset* AnimAsset;
};
