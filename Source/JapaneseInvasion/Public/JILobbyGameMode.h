// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "GameFramework/GameModeBase.h"
#include "JILobbyGameMode.generated.h"

/**
 * 
 */
UCLASS()
class JAPANESEINVASION_API AJILobbyGameMode : public AGameModeBase
{
	GENERATED_BODY()
		

public:
	AJILobbyGameMode();
	
};
