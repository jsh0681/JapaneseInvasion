#pragma once
#include "JapaneseInvasion.h"
#include "GameFramework/PlayerController.h"
#include "JILobbyPlayerController.generated.h"

UCLASS()
class JAPANESEINVASION_API AJILobbyPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	AJILobbyPlayerController();

	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadwrite, Category = UI)
		TSubclassOf<class UJILobbyWidget> LobbyWidgetClass;

	UPROPERTY()
		class UJILobbyWidget* LobbyWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadwrite, Category = UI)
		TSubclassOf<class UJILobbySelectWidget> LobbySelectWidgetClass;

	UPROPERTY()
		class UJILobbySelectWidget* LobbySelectWidget;
private:
	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* LobbyBGMAudioComponent;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		USoundCue* LobbyBGMSoundCue;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* HoveredAudioComponent;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		USoundCue* HoveredSoundCue;
};