// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "JILobbySelectWidget.generated.h"

/**
 * 
 */
UCLASS()
class JAPANESEINVASION_API UJILobbySelectWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	virtual void NativeConstruct() override;

public:
	UPROPERTY(BlueprintReadOnly, Category = "UI")
		class UButton* btnLobbyFirst;

	UPROPERTY(BlueprintReadOnly, Category = "UI")
		class UButton* btnLobbySecond;


	UPROPERTY(BlueprintReadOnly, Category = "UI")
		class UButton* btnLobbyThird;


	UPROPERTY(BlueprintReadOnly, Category = "UI")
		class UButton* btnLobbyFourth;


public:
	UFUNCTION()
		void OnBtnLobbyFirstClicked();

	UFUNCTION()
		void OnBtnLobbySecondClicked();

	UFUNCTION()
		void OnBtnLobbyThirdClicked();

	UFUNCTION()
		void OnBtnLobbyFourthClicked();


	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* HoveredAudioComponent;


	UPROPERTY()
		class UJILobbyWidget* LobbyWidget;


	void SetLobbyWidget(UJILobbyWidget* widget);

	void SetAudioComponent(UAudioComponent* Audio);
};
