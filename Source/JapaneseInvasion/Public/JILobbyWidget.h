#pragma once

#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"
#include "JILobbyWidget.generated.h"

UCLASS()
class JAPANESEINVASION_API UJILobbyWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UJILobbyWidget(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void NativeConstruct() override;

public:
	UPROPERTY()
		class UButton* btnJoseon;

	UPROPERTY()
		class UButton* btnJapan;

	UPROPERTY()
		class UButton* btnGameStart;

	UPROPERTY()
		class UButton* btnBack;

	UPROPERTY()
		class UTextBlock* Txt_ID;

	UPROPERTY(VisibleAnywhere, Category = "Image")
		TArray<UTextBlock*> CurrentUserEnterTxtBlock;

	UPROPERTY(VisibleAnywhere, Category = "Image")
		TArray<UTextBlock*> TeamJoseonTxtBlock;

	UPROPERTY(VisibleAnywhere, Category = "Image")
		TArray<UTextBlock*> TeamJapanTxtBlock;

	UPROPERTY(VisibleAnywhere, Category = "Image")
		TArray<UImage*> TeamJapanImg;

	UPROPERTY(VisibleAnywhere, Category = "Image")
		TArray<UImage*> TeamJoseonImg;

	UPROPERTY(VisibleAnywhere, Category = "Image")
		TArray<UImage*> CrownImg;

	UPROPERTY()
		class UTexture2D* JoseonFlagTex;
	UPROPERTY()
		class UTexture2D* JapanFlagTex;
	UPROPERTY()
		class UTexture2D* NullTexture;

	UPROPERTY()
		class UTexture2D* CrownTex; 

	UPROPERTY()
		class UTexture2D* nullBack;

public:
	UFUNCTION()
		FSlateBrush MakeBrush(UTexture2D* inputTexture, int x, int y);

	UFUNCTION()
		void OnBtnJoseonClicked();

	UFUNCTION()
		void OnBtnJapanClicked();

	UFUNCTION()
		void OnBtnGameStartClicked();

	UFUNCTION()
		void OnBtnBackClicked();

	UFUNCTION()
		void OnBtnJoseonHovered();

	UFUNCTION()
		void OnBtnJapanHovered();

	UFUNCTION()
		void OnBtnGameStartHovered();

	UFUNCTION()
		void OnBtnBackHovered();

	void SetAudioComponent(UAudioComponent* Audio);

public:
	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* HoveredAudioComponent;
};
