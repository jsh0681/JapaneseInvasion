#pragma once

#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "JIPlayerController.h"
#include "JIMenuWidget.generated.h"

UCLASS()
class JAPANESEINVASION_API UJIMenuWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;
public:
	UPROPERTY()
		class UButton* OnBtnResume;

	UPROPERTY()
		class UButton* OnBtnReturnToTitle;

	UFUNCTION()
		void OnBtnReturnToTitleClicked();

	UFUNCTION()
		void OnBtnResumeClicked();


	UFUNCTION()
		void Resume();

};
