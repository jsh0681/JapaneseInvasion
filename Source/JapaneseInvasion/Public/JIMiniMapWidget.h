#pragma once
#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "JIMiniMapWidget.generated.h"

UCLASS()
class JAPANESEINVASION_API UJIMiniMapWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UJIMiniMapWidget(const FObjectInitializer& ObjectInitializer);
protected:
	virtual void NativeConstruct() override;
	virtual void NativePreConstruct() override;
private:
	bool bIsOn;

public:

	UFUNCTION()
		void OnMapClicked();

	UPROPERTY()
		class UImage* MinimapImage;

	UPROPERTY()
		class UButton* MinimapButton;

	UFUNCTION(BlueprintCallable)
		UWidgetAnimation* GetAnimationByName(FName AnimationName) const;

	virtual void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

	UFUNCTION(BlueprintCallable)
		bool PlayAnimationByName(FName AnimationName,
			float StartAtTime = 0.0f,
			int32 NumLoopsToPlay = 1,
			EUMGSequencePlayMode::Type PlayMode = EUMGSequencePlayMode::Forward,
			float PlaybackSpeed = 1.0f);

public:
	void SetAudioComponent(UAudioComponent* AudioComponent);
protected:
	void FillAnimationMap();
	TMap<FName, UWidgetAnimation*> AnimationsMap;

	//����
private:
	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* AudioComponent;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		USoundCue* SoundCue;
	UPROPERTY(VisibleAnywhere, Category = "Location")
		FVector2D WidgetClickPos;

};