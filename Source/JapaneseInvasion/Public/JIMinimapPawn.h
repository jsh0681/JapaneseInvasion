// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "GameFramework/Pawn.h"
#include "JIMinimapPawn.generated.h"

UCLASS()
class JAPANESEINVASION_API AJIMinimapPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AJIMinimapPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere, Category = Root)
		UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleAnywhere, Category = SpringArm)
		USpringArmComponent* MiniMapSpringArm;

	UPROPERTY(VisibleAnywhere, Category = MiniMap)
		USceneCaptureComponent2D* Minimap;
};
