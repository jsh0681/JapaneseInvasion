#pragma once
#include "JapaneseInvasion.h"
#include "OceanManager.h"
#include "Components/BillboardComponent.h"
#include "JIGameInstance.h"
#include "Components/StaticMeshComponent.h"
#include "Components/PlanarReflectionComponent.h"
#include "InfiniteSystemComponent.h"
#include "Materials/MaterialInstance.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Materials/MaterialParameterCollection.h"
#include "Math/Color.h"
#include "InfiniteSystemComponent.h"
#include "JIOcean.generated.h"

UCLASS()
class JAPANESEINVASION_API AJIOcean : public AOceanManager
{
	GENERATED_BODY()
	
public:
	AJIOcean();

protected:
	virtual void BeginPlay() override;
public:
	virtual void Tick(float DeltaTime) override;

	virtual void OnConstruction(const FTransform& Transform) override;
public:
	void CreateWaveSet();
	void SetGlobalParameters();
	void SetDisplayParameters();
	void SetOceanState(float DeltaTime);
	AJIOcean* GetOcean();


	float InterpSpeed;
public:

	int Time = 0;

	UPROPERTY(Category = "Data", BlueprintReadWrite, EditAnywhere)
		FOceanValue OceanValue;

	UPROPERTY(Category = "Data", BlueprintReadWrite, EditAnywhere)
		UJIGameInstance* JIGameInstance; 

	UPROPERTY(Category = "Icon", BlueprintReadWrite, EditAnywhere)
		UBillboardComponent* Icon;

	UPROPERTY(Category = "Plane", BlueprintReadWrite, EditAnywhere)
		UStaticMeshComponent* OceanPlane;

	UPROPERTY(Category = "Plane", BlueprintReadWrite, EditAnywhere)
		UStaticMeshComponent* DepthPlaneUnderside;

	UPROPERTY(Category = "Plane", BlueprintReadWrite, EditAnywhere)
		UStaticMeshComponent* DepthPlaneTopside;


	UPROPERTY(Category = "InfiniteSystem", BlueprintReadWrite, EditAnywhere)
		UInfiniteSystemComponent* InfiniteSystem;

	UPROPERTY(Category = "PlaneReflection", BlueprintReadWrite, EditAnywhere)
		UPlanarReflectionComponent* PlanarReflection;

	void PrintLog();

	UPROPERTY()
		FTimerHandle UnusedHandle;

	void TimeAdd();
	void Timer();
	bool bCanPlus = false;



	UPROPERTY(Category = "PlaneReflection", BlueprintReadWrite, EditAnywhere)
		UMaterialInstance* OceanDepthShader;


	UPROPERTY(Category = "PlaneReflection", BlueprintReadWrite, EditAnywhere)
		UMaterialInstanceDynamic* MIDOcean;

	UPROPERTY(Category = "PlaneReflection", BlueprintReadWrite, EditAnywhere)
		UMaterialInstanceDynamic* MIDOceanDepth;

	UPROPERTY(Category = "PlaneReflection", BlueprintReadWrite, EditAnywhere)
		UMaterialParameterCollection* MPC_Ocean;

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//표면 색상의 혼합 계수를 제어합니다//
	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		FLinearColor BaseColorDark;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		FLinearColor BaseColorLight;

	//The color that gets blended for shallow water
	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		FLinearColor ShallowWaterColor;

	//Controls the blending factor for the surface color
	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float BaseColorLerp;

	//The amount of fresnel to use in the depth fade
	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float FresnelPower;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float BaseFresnelReflect;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float Metallic;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float Roughness;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float Specular;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float TesselationMultiplier;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float Opacity;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float BaseDepthFade;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float DistortionStrength;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float SceneColorCustomDepth;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float FoamScale;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float FoamDepth1;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float FoamDepth2;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float FoamTimeScale;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float FoamSoftness1;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float SceneDepthSoftness;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		float BaseDepthFadeSoftness;

	UPROPERTY(Category = "Appearance", BlueprintReadWrite, EditAnywhere)
		UMaterialInstance* OceanShader;


	UPROPERTY(Category = "Subsurface Scattering", BlueprintReadWrite, EditAnywhere)
		FLinearColor SSS_Color;

	UPROPERTY(Category = "Subsurface Scattering", BlueprintReadWrite, EditAnywhere)
		float SSS_Scale;

	UPROPERTY(Category = "Subsurface Scattering", BlueprintReadWrite, EditAnywhere)
		float SSS_Intensity;

	UPROPERTY(Category = "Subsurface Scattering", BlueprintReadWrite, EditAnywhere)
		float SSS_LightDepth;

	UPROPERTY(Category = "Subsurface Scattering", BlueprintReadWrite, EditAnywhere)
		float SSS_MacroNormalStrength;



	UPROPERTY(Category = "Detail, Medium & Far Normals", BlueprintReadWrite, EditAnywhere)
		float DetailNormalScale;

	UPROPERTY(Category = "Detail, Medium & Far Normals", BlueprintReadWrite, EditAnywhere)
		float DetailNormalSpeed;

	UPROPERTY(Category = "Detail, Medium & Far Normals", BlueprintReadWrite, EditAnywhere)
		float DetailNormalStrength;

	UPROPERTY(Category = "Detail, Medium & Far Normals", BlueprintReadWrite, EditAnywhere)
		float MediumNormalScale;

	UPROPERTY(Category = "Detail, Medium & Far Normals", BlueprintReadWrite, EditAnywhere)
		float MediumNormalSpeed;

	UPROPERTY(Category = "Detail, Medium & Far Normals", BlueprintReadWrite, EditAnywhere)
		float MediumNormalStrength;

	UPROPERTY(Category = "Detail, Medium & Far Normals", BlueprintReadWrite, EditAnywhere)
		float FarNormalScale;

	UPROPERTY(Category = "Detail, Medium & Far Normals", BlueprintReadWrite, EditAnywhere)
		float FarNormalSpeed;

	UPROPERTY(Category = "Detail, Medium & Far Normals", BlueprintReadWrite, EditAnywhere)
		float FarNormalStrength;

	UPROPERTY(Category = "Detail, Medium & Far Normals", BlueprintReadWrite, EditAnywhere)
		float FarNormalBlendDistance;

	UPROPERTY(Category = "Detail, Medium & Far Normals", BlueprintReadWrite, EditAnywhere)
		float FarNormalBlendFalloff;




	UPROPERTY(Category = "Panning Waves(Old Shader)", BlueprintReadWrite, EditAnywhere)
		float PanWaveLerp;

	UPROPERTY(Category = "Panning Waves(Old Shader)", BlueprintReadWrite, EditAnywhere)
		float PanWaveIntensity;

	UPROPERTY(Category = "Panning Waves(Old Shader)", BlueprintReadWrite, EditAnywhere)
		float PanWaveTimeScale;

	UPROPERTY(Category = "Panning Waves(Old Shader)", BlueprintReadWrite, EditAnywhere)
		float PanWaveSize;

	UPROPERTY(Category = "Panning Waves(Old Shader)", BlueprintReadWrite, EditAnywhere)
		FVector Panner01Speed;

	UPROPERTY(Category = "Panning Waves(Old Shader)", BlueprintReadWrite, EditAnywhere)
		FVector Panner02Speed;

	UPROPERTY(Category = "Panning Waves(Old Shader)", BlueprintReadWrite, EditAnywhere)
		FVector Panner03Speed;

	UPROPERTY(Category = "Panning Waves(Old Shader)", BlueprintReadWrite, EditAnywhere)
		float MacroWaveScale;

	UPROPERTY(Category = "Panning Waves(Old Shader)", BlueprintReadWrite, EditAnywhere)
		float MacroWaveSpeed;

	UPROPERTY(Category = "Panning Waves(Old Shader)", BlueprintReadWrite, EditAnywhere)
		float MacroWaveAmplify;



	UPROPERTY(Category = "Foam Wave Caps", BlueprintReadWrite, EditAnywhere)
		float FoamCapsOpacity;

	UPROPERTY(Category = "Foam Wave Caps", BlueprintReadWrite, EditAnywhere)
		float FoamCapsHeight;

	UPROPERTY(Category = "Foam Wave Caps", BlueprintReadWrite, EditAnywhere)
		float FoamCapsPower;



	UPROPERTY(Category = "Seafoam(Based on Heightmap)", BlueprintReadWrite, EditAnywhere)
		float SeafoamScale;

	UPROPERTY(Category = "Seafoam(Based on Heightmap)", BlueprintReadWrite, EditAnywhere)
		float SeafoamSpeed;

	UPROPERTY(Category = "Seafoam(Based on Heightmap)", BlueprintReadWrite, EditAnywhere)
		float SeafoamDistortion;

	UPROPERTY(Category = "Seafoam(Based on Heightmap)", BlueprintReadWrite, EditAnywhere)
		float SeafoamHeightPower;

	UPROPERTY(Category = "Seafoam(Based on Heightmap)", BlueprintReadWrite, EditAnywhere)
		float SeafoamHeightMultiply;




	UPROPERTY(Category = "Heightmap", BlueprintReadWrite, EditAnywhere)
		float HeightmapDisplacement;

	UPROPERTY(Category = "Heightmap", BlueprintReadWrite, EditAnywhere)
		float HeightmapScale;

	UPROPERTY(Category = "Heightmap", BlueprintReadWrite, EditAnywhere)
		float HeightmapSpeed;



	UPROPERTY(Category = "CubeMap Reflection", BlueprintReadWrite, EditAnywhere)
		float CubemapReflectionStrength;





	UPROPERTY(Category = "Infinite Ocean System", BlueprintReadWrite, EditAnywhere)
		bool UpdateInEditor;

	UPROPERTY(Category = "Infinite Ocean System", BlueprintReadWrite, EditAnywhere)
		TEnumAsByte<enum EFollowMethod> FollowMethod;

	UPROPERTY(Category = "Infinite Ocean System", BlueprintReadWrite, EditAnywhere)
		float GridSnapSize;

	UPROPERTY(Category = "Infinite Ocean System", BlueprintReadWrite, EditAnywhere)
		float MaxLookAtDistance;

	UPROPERTY(Category = "Infinite Ocean System", BlueprintReadWrite, EditAnywhere)
		bool ScaleByDistance;

	UPROPERTY(Category = "Infinite Ocean System", BlueprintReadWrite, EditAnywhere)
		float ScaleDistanceFactor;

	UPROPERTY(Category = "Infinite Ocean System", BlueprintReadWrite, EditAnywhere)
		float ScaleStartDistance;

	UPROPERTY(Category = "Infinite Ocean System", BlueprintReadWrite, EditAnywhere)
		float ScaleMin;

	UPROPERTY(Category = "Infinite Ocean System", BlueprintReadWrite, EditAnywhere)
		float ScaleMax;




	UPROPERTY(Category = "Textures", BlueprintReadWrite, EditAnywhere)
		UTexture* SmallWaveNormal;

	UPROPERTY(Category = "Textures", BlueprintReadWrite, EditAnywhere)
		UTexture* MediumWaveNormal;

	UPROPERTY(Category = "Textures", BlueprintReadWrite, EditAnywhere)
		UTexture* FarWaveNormal;

	UPROPERTY(Category = "Textures", BlueprintReadWrite, EditAnywhere)
		UTexture* ShoreFoam;

	UPROPERTY(Category = "Textures", BlueprintReadWrite, EditAnywhere)
		UTexture* ShoreFoam2;

	UPROPERTY(Category = "Textures", BlueprintReadWrite, EditAnywhere)
		UTexture* ShoreFoamRoughness;

	UPROPERTY(Category = "Textures", BlueprintReadWrite, EditAnywhere)
		UTexture* HeightmapWaves;


	UPROPERTY(Category = "Textures", BlueprintReadWrite, EditAnywhere)
		UTexture* Seafoam;

	UPROPERTY(Category = "Textures", BlueprintReadWrite, EditAnywhere)
		UTexture* ReflectionCubemap;


	UPROPERTY(Category = "Default", BlueprintReadWrite, EditAnywhere)
		FWaveSetParameters SetParams;

};
