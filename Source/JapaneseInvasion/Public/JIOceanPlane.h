// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "GameFramework/Actor.h"
#include "JIOceanPlane.generated.h"

UCLASS()
class JAPANESEINVASION_API AJIOceanPlane : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AJIOceanPlane();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = Movement, meta = (AllowPrivateaAccess = true))
		UStaticMeshComponent* Plane;

};
