// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"

#include "JIOceanWidget.generated.h"

/**
 * 
 */
UCLASS()
class JAPANESEINVASION_API UJIOceanWidget : public UUserWidget
{
	GENERATED_BODY()
public:
    UJIOceanWidget(const FObjectInitializer& ObjectInitializer);

protected:
    virtual void NativeConstruct() override;

public:

    UPROPERTY(VisibleAnywhere, Category = "Image")
        TArray<UImage*> OceanStateImg;

    UPROPERTY()
        class UTexture2D* RedEastTex;
    UPROPERTY()
        class UTexture2D* RedWestTex;
    UPROPERTY()
        class UTexture2D* RedNorthTex;
    UPROPERTY()
        class UTexture2D* RedSouthTex;
    UPROPERTY()
        class UTexture2D* RedNEastTex;
    UPROPERTY()
        class UTexture2D* RedNWestTex;
    UPROPERTY()
        class UTexture2D* RedSEastTex;
    UPROPERTY()
        class UTexture2D* RedSWestTex;

    UPROPERTY()
        class UTexture2D* OrangeEastTex;
    UPROPERTY()
        class UTexture2D* OrangeWestTex;
    UPROPERTY()
        class UTexture2D* OrangeNorthTex;
    UPROPERTY()
        class UTexture2D* OrangeSouthTex;
    UPROPERTY()
        class UTexture2D* OrangeNEastTex;
    UPROPERTY()
        class UTexture2D* OrangeNWestTex;
    UPROPERTY()
        class UTexture2D* OrangeSEastTex;
    UPROPERTY()
        class UTexture2D* OrangeSWestTex;

    UPROPERTY()
        class UTexture2D* YellowEastTex;
    UPROPERTY()
        class UTexture2D* YellowWestTex;
    UPROPERTY()
        class UTexture2D* YellowNorthTex;
    UPROPERTY()
        class UTexture2D* YellowSouthTex;
    UPROPERTY()
        class UTexture2D* YellowNEastTex;
    UPROPERTY()
        class UTexture2D* YellowNWestTex;
    UPROPERTY()
        class UTexture2D* YellowSEastTex;
    UPROPERTY()
        class UTexture2D* YellowSWestTex;

    UPROPERTY()
        class UButton* OceanButton;

    UFUNCTION()
        void OnOceanClicked();

    class AJIOcean* OceanPtr;

    void SetOceanData();
    void SetOceanPtr(AJIOcean* ocean);
    FSlateBrush MakeBrush(UTexture2D* inputTexture, int x, int y);

    UFUNCTION(BlueprintCallable)
        UWidgetAnimation* GetAnimationByName(FName AnimationName) const;

    UFUNCTION(BlueprintCallable)
        bool PlayAnimationByName(FName AnimationName,float StartAtTime = 0.0f, int32 NumLoopsToPlay = 1, EUMGSequencePlayMode::Type PlayMode = EUMGSequencePlayMode::Forward, float PlaybackSpeed = 1.0f);

    void FillAnimationMap();
    // �ִϸ��̼� ��
    TMap<FName, UWidgetAnimation*> AnimationsMap;


    bool bIsOn;
};
