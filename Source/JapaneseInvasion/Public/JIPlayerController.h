#pragma once
#include "JapaneseInvasion.h"
#include "JIHUD.h"
#include "JIShip.h"
#include "JIGeneral.h"
#include "JICameraPawn.h"
#include "JIGameMode.h"
#include "DrawDebugHelpers.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "GameFramework/PlayerController.h"
#include "protocol.h"
#include "JIPlayerController.generated.h"

USTRUCT()
struct FUnitDesignation
{
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY()
		TArray<AJIShip*> Ship;
};
 
UCLASS()
class JAPANESEINVASION_API AJIPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	AJIPlayerController();

protected:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;
	virtual void Tick(float DeltaTime) override;

private:
	/////////////////////////////Bind Action Function////////////////////
	UFUNCTION()
		void OnSpawn();

	UFUNCTION()
		void OnFirePressed();

	UFUNCTION()
		void OnFireReleased();

	UFUNCTION()
		void OnReload();
	UFUNCTION()
		void OnReloadReleased();

	UFUNCTION()
		void OnStopPressed();

	UFUNCTION()
		void OnStopReleased();
	
	UFUNCTION()
		void OnCapture();

	UFUNCTION()
		void OnSetCaptureLocation();

	UFUNCTION()
		void OnSelectionPressed();

	UFUNCTION()
		void OnSelectionReleased();

	UFUNCTION()
		void OnMovePressed();

	UFUNCTION()
		void OnMoveReleased();
public:
	UFUNCTION()
		void OnTab();
private:
	UFUNCTION()
		void OnZoomIn();

	UFUNCTION()
		void OnZoomOut();

	UFUNCTION()
		void OnZoomInReleased();

	UFUNCTION()
		void OnZoomOutReleased();

	void SetTatics1();

	void SetTatics2();

	UFUNCTION()
		void UnitDesignation1();

	UFUNCTION()
		void UnitDesignation2();

	UFUNCTION()
		void UnitDesignation3();

	UFUNCTION()
		void UnitDesignation4();

	UFUNCTION()
		void UnitDesignation5();

	UFUNCTION()
		void LoadUnitDesignation1();

	UFUNCTION()
		void LoadUnitDesignation2();

	UFUNCTION()
		void LoadUnitDesignation3();

	UFUNCTION()
		void LoadUnitDesignation4();

	UFUNCTION()
		void LoadUnitDesignation5(); 

	UFUNCTION()
		void LoadUnitDesignationPos1();

	UFUNCTION()
		void LoadUnitDesignationPos2();

	UFUNCTION()
		void LoadUnitDesignationPos3();

	UFUNCTION()
		void LoadUnitDesignationPos4();

	UFUNCTION()
		void LoadUnitDesignationPos5(); 
	
	UFUNCTION()
		void SetClickCountZero();
	

	UFUNCTION()
		void SaveScreen1();

	UFUNCTION()
		void SaveScreen2();

	UFUNCTION()
		void SaveScreen3();

	UFUNCTION()
		void LoadScreen1();

	UFUNCTION()
		void LoadScreen2();

	UFUNCTION()
		void LoadScreen3();

	void OnPause();

	void OnMiniMap();
	void OnMiniMapReleased();

	void OnOceanButton();
	void OnOceanButtonReleased();

	void OnGamePause();
	void OnGamePauseReleased();

	UFUNCTION()
		void OnWeaponChange();
public:
	void OnVictoryWidget();

	void OnDefeatWidget();
	/////////////////////////////Bind Axis Function////////////////////
	UFUNCTION()
		void MoveForward(float NewAxisValue);

	UFUNCTION()
		void MoveRight(float NewAxisValue);

	UFUNCTION()
		void Roll(float NewAxisValue);

	UFUNCTION()
		void LookUp(float NewAxisValue);

	UFUNCTION()
		void Turn(float NewAxisValue);

	UFUNCTION()
		void MoveLeftRight(float NewAxisValue);

	UFUNCTION()
		void MoveUpDown(float NewAxisValue);

public:
	//Get
	bool GetIsOnPause() { return bIsOnPause; };
	EViewMode GetViewMode() { return CurrentView; }

	//Set
	void SetInputModeGameOnly(bool InConsumeCaptureMouseDown);
	void SetIsOnPause() { bIsOnPause = false; }
	void SetViewMode(EViewMode NewViewMode);

	FVector EdgeScoll(float x, float y);
	
	void MinimapClick(FVector2D);

public:
	AJIHUD* HUDPtr;
	AJIGameMode* GameModePtr;
	AJICameraPawn* TopViewCamera;
	AJIOcean* OceanPtr;
	AJIGeneral* General; 
	AJIGeneral* General2;
	AJIGeneral* General3;

	int PressedCnt1 = 0;
	int PressedCnt2 = 0;
	int PressedCnt3 = 0;
	int PressedCnt4 = 0;
	int PressedCnt5 = 0;

	void Pause();
	void GoToTitle();
	void Resume();
	void ChangeInputMode(bool tf);
private:
	FVector CaptureLocation;

	bool bOnVictoryWidget = false;
	bool bOnDefeatWidget = false;
	bool bBlueRed = false;
	bool m_bViewChange = true;
	bool bIsOnPause = false;
	bool bOnFire = false;
	bool bClickMouse;
	bool bMoveToMouseCursor;
	bool bMouseOnMinimap;
	bool bLeftMouseButtonDown = false;
	bool bRightMouseButtonDown = false;
	
	float interval;
	float ScrollSpeed; 

	int clickCount = 0;


	TEAM_COLOR m_eTeamColor; // 수정
	EViewMode CurrentView = EViewMode::TOPVIEW;
	GENERAL_STATUS m_eStatus;
public:
	UPROPERTY()
		TArray<AJIShip*> SelctedActors;

	UPROPERTY(Category = MapsAndSets, EditAnywhere)
		TMap<int32, FUnitDesignation> UnitDesignationMap;

	UPROPERTY(Category = MapsAndSets, EditAnywhere)
		TMap<int32, FVector> SaveScreenMap;

	UPROPERTY()
		TArray<AJIGeneral*> SelctedGenerals;
	//부대지정하기 위한 map 5개까지 만들수있도록 한다. 인자로 TArray를 넣었는데 언리얼에서 지원하지않아서 UStruct로 한번 감싼후에 Value 자료형으로 넣어준것.
	UPROPERTY()
		TArray<AJICameraPawn*> SelectedCameras;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = UI)
		TSubclassOf<class UJIHUDWidget> HUDWidgetClass;
	UPROPERTY()
		class UJIHUDWidget* HUDWidget;


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = UI)
		TSubclassOf<class UJIMenuWidget> MenuWidgetClass;
	UPROPERTY()
		class UJIMenuWidget* MenuWidget;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = UI)
		TSubclassOf<class UJIVictoryWidget> VictoryWidgetClass;
	UPROPERTY()
		class UJIVictoryWidget* VictoryWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = UI)
		TSubclassOf<class UJIDefeatWidget> DefeatWidgetClass;
	UPROPERTY()
		class UJIDefeatWidget* DefeatWidget;


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = UI)
		TSubclassOf<class UJIMiniMapWidget> MiniMapWidgetClass;
	UPROPERTY()
		class UJIMiniMapWidget* MiniMapWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = UI)
		TSubclassOf<class UJIOceanWidget> OceanWidgetClass;
	UPROPERTY()
		class UJIOceanWidget* OceanWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = UI)
		TSubclassOf<class UJIChattingWidget> ChattingWidgetClass;
	UPROPERTY()
		class UJIChattingWidget* ChattingWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = UI)
		TSubclassOf<class UJIGeneralHUDWidget> GeneralHUDWidgetClass;
	UPROPERTY()
		class UJIGeneralHUDWidget* GeneralHUDWidget;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Decal", meta = (AllowPrivateAccess = true))
		class UDecalComponent* Cursor;

	UPROPERTY(BlueprintReadOnly, Category = "Audio")
		UAudioComponent* BGMAudioComponent;

	UPROPERTY(BlueprintReadOnly, Category = "Audio")
		USoundCue* HansanSoundCue;

	UPROPERTY(BlueprintReadOnly, Category = "Audio")
		UAudioComponent* UIAudioComponent;

	UPROPERTY(BlueprintReadOnly, Category = "Audio")
		USoundCue* UISoundCue;

	UPROPERTY(BlueprintReadOnly, Category = "Audio")
		UAudioComponent* ClickAudioComponent;

	UPROPERTY(BlueprintReadOnly, Category = "Audio")
		USoundCue* ClickSoundCue;

	FInputModeGameAndUI GameAndUIInputMode;
	FInputModeGameOnly GameInputMode;
	FInputModeUIOnly UIInputMode;
};
