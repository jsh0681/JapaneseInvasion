#pragma once

#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "JIReEntryWidget.generated.h"

/**
 * 
 */
UCLASS()
class JAPANESEINVASION_API UJIReEntryWidget : public UUserWidget
{
	GENERATED_BODY()

private:
		UPROPERTY()
		class UButton* OnBtnConfirm;
protected:
	virtual void NativeConstruct() override;

public:
	UFUNCTION()
		void OnBtnConfirmHovered();

	UFUNCTION()
		void OnBtnConfirmClicked();

	void SetFocusOn();
	void SetAudioComponent(UAudioComponent* Audio);

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* HoveredAudioComponent;

};
