// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "Components/CircularThrobber.h"
#include "JIReloadWidget.generated.h"

/**
 * 
 */
UCLASS()
class JAPANESEINVASION_API UJIReloadWidget : public UUserWidget
{
	GENERATED_BODY()


protected:
	virtual void NativeConstruct() override;

public:
	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UCircularThrobber* CT_Reload;

};
