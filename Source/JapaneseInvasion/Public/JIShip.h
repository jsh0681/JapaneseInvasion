#pragma once
#include "JapaneseInvasion.h"
#include "GameFramework/Pawn.h"
#include "Components/DecalComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "BuoyancyForceComponent.h"
#include "protocol.h"
#include "JIShip.generated.h"
UCLASS()
class JAPANESEINVASION_API AJIShip : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AJIShip();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public://상속받은 함수들
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void PostInitializeComponents() override;
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void PossessedBy(AController* NewController) override;
	virtual void NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;

public://Custom Function
	/*Fire Projectile*/
	void Fire();
	void LoadBomb(FVector ShootLocation);

	FTimerHandle LoadShellHandle;
	/*Reload Shell*/
	void OnReload();

	void SetDetailOption();
	void SetStop() { bIsMoving = false; };
	bool GetRangeDecal() { return bOnRangeDecal; };
	void SetAttackPos(FVector pos){ AttackPos = pos; }
	bool IsInAttackRange(FVector Pos);

	void Reloading();

	void SetArrival(FVector MoveLocation);
	void SetSelection(bool tf);
	

	UUserWidget* GetShipWidget();
public:
	void SetIsMoving(bool tf);
	bool bOnce = true;
	bool bIsShellLoading = false;

	UPROPERTY(VisibleAnywhere, Category = "Stat")
		class UJIKoreaShipStatComponent* ShipStat;

	UPROPERTY(VisibleAnywhere, Category = "UI")
		class UWidgetComponent* ShipBarWidget;
	
	UPROPERTY(VisibleAnywhere, Category = "Stat")
		class UJIReloadWidget* ReloadWidget;

	UPROPERTY(VisibleAnywhere, Category = "UI")
		class UWidgetComponent* ReloadBarWidget;

	bool bOnRangeDecal;
public: // 수정
	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* FireAudioComponent;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		USoundCue* FireSoundCue;
	
	UPROPERTY(VisibleAnywhere, Category = "Audio")
		USoundCue* GunFireSoundCue;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* BombLoadAudioComponent;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		USoundCue* BombLoadSoundCue;


	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* RangeOutAudioComponent;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		USoundCue* RangeOutSoundCue;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* GunEmptyAudioComponent;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		USoundCue* GunEmptySoundCue;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* GunReloadAudioComponent;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		USoundCue* GunReloadSoundCue;
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mesh", meta = (AllowPrivateAccess = true))
		UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Buoyancy", meta = (AllowPrivateAccess = true))
		UBuoyancyForceComponent* BuoyancyForce;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = true))
		UPawnMovementComponent* Movement;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Selection", meta = (AllowPrivateAccess = true))
		UStaticMeshComponent* SelectionCircle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "MinimapPoint", meta = (AllowPrivateAccess = true))
		UStaticMeshComponent* MinimapVisualPoint;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "FireRange", meta = (AllowPrivateAccess = true))
		UStaticMeshComponent* fireRange;

	FVector Dest;
	FVector Start;

	FVector AttackPos;
	float ShootRange;
	
	bool bIsSelection;
	FRotator CurDir;

	UPROPERTY()
		FTimerHandle UnusedHandle;
public:
	float m_fShipSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo)
	int32 loadedAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo)
	int32 ammoPool;

	class AJIOcean* OceanPtr;

	UPROPERTY(VisibleAnywhere, Category = Tags)
		TArray<FName> ActorTag;

	// 수정
	OBJ_TYPE m_eObjType = OBJ_TYPE::SHIP;
	TEAM_COLOR m_eTeamColor;
	bool bIsMoving;
	bool bIsBackView = false;
	int m_iId = -1;

	bool bCanSend = false;

	void Timer();
	void TimeAdd();
	FTimerHandle TimerHandle;
	//
	void FireCoolTimer();
	void FireCoolTimeAdd();
	FTimerHandle FireTimerHandle;
	bool bCanFire = false;

	void SetAsset();
	int32 AssetIndex = 0;
	int DokingCnt = 0;
public:
	void OnAssetLoadCompleted();

	FSoftObjectPath ShipAssetToLoad = FSoftObjectPath(nullptr);
	TSharedPtr<struct FStreamableHandle> AssetStreamingHandle;
};