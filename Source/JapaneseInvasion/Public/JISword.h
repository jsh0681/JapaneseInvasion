#pragma once

#include "JIWeapon.h"
#include "JISword.generated.h"

UCLASS()
class JAPANESEINVASION_API AJISword : public AJIWeapon
{
	GENERATED_BODY()
public:
	AJISword();

public:
	virtual void PostInitializeComponents() override;
	void PlayAnimForTrail();

protected:
	virtual void BeginPlay() override;


	UAnimationAsset* AnimAsset;
};
