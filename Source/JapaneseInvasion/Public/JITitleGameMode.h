#pragma once
#include "JapaneseInvasion.h"
#include "GameFramework/GameModeBase.h"
#include "JITitleGameMode.generated.h"

UCLASS()
class JAPANESEINVASION_API AJITitleGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AJITitleGameMode();
};