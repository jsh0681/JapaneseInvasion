// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "JITitleGameMode.h"
#include "JIReEntryWidget.h"
#include "Runtime/UMG/Public/UMG.h"
#include "JITitleWidget.generated.h"

UCLASS()
class JAPANESEINVASION_API UJITitleWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UJITitleWidget(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void NativeConstruct() override;

	UFUNCTION()
		void OnIDCommitted(const FText& InText, ETextCommit::Type InCommitType);
public:
	UPROPERTY()
		class UEditableTextBox* TextBox;

	UPROPERTY()
		class UEditableTextBox* TextIP;

	UPROPERTY()
		class UButton* OnBtnNewGame;

	UPROPERTY()
		class UButton* OnBtnContinue;

	UPROPERTY()
		class UButton* OnBtnExit;

	UFUNCTION()
		void OnBtnNewGameClicked();

	UFUNCTION()
		void OnBtnContinueClicked();

	UFUNCTION()
		void OnBtnExitClicked();

	UFUNCTION()
		void OnBtnNewGameHovered();

	UFUNCTION()
		void OnBtnContinueHovered();

	UFUNCTION()
		void OnBtnExitHovered();

	UFUNCTION()
		void SetReEntryWidget(UJIReEntryWidget* widget);

	UPROPERTY()
		class UJIReEntryWidget* ReEntryWidget;

	void SetAudioComponent(UAudioComponent* Audio);
	AJITitleGameMode* TitleGameModePtr;
public:
	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* HoveredAudioComponent;

};