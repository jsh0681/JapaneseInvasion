#pragma once
#include "JapaneseInvasion.h"
#include "GameFramework/PlayerController.h"
#include "JIUIPlayerController.generated.h"

UCLASS()
class JAPANESEINVASION_API AJIUIPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AJIUIPlayerController();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadwrite,Category = UI)
	TSubclassOf<class UJITitleWidget> TitleWidgetClass;

	UPROPERTY()
	class UJITitleWidget* TitleWidget;

private:
	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* BGMAudioComponent;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		USoundCue* BGMSoundCue;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		UAudioComponent* HoveredAudioComponent;

	UPROPERTY(VisibleAnywhere, Category = "Audio")
		USoundCue* HoveredSoundCue;
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = UI)
		TSubclassOf<class UJIReEntryWidget> ReEntryWidgetClass;

	UPROPERTY()
		class UJIReEntryWidget* ReEntryWidget;

	UFUNCTION()
		void OnEnter();

	virtual void Tick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	//UPROPERTY(EditDefaultsOnly, BlueprintReadwrite, Category = UI)
	//	TSubclassOf<class UUserWidget> UIWidgetClass;

	//UPROPERTY()
	//	class UUserWidget* UIWidgetInstance;
		
};
