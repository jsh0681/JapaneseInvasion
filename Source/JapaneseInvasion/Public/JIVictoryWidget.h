// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "Blueprint/UserWidget.h"
#include "JIPlayerController.h"
#include "Blueprint/UserWidget.h"
#include "JIVictoryWidget.generated.h"

/**
 * 
 */
UCLASS()
class JAPANESEINVASION_API UJIVictoryWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

public:
	UPROPERTY()
		class UButton* btnQuit;

	UPROPERTY()
		class UButton* btnReturnToTitle;

	UFUNCTION()
		void OnBtnQuit();

	UFUNCTION()
		void OnBtnReturnToTitle();
};