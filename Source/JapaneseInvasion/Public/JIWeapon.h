// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "JapaneseInvasion.h"
#include "GameFramework/Actor.h"
#include "JIWeapon.generated.h"

UCLASS()
class JAPANESEINVASION_API AJIWeapon : public AActor
{
	GENERATED_BODY()
public:
		AJIWeapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void PostInitializeComponents() override;
	virtual void Tick(float DeltaTime) override;

public:
	void AttachMeshToPawn(USkeletalMeshComponent* mesh, FName WeaponSocketName);
	void DetachMeshFromPawn();


public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
		USkeletalMeshComponent* Weapon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
		UCapsuleComponent* Collision;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
		FName SocketName;
};
