#pragma once
#include "Engine.h"
#include "EngineMinimal.h"
#include "UnrealMathUtility.h"

DECLARE_LOG_CATEGORY_EXTERN(JapaneseInvasion, Log, All);
UENUM(BlueprintType)
enum class EViewMode : uint8
{
	PREVIEW,
	TOPVIEW,
	BACKVIEW,
	DEAD
};

UENUM(BlueprintType)
enum class ECharacterState : uint8
{
	PREINIT,
	LOADING,
	READY,
	DEAD
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	SWORD,
	BOW,
	LANCE
};

UENUM(BlueprintType)
enum class EObjectType : uint8
{
	JOSEON_GENERAL = 1,
	JOSEON_SOLDIER = 2,
	JAPAN_SOLDIER = 3,
	JAPAN_GENERAL = 4
};

#define JILOG_CALLINFO (FString(__FUNCTION__)+TEXT("(")+FString::FromInt(__LINE__)+TEXT(")"))
#define JILOG_S(Verbosity) UE_LOG(JapaneseInvasion, Verbosity, TEXT("%s"), *JILOG_CALLINFO)
#define JILOG(Verbosity, Format, ...) UE_LOG(JapaneseInvasion, Verbosity, TEXT("%s%s"), *JILOG_CALLINFO, *FString::Printf(Format, ##__VA_ARGS__))
#define JICHECK(Expr, ...) { if(!(Expr)) { JILOG(Error, TEXT("ASSERTION : %s"), TEXT("'"#Expr"'")); return __VA_ARGS__; } }