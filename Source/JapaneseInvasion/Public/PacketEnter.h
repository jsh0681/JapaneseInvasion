// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "protocol.h"

/**
 * 
 */
class JAPANESEINVASION_API PacketEnter
{
private:
	char	m_Size;
	char	m_Type;
	int		id;
	char	name[MAX_ID_LEN];
	char	o_type;
	float	x, y, z;

	sc_packet_enter* m_enterpacket;

public:
	void send_enter_packet(char buf[]);

public:
	void SetEnterPacket(sc_packet_enter* packet) { m_enterpacket = packet; }

	void ZeroSetEnterPacket() { memset(m_enterpacket, 0, sizeof(sc_packet_enter)); }
	sc_packet_enter& GetEnterPacket() { return *m_enterpacket; }

public:
	PacketEnter();
	~PacketEnter();
};
