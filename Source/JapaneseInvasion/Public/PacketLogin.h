// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "protocol.h"
/**
 * 
 */
class JAPANESEINVASION_API PacketLogin
{
private:
	char			m_szSize;
	char			m_szType;
	int				m_iId;
	char			m_szName;

	float			x, y, z;
	short			hp;
	short			level;
	int				exp;

	sc_packet_login_ok* m_loginpacket;

public:
	void enter_game(int user_id, char buf[]);
	void send_login_ok_packet(int user_id);

	void SetLoginOkPacket(sc_packet_login_ok* packet) {	m_loginpacket = packet;	}

	void ZeroSetLoginOkPacket() { memset(m_loginpacket, 0, sizeof(sc_packet_login_ok)); }

	sc_packet_login_ok& GetLoginOkPacket() { return *m_loginpacket; }

public:
	PacketLogin();
	~PacketLogin();
};
