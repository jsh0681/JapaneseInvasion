// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "protocol.h"
#include "Sockets.h"


class PacketLogin;
class PacketMove;
class PacketEnter;
class AJIGeneral;
class UWorld;
class AJIPlayerController;
class AJIUIPlayerController;
class UJILobbyWidget;
class UJIChattingWidget;

enum class ERROR { ERROR, SUCCESS, BIND_ERROR, LISTEN_ERROR, ACCEPT_ERROR, LOGIN_ERROR, MOVE_ERROR };

class JAPANESEINVASION_API PacketMgr
{
private:
	ERROR			m_eError;

	PacketLogin* m_pLoginPacket;
	PacketMove* m_pMovePacket;
	PacketEnter* m_pEnterPacket;

	FSocket* m_Socket;

	UWorld* m_pUWorld;
	UWorld* m_pPCWorld;

	AJIPlayerController* m_pController;
	AJIUIPlayerController* m_pUIController;
	UJILobbyWidget* m_pLobbyWidget;
	UJIChattingWidget* m_pChattingWidget;




public:
	void process_packet(char buf[]);
	void process_data(char* buf, uint8 io_byte);
	void send_packet(void* packet);

	void SetSocket(FSocket* pSocket) { if (nullptr == m_Socket) 	m_Socket = pSocket; }
	FSocket* GetSocket() { return m_Socket; }

	PacketMove* GetMovePacket() { return m_pMovePacket; }
	PacketLogin* GetLoginPacket() { return m_pLoginPacket; }
	PacketEnter* GetEnterPacket() { return m_pEnterPacket; }

	void Init_Login_Packet() { m_pLoginPacket = nullptr; }
	void Init_Enter_Packet() { m_pEnterPacket = nullptr; }
	void Init_Move_Packet() { m_pMovePacket = nullptr; }

	void SetPlayerController(AJIPlayerController* playercontroller) { m_pController = playercontroller; }
	void SetUIController(AJIUIPlayerController* uiplayercontroller) { m_pUIController = uiplayercontroller; }
	void SetLobbyWidget(UJILobbyWidget* lobbyWidget) { m_pLobbyWidget = lobbyWidget; }
	void SetChattingWidget(UJIChattingWidget* chattingWidget) { m_pChattingWidget = chattingWidget; }

	void SetPCWorld(UWorld* world) { m_pPCWorld = world; }
	void SetWorld(UWorld* world) { m_pUWorld = world; }

public:
	static PacketMgr* GetInst()
	{
		static PacketMgr mgr;
		return &mgr;
	}
private:
	PacketMgr() {};
	~PacketMgr() {};
};
