// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "protocol.h"
/**
 * 
 */
class JAPANESEINVASION_API PacketMove
{
private:
	char	m_Size;
	char	m_Type;

	int		m_Dir;

	sc_packet_move* m_movepacket;

public:
	void move(int dir);
	void send_move_packet( unsigned char dir, OBJ_TYPE eType, FVector fVec, float camvalue, float RotValue, float TurnValue);
	void SetMovePacket(sc_packet_move* packet) {m_movepacket = packet;}

	void ZeroSetMovePacket() { memset(m_movepacket, 0, sizeof(sc_packet_move)); }
	sc_packet_move& GetMovePacket() { return *m_movepacket; }

public:
	PacketMove();
	~PacketMove();
};
