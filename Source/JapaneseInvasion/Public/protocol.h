// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Sockets.h"
#include "Map.h"

class AJIGeneral;
class AJIShip;
class AJIBomb;
class AJIBullet;
class AJIArrow;
class AJICharacter;
class AJIAICharacter;

constexpr int MAX_ID_LEN = 50;
constexpr int MAX_STR_LEN = 50;

#define WORLD_WIDTH      8 // 수정
#define WORLD_HEIGHT   8 // 수정

#define SERVER_PORT      8171

#define C2S_LOGIN         1
#define C2S_SELECT_LOBBY   2
#define C2S_QUIT_LOBBY      3
#define C2S_START_GAME      4
#define C2S_SELECT_TEAM      5
#define C2S_ENTER_INGAME   6
#define C2S_MOVE         7
#define C2S_ATTACK         8
#define C2S_SHELL_SPAWN      9
#define C2S_BULLET_SPAWN   10
#define C2S_ARROW_SPAWN      11
#define C2S_SHELL_MOVE      12
#define C2S_BULLET_MOVE      13
#define C2S_ARROW_MOVE      14
#define C2S_SET_HP         15
#define C2S_VIEWMODE      16
#define C2S_CHAT         17 // 추가
#define C2S_SOLDIER_GEN      18 // 추가
#define C2S_CHANGE_WEAPON   19
#define C2S_LOGOUT         20
#define C2S_SHIP_MOVE      21
#define C2S_CLIENT_QUIT_GAME 22
#define C2S_PAUSE         23
#define C2S_RESUME         24
#define C2S_EXIT         25


#define S2C_CERTIFIFY_OK    1
#define S2C_CERTIFIFY_FAIL   2
#define S2C_LOGIN_OK      3 // 이건 인게임 여부가되었음 // 장군, 배 등 -> 바꿔야 함. // set ok 로 되어야 함.
#define S2C_SELECT_LOBBY   4
#define S2C_QUIT_LOBBY      5
#define S2C_START_GAME      6
#define S2C_START_GAME_FAIL 7
#define S2C_ENTER_INGAME   8
#define S2C_SELECT_TEAM      9
#define S2C_MOVE         10
#define S2C_ATTACK         11
#define S2C_SHELL_SPAWN      12
#define S2C_BULLET_SPAWN   13
#define S2C_ARROW_SPAWN      14
#define S2C_SHELL_MOVE      15
#define S2C_BULLET_MOVE      16
#define S2C_ARROW_MOVE      17
#define S2C_SET_HP         18
#define S2C_VIEWMODE      19
#define S2C_ENTER         20
#define S2C_CHAT         21 // 추가
#define S2C_SOLDIER_GEN      22 // 추가
#define S2C_CHANGE_WEAPON   23
#define S2C_FINISH_GAME      24
#define S2C_LEAVE         25
#define S2C_SHIP_MOVE      26
#define S2C_PAUSE         27
#define S2C_RESUME         28
#define S2C_EXIT         29

#pragma pack(push ,1)

extern TMap<int, AJIGeneral*> g_mapPlayer;
extern TMap<int, AJIShip*> g_mapShip;
extern TMap<int, AJIBomb*> g_mapShell;
extern TMap<int, AJIArrow*> g_mapArrow;
extern TMap<int, AJIBullet*> g_mapBullet;
extern TMap<int, AJICharacter*> g_mapJoseonSoldier;
extern TMap<int, AJIAICharacter*> g_mapJapanSoldier;

extern FSocket* g_Socket;
extern int joseon_team_num;
extern int japan_team_num;
extern int each_team_num;


constexpr float JOSEON_1_X = 112800.f;
constexpr float JOSEON_1_Y = 226000.f;
constexpr float JOSEON_2_X = 112800.f;
constexpr float JOSEON_2_Y = 221000.f;

constexpr float JAPAN_1_X = 116000.f;
constexpr float JAPAN_1_Y = -19000.f;
constexpr float JAPAN_2_X = 116000.f;
constexpr float JAPAN_2_Y = -14000.f;

constexpr auto EACH_SHIP = 3;

enum class OBJ_TYPE { GENERAL, SHIP, SOLDIER, SHELL };
enum class TEAM_COLOR { JOSEON_1, JOSEON_2, JAPAN_1, JAPAN_2 };
enum class GAME_RESULT { NONE, WIN, LOSE };
enum class GENERAL_STATUS { IDLE, JUMP, RUN, SWORD_ATTACK, BOW_ATTACK };
enum class SHIP_TYPE { TURTLE, PANOK, ANTAEK };
enum class CERTIFIFY_FAIL { NO_ID, ALREADY_LOGIN };
enum class HIT_OBJ { JAPAN_SOLDIER, JOSEON_SOLDIER, JAPAN_GENERAL, JOSEON_GENERAL, SHELL, ARROW, BULLET };
enum class CHANGE_VIEW_TYPE { JUST, COMBAT };

extern TEAM_COLOR client_teamcolor;

constexpr unsigned char O_PLAYER = 0;
constexpr unsigned char O_NPC = 1;

constexpr unsigned char D_UP = 0;
constexpr unsigned char D_DOWN = 1;
constexpr unsigned char D_LEFT = 2;
constexpr unsigned char D_RIGHT = 3;

constexpr unsigned char A_KNIFE = 0;
constexpr unsigned char A_BOW = 1;

constexpr unsigned char M_PREVIEW = 0;
constexpr unsigned char M_TOPVIEW = 1;
constexpr unsigned char M_BACKVIEW = 2;
constexpr unsigned char M_DEAD = 3;

//=============================sc
struct sc_packet_certifify_ok
{
    char size;
    char type;
    wchar_t   name[MAX_ID_LEN];
};

struct sc_packet_certifify_fail
{
    char size;
    char type;
    char failtype;
};

struct sc_packet_login_ok { // 6 + 12 + 4 + 4 + 2
    char size;
    char type;
    int id;
    float x, y, z;
    short hp;
    short speed;
    OBJ_TYPE objtype;
    TEAM_COLOR teamcolor;
    SHIP_TYPE shiptype;
    short general_id;
    char owner;
};

struct sc_packet_move { // 6 + 24 + 5
    char size;
    char type;
    int id;
    float x, y, z, velx, vely, velz, CamValue, RotValue, TurnValue;
    char dir;
    char status;
    OBJ_TYPE objtype;
    int      m_id;
};

struct sc_packet_ship_move
{
    char size;
    char type;
    float x, y, z, TurnValue;
    short      m_id;
    OBJ_TYPE objtype;
};

struct sc_packet_set_hp
{
    char size;
    char type;
    int damage;
    int id;
    short hp;
    OBJ_TYPE objtype;
};

struct sc_packet_change_weapon
{
    char size;
    char type;
    char weapon;
    int general_id;
};
struct sc_packet_shell_spawn // 42
{
    char size;
    char type;
    float posx, posy, posz, rotx, roty, rotz, velx, vely, velz;
    int shipid;
    int id;
};

struct sc_packet_bullet_spawn // 42
{
    char size;
    char type;
    float posx, posy, posz, rotx, roty, rotz, velx, vely, velz;
    int shipid;
    int id;
};

struct sc_packet_arrow_spawn
{
    char size;
    char type;
    float posx, posy, posz, controllerRotx, controllerRoty, controllerRotz, rotx, roty, rotz, velx, vely, velz;
    int id;
};

struct sc_packet_shell_move
{
    char size;
    char type;
    float x, y, z;
    int id;
};

struct sc_packet_bullet_move
{
    char size;
    char type;
    float x, y, z;
    int id;
};

struct sc_packet_arrow_move
{
    char size;
    char type;
    float x, y, z;
    int id;
};

struct sc_packet_enter { // 6 + 8
    char size;
    char type;
    int id;
    char o_type;
    float x, y, z;
    wchar_t   name[MAX_ID_LEN];
};

struct sc_packet_select_lobby
{
    char size;
    char type;
    short lobbynum;
    char order;
    char owner;
    int id;
    char team;
    char joseon_num;
    char japan_num;
    wchar_t   name[MAX_ID_LEN];
};

struct sc_packet_quit_lobby
{
    char size;
    char type;
    short lobbynum;
    char prevquitorder;
    char quitisowner;
    char myorder;
    char amiowner;
    char nextownerid;
    char prevteam;
    int id;
};


struct sc_packet_start_game
{
    char size;
    char type;
};

struct sc_packet_start_game_fail
{
    char size;
    char type;
};

struct sc_packet_enter_ingame
{
    char size;
    char type;
};

struct sc_packet_select_team
{
    char size;
    char type;
    int id;
    char team;
    char prevteam;
    char joseon_team_num;
    char japan_team_num;
    wchar_t   name[MAX_ID_LEN];
};

struct sc_packet_leave { // 6
    char size;
    char type;
    int id;
};

struct sc_packet_chat { // 7 // 추가
    char size;
    char type;
    int    id;
    int sender_id;
    wchar_t chat[MAX_STR_LEN];
};

struct sc_packet_attack { // 7
    char size;
    char type;
    int id;
    char attack;
    char objtype;
};

struct sc_packet_view { // 7
    char size;
    char type;
    int id;
    char viewmode;
    char teamcolor;
    CHANGE_VIEW_TYPE cause;
};

struct sc_packet_soldier_gen // 추가
{
    char size;
    char type;
    short hp;
    int soldier_id;
    float x, y, z;
    OBJ_TYPE objtype;
    TEAM_COLOR teamcolor;
};

struct sc_packet_pause
{
    char size;
    char type;
    int id;
};

struct sc_packet_resume
{
    char size;
    char type;
    int id;
};

struct sc_packet_exit
{
    char size;
    char type;
    int id;
};

struct sc_packet_finish_game {
    char size;
    char type;
    char result;
};

//=================================cs

struct cs_packet_start_game
{
    char size;
    char type;
};

struct cs_packet_select_lobby
{
    char size;
    char type;
    short lobbynum;
};

struct cs_packet_quit_lobby
{
    char size;
    char type;
    short lobbynum;
};

struct cs_packet_select_team
{
    char size;
    char type;
    char team;
};

struct cs_packet_enter_ingame
{
    char size;
    char type;
};

struct cs_packet_login { // 3
    char   size;
    char   type;
    wchar_t   name[MAX_ID_LEN];
};


struct cs_packet_move { // 11
    char   size;
    char   type;
    float   x, y, z, velx, vely, velz, CamValue, RotValue, TurnValue;
    char   dir;
    char status;
    OBJ_TYPE objtype;
    int      m_id;
};

struct cs_packet_ship_move
{
    char size;
    char type;
    float x, y, z, TurnValue;
    short      m_id;
    OBJ_TYPE objtype;
};

struct cs_packet_set_hp {
    char size;
    char type;
    int damage;
    int id;
    short hp;
    OBJ_TYPE objtype;
    HIT_OBJ damagecauser;
};

struct cs_packet_attack { // 3
    char size;
    char type;
    char attack;
    char objtype;
    int id;
};

struct cs_packet_view { // 3
    char size;
    char type;
    char viewmode;
    char teamcolor;
    int id;
};

struct cs_packet_shell_spawn // 38
{
    char size;
    char type;
    float posx, posy, posz, rotx, roty, rotz, velx, vely, velz;
    int id;
};

struct cs_packet_bullet_spawn
{
    char size;
    char type;
    float posx, posy, posz, rotx, roty, rotz, velx, vely, velz;
    int id;
};

struct cs_packet_arrow_spawn
{
    char size;
    char type;
    float posx, posy, posz, controllerRotx, controllerRoty, controllerRotz, rotx, roty, rotz, velx, vely, velz;
    int id;
};


struct cs_packet_shell_move
{
    char size;
    char type;
    float x, y, z;
    int id;
};

struct cs_packet_bullet_move
{
    char size;
    char type;
    float x, y, z;
    int id;
};

struct cs_packet_arrow_move
{
    char size;
    char type;
    float x, y, z;
    int id;
};

struct cs_packet_chat
{
    char size;
    char type;
    wchar_t message[MAX_STR_LEN];
};


struct cs_packet_change_weapon
{
    char size;
    char type;
    char weapon;
    int general_id;
};

struct cs_packet_soldier_gen // 추가
{
    char size;
    char type;
    int my_general_id;
    int other_general_id;
    int ship_id;
};

struct cs_packet_pause
{
    char size;
    char type;
};

struct cs_packet_resume
{
    char size;
    char type;
};

struct cs_quit_game
{
    char size;
    char type;
};

struct cs_packet_exit
{
    char size;
    char type;
};

struct cs_packet_logout
{
    char size;
    char type;
};

#pragma pack (pop)

class JAPANESEINVASION_API protocol
{
public:
    protocol();
    ~protocol();
};

#pragma once

extern bool level;