#pragma once
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "JIGeneralSetting.generated.h"

UCLASS(config = General)
class JAPANESEINVASIONSETTING_API UJIGeneralSetting : public UObject
{
	GENERATED_BODY()
public:
	UJIGeneralSetting();

	UPROPERTY(config)
		TArray<FSoftObjectPath> GeneralAssets;
	
};