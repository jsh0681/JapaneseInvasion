#pragma once
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "JIShipSetting.generated.h"

UCLASS(config=JapaneseInvasion)
class JAPANESEINVASIONSETTING_API UJIShipSetting : public UObject
{
	GENERATED_BODY()

public:
	UJIShipSetting();

	UPROPERTY(config)
		TArray<FSoftObjectPath> ShipAssets;
};
